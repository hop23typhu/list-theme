<?php $ya_box_layout = ya_options()->getCpanelValue('layout'); ?>
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>
<div class="body-wrapper theme-clearfix<?php echo ( $ya_box_layout == 'boxed' )? ' box-layout' : '';?> ">
<?php 
	$ya_colorset = ya_options()->getCpanelValue('scheme');
	$ya_header_style = ya_options()->getCpanelValue('header_style');
	$ya_top_shortcode 	= ya_options()->getCpanelValue('header_shortcode');
if ($ya_header_style == ''){
?>
<div class="header-style1">
	<header id="header"  class="header">
		<?php if( $ya_top_shortcode != '' ): ?>
		<div class="header-shortcode">
			<?php echo stripslashes( do_shortcode($ya_top_shortcode) ); ?>
		</div>
		<a href="javascript:void(0)" class="button-countdown"></a>
		<?php endif; ?>
		<div class="header-msg">
			<div class="container">
			<?php if (is_active_sidebar_YA('top')) {?>
				<div id="sidebar-top" class="sidebar-top">
					<?php dynamic_sidebar('top'); ?>
				</div>
			<?php }?>
			</div>
		</div>
		<div class="top">
			<div class="container">
			<div class="row">
				<div class="top-header col-lg-3 col-xs-12 pull-left">
					<div class="ya-logo">
						<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<?php if(ya_options()->getCpanelValue('sitelogo')){ ?>
									<img src="<?php echo esc_attr( ya_options()->getCpanelValue('sitelogo') ); ?>" alt="<?php bloginfo('name'); ?>"/>
								<?php }else{
									if ($ya_colorset){$logo = get_template_directory_uri().'/assets/img/logo-'.$ya_colorset.'.png';}
									else $logo = get_template_directory_uri().'/assets/img/logo-default.png';
								?>
									<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php bloginfo('name'); ?>"/>
								<?php } ?>
						</a>
					</div>
				</div>
				<div id="sidebar-top-header" class="sidebar-top-header col-lg-9 col-xs-12 pull-right">
					<div id="sidebar-top-menu" class="sidebar-top-menu">
						<?php get_template_part( 'woocommerce/minicart-ajax-style1' ); ?>
					</div>
					<a class="home-search  fa fa-search" href="javascript:void(0)" title="Search"></a>
					<div class="widget ya_top-3 ya_top non-margin pull-right">
						<div class="widget-inner">
							<?php get_template_part( 'widgets/ya_top/searchcate' ); ?>
						</div>
					</div>
					<?php if ( has_nav_menu('primary_menu') ) {?>
						<!-- Primary navbar -->
					<div id="main-menu" class="main-menu pull-right">
						<nav id="primary-menu" class="primary-menu">
							<div class="container">
								<div class="mid-header clearfix">
									<a href="javascript:void(0)" class="phone-icon-menu"></a>
									<div class="navbar-inner navbar-inverse">
											<?php
												$ya_menu_class = 'nav nav-pills';
												if ( 'mega' == ya_options()->getCpanelValue('menu_type') ){
													$ya_menu_class .= ' nav-mega';
												} else $ya_menu_class .= ' nav-css';
											?>
											<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $ya_menu_class)); ?>
									</div>
								</div>
							</div>
						</nav>
					</div>
						<!-- /Primary navbar -->
					<?php 
						} 
					?>
				</div>
			</div>
			</div>
		</div>
	</header>
</div>
	<!-- /Primary navbar -->
<?php 
} else {
    echo '<div class="header-' . $ya_header_style . '">';
    get_template_part('templates/header', $ya_header_style);
    echo '</div>';
}	
?>

<div id="main" class="main theme-clearfix" role="document">
<?php
	if (!is_front_page() ) {
		if (function_exists('ya_breadcrumb')){
			ya_breadcrumb('<div class="breadcrumbs theme-clearfix"><div class="container">', '</div></div>');
		} 
	} 

?>