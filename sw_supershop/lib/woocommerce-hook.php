<?php
add_theme_support( 'woocommerce' );

/*minicart via Ajax*/
$ya_header  = ya_options()->getCpanelValue( 'header_style' );
if( $ya_header == 'style2' ){
	add_filter('add_to_cart_fragments', 'ya_add_to_cart_fragment', 100);
	function ya_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
		<?php get_template_part( 'woocommerce/minicart-ajax' ); ?>
		<?php
		$fragments['.ya-minicart'] = ob_get_clean();
		return $fragments;
		
	}
}else{
	add_filter('add_to_cart_fragments', 'ya_add_to_cart_fragment_style1', 101);
	function ya_add_to_cart_fragment_style1( $fragments ) {
		ob_start();
		?>
		<?php get_template_part( 'woocommerce/minicart-ajax-style1' ); ?>
		<?php
		$fragments['.ya-minicart-style1'] = ob_get_clean();
		return $fragments;
		
	}
}
/* change position */
remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
add_action('woocommerce_single_product_summary','woocommerce_template_single_price',20);
add_action('woocommerce_single_product_summary','woocommerce_template_single_excerpt',10);
/*remove woo breadcrumb*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

/*Product share*/
add_action( 'woocommerce_single_product_summary', 'ya_product_share', 60);

function ya_product_share(){
	$html  ='';
	$html .= get_social();
	echo $html;
}
/* Change onsale */
//remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
/* Change Image if have gallery */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'ya_woocommerce_template_loop_product_thumbnail', 10 );
function ya_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
	global $post, $product;
	$html = '';
	/* add to cart */
	$bt_cart = apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( $product->id ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
			esc_attr( $product->product_type ),
			esc_html( $product->add_to_cart_text() )
		),
	$product );
	/* quickview */
	$nonce = wp_create_nonce("ya_quickviewproduct_nonce");
	$link = admin_url('admin-ajax.php?ajax=true&amp;action=ya_quickviewproduct&amp;post_id='.$post->ID.'&amp;nonce='.$nonce);
	$linkcontent ='<a href="'. $link .'" data-fancybox-type="ajax" class="group fancybox fancybox.ajax">'.apply_filters( 'out_of_stock_add_to_cart_text', __( 'Quick View ', 'sw_supershop' ) ).'</a>';			
	$id = get_the_ID();
	$gallery = get_post_meta($id, '_product_image_gallery', true);
	$attachment_image = '';
	if(!empty($gallery)) {
		$gallery = explode(',', $gallery);
		$first_image_id = $gallery[0];
	}
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), '' );
	if ( has_post_thumbnail() ){
		if( $attachment_image ){
			$html .= '<div class="product-thumb-hover">';
			$html .= get_the_post_thumbnail( $post->ID, $size );
			$html .= $attachment_image;
			$html .= '</div>';
		}else{
			$html .= get_the_post_thumbnail( $post->ID, $size );
		}
		$html .= '<div class="product-thumb-button">';
		$html .= $bt_cart;
		$html .= do_shortcode( "[yith_compare_button]" );
		$html .= do_shortcode( "[yith_wcwl_add_to_wishlist]" );
		$html .= $linkcontent;
		$html .= '</div>';
		return $html;
	}elseif( wc_placeholder_img_src() ){
		$html .= wc_placeholder_img( $size );
		return $html;
	}
}
function ya_woocommerce_template_loop_product_thumbnail(){
	echo ya_product_thumbnail();
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
/*filter order*/
function ya_addURLParameter($url, $paramName, $paramValue) {
     $url_data = parse_url($url);
     if(!isset($url_data["query"]))
         $url_data["query"]="";

     $params = array();
     parse_str($url_data['query'], $params);
     $params[$paramName] = $paramValue;
     $url_data['query'] = http_build_query($params);
     return ya_build_url($url_data);
}


function ya_build_url($url_data) {
 $url="";
 if(isset($url_data['host']))
 {
	 $url .= $url_data['scheme'] . '://';
	 if (isset($url_data['user'])) {
		 $url .= $url_data['user'];
			 if (isset($url_data['pass'])) {
				 $url .= ':' . $url_data['pass'];
			 }
		 $url .= '@';
	 }
	 $url .= $url_data['host'];
	 if (isset($url_data['port'])) {
		 $url .= ':' . $url_data['port'];
	 }
 }
 if (isset($url_data['path'])) {
	$url .= $url_data['path'];
 }
 if (isset($url_data['query'])) {
	 $url .= '?' . $url_data['query'];
 }
 if (isset($url_data['fragment'])) {
	 $url .= '#' . $url_data['fragment'];
 }
 return $url;
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action('woocommerce_before_shop_loop', 'ya_woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop', 'ya_woocommerce_pagination', 35);
add_action('woocommerce_after_shop_loop', 'ya_woocommerce_catalog_ordering', 5);
add_action('woocommerce_before_shop_loop','ya_woommerce_view_mode_wrap',15);
remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
add_action('woocommerce_message','wc_print_notices', 10);

function ya_woommerce_view_mode_wrap () {
	$html='<div class="view-mode-wrap">
				<p class="view-mode">
						<a href="javascript:void(0)" class="grid-view active" title="'. __('Grid view', 'sw_supershop').'"><span>'. __('Grid view', 'sw_supershop').'</span></a>
						<a href="javascript:void(0)" class="list-view" title="'. __('List view', 'sw_supershop') .'"><span>'.__('List view', 'sw_supershop').'</span></a>
				</p>	
					</div>';
	echo $html;
}

function ya_woocommerce_pagination() {
	global $wp_query;
	if ( is_shop() && get_option( 'woocommerce_shop_page_display' ) == 'subcategories' ) {
		$wp_query->post_count    = 0;
		$wp_query->max_num_pages = 0;
	}
	if ( get_option( 'woocommerce_category_archive_display' ) == 'subcategories' ) {
		$wp_query->post_count    = 0;
		$wp_query->max_num_pages = 0;
	}
	wc_get_template( 'loop/pagination.php' );
}

function ya_woocommerce_catalog_ordering() {
	global $data;
	parse_str($_SERVER['QUERY_STRING'], $params);

	$query_string = '?'.$_SERVER['QUERY_STRING'];

	// replace it with theme option
	if($data['woo_items']) {
		$per_page = $data['woo_items'];
	} else {
		$per_page = 8;
	}

	$pob = !empty($params['product_orderby']) ? $params['product_orderby'] : 'default';
	$po = !empty($params['product_order'])  ? $params['product_order'] : 'asc';
	$pc = !empty($params['product_count']) ? $params['product_count'] : $per_page;

	$html = '';
	$html .= '<div class="catalog-ordering clearfix">';

	$html .= '<div class="orderby-order-container">';
	$html .= '<span class="sort">Sort by</span>';
	$html .= '<ul class="orderby order-dropdown">';
	$html .= '<li>';
	$html .= '<span class="current-li"><span class="current-li-content"><a>'.__('Sort by', 'sw_supershop').'</a></span></span>';
	$html .= '<ul>';
	$html .= '<li class="'.(($pob == 'default') ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_orderby', 'default').'">'.__('', 'sw_supershop').__('Default', 'sw_supershop').'</a></li>';
	$html .= '<li class="'.(($pob == 'name') ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_orderby', 'name').'">'.__('', 'sw_supershop').__('Name', 'sw_supershop').'</a></li>';
	$html .= '<li class="'.(($pob == 'price') ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_orderby', 'price').'">'.__('', 'sw_supershop').__('Price', 'sw_supershop').'</a></li>';
	$html .= '<li class="'.(($pob == 'date') ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_orderby', 'date').'">'.__('', 'sw_supershop').__('Date', 'sw_supershop').'</a></li>';
	$html .= '<li class="'.(($pob == 'rating') ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_orderby', 'rating').'">'.__('', 'sw_supershop').__('Rating', 'sw_supershop').'</a></li>';
	$html .= '</ul>';
	$html .= '</li>';
	$html .= '</ul>';
	$html .= '<ul class="order">';
	if($po == 'desc'):
	$html .= '<li class="desc"><a href="'.ya_addURLParameter($query_string, 'product_order', 'asc').'"><i class="icon-arrow-up"></i></a></li>';
	endif;
	if($po == 'asc'):
	$html .= '<li class="asc"><a href="'.ya_addURLParameter($query_string, 'product_order', 'desc').'"><i class="icon-arrow-down"></i></a></li>';
	endif;
	$html .= '</ul>';
	$html .= '</div>';
	$html .= '</div>';
	
	echo $html;
}

add_action('woocommerce_before_shop_loop', 'ya_woocommerce_count_page', 36);
add_action( 'woocommerce_after_shop_loop', 'ya_woocommerce_count_page', 36);
function ya_woocommerce_count_page(){
	global $data;
	parse_str($_SERVER['QUERY_STRING'], $params);

	$query_string = '?'.$_SERVER['QUERY_STRING'];
	
	// replace it with theme option
	if($data['woo_items']) {
		$per_page = $data['woo_items'];
	} else {
		$per_page = 12;
	}
	$pc = !empty($params['product_count']) ? $params['product_count'] : $per_page;
	
	$html = '';
	$html .='<div class="count-per-page">';
	$html .='</span><span>Show </span>';
	$html .= '<ul class="sort-count order-dropdown">';
	$html .= '<li>';
	$html .= '<span class="current-li"><a>'.__('12', 'sw_supershop').'</a></span>';
	$html .= '<ul>';
	$html .= '<li class="'.(($pc == $per_page) ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_count', $per_page).'">'.$per_page.'</a></li>';
	$html .= '<li class="'.(($pc == $per_page*2) ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_count', $per_page*2).'">'.($per_page*2).'</a></li>';
	$html .= '<li class="'.(($pc == $per_page*3) ? 'current': '').'"><a href="'.ya_addURLParameter($query_string, 'product_count', $per_page*3).'">'.($per_page*3).'</a></li>';
	$html .= '</ul>';
	$html .= '</li>';
	$html .= '</ul>';
	$html .='<span class="per-page">per page</span>';
	$html .='</div>';
	echo $html;
}

add_action('woocommerce_get_catalog_ordering_args', 'ya_woocommerce_get_catalog_ordering_args', 20);
function ya_woocommerce_get_catalog_ordering_args($args)
{
	global $woocommerce;

	parse_str($_SERVER['QUERY_STRING'], $params);

	$pob = !empty($params['product_orderby']) ? $params['product_orderby'] : 'default';
	$po = !empty($params['product_order'])  ? $params['product_order'] : 'asc';

	switch($pob) {
		case 'date':
			$orderby = 'date';
			$order = 'desc';
			$meta_key = '';
		break;
		case 'price':
			$orderby = 'meta_value_num';
			$order = 'asc';
			$meta_key = '_price';
		break;
		case 'popularity':
			$orderby = 'meta_value_num';
			$order = 'desc';
			$meta_key = 'total_sales';
		break;
		case 'title':
			$orderby = 'title';
			$order = 'asc';
			$meta_key = '';
		break;
		case 'default':
		default:
			$orderby = 'menu_order title';
			$order = 'asc';
			$meta_key = '';
		break;
	}

	switch($po) {
		case 'desc':
			$order = 'desc';
		break;
		case 'asc':
			$order = 'asc';
		break;
		default:
			$order = 'asc';
		break;
	}

	$args['orderby'] = $orderby;
	$args['order'] = $order;
	$args['meta_key'] = $meta_key;

	if( $pob == 'rating' ) {
		$args['orderby']  = 'menu_order title';
		$args['order']    = $po == 'desc' ? 'desc' : 'asc';
		$args['order']	  = strtoupper( $args['order'] );
		$args['meta_key'] = '';

		add_filter( 'posts_clauses', 'ya_order_by_rating_post_clauses' );
	}

	return $args;
}
function ya_order_by_rating_post_clauses( $args ) {
	global $wpdb;

	$args['where'] .= " AND $wpdb->commentmeta.meta_key = 'rating' ";

	$args['join'] .= "
		LEFT JOIN $wpdb->comments ON($wpdb->posts.ID = $wpdb->comments.comment_post_ID)
		LEFT JOIN $wpdb->commentmeta ON($wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id)
	";

	$args['orderby'] = "$wpdb->commentmeta.meta_value DESC";

	$args['groupby'] = "$wpdb->posts.ID";

	return $args;
}
add_filter('loop_shop_per_page', 'ya_loop_shop_per_page');
function ya_loop_shop_per_page()
{
	global $data;

	parse_str($_SERVER['QUERY_STRING'], $params);

	if($data['woo_items']) {
		$per_page = $data['woo_items'];
	} else {
		$per_page = 12;
	}

	$pc = !empty($params['product_count']) ? $params['product_count'] : $per_page;

	return $pc;
}

/* Custom Hook Other */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'ya_woocommerce_template_loop_price', 4 );
function ya_woocommerce_template_loop_price(){
	wc_get_template( 'loop/price.php' );
}
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'ya_woocommerce_template_single_rating', 3 );
function ya_woocommerce_template_single_rating(){
	wc_get_template( 'loop/rating.php' );
}
add_action( 'woocommerce_after_shop_loop_item_title', 'ya_woocommerce_template_loop_description', 5 );
function ya_woocommerce_template_loop_description(){
	$description = '<div class="item-desc">'.get_the_excerpt().'</div>';
	echo $description;
}
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

/*********QUICK VIEW PRODUCT**********/

add_action("wp_ajax_ya_quickviewproduct", "ya_quickviewproduct");
add_action("wp_ajax_nopriv_ya_quickviewproduct", "ya_quickviewproduct");
function ya_quickviewproduct(){
	
	$productid = (isset($_REQUEST["post_id"]) && $_REQUEST["post_id"]>0) ? $_REQUEST["post_id"] : 0;
	
	$query_args = array(
		'post_type'	=> 'product',
		'p'			=> $productid
	);
	$outputraw = $output = '';
	$r = new WP_Query($query_args);
	if($r->have_posts()){ 

		while ($r->have_posts()){ $r->the_post(); setup_postdata($r->post);
			global $product;
			ob_start();
			woocommerce_get_template_part( 'content', 'quickview-product' );
			$outputraw = ob_get_contents();
			ob_end_clean();
		}
	}
	$output = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $outputraw);
	echo $output;exit();
}

?>