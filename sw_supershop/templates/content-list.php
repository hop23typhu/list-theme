<?php if (!have_posts()) : ?>
<?php get_template_part('templates/no-results'); ?>
<?php endif; ?>
<div class="blog-content-list">
<?php 
	while (have_posts()) : the_post(); 
	$format = get_post_format();
	global $instance;
?>
	<div id="post-<?php the_ID();?>" <?php post_class( 'theme-clearfix' ); ?>>
		<div class="entry clearfix">
			<?php if( $format == '' ){?>
			<?php if (get_the_post_thumbnail()){?>
				<div class="entry-thumb">
					<a class="entry-hover" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">			
						<?php the_post_thumbnail('ya-detail_thumb')?>
					</a>
				</div>
			<?php }?>
				<div class="entry-content <?php echo ( get_the_title() ) ? ' ' : 'no-title' ; ?>">
					<div class="entry-top">
						<div class="entry-meta">
							<span class="entry-date">
								<?php echo ( get_the_title() ) ? '<span class="day">'.date( 'd',strtotime($post->post_date)).'</span><span class="month">'.date( 'M',strtotime($post->post_date)).'</span>' : '<a href="'.get_the_permalink().'"><span class="day">'.date( 'd',strtotime($post->post_date)).'</span><span class="month">'.date( 'M',strtotime($post->post_date)).'</span></a>'; ?>
							</span>
						</div>
						<div class="entry-right">
							<div class="title-blog">
								<h3>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
								</h3>
							</div>
							<div class="meta-list">
								<span class="category-blog"><i class="fa fa-folder-open"></i><?php esc_html_e( '', 'sw_supershop' ); ?> <?php the_category(', '); ?></span>
								<span class="author"><i class="fa fa-user"></i><?php the_author_posts_link(); ?></span>	
							</div>
						</div>
					</div>
					<div class="entry-description">
						<?php 
													
							if ( preg_match('/<!--more(.*?)?-->/', $post->post_content, $matches) ) {
								$content = explode($matches[0], $post->post_content, 2);
								$content = $content[0];
								$content = wp_trim_words($post->post_content, 30, '...');
								echo $content;	
							} else {
								the_content('...');
							}		
						?>
						<div class="bl_read_more"><a href="<?php the_permalink(); ?>"><?php esc_html_e('Read more','sw_supershop')?></a></div>
					</div>
					 <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sw_supershop' ).'</span>', 'after' => '</div>' , 'link_before' => '<span>', 'link_after'  => '</span>' ) ); ?>
				</div>
			<?php } elseif( !$format == ''){?>
			<div class="entry-thumb">	
						<?php if( $format == 'video' || $format == 'audio' ){ ?>	
							<?php echo ( $format == 'video' ) ? '<div class="video-wrapper">'. get_entry_content_asset($post->ID) . '</div>' : get_entry_content_asset($post->ID); ?>										
						<?php } ?>
						<?php if( $format == 'image' ){?>
							<div class="entry-thumb-content">
								<a class="entry-hover" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail('ya-detail_thumb');?>				
								</a>	
							</div>
						<?php } ?>
						<?php if( $format == 'gallery' ) { 
							if(preg_match_all('/\[gallery(.*?)?\]/', get_post($instance['post_id'])->post_content, $matches)){
								$attrs = array();
								if (count($matches[1])>0){
									foreach ($matches[1] as $m){
										$attrs[] = shortcode_parse_atts($m);
									}
								}
								if (count($attrs)> 0){
									foreach ($attrs as $attr){
										if (is_array($attr) && array_key_exists('ids', $attr)){
											$ids = $attr['ids'];
											break;
										}
									}
								}
							?>
								<div id="gallery_slider_<?php echo $post->ID; ?>" class="carousel slide gallery-slider" data-interval="0">	
									<div class="carousel-inner">
										<?php
											$ids = explode(',', $ids);						
											foreach ( $ids as $i => $id ){ ?>
												<div class="item<?php echo ( $i== 0 ) ? ' active' : '';  ?>">			
														<?php echo wp_get_attachment_image($id, 'full'); ?>
												</div>
											<?php }	?>
									</div>
									<a href="#gallery_slider_<?php echo $post->ID; ?>" class="left carousel-control" data-slide="prev"><?php esc_html_e( 'Prev', 'sw_supershop' ) ?></a>
									<a href="#gallery_slider_<?php echo $post->ID; ?>" class="right carousel-control" data-slide="next"><?php esc_html_e( 'Next', 'sw_supershop' ) ?></a>
								</div>
							<?php }	?>							
						<?php } ?>

						<?php if( $format == 'quote' ) { ?>
							<div class="entry-thumb" style="display: none;">
							</div>
						<?php } ?>
				</div>
				<div class="entry-content">
					<div class="entry-top">
						<div class="entry-meta">
							<span class="entry-date">
								<?php echo ( get_the_title() ) ? '<span class="day">'.date( 'd',strtotime($post->post_date)).'</span><span class="month">'.date( 'M',strtotime($post->post_date)).'</span>' : '<a href="'.get_the_permalink().'">'.date( 'F j, Y',strtotime($post->post_date)).'</a>'; ?>
							</span>
						</div>
						<div class="entry-right">
							<div class="title-blog">
								<h3>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?> </a>
								</h3>
							</div>
							<div class="meta-list">
								<span class="category-blog"><i class="fa fa-folder-open"></i><?php esc_html_e( '', 'sw_supershop' ); ?> <?php the_category(', '); ?></span>
								<span class="author"><i class="fa fa-user"></i><?php the_author_posts_link(); ?></span>	
							</div>
						</div>
					</div>
					<div class="entry-description">
					  <?php the_content( '...' ); ?>
					</div>
				  </div>
			<?php } ?>	
		</div>
	</div>
<?php endwhile; ?>
</div>
<div class="clearfix"></div>