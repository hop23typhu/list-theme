<?php 
	$ya_footer_style = ya_options() -> getCpanelValue( 'footer_style' );
?>
<?php if (is_active_sidebar_YA('footer-block')){ ?>
<div class="sidebar-above-footer theme-clearfix">
       <div class="above-footer-top theme-clearfix"></div>
		<div class="footer-block theme-clearfix">
			<div class="container">
				<?php if (is_active_sidebar_YA('footer-block')){ ?>
									
						<?php dynamic_sidebar('footer-block'); ?>
					
				<?php } ?>
			</div>
		</div>
</div>
<?php } ?>
<footer class="footer theme-clearfix footer-<?php echo esc_attr( $ya_footer_style ); ?>" >
	<div class="container theme-clearfix">
		<div class="footer-top">
			<?php if (is_active_sidebar_YA('footer1')){ ?>
								
					<?php dynamic_sidebar('footer1'); ?>
				
			<?php } ?>
		</div>
		<div class="footer-menu theme-clearfix">
			<div class="row">
				<div class="footer-menu-top">
					<?php if (is_active_sidebar_YA('footer-menu1')){ ?>
										
							<?php dynamic_sidebar('footer-menu1'); ?>
						
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright theme-clearfix">
		<div class="container clearfix">
			<div class="row">
				<?php if (is_active_sidebar_YA('footer-copyright1')){ ?>
					<div class="sidebar-copyright">
							<div class="col-md-12 theme-clearfix">
								<?php dynamic_sidebar('footer-copyright1'); ?>
							</div>
					</div>
				<?php } ?>
				<div class="copyright-text">
					<p>&copy;<?php echo date('Y'); ?> <a href="http://www.flytheme.net/wordpress/theme-showcase"><?php esc_html_e('Wordpress Theme ', 'sw_supershop'); ?></a><?php esc_html_e('SW Supershop Store. All Rights Reserved. Designed by ','sw_supershop'); ?><a class="mysite" href="http://www.flytheme.net/"><?php esc_html_e('Flytheme.net','sw_supershop');?></a>.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
