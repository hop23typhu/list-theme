<?php 
	$ya_colorset 		= ya_options()->getCpanelValue('scheme');
?>
<header id="header"  class="header">
    <div class="header-top">
        <div class="container">
        <?php if (is_active_sidebar_YA('top-language')) {?>
            <div class="sidebar-language">
                <?php dynamic_sidebar('top-language'); ?>
            </div>
        <?php }?>
        </div>
    </div>
	<div class="top">
		<div class="container">
			<div class="row">
				<div class="top-header col-lg-3 col-md-2 col-xs-12 pull-left">
					<div class="ya-logo">
						<a  href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<?php if(ya_options()->getCpanelValue('sitelogo')){ ?>
									<img src="<?php echo esc_attr( ya_options()->getCpanelValue('sitelogo') ); ?>" alt="<?php bloginfo('name'); ?>"/>
								<?php }else{
									if ($ya_colorset){$logo = get_template_directory_uri().'/assets/img/logo-header3-'.$ya_colorset.'.png';}
									else $logo = get_template_directory_uri().'/assets/img/logo-default.png';
								?>
									<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php bloginfo('name'); ?>"/>
								<?php } ?>
						</a>
					</div>
				</div>
				<div class="header-right col-md-3 col-xs-12 pull-right">
					<div class="content-wrap">
						<?php if (is_active_sidebar_YA('top-home3')) {?>
							<div id="sidebar-top" class="sidebar-top">
								<?php dynamic_sidebar('top-home3'); ?>
							</div>
						<?php }?>
					</div>
				</div>
				<div id="sidebar-top-header" class="sidebar-top-header col-lg-6 col-md-7 col-xs-12 pull-right">
					<?php if ( has_nav_menu('primary_menu') ) {?>
						<!-- Primary navbar -->
					<div id="main-menu" class="main-menu pull-left">
						<nav id="primary-menu" class="primary-menu">
							<div class="container">
								<div class="mid-header clearfix">
									<a href="javascript:void(0)" class="phone-icon-menu"></a>
									<div class="navbar-inner navbar-inverse">
											<?php
												$ya_menu_class = 'nav nav-pills';
												if ( 'mega' == ya_options()->getCpanelValue('menu_type') ){
													$ya_menu_class .= ' nav-mega';
												} else $ya_menu_class .= ' nav-css';
											?>
											<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $ya_menu_class)); ?>
									</div>
								</div>
							</div>
						</nav>
					</div>
						<!-- /Primary navbar -->
					<?php 
						} 
					?>
				</div>
			</div>
		</div>
	</div>	
	<div class="header-bottom">
		<div class="container">
				<div class="col-lg-3 col-md-3 col-sm-6 vertical_megamenu-4 vertical_megamenu-header verticle-menu-index3">
					<div class="mega-left-title"><strong><?php esc_html_e('All categories','sw_supershop')?></strong></div>
					<?php echo do_shortcode('[ya_mega_menu menu_locate="menu-vertical-category2"]') ?>
				</div>
				<div id="sidebar-top-menu" class="sidebar-top-menu pull-right">
					<?php get_template_part( 'woocommerce/minicart-ajax' ); ?>
				</div>
				<div class="widget ya_top-3 ya_top non-margin pull-left">
					<div class="widget-inner">
						<?php get_template_part( 'widgets/ya_top/searchcate' ); ?>
					</div>
				</div>
		</div>
	</div>
</header>

