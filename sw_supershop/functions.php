<?php
if ( !defined('__YATHEME__') ){
	// Define helper constants
	define( '__YATHEME__', 'sw_supershop' );
}
/**
 * Variables
 */
require_once (get_template_directory().'/lib/defines.php');
/**
 * Roots includes
 */
require_once (get_template_directory().'/lib/classes.php');		// Utility functions
require_once (get_template_directory().'/lib/utils.php');			// Utility functions
require_once (get_template_directory().'/lib/init.php');			// Initial theme setup and constants
require_once (get_template_directory().'/lib/config.php');		// Configuration
require_once (get_template_directory().'/lib/cleanup.php');		// Cleanup
require_once (get_template_directory().'/lib/nav.php');			// Custom nav modifications
require_once (get_template_directory().'/lib/rewrites.php');		// URL rewriting for assets
require_once (get_template_directory().'/lib/htaccess.php');		// HTML5 Boilerplate .htaccess
require_once (get_template_directory().'/lib/widgets.php');		// Sidebars and widgets
require_once (get_template_directory().'/lib/scripts.php');		// Scripts and stylesheets
require_once (get_template_directory().'/lib/customizer.php');	// Custom functions
require_once (get_template_directory().'/lib/plugin-requirement.php');			// Custom functions
if( class_exists( 'WooCommerce' ) ){
	require_once (get_template_directory().'/lib/plugins/currency-converter/currency-converter.php'); // currency converter
	require_once (get_template_directory().'/lib/woocommerce-hook.php');	// Utility functions
}

// add image thumbnail lastest blog
add_image_size( 'ya-lastest-blog', 230, 140, true);
// add image thumbnail lastest blog2
add_image_size( 'ya-lastest-blog2', 465, 300, true);
// add image thumbnail grid blog
add_image_size( 'ya-grid_blog', 420,280, true);
// add image thumbnail related post
add_image_size( 'ya-related_post', 270,175, true);
// add image thumbnail r
add_image_size( 'ya-the_blog', 230,160, true);
// add image thumbnail r
add_image_size( 'ya-product_thumb', 80,60, true);
// add image blog detail
add_image_size( 'ya-detail_thumb', 870,370, true);
// add image blog detail
add_image_size( 'ya_thumbnail_cart', 80, 80, true );

function Ya_SearchFilter( $query ) {
	if ( $query->is_search ) {
		$query->set( 'post_type', array( 'post', 'product' ) );
	}
	return $query;
}
add_filter('pre_get_posts','Ya_SearchFilter');
