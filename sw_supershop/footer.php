<?php 
	$ya_footer_style = ya_options() -> getCpanelValue( 'footer_style' );
?>

<?php 
	if($ya_footer_style == 'default' || $ya_footer_style == '') {
?>
<footer class="footer theme-clearfix footer-<?php echo esc_attr($ya_footer_style); ?>" >
	<div class="container theme-clearfix">
		<div class="footer-top">
			<div class="row">
				<?php if (is_active_sidebar_YA('footer')){ ?>
									
						<?php dynamic_sidebar('footer'); ?>
					
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="footer-menu theme-clearfix">
		<div class="container footer-menu-top">
			<?php if (is_active_sidebar_YA('footer-menu')){ ?>
								
					<?php dynamic_sidebar('footer-menu'); ?>
				
			<?php } ?>
		</div>
	</div>
	<div class="copyright theme-clearfix">
			<div class="container clearfix">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-6 clearfix pull-left">
						<div class="copyright-text pull-left">
							<p>&copy;<?php echo date('Y'); ?> <a href="http://www.flytheme.net/wordpress/theme-showcase"><?php esc_html_e('Wordpress Theme ', 'sw_supershop'); ?></a><?php esc_html_e('SW Supershop Store. All Rights Reserved. Designed by ','sw_supershop'); ?><a class="mysite" href="http://www.flytheme.net/"><?php esc_html_e('Flytheme.net','sw_supershop');?></a>.</p>
						</div>
					</div>
					<?php if (is_active_sidebar_YA('footer-copyright')){ ?>
						<div class="sidebar-copyright pull-right">
								<div class="col-md-12 theme-clearfix">
									<?php dynamic_sidebar('footer-copyright'); ?>
								</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
</footer>
<?php } else {
		 get_template_part('templates/footer', $ya_footer_style);
	 }
?>
<?php if(ya_options()->getCpanelValue('back_active') == '1') { ?>
<a id="ya-totop" href="#" ></a>
<?php }?>
</div>
</div>
<script type="text/javascript">
/*** remove <p> *****/
jQuery('.panel-group .panel-default br').remove(); 
</script>
<?php wp_footer(); ?>
