<?php
/**
 * Widget Style: Col-4-footer
 *
 */
$ws['col-4-footer'] = array(
		'before_title' => '<h3><span>',
		'after_title' => '</span></h3>',
		'before_widget' => '<div class="col-lg-4 col-md-4 col-sm-12 widget %1$s %2$s "><div class="widget-inner">',
		'after_widget' => '</div></div>',
	);