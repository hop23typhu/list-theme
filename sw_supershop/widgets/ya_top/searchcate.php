<div class="top-form top-search pull-left">
	<div class="topsearch-entry">
		<form method="get" id="searchform_special" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
			<div>
				<?php
				$ya_taxonomy = class_exists( 'sw_supershop' ) ? 'product_cat' : 'category';
				$args = array(
				'type' => 'post',
				'parent' => 0,
				'orderby' => 'id',
				'order' => 'ASC',
				'hide_empty' => false,
				'hierarchical' => 1,
				'exclude' => '',
				'include' => '',
				'number' => '',
				'taxonomy' => $ya_taxonomy,
				'pad_counts' => false,

				);
				$product_categories = get_categories($args);
				if( count( $product_categories ) > 0 ){
				?>
				<div class="cat-wrapper">
					<label class="label-search">
						<select name="search_category" class="s1_option">
							<option value=""><?php esc_html_e( 'All Categories', 'sw_supershop' ) ?></option>
							<?php foreach( $product_categories as $cat ) {
								$selected = ( isset($_GET['search_category'] ) && ($_GET['search_category'] == $cat->term_id )) ? 'selected=selected' : '';
							echo '<option value="'. esc_attr( $cat-> term_id ) .'" '.$selected.'>' . esc_html( $cat->name ). '</option>';
							}
							?>
						</select>
					</label>
				</div>
				<?php } ?>
				<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php esc_html_e( 'Search...', 'sw_supershop' ); ?>" />
				<button type="submit" title="Search" class="icon-search button-search-pro form-button"></button>
				<input type="hidden" name="search_posttype" value="product" />
			</div>
		</form>
	</div>
</div>
