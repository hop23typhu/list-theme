
<?php do_action( 'before' ); ?>
<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
<?php global $woocommerce; ?>
<div class="top-login pull-right">
	<?php if ( ! is_user_logged_in() ) {  ?>
	<div class="div-login">
		<?php echo esc_html__("Welcome customer, you can", "sw_supershop").' <a href="javascript:void(0);" data-toggle="modal" data-target="#login_form"><span>'.esc_html__('Login', 'sw_supershop').'</span></a> ' .esc_html__("or", "sw_supershop"). ' <a href="javascript:void(0);" data-toggle="modal" data-target="#register_form"><span>'.esc_html__("Create an account", "sw_supershop").'</span></a>
					'; ?>
		<div class="modal fade" id="register_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabelReg" aria-hidden="true">
			<div class="modal-dialog register-form-width">
				<div class="modal-content">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabelReg"><?php esc_html_e('Register Account', 'sw_supershop'); ?></h4>
					</div>
					<div class="modal-body">
						<?php get_template_part('woocommerce/myaccount/register-form'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="login_form" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabelLog"><?php esc_html_e('Login or Register', 'sw_supershop'); ?></h4>
					</div>
					<div class="modal-body">
						<?php get_template_part('woocommerce/myaccount/login-form'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php } else{?>
		<div class="div-logined">
			<?php 
				$user_id = get_current_user_id();
				$user_info = get_userdata( $user_id );	
			?>
			<?php esc_html_e('Welcome ', 'sw_supershop'); ?> <?php echo $user_info-> user_nicename;  ?> - <a href="<?php echo wp_logout_url( home_url('/') ); ?>" title="Logout"><?php esc_html_e('Logout', 'sw_supershop'); ?></a>
		</div>
	<?php } ?>
	
</div>
<?php } ?>
