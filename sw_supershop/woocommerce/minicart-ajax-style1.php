<?php 
if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { 
	return false;
}
global $woocommerce; ?>
<div class="top-form top-form-minicart ya-minicart-style1 pull-right">
	<div class="top-minicart-icon pull-right">
		<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php esc_html_e('View your shopping cart', 'sw_supershop'); ?>"><?php echo '<span class="minicart-number">'.$woocommerce->cart->cart_contents_count.'</span>'; ?></a>
	</div>
	<div class="wrapp-minicart">
		<div class="minicart-padding">
			<h2><?php echo '<span class="minicart-number">'.$woocommerce->cart->cart_contents_count; esc_html_e(' Items ', 'sw_supershop'); echo '</span>'; esc_html_e('add your cart', 'sw_supershop')?> </h2>
			<ul class="minicart-content">
			<?php foreach($woocommerce->cart->cart_contents as $cart_item_key => $cart_item): ?>
				<li>
					<a href="<?php echo get_permalink($cart_item['product_id']); ?>" class="product-image">
						<?php $thumbnail_id = ($cart_item['variation_id']) ? $cart_item['variation_id'] : $cart_item['product_id']; ?>
						<?php echo get_the_post_thumbnail($thumbnail_id, 'ya-thumbnail_cart'); ?>
					</a>
					<?php 	global $product, $post, $wpdb, $average;
			$count = $wpdb->get_var($wpdb->prepare("
				SELECT COUNT(meta_value) FROM $wpdb->commentmeta
				LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
				WHERE meta_key = 'rating'
				AND comment_post_ID = %d
				AND comment_approved = '1'
				AND meta_value > 0
			",$cart_item['product_id']));

			$rating = $wpdb->get_var($wpdb->prepare("
				SELECT SUM(meta_value) FROM $wpdb->commentmeta
				LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
				WHERE meta_key = 'rating'
				AND comment_post_ID = %d
				AND comment_approved = '1'
			",$cart_item['product_id']));?>		
						 
	<div class="detail-item">
		<div class="product-details"> 
				<a class="product-title" href="<?php echo get_permalink($cart_item['product_id']); ?>"><?php echo esc_html( $cart_item['data']->post->post_title ); ?></a>
			<div class="rating-container">
					<div class="ratings">
						 <?php
							if( $count > 0 ){
								$average = number_format($rating / $count, 1);
						?>
							<div class="star"><span style="width: <?php echo ($average*14).'px'; ?>"></span></div>
							
						<?php } else { ?>
						
							<div class="star"></div>
							
						<?php } ?>			      
					
					</div>
					<div class="item-number-rating">
						<?php echo '(' . $count . ')'; esc_html_e(' Reviews', 'sw_supershop');?>
					</div>
			</div>	  		
			<div class="product-price">
				 <span class="price"><?php echo $woocommerce->cart->get_product_subtotal($cart_item['data'], 1); ?></span>		        		        		    		
			</div>
			<div class="qty">
				<span class="qty-label"><?php echo esc_html_e('Qty:', 'sw_supershop');?></span>
				<?php echo '<span class="qty-number">'.esc_html( $cart_item['quantity'] ).'</span>'; ?>
			</div>
			<div class="product-action">
				<a class="btn-edit" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php esc_html_e('View your shopping cart', 'sw_supershop'); ?>"><span></span></a>
				<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="btn-remove" title="%s"><span></span></a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'sw_supershop' ) ), $cart_item_key ); ?>               
			</div>
		</div>	
	</div>
							
	</li>
<?php
endforeach;
?>
</ul>
			<div class="cart-checkout">
				<div class="cart-links">
					<div class="cart-link"><a href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>" title="Cart"><?php esc_html_e('View Cart', 'sw_supershop'); ?></a></div>
					<div class="checkout-link"><a href="<?php echo get_permalink(get_option('woocommerce_checkout_page_id')); ?>" title="Check Out"><?php esc_html_e('Check Out', 'sw_supershop'); ?></a></div>
				</div>
			</div>
		</div>
	</div>
</div>