# Copyright 2009-2015 Rocketgenius, Inc.
msgid ""
msgstr ""
"Project-Id-Version: Gravity Forms Batchbook Add-On 1.0.1\n"
"Report-Msgid-Bugs-To: http://www.gravtiyhelp.com\n"
"POT-Creation-Date: 2015-07-31 07:42:55+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2015-MO-DA HO:MI+ZONE\n"
"Last-Translator: Rocketgenius <customerservice@rocketgenius.com>\n"
"Language-Team: Rocketgenius <customerservice@rocketgenius.com>\n"
"X-Generator: Gravity Forms Build Server\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Project-Id-Version: gravityformsbatchbook\n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-Bookmarks: \n"
"X-Poedit-Country: United States\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SourceCharset: utf-8\n"
"X-Textdomain-Support: yes\n"

#: class-gf-batchbook.php:81
msgid "Account URL"
msgstr ""

#: class-gf-batchbook.php:89
msgid "API Token"
msgstr ""

#: class-gf-batchbook.php:97
msgid "Batchbook settings have been updated."
msgstr ""

#: class-gf-batchbook.php:116
msgid ""
"Batchbook is a contact management tool makes it easy to track "
"communications, deals and people. Use Gravity Forms to collect customer "
"information and automatically add them to your Batchbook account. If you "
"don't have a Batchbook account, you can %1$s sign up for one here.%2$s"
msgstr ""

#: class-gf-batchbook.php:124
msgid ""
"Gravity Forms Batchbook Add-On requires your account URL and API Key, which "
"can be found on your Personal Settings page."
msgstr ""

#: class-gf-batchbook.php:147
msgid "Feed Name"
msgstr ""

#: class-gf-batchbook.php:152 class-gf-batchbook.php:490
msgid "Name"
msgstr ""

#: class-gf-batchbook.php:152
msgid "Enter a feed name to uniquely identify this setup."
msgstr ""

#: class-gf-batchbook.php:159
msgid "Person Details"
msgstr ""

#: class-gf-batchbook.php:163 class-gf-batchbook.php:166
msgid "Map Fields"
msgstr ""

#: class-gf-batchbook.php:166
msgid ""
"Select which Gravity Form fields pair with their respective Batchbook "
"fields. Batchbook custom fields must be a text field type to be mappable."
msgstr ""

#: class-gf-batchbook.php:177
msgid "Tags"
msgstr ""

#: class-gf-batchbook.php:183
msgid "About"
msgstr ""

#: class-gf-batchbook.php:189 class-gf-batchbook.php:191
msgid "Update Person"
msgstr ""

#: class-gf-batchbook.php:191
msgid ""
"If enabled and an existing person is found, their contact details will "
"either be replaced or appended. Job title and company will be replaced "
"whether replace or append is chosen."
msgstr ""

#: class-gf-batchbook.php:194
msgid "Update Person if already exists"
msgstr ""

#: class-gf-batchbook.php:200
msgid "and replace existing data"
msgstr ""

#: class-gf-batchbook.php:204
msgid "and append new data"
msgstr ""

#: class-gf-batchbook.php:212
msgid "Mark as Champion"
msgstr ""

#: class-gf-batchbook.php:217
msgid "Mark Person as Champion"
msgstr ""

#: class-gf-batchbook.php:226
msgid "Feed Conditional Logic"
msgstr ""

#: class-gf-batchbook.php:231 class-gf-batchbook.php:234
msgid "Conditional Logic"
msgstr ""

#: class-gf-batchbook.php:232
msgid "Enable"
msgstr ""

#: class-gf-batchbook.php:233
msgid "Export to Batchbook if"
msgstr ""

#: class-gf-batchbook.php:234
msgid ""
"When conditional logic is enabled, form submissions will only be exported "
"to Batchbook when the condition is met. When disabled, all form submissions "
"will be posted."
msgstr ""

#: class-gf-batchbook.php:255
msgid "First Name"
msgstr ""

#: class-gf-batchbook.php:262
msgid "Last Name"
msgstr ""

#: class-gf-batchbook.php:269 class-gf-batchbook.php:299
msgid "Email Address"
msgstr ""

#: class-gf-batchbook.php:288
msgid "Choose a Field"
msgstr ""

#: class-gf-batchbook.php:292
msgid "Job Title"
msgstr ""

#: class-gf-batchbook.php:296
msgid "Company Name"
msgstr ""

#: class-gf-batchbook.php:302 class-gf-batchbook.php:323
#: class-gf-batchbook.php:352 class-gf-batchbook.php:381
msgid "Work"
msgstr ""

#: class-gf-batchbook.php:306 class-gf-batchbook.php:331
#: class-gf-batchbook.php:356 class-gf-batchbook.php:385
msgid "Home"
msgstr ""

#: class-gf-batchbook.php:310 class-gf-batchbook.php:339
#: class-gf-batchbook.php:368 class-gf-batchbook.php:389
msgid "Other"
msgstr ""

#: class-gf-batchbook.php:316
msgid "Phone Number"
msgstr ""

#: class-gf-batchbook.php:319 class-gf-batchbook.php:348
#: class-gf-batchbook.php:377
msgid "Main"
msgstr ""

#: class-gf-batchbook.php:327
msgid "Mobile"
msgstr ""

#: class-gf-batchbook.php:335
msgid "Fax"
msgstr ""

#: class-gf-batchbook.php:345
msgid "Address"
msgstr ""

#: class-gf-batchbook.php:360
msgid "Billing"
msgstr ""

#: class-gf-batchbook.php:364
msgid "Shipping"
msgstr ""

#: class-gf-batchbook.php:374
msgid "Website"
msgstr ""

#: class-gf-batchbook.php:491
msgid "Action"
msgstr ""

#: class-gf-batchbook.php:505
msgid "Create New Person"
msgstr ""

#: class-gf-batchbook.php:525
msgid "Feed was not processed because API was not initialized."
msgstr ""

#: class-gf-batchbook.php:539 class-gf-batchbook.php:614
msgid "Person was not created because email address was not provided."
msgstr ""

#: class-gf-batchbook.php:606
msgid "Person was not created because first and/or last name were not provided."
msgstr ""

#: class-gf-batchbook.php:654
msgid "Person could not be created. %s"
msgstr ""

#: class-gf-batchbook.php:828
msgid "Person could not be updated. %s"
msgstr ""

#: class-gf-batchbook.php:982
msgid "Company could not be created. %s"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Gravity Forms Batchbook Add-On"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.gravityforms.com"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Integrates Gravity Forms with Batchbook, allowing form submissions to be "
"automatically sent to your Batchbook account."
msgstr ""

#. Author of the plugin/theme
msgid "rocketgenius"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.rocketgenius.com"
msgstr ""