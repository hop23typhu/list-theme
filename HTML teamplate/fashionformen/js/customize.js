$(window).load(function() {
	// product-detail
	$('.product-carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 120,
		maxItems: 3,
		move: 1,
		asNavFor: '#slider'
	});

	$('.product-slider').flexslider({
		animation: "slide",
		directionNav: false,
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
	});
	// slider-home-top
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 230,
		itemMargin: 5,
		minItems: 1,
		maxItems: 3,
		move: 1,
		asNavFor: '#slider'
	});

	$('#slider').flexslider({
		animation: "slide",
		directionNav: false,
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
	});
	// slider-home-bottom
	$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: false,
		itemWidth: 265,
		itemMargin: 5,
		minItems: 1,
		maxItems: 5,
		move: 1,
	});
	// header-menu
	$('#pull').click(function(event){
		event.preventDefault();
		$('.header-menu-left>ul').slideToggle();
	});
	// menu-responsive
	var wd=$(window).width();
	if(wd<769){
		$('.header-menu-left>ul>li>a').click(function(event){
			$(this).toggleClass('active');
			if($(this).hasClass('active')){
				event.preventDefault();
				$(this).next().slideDown();
			}else{
				$(this).next().slideUp();
			}
		});
	}
	//Image Lightbox
	$('.fancybox').fancybox();

	//$("a.fancybox").fancybox().hover(function() {
	//	$(this).click();
	//});


	//Title Page
	var padding=($(window).width()-$('.container').width())/2;
	$('.title-page').css('padding-left',padding);	
	$('.title-page').width($('.title-page span').width()+padding-30);
	//Tab Product
	$('.list-tab-title li').click(function(event){
		event.preventDefault();
		$('.content-tab-list li').slideUp();
		$('.content-tab-list li').eq($(this).index()).slideDown();
	});
	//Select Quote
	$('.wrap-quote select').rsSelectBox();
	//Get URL
	$('.zoom-product-detail').attr('href',$('#slider .flex-active-slide img').attr('src'));
	//Fix Height Upsell
	$('.upsell-thumb').height($('.first-thumb').height());
});
