<?php 

/* Enqueue scripts & styles */
function pt_login_scripts() {

	wp_enqueue_script( 'validate-script', get_template_directory_uri(). '/js/jquery.validate.min.js', array('jquery'), '1.12.0', true );
	wp_enqueue_script( 'ajax-auth-script', get_template_directory_uri(). '/js/ajax-auth-script.js', array('jquery'), '1.0', true );
   
    if ( class_exists( 'WooCommerce' ) ) {
        $redirect_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
    } else {
        $redirect_url = home_url();
    }

    wp_localize_script( 'ajax-auth-script', 'ajax_auth_object', array( 
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'redirecturl' => esc_url($redirect_url),
        'loadingmessage' => __('Sending user info, please wait...', 'plumtree')
    ));

    // Enable the user with no privileges to run ajax_login() in AJAX
    add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'ajax_register' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'pt_login_scripts');
}
 
function ajax_login(){

    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    // Nonce is checked, get the POST data and sign user on
  	// Call auth_user_login
	auth_user_login($_POST['username'], $_POST['password'], __('Login', 'plumtree')); 
	
    die();
}

function registration_handle($username, $email, $password, $become_vendor, $terms) {
    $errors = new WP_Error();
    if ( get_user_by( 'login', $username ) ) {
        $errors->add( 'login_exists', __('This username is already registered.', 'plumtree') );
    }
    if ( get_user_by( 'email', $email ) ) {
        $errors->add( 'email_exists', __('This email address is already registered.', 'plumtree') );
    }
    if ( class_exists('WCV_Vendors') && class_exists( 'WooCommerce' ) && $become_vendor == 1 ) {
        $terms_page = WC_Vendors::$pv_options->get_option( 'terms_to_apply_page' );
        if ( $terms_page && $terms_page!='' && $terms=='' ) {
            $errors->add( 'must_accept_terms', __('You must accept the terms and conditions to become a vendor.', 'plumtree') );
        }
    }
    $err_var = $errors->get_error_codes();
    if ( ! empty( $err_var ) )
        return $errors;
}

function ajax_register(){
    // First check the nonce, if it fails the function will break
    check_ajax_referer( 'ajax-register-nonce', 'security' );

    // Check for errors before creating new user
    $user_check = registration_handle($_POST['username'],$_POST['email'],$_POST['password'],$_POST['vendor'],$_POST['terms']);
    if ( is_wp_error($user_check) ){ 
        $error  = $user_check->get_error_codes() ;
        
        if(in_array('login_exists', $error))
            echo json_encode(array('loggedin'=>false, 'message'=> ($user_check->get_error_message('login_exists'))));
        elseif(in_array('email_exists',$error))
            echo json_encode(array('loggedin'=>false, 'message'=> ($user_check->get_error_message('email_exists'))));
        elseif(in_array('must_accept_terms',$error))
        echo json_encode(array('loggedin'=>false, 'message'=> ($user_check->get_error_message('must_accept_terms'))));
    } else {
    // Nonce is checked, get the POST data and sign user on
        $info = array();
        $info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
        $info['user_pass'] = sanitize_text_field($_POST['password']);
        $info['user_email'] = sanitize_email( $_POST['email']);
    
    // Register the user
        $user_register = wp_insert_user( $info );

        if ( class_exists('WCV_Vendors') && class_exists( 'WooCommerce' ) && $_POST[ 'vendor' ] == 1 ) {
            $manual = WC_Vendors::$pv_options->get_option( 'manual_vendor_registration' );
            $role   = apply_filters( 'wcvendors_pending_role', ( $manual ? 'pending_vendor' : 'vendor' ) );
            $wp_user_object = new WP_User( $user_register );
            $wp_user_object->set_role( $role );
            do_action( 'wcvendors_application_submited', $user_register );
        }
    // Login to new account
        auth_user_login($info['nickname'], $info['user_pass'], __('Registration', 'plumtree'));
    }
    die();
}

function auth_user_login($user_login, $password, $login)
{
	$info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $password;
    $info['remember'] = true;
	
	$user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.', 'plumtree')));
    } else {
		wp_set_current_user($user_signon->ID); 
        echo json_encode(array('loggedin'=>true, 'message'=> $login.__(' successful, redirecting...', 'plumtree')));
    }
	
	die();
}