<?php
/**
 * The Template for displaying the product ratings in the product panel
 *
 * Override this template by copying it to yourtheme/wc-vendors/front/ratings
 *
 * @package    WCVendors_Pro
 * @version    1.0.3
 */

// This outputs the star rating 
$stars = ''; 
for ($i = 1; $i<=stripslashes( $rating ); $i++) { $stars .= "<i class='fa fa-star'></i>"; } 
for ($i = stripslashes( $rating ); $i<5; $i++) { $stars .=  "<i class='fa fa-star-o'></i>"; }
?> 

<h6><?php echo $rating_title; ?>  <?php echo $stars; ?></h6>
<span><?php _e( 'Posted on', 'plumtree'); ?> <?php echo $post_date; ?></span><?php _e( 'by', 'plumtree'); echo $customer_name; ?><br />
<p><?php echo $comment; ?></p>
<hr />