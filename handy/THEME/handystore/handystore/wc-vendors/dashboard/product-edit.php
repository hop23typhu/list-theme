<?php
/**
 * The template for displaying the Product edit form
 *
 * Override this template by copying it to yourtheme/wc-vendors/dashboard/
 *
 * @package    WCVendors_Pro
 * @version    1.2.0
 */
/**
 *   DO NOT EDIT ANY OF THE LINES BELOW UNLESS YOU KNOW WHAT YOU'RE DOING
 *
*/

$title = 	( is_numeric( $object_id ) ) ? __('Save Changes', 'wcvendors-pro') : __('Add Product', 'wcvendors-pro');
$product = 	( is_numeric( $object_id ) ) ? wc_get_product( $object_id ) : null;

// Get basic information for the product
$product_title     			= ( isset($product) && null !== $product ) ? $product->post->post_title    : '';
$product_description        = ( isset($product) && null !== $product ) ? $product->post->post_content  : '';
$product_short_description  = ( isset($product) && null !== $product ) ? $product->post->post_excerpt  : '';

/**
 *  Ok, You can edit the template below but be careful!
*/
?>

<h2><?php echo $title; ?></h2>

<!-- Product Edit Form -->
<form method="post" action="" id="wcv-product-edit" class="wcv-form wcv-formvalidator">

	<!-- Basic Product Details -->
	<div class="wcv-product-basic wcv-product">
		<!-- Product Title -->
		<?php WCVendors_Pro_Product_Form::title( $object_id, $product_title ); ?>
		<!-- Product Description -->
		<?php WCVendors_Pro_Product_Form::description( $object_id, $product_description );  ?>
		<!-- Product Short Description -->
		<?php WCVendors_Pro_Product_Form::short_description( $object_id, $product_short_description );  ?>
		<!-- Product Categories -->
		<?php WCVendors_Pro_Product_Form::categories( $object_id, true ); ?>
		<!-- Product Tags -->
		<?php WCVendors_Pro_Product_Form::tags( $object_id ); ?>
		<!-- Product Attributes (if any) -->
		<?php WCVendors_Pro_Form_helper::attribute( $object_id ); ?>
	</div>

	<div class="all-100">
    	<!-- Media uploader -->
		<div class="wcv-product-media">
			<?php WCVendors_Pro_Form_helper::product_media_uploader( $object_id ); ?>
		</div>
	</div>

	<hr />

	<div class="all-100">
		<!-- Product Type -->
		<div class="wcv-product-type">
			<?php WCVendors_Pro_Product_Form::product_type( $object_id ); ?>
		</div>
	</div>

	<div class="all-100">
		<div class="wcv-tabs top" data-prevent-url-change="true">

			<?php WCVendors_Pro_Product_Form::product_meta_tabs( ); ?>

			<?php do_action( 'wcv_before_general_tab' ); ?>

			<!-- General Product Options -->
			<div class="wcv-product-general tabs-content" id="general">

				<div class="hide_if_grouped">
					<!-- SKU  -->
					<?php WCVendors_Pro_Product_Form::sku( $object_id ); ?>
					<!-- Private listing  -->
					<?php WCVendors_Pro_Product_Form::private_listing( $object_id ); ?>
				</div>


				<div class="options_group show_if_external">
					<?php WCVendors_Pro_Product_Form::external_url( $object_id ); ?>
					<?php WCVendors_Pro_Product_Form::button_text( $object_id ); ?>
				</div>

				<div class="show_if_simple show_if_external">
					<!-- Price and Sale Price -->
					<?php WCVendors_Pro_Product_Form::prices( $object_id ); ?>
				</div>

				<div class="show_if_simple show_if_external">
					<!-- Tax -->
					<?php WCVendors_Pro_Product_Form::tax( $object_id ); ?>
				</div>

				<div class="show_if_downloadable" id="files_download">
					<!-- Downloadable files -->
					<?php WCVendors_Pro_Product_Form::download_files( $object_id ); ?>
					<!-- Download Limit -->
					<?php WCVendors_Pro_Product_Form::download_limit( $object_id ); ?>
					<!-- Download Expiry -->
					<?php WCVendors_Pro_Product_Form::download_expiry( $object_id ); ?>
					<!-- Download Type -->
					<?php WCVendors_Pro_Product_Form::download_type( $object_id ); ?>
				</div>
			</div>

			<?php do_action( 'wcv_after_general_tab' ); ?>

			<?php do_action( 'wcv_before_inventory_tab' ); ?>

			<!-- Inventory -->
			<div class="wcv-product-inventory inventory_product_data tabs-content" id="inventory">

				<?php WCVendors_Pro_Product_Form::manage_stock( $object_id ); ?>

				<?php do_action( 'woocommerce_product_options_stock' ); ?>

				<div class="stock_fields show_if_simple show_if_variable">
					<?php WCVendors_Pro_Product_Form::stock_qty( $object_id ); ?>
					<?php WCVendors_Pro_Product_Form::backorders( $object_id ); ?>
				</div>

				<?php WCVendors_Pro_Product_Form::stock_status( $object_id ); ?>
				<div class="options_group show_if_simple show_if_variable">
					<?php WCVendors_Pro_Product_Form::sold_individually( $object_id ); ?>
				</div>

				<?php do_action( 'woocommerce_product_options_sold_individually' ); ?>

				<?php do_action( 'woocommerce_product_options_inventory_product_data' ); ?>

			</div>

			<?php do_action( 'wcv_after_inventory_tab' ); ?>

			<?php do_action( 'wcv_before_shipping_tab' ); ?>

			<!-- Shipping  -->
			<div class="wcv-product-shipping shipping_product_data tabs-content" id="shipping">

				<div class="hide_if_grouped">

					<!-- Shipping rates  -->
					<?php WCVendors_Pro_Product_Form::shipping_rates( $object_id ); ?>
					<!-- weight  -->
					<?php WCVendors_Pro_Product_Form::weight( $object_id ); ?>
					<!-- Dimensions -->
					<?php WCVendors_Pro_Product_Form::dimensions( $object_id ); ?>
					<?php do_action( 'woocommerce_product_options_dimensions' ); ?>
					<!-- shipping class -->
					<?php WCVendors_Pro_Product_Form::shipping_class( $object_id ); ?>
					<?php do_action( 'woocommerce_product_options_shipping' ); ?>
				</div>

			</div>

			<?php do_action( 'wcv_after_shipping_tab' ); ?>

			<?php do_action( 'wcv_before_linked_tab' ); ?>

			<!-- Upsells and grouping -->
			<div class="wcv-product-upsells tabs-content" id="linked_product">
				<?php WCVendors_Pro_Product_Form::up_sells( $object_id ); ?>

				<?php WCVendors_Pro_Product_Form::crosssells( $object_id ); ?>
			</div>

			<?php do_action( 'wcv_after_linked_tab' ); ?>

			<hr />

			<h2><?php _e('Handy Store extra Settings', 'plumtree'); ?></h2>
			<div class="all-100">
		    	<!-- Extra Gallery -->
		    	<?php $values = get_post_custom($object_id);
					  $pt_product_extra_gallery = isset( $values['pt_product_extra_gallery'] ) ? esc_attr( $values['pt_product_extra_gallery'][0] ) : 'off';
					  $pt_vendor_special_offers_carousel = isset( $values['pt_vendor_special_offers_carousel'] ) ? esc_attr( $values['pt_vendor_special_offers_carousel'][0] ) : 'off'; ?>
		    	<div class="product-extra-gallery">
					<input type="checkbox" class="input-checkbox" name="pt_product_extra_gallery" id="pt_product_extra_gallery" <?php checked( $pt_product_extra_gallery, 'on' ); ?> /><label class="checkbox" for="pt_product_extra_gallery"><?php _e( 'Use extra gallery for this product', 'plumtree' ) ?></label>
					<p><?php _e( 'Check the checkbox if you want to use extra gallery (appeared on hover) for this product. The first 3 images of the product gallery are going to be used for gallery.', 'plumtree'); ?></p>
				</div>
				<br />
				<div class="vendor-special-offers-carousel">
					<input type="checkbox" class="input-checkbox" name="pt_vendor_special_offers_carousel" id="pt_vendor_special_offers_carousel" <?php checked( $pt_vendor_special_offers_carousel, 'on' ); ?> /><label class="checkbox" for="pt_vendor_special_offers_carousel"><?php _e( 'Add this product to "Special Offers" carousel', 'plumtree' ) ?></label>
					<p><?php _e( 'Check the checkbox if you want to add this product to the "Special Offers" carousel on your Vendor Store Page.', 'plumtree'); ?></p>
				</div>

			</div>

			<?php WCVendors_Pro_Product_Form::form_data( $object_id ); ?>
			<?php WCVendors_Pro_Product_Form::save_button( $title ); ?>
			<?php WCVendors_Pro_Product_Form::draft_button( __('Save Draft',' wcvendors-pro') ); ?>

		</div>
	</div>

</form>
