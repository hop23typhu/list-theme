<?php
/**
 * Template Name: Front Page
 */

get_header(); ?>

  <?php /* Banners Section */
    if (get_option('front_page_shortcode_section')=='on') { ?>
    <div class="front-page-shortcode col-xs-12 col-sm-12 col-md-12">
      <div class="row">
        <?php echo do_shortcode( get_option('front_page_shortcode_section_shortcode') ); ?>
      </div>
    </div>
  <?php } ?>

    <main class="site-content<?php if (function_exists('pt_main_content_class')) pt_main_content_class(); ?>" itemscope="itemscope" itemprop="mainContentOfPage"><!-- Main content -->

        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
              <div class="entry-content">
                  <?php the_content(); ?>
              </div>
            <?php endwhile; ?>
        <?php endif; ?>

    </main><!-- end of Main content -->
        
    <?php get_sidebar(); ?>

    <?php /* Special front page sidebar */
      if (get_option('front_page_special_sidebar')=='on') {
        if ( is_active_sidebar( 'front-special-sidebar' ) ) { ?>
          <div id="special-sidebar-front" class="widget-area col-xs-12 col-sm-12 col-md-12 sidebar lazyload" data-expand="-100" role="complementary">
            <div class="row">
              <?php dynamic_sidebar( 'front-special-sidebar' ); ?>
            </div>
          </div>
        <?php
        }
      } ?>

<?php get_footer(); ?>