/* Handy store page js functions ver. 1.0 */

jQuery(document).ready(function($){
    $(window).load(function(){
    	"use strict";

		/* List/Grid Switcher */
		function view_swither() {
			var container = $('ul.products:not(.owl-carousel)');
			if ( $('.pt-view-switcher span.pt-list').hasClass('active') ) {
				container.find('.product').each(function(){
					if ($(this).not('.list-view')) {
						$(this).addClass('list-view');
					};
				});
			};

			$('.pt-view-switcher').on( 'click', 'span', function(e) {
				e.preventDefault();
				if ( (e.currentTarget.className == 'pt-grid active') || (e.currentTarget.className == 'pt-list active') ) {
					return false;
				}

				if ( $(this).hasClass('pt-grid') && $(this).not('.active') ) {
					container.animate({opacity:0},function(){
						$('.pt-view-switcher .pt-list').removeClass('active');
						$('.pt-view-switcher .pt-grid').addClass('active');
						container.find('.product').each(function(){
							$(this).removeClass('list-view');
						});
						container.stop().animate({opacity:1});
					});
				}

				if ( $(this).hasClass('pt-list') && $(this).not('.active') ) {
					container.animate({opacity:0},function(){
						$('.pt-view-switcher .pt-grid').removeClass('active');
						$('.pt-view-switcher .pt-list').addClass('active');
						container.find('.product').each(function(){
							$(this).addClass('list-view');
						});
						container.stop().animate({opacity:1});
					});
				}
			});	
		}
		view_swither();

		/* Extra product gallery animation */
		function extra_gallery_animation() {
	 		$('li.product .inner-product-content.slide-hover').each(function(){	

		 		var current_product = $(this),
		 			parent = current_product.parent(),
		 			main_image_width = current_product.find('.pt-extra-gallery-img').width(),
					thumbs_width = current_product.find('.pt-extra-gallery-thumbs').outerWidth(),
					buttons_height = current_product.find('.additional-buttons').outerHeight(),
					initial_product_width = current_product.outerWidth(),
					initial_product_height = current_product.outerHeight();

				if ( !parent.hasClass('list-view') ) {
					current_product.css({
						"width": initial_product_width,
						"height": initial_product_height,
					});
					current_product.find('.pt-extra-gallery-img').width(main_image_width);			
				};
	 		
	 			current_product.hoverIntent({
					sensitivity: 1,   // number = sensitivity threshold (must be 1 or higher)
					interval: 10,     // number = milliseconds of polling interval
					over: function () {
						if ( !parent.hasClass('list-view') ) {
							var new_width = initial_product_width+thumbs_width;
							var new_height = initial_product_height+buttons_height;
							current_product.css({
								"width": new_width,
								"height": new_height,
							});
							current_product.find('.pt-extra-gallery-thumbs').css({'opacity':1});
						};
						parent.css('z-index',20).siblings().css('z-index',10);
					},
					timeout: 0,       // number = milliseconds delay before onMouseOut function call
					out: function () {
						if ( !parent.hasClass('list-view') ) {
							current_product.css({
								"width": initial_product_width,
								"height": initial_product_height,
							});
							current_product.find('.pt-extra-gallery-thumbs').css({'opacity':0});
						};
						parent.css('z-index',11);
					},
				});

	 		});			
		}
		extra_gallery_animation();

		/* Extra product gallery images links */
		function gallery_links() {
		    $("ul.pt-extra-gallery-thumbs li a").on( 'click', function(e) {
		        e.preventDefault();
		        var mainImage = $(this).attr("href"),
		        mainImageContainer = $(this).parent().parent().parent().find(".pt-extra-gallery-img img");
		        mainImageContainer.attr({ src: mainImage });
		        return false;
		    });
		}
		gallery_links();

		/* Product dropdown filters animation */
		var settings = {
			interval: 100,
			timeout: 200,
			over: mousein_triger,
			out: mouseout_triger
		};

		function mousein_triger(){
			$(this).find('.filters-group').css('visibility', 'visible');
			$(this).addClass('hovered');
		}
		function mouseout_triger() {
			$(this).removeClass('hovered');
			$(this).find('.filters-group').delay(300).queue(function() {
				$(this).css('visibility', 'hidden').dequeue();
			});
		}

		$('#filters-sidebar .dropdown-filters').hoverIntent(settings);

		/* AJAX callback for product filters */

		// Variables
		var obj = {};
		var obj_ajax = {};
		var out = '';
		var filters_container = $('.filters-group');

		// Update counters for filters
		function get_updated_counters() {
			var all_filters = {};
			var updated_counters = {};
			var filtered_elements = '';
			var filter = null;
			var filters_container = $('.filters-group');

			// get all available filters
			filters_container.each(function(){
				$(this).find('.filter').each(function(){
					filter = $(this).attr('id');
					if ( ($.inArray(filter, all_filters) == -1) && filter!='' ) all_filters[filter] = 0;
				});
			});

			// get data from filtered products
			$('[data-filters=container] .product').each( function() {
				filtered_elements += $(this).data('filter');
			});

			// get updated counters
			var updated_counters = all_filters;
			$.each( updated_counters, function (key, value) {
				updated_counters[key] = (filtered_elements.match(new RegExp(key, 'g'))||[]).length;
			});
			
			// update counters
			$.each( updated_counters, function (key, value) {
				$('.filter').each(function(){
					if ( $(this).attr('id') == key ) {
						$(this).find('.counter').text(value);
					};
				});
			});
		}

		filters_container.on( 'click', '.filter', function() {
			// collect the input value and name
			var val = $(this).data('value');
			var name = $(this).data('name');

			// if the object is not an array make sure to instantiate it
			if ($.type(obj[name]) !== 'array') {
				obj[name] = [];
			}

			// if the radiobox was checked make sure to push the value if absent
			if ($.inArray(val, obj[name]) === -1) {
				obj[name] = [val];
			} else {
				obj[name].length = 0;
			}
			if ( val == '*') {
				obj[name].length = 0;
			}

			// iterate over each object elements 
			$.each(obj, function (key, value) {
				var ajax_key = key;
				// destroy empty arrays
				if ($(this).length === 0) {
					delete(obj[key]);
					delete(obj_ajax['filter_' + ajax_key]);
					/*delete(obj_ajax['query_type_' + ajax_key]);*/
				}
				else {
					// construct ajax object elements
					obj_ajax['filter_' + ajax_key] = value.join(',');
					/*obj_ajax['query_type_' + ajax_key] = 'and';*/
				}
			});

			// Add special Animation wrapper
			$('[data-filters=container]').css({'opacity': '0', 'transition':'none'}).before('<div class="isotope-animation-wrapper"></div>');

			// Get new query
			var shop_link = window.location.pathname;
			if (~shop_link.indexOf("page")) {
				shop_link = shop_link.substring(0,shop_link.indexOf("page"));
			};
			$.get(shop_link, obj_ajax, function (response) {
				var new_products = $(response).find('.products').html();
				var new_pagination = '';
						
				if ( $(response).find('.woocommerce-pagination') ) {
					new_pagination = $(response).find('.woocommerce-pagination').html();
				}
				$('.products').html(new_products);
				if (new_pagination) {
					$('.woocommerce-pagination').html(new_pagination).show();
				} else {
					$('.woocommerce-pagination').hide();
				}

				view_swither();
				extra_gallery_animation();
				gallery_links();
				get_updated_counters();                 
						
				// Remove Wrapper
				$('[data-filters=container]').css({'opacity':'1', 'transition':'opacity .3s ease-in-out'});
				$(".isotope-animation-wrapper").remove();
			});
		});

		$('.filters-group').each( function( i, buttonGroup ) {
	        var buttonGroup = $( buttonGroup );
	        buttonGroup.on( 'click', '.filter', function() {
	            buttonGroup.find('.is-checked').removeClass('is-checked');
	            $( this ).addClass('is-checked');
	        });
	    });	

		

    });
});





