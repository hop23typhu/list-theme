<?php

/*
 WARNING: This file is part of the core Genesis Framework. DO NOT edit
 this file under any circumstances. Please do all modifications
 in the form of a child theme.
 */

/**
 * This file calls the init.php file, but only
 * if the child theme hasn't called it first.
 *
 * This method allows the child theme to load
 * the framework so it can use the framework
 * components immediately.
 *
 * This file is a core Genesis file and should not be edited.
 *
 * @category Genesis
 * @package  Templates
 * @author   StudioPress
 * @license  GPL-2.0+
 * @link     http://my.studiopress.com/themes/genesis
 */

require_once( dirname( __FILE__ ) . '/lib/init.php' );


function faci_dashboard() { ?>
<h3>Chào mừng bạn đến với trang Quản Trị Website <?php echo bloginfo( 'name' ); ?>.</h3>
<p><strong>THÔNG TIN WEBSITE:</strong></p>
<p><?php echo bloginfo( 'name' ); ?> | <?php echo bloginfo( 'description' ); ?></p>
<p>Website được phát triển bởi <strong><a target="_blank" href="https://www.facebook.com/steve.luong.5">Lương Bá Hợp</a></strong> Sử dụng <strong> WordPress Phiên bản <?php echo bloginfo("version"); ?> </strong>.</p>
	
<p><strong>THÔNG TIN LIÊN HỆ:</strong><br>
<strong>Web Developer</strong>:  Lương Bá Hợp<br>
<strong>Email</strong>: luonghop.lc@gmail.com<br>
<strong>Website</strong>: <a target="_blank" href="http://teachyourself.vn/">Teachyourself.vn</a></p> 
<?php }
add_action('wp_dashboard_setup', 'faci_welcome');
function faci_welcome() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Faci QSOFT Theme', 'faci_dashboard');
}


//change footer backend wordpress
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
	echo 'Powered by Faci Theme | Designed by <a href="http://teachyourself.vn">Lương Bá Hợp</a> </p>';
}



// disable logo wordpress in backend
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}



// add target blank to visit site
add_action( 'admin_bar_menu', 'customize_my_wp_admin_bar', 80 );
function customize_my_wp_admin_bar( $wp_admin_bar ) {
    //Get a reference to the view-site node to modify.
    $node = $wp_admin_bar->get_node('view-site');
    //Change target
    $node->meta['target'] = '_blank';
    //Update Node.
    $wp_admin_bar->add_node($node);
}

