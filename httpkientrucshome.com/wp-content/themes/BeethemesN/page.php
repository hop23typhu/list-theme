<?php
    get_header();
	do_action( 'genesis_before_content_sidebar_wrap' );
?>
	<div class="content-sidebar-wrap">
		<?php do_action( 'genesis_before_content' ); ?>
			<main class="content" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
			<?php
				do_action( 'genesis_before_loop' );
				do_action( 'genesis_before_entry' );
			?>
            <div id="home-content" class="clearfix">
    			<?php if( have_posts() ) : the_post();  ?>
    				<h1 class="entry-title"><?php the_title(); ?></h1>
    					<div class="entry-content">
    						<?php the_content(); ?>
    					</div>
    					
    			<?php endif; ?>

    			<?php do_action( 'genesis_after_loop' ); ?>
            </div>
    </main><!-- end #content -->
	<?php do_action( 'genesis_after_content' ); ?>
</div><!-- end #content-sidebar-wrap -->
<?php
	do_action( 'genesis_after_content_sidebar_wrap' );
	get_footer();
?>