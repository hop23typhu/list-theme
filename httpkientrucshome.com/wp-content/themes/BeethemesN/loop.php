<?php
	global $url,$no_thum;

	$gia = get_field('dientich');
	$khuyenmai = get_field('giakhuyenmai');
?>
<div class="product-item">
    <div class="product-img">
        <a class="img" href="<?php the_permalink();?>" title="<?php the_title();?>">
            <?php if(has_post_thumbnail()) the_post_thumbnail("medium",array("alt" => get_the_title()));
                else echo $no_thum; ?>
        </a>
    </div>
<a class="product-title" href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a>
<?php
    if(!empty($khuyenmai)) {
        echo "<p class='price'><span> diện tích:</span></p>";
    }
?>
<p class="price<?php if(!empty($khuyenmai)) echo " textline"?>">
    <?php
        if(!empty($gia)) echo "Diện tích:".$gia; else echo " Liên Hệ"; ?>
</p> 
</div><!--End .product-item-->