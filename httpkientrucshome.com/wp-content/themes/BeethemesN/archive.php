<?php
	get_header();
    do_action( 'genesis_before_content_sidebar_wrap' );
    $catid = get_query_var('cat');
?>
	<div class="content-sidebar-wrap wrapcontent">
		<?php do_action( 'genesis_before_content' );
         ?>
		 <main class="content" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
			<?php
				do_action( 'genesis_before_loop' );
			?>
            <div id="home-content" class="clearfix">
                    <h1 class="heading"><?php echo get_cat_name( $catid ); ?></h1>
                    <div class="product-list clearfix" id="product-4">
					<?php
                       $q = new WP_Query( 'cat=' .$catid. '&paged=' .get_query_var( 'paged' ) );
                            if ( $q->have_posts() ) :
                                while( $q->have_posts() ) :
                                    $q->the_post();

                            get_template_part('loop');

                            endwhile;
                            if ( function_exists( 'wp_pagenavi' ) ) { wp_pagenavi(); }
                            wp_reset_postdata();
                        endif;
                    ?>
					</div><!--End .product-list-->
                </div><!--End #news-wrap-->
            <?php
				do_action( 'genesis_after_loop' );
			?>
		</main><!-- end #content -->
		<?php do_action( 'genesis_after_content' ); ?>
	</div><!-- end #content-sidebar-wrap -->
<?php
	do_action( 'genesis_after_content_sidebar_wrap' );
	get_footer();
?>