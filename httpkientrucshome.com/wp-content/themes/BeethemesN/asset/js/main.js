(function($) {
	"use strict";
$( document ).ready( function() {

	// back top
	var backtop = $( ".back-to-top" );
	backtop.hide();
	$( window ).scroll( function () {
        if ( $( this ).scrollTop() > 100 ) {
            backtop.fadeIn();
        } else {
            backtop.fadeOut();
        }
    });
    $( '.back-to-top' ).click(function () {
        $( 'body,html' ).animate( {
            scrollTop: 0
        }, 800 );
        return false;
    });
 	// end back top

    /* * demo owl
        $("#owl-example").owlCarousel({
            items: 5,
            itemsDesktop: [1199,4],
            itemsCustom: [[0, 2], [400, 4], [700, 6], [1000, 8], [1200, 10], [1600, 16]],
            singleItem: false,
            slideSpeed: 200,
            paginationSpeed: 800,
            autoPlay: false,
            stopOnHover: false,
            navigation: false,
            navigationText: ["prev","next"],
            pagination: true,
            paginationNumbers: false,
            responsive: true,
            addClassActive: false
        });
    */
    var nav = $('.site-header');

        $(window).scroll(function () {
            if ($(this).scrollTop() > 76) {
                nav.addClass("f-nav animated fadeInDown");
            } else {
                nav.removeClass("f-nav animated fadeInDown");
            }
        });

    $("#imgqc-4 .image-adv").owlCarousel({
        items: 4,
        singleItem: false,
        slideSpeed: 400,
        paginationSpeed: 800,
        autoPlay: true,
        stopOnHover: false,
        navigation: false,
        navigationText: ["prev","next"],
        pagination: true,
        paginationNumbers: false,
        responsive: true,
        addClassActive: false
    });

  
});

$(window).load(function() {
      $('.product-list').isotope({
      itemSelector: '.product-item',
      percentPosition: true,
      masonry: {
        // use outer width of grid-sizer for columnWidth
        columnWidth: '.product-item'
      }
    })
});
 
})(jQuery);