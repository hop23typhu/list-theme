<?php
/**
 * @version    1.0
 * @package    BeeThemes
 * @author     Sang Minh <sang.nguyen1691@gmail.com>
 * @copyright  Copyright (C) 2015 Bee Work All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: http://beedesign.vn
*/

// create function shortcodes
function create_bee_product( $arg, $content = null ) {

	$arg = shortcode_atts( array(
		'cat' => '',
		'orderby' => 'date',
		'order' => 'DESC',
		'limit' => '',
		'pid' => '',
		'column' => '',
		'slider' => '',
	) );

    $query = array(
    	'cat' => $arg['cat'],
    	'orderby' => $arg['orderby'],
    	'order' => $arg['order'],
    	'showposts' => $arg['limit'],
    	'p' => $arg['pid']
    );

    // mode columns
    $cls = array();
	if ( $arg['column'] == 4 ) {
		$cls[] = 'column-4';
	} elseif( $arg['column'] == 5 ) {
		$cls[] = 'column-5';
	} elseif ( $arg['column'] == 3 ) {
		$cls[] = 'column-3';
	}elseif ( $arg['column'] == 2 ) {
		$cls[] = 'column-2';
	}

	if ( $arg['slider'] == 1 ) {
		$cls[] = 'pro-slide';
	}

	$q = new WP_Query( $query );

	ob_start();

	echo '<div class="product-list ' .implode( ' ', $cls ). '">';
	while ( $q -> have_posts() ) :
    	$q -> the_post();

		get_template_part( 'loop' );

	endwhile; wp_reset_postdata();
	echo '</div>';

	if ( $arg['slider'] == 1 ) {
?>
	<script type="text/javascript">
	(function($) {
		"use strict";
	$( document ).ready( function() {
		$(".pro-slide").owlCarousel({
	            items: <?php if ( ! empty( $arg['item'] ) ) echo $arg['item']; else echo '1'; ?>,
	            slideSpeed: 500,
	            navigation: true,
	            navigationText: [
                                "<i class='fa fa-angle-left'></i>",
                                "<i class='fa fa-angle-right'></i>"
                                ]
        });
    });
	})(jQuery);
	</script>

<?php
	}

	$result = ob_get_contents();

	ob_end_clean();

	return $result;
}
add_shortcode( 'bee_product', 'create_bee_product' );

// shortcode social
function create_bee_social( $atts, $content = null ) {

	$html = '';
		extract( shortcode_atts(
			array(
				'style'      => '',
				'facebook'   => '',
				'twitter'    => '',
				'linkedin'   => '',
				'instagram'  => '',
				'gplus'      => '',
				'skype'      => '',
				'pinterest'  => '',
				'github'     => '',
				'foursquare' => '',
				'dribbble'   => '',
				'youtube'    => '',
				'rss'        => '',
			), $atts ) );

	// Style for social bar
	if ( isset( $atts['style'] ) && ! in_array( $atts['style'], array( 'large-icon', 'dark-icon', 'line-icon', 'colors-icon' ) ) ) {
		$atts['style'] = ' ';
	}

	$html .= '<div class="social-bar '. $style .'">';
	if ( $facebook ) {
		$html .='<a class="facebook" href="' . $facebook . '"><i class="fa fa-facebook"></i></a>';
	}
	if ( $twitter ) {
		$html .='<a class="twitter" href="' . $twitter . '"><i class="fa fa-twitter"></i></a>';
	}
	if ( $linkedin ) {
		$html .='<a class="linkedin" href="' . $linkedin . '"><i class="fa fa-linkedin"></i></a>';
	}
	if ( $instagram ) {
		$html .='<a class="instagram" href="' . $instagram . '"><i class="fa fa-instagram"></i></a>';
	}
	if ( $gplus ) {
		$html .='<a class="gplus" href="' . $gplus . '"><i class="fa fa-google-plus"></i></a>';
	}
	if ( $skype ) {
		$html .='<a class="skype" href="skype:' . $skype . '?call"><i class="fa fa-skype"></i></a>';
	}
	if ( $pinterest ) {
		$html .='<a class="pinterest" href="' . $pinterest . '"><i class="fa fa-pinterest"></i></a>';
	}
	if ( $github ) {
		$html .='<a class="github" href="' . $github . '"><i class="fa fa-github"></i></a>';
	}
	if ( $foursquare ) {
		$html .='<a class="foursquare" href="' . $foursquare . '"><i class="fa fa-foursquare"></i></a>';
	}
	if ( $dribbble ) {
		$html .='<a class="dribbble" href="' . $dribbble . '"><i class="fa fa-dribbble"></i></a>';
	}
	if ( $youtube ) {
		$html .='<a class="youtube" href="' . $youtube . '"><i class="fa fa-youtube"></i></a>';
	}
	if ( $rss ) {
		$html .='<a class="rss" href="' . $rss . '"><i class="fa fa-rss"></i></a>';
	}
	$html .= '</div>';

	return apply_filters( 'wr_wooplus_social_bars', force_balance_tags( $html ) );
}
add_shortcode( 'bee_social', 'create_bee_social' );

add_filter('widget_text', 'do_shortcode');