<?php
    get_header();
    do_action( 'genesis_before_content_sidebar_wrap' );
    global $post;
    $category = wp_get_object_terms( $post->ID, 'category', array( 'orderby' => 'parent', 'order' => 'DESC' ) );
    $catid = $category->term_id;
?>
	<div class="content-sidebar-wrap">
		<?php do_action( 'genesis_before_content' ); ?>
		<main class="content" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
			<?php
				do_action( 'genesis_before_loop' );
				do_action( 'genesis_before_entry' );
			?>
                <div id="product-detail" class="cleafix">
                	<?php if( have_posts() ) : the_post();
                    ?>
                    <h1 class="entry-title heading"><?php the_title(); ?></h1>
                    <div class="clearfix"></div>

                    <div class="entry-content">
						<?php
							the_content();
						?>
                    </div>

                	<?php endif; // end loop single ?>
                </div><!--End. Product-Detail-->

                    <div id="related-post">
                        <h3 class="heading">Sản Phẩm Liên Quan</h3>
                        <div class="product-list">
                          <?php
                            $rel = new WP_Query(array(
                                'cat' => $catid,
                                'showposts' => 9,
                                'post__not_in' => array($post->ID)
                            ));
                            if($rel->have_posts()):
                                while($rel->have_posts()):
                                    $rel->the_post();

                               get_template_part('loop');

                                    endwhile;
                                endif;
                            ?>
                        </div>

                    </div> <!-- end related -->
            <?php
				do_action( 'genesis_after_loop' );
			?>

		</main><!-- end #content -->
		<?php do_action( 'genesis_after_content' ); ?>
	</div><!-- end #content-sidebar-wrap -->
	<?php
	do_action( 'genesis_after_content_sidebar_wrap' );
	get_footer();
?>