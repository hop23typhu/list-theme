<?php
if( !function_exists( 'jm_get_resume_setting' ) ) :
	function jm_get_resume_setting($id = null ,$default = null){
		return jm_get_setting('noo_resume_general', $id, $default);
	}
endif;

if( !function_exists( 'jm_resume_enabled' ) ) :
	function jm_resume_enabled(){
		return (bool) jm_get_resume_setting('enable_resume', 1);
	}
endif;

if( !function_exists( 'jm_get_resume_status' ) ) :
	function jm_get_resume_status() {
		return apply_filters('noo_resume_status', array(
			'draft'           => _x( 'Draft', 'Job status', 'noo' ),
			// 'preview'         => _x( 'Preview', 'Job status', 'noo' ),
			'pending'         => _x( 'Pending Approval', 'Job status', 'noo' ),
			'pending_payment' => _x( 'Pending Payment', 'Job status', 'noo' ),
			'publish'         => _x( 'Published', 'Job status', 'noo' ),
		));
	}
endif;
