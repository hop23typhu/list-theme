<?php
if( !function_exists( 'jm_get_theme_mod' ) ) :
	function jm_get_theme_mod( $option, $default = null ) {
		$value = get_theme_mod( $option, $default );
		// $value = get_option( $option, $default );
		$value = ( $value === null || $value === '' ) ? $default : $value;

		return apply_filters( 'jm_theme_mod', $value, $option, $default );
	}
endif;

if( !function_exists( 'jm_force_redirect' ) ) :
	function jm_force_redirect( $location, $status = 302 ) {
		wp_safe_redirect( $location, $status );
		exit;
	}
endif;
