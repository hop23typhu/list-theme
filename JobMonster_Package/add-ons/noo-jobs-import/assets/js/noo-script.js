
// jQuery(document).ready(function($) {
	
// 	// $('#noo_form').on('click', '.noo_import', function(event) {
// 	// 	event.stopPropagation();
// 	// 	event.preventDefault();
// 	// 	console.log('sds');
	
// 	// });
// 	$('#noo_form').noo_upload_file({ 
// 		id: 'upload_xml',
// 		notice: '.help-block'
// 	});

// });

// -- function upload file

(function ( $ ) {
	$.fn.noo_upload_file = function( opt ) {

		// -- set default value 
			var defaults = {

				id : 'upload_file',
				notice : 'notice',
				extensions : 'xml'

			}

		// --
			var option = $.extend( defaults, opt );
			
		this.on('change', 'input[type=file]#'+ option.id, function(event) {
			event.stopPropagation();
			event.preventDefault();
			var $this = this;
			// -- get file extensions
				var ext = $('#' + option.id).val().split('.').pop().toLowerCase();
			
			// covert extensions string to array
				var exts = (option.extensions).split(',');

			// -- processing file
				if ( $.inArray( ext, exts) == -1 )
					$(option.notice).html( 'Invalid extensions!' ).css({
						color: 'red',
					});
				else $(option.notice).html( '' );
				// console.log(this.value);

				// var formData = this.files[0];
				//  var reader = new FileReader();
		  //       reader.readAsText(formData);
		  //       reader.onload = function(e) {
		  //           // browser completed reading file - display it
		  //           alert(e.target.result);
		  //       };
		  		console.log(this);
				var data = {
					action: 'noo_processing',
					file: this.value,
					// contentType: false,
					// enctype: 'multipart/form-data',
					// processData: false,
				};

				jQuery.post(noo_ajax.ajax_url, data, function(response) {
					console.log(response);
				});
				// $.ajax({
				// 	url: noo_ajax.ajax_url,
				// 	'action': 'noo_test',
				// 	'data': formData,
				// 	contentType: false,
				//     enctype: 'multipart/form-data',
				//     processData: false,
				// })
				// .done(function( data ) {
				// 	console.log(data);
				// })
				// .fail(function() {
				// 	console.log("error");
				// })
				
		});


	}

}( jQuery ));


jQuery(document).ready(function($) {
	
	$('.generate_xml').on('click', function(event) {
		event.preventDefault();
		$('.main_import').hide('slow');
		$('.main_generate').show('slow');
		$('h2').html('Generate XML For Indeed');
	});

	$('.main_generate').on('click', '.download_xml', function(event) {
		event.preventDefault();
		// var post_type = $(this).data('post-type');
		var data = $('form.main_generate').serialize();
		$('.download_xml span').removeClass('glyphicon-cloud-download').addClass('noo_reload');
		$.post(noo_ajax.ajax_url, data, function( info, status ) {
			if ( status == 'success' ) {

				$('.download_xml span').removeClass('noo_reload').addClass('glyphicon-ok');
				// console.log(info);
				var request = new XMLHttpRequest();
				request.open("GET", info, true);
				request.onreadystatechange = function(){
				    if (request.readyState == 4) {
				        if (request.status == 200 || request.status == 0) {
				            myXML = request.responseXML;
				        }
				    }
				}
				request.send();
				window.location.href = info; 

			}
		});
	});

	$('#post_type').change(function(event) {
		$('.download_xml').data('post-type', this.value);
	});

});
