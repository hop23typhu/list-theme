<?php
/*
Plugin Name: 	Noo Jobs Import
Plugin URI: 	https://www.nootheme.com
Description: 	Export any post type to a XML file. Edit the exported data WPJobManager, and then re-import it later using Noo Jobs Import.
Version: 		1.0.3
Author: 		Nootheme
Author URI: 	https://www.nootheme.com
*/

class Noo_Import {

	public function __construct() {

		add_action( 'admin_menu', array( $this, 'create_menu_sidebar' ) );

		if ( isset($_GET['page']) &&  ( $_GET['page'] == 'noo-import' || $_GET['page'] == 'noo-wpjobmanager' || $_GET['page'] == 'noo-indeed' ) ) : 
			// -- Load style
				add_action( 'admin_enqueue_scripts', array( $this, 'load_enqueue_style' ) );

			// -- Load script
				add_action( 'admin_enqueue_scripts', array( $this, 'load_enqueue_script' ) );
		endif;
		
		// -- Load event ajax
			add_action( 'wp_ajax_load_xml', array( $this, 'load_xml' ) );
		
	}

	public function create_menu_sidebar() {
		// -- Create parent menu
			add_menu_page( __( 'Noo Jobs Import', 'noo' ), __( 'Noo Jobs Import', 'noo' ), 'manage_options', 'noo-import', array( $this, 'main_content' ), 'dashicons-migrate', 76 );

		// -- Create child menu
			// -- Import WPJobManager
				add_submenu_page( 'noo-import', 'Import jobs from WP Job Manager', 'from WPJobManager', 'manage_options', 'noo-wpjobmanager', array( $this, 'main_wpjobmanager' ) );

			// -- Import Indeed
				add_submenu_page( 'noo-import', 'Import from Indeed.com', 'from Indeed.com', 'manage_options', 'noo-indeed', array( $this, 'main_indeed' ) );

	}

	public function load_enqueue_style() {

		wp_register_style( 'noo-css', plugin_dir_url( __FILE__ ) . 'assets/css/noo-import.css' );
		wp_enqueue_style( 'noo-css' );

	}

	public function load_enqueue_script() {

		wp_register_script( 'noo-script', plugin_dir_url( __FILE__ ) . 'assets/js/noo-script.js', array( 'jquery'), null, true );
		
		wp_enqueue_script( 'noo-script' );

		// -- Load ajax
			wp_localize_script( 'noo-script', 'noo_ajax',
	            array( 
	            	'ajax_url' => admin_url( 'admin-ajax.php' )
	            )
	        );

	}

	public function main_content() {
		?>
		<div class="container">
			<h2 class="page-title"><?php _e( 'Import Jobs', 'noo' ); ?></h2>
			<div class="row">
				<div class="col-md-6">
				    <a href="<?php echo admin_url( 'admin.php' ).'?page=noo-wpjobmanager'; ?>" class="btn btn-primary" ><?php _e( 'From WP Job Manager' , 'noo' ); ?></a>
				    <p class="help-block"><?php _e( 'Import from your old website with WP Job Manager plugin', 'noo' ); ?></p>
				</div>
				<div class="col-md-6">
				    <a href="<?php echo admin_url( 'admin.php' ).'?page=noo-indeed'; ?>" class="btn btn-warning" ><?php _e( 'From Indeed.com' , 'noo' ); ?></a>
				    <p class="help-block"><?php _e( 'Download jobs from Indeed.com site', 'noo' ); ?></p>
				</div>
			</div>
		</div>
		<?php
	}

	public function main_wpjobmanager() {
		$user = get_users( 'orderby=nicename' );
		error_reporting(0);
		?>
		<div class="container">
			<h2 class="page-title"><?php _e( 'Import jobs from WP Job Manager', 'noo' ); ?></h2>
			<?php $this->processing_data(); ?>
			<form method="POST" id="noo_form" enctype="multipart/form-data" class="row">
				<div class="col-md-4">
					<div class="form-group">
					    <label for="upload_xml"><?php _e( 'Upload .XML file' , 'noo' ); ?></label>
					    <input type="file" name="file" id="upload_xml">
					    <p class="help-block"><?php _e( 'If you don\'t have any .xml file please use the Export function on your old WP Job Manager site and export a XML file for Jobs data', 'noo' ); ?></p>
					</div>
				</div>
				<div style="border-left: 1px solid #ddd" class="col-md-8 container">
					<div class="form-group row">
						<label class="col-sm-4 control-label" for="noo_job_author" ><?php _e( 'Job Author' , 'noo' ); ?></label>
						<div class="col-sm-8">
							<select name="author" id="noo_job_author" class="form-control">
								<option value="auto"><?php _e( 'Automatically create new Authors', 'noo' ); ?></option>
								<?php
									foreach ($user as $info) {
										echo "<option value='{$info->ID}'>{$info->display_name}</option>";
									}
								?>
							</select>
							<p class="help-block"><?php _e('You can choose one author to import jobs to or create new authors base on the data from your old site', 'noo'); ?></p>
						</div>
					</div>
					<script type="text/javascript">
					jQuery(document).ready(function($) {
						// Checking value option
							if( $('#noo_job_author').val() != 'auto' ) $('.noo_hide').hide();
							else $('.noo_hide').show();

						// Event
							$('#noo_job_author').change(function(event) {
								if( $(this).val() != 'auto' ) $('.noo_hide').hide();
								else $('.noo_hide').show();
							});

					});
					</script>
					<div class="form-group row noo_hide">
						<label class="col-sm-4 control-label" for="noo_pws"><?php _e( 'Author Password' , 'noo' ); ?></label>
						<div class="col-sm-8">
							<input type="password" style="width: 100%" class="form-control" id="noo_pws" name="pws" value="<?php isset($_POST['pws']) ? $_POST['pws'] : '' ?>" placeholder="<?php _e( 'The password for the new Authors, default is 123456', 'noo' ); ?>" />
						</div>
					</div>
					<div class="form-group row">
					    <div class="col-sm-offset-4 col-sm-8">
					      	<button style="margin-top: 10px;" name="noo_import" type="submit" class="btn btn-primary noo_import"><?php echo __('Submit', 'noo'); ?></button>
					    </div>
					</div>
				</div>
				
			</form>
		</div>
		<?php
	}

	/**
	 * Display main page import
	 *
	 * @package 	Noo Jobs Import
	 * @author 		KENT <tuanlv@vietbrain.com>
	 * @version 	1.1
	 */
	public function main_indeed() {
		$user = get_users( 'orderby=nicename' );
		?>
		<div class="container">
			<h2 class="page-title">
				<?php _e( 'Import jobs from Indeed.com', 'noo' ); ?>
			</h2>

			<?php $this->processing_data(); ?>
				<form method="POST" style="display: none; max-width: 500px" class="main_generate">
					<div class="form-group" style="padding-bottom: 20px">
					    <label for="country" class="col-sm-5 control-label"><?php _e( 'Country', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="country" name="country" placeholder="<?php _e( 'Ex: US', 'noo' ); ?>" />
					    </div>
					</div>

					<div class="form-group" style="padding-bottom: 20px">
					    <label for="state" class="col-sm-5 control-label"><?php _e( 'State', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="state" name="state" placeholder="<?php _e( 'Ex: TX', 'noo' ); ?>" />
					    </div>
					</div>

					<div class="form-group" style="padding-bottom: 20px">
					    <label for="city" class="col-sm-5 control-label"><?php _e( 'City', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="city" name="city" placeholder="<?php _e( 'EX: Austin', 'noo' ); ?>" />
					    </div>
					</div>

					<div class="form-group" style="padding-bottom: 20px">
					    <label for="postalcode" class="col-sm-5 control-label"><?php _e( 'Post Code', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="<?php _e( 'Ex: 78757', 'noo' ); ?>" />
					    </div>
					</div>

					<div class="form-group" style="padding-bottom: 20px">
					    <label for="salary" class="col-sm-5 control-label"><?php _e( 'Salary', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="salary" name="salary" placeholder="<?php _e( 'Ex: $50K per year', 'noo' ); ?>" />
					    </div>
					</div>

					<div class="form-group" style="padding-bottom: 20px">
					    <label for="experience" class="col-sm-5 control-label"><?php _e( 'Experience', 'noo' ); ?></label>
					    <div class="col-sm-6">
					      	<input type="text" class="form-control" id="experience" name="experience" placeholder="<?php _e( 'Ex: 3+ years', 'noo' ); ?>" />
					    </div>
					</div>

					<input type="hidden" name="action" value="load_xml" />
					<input type="hidden" name="post_type" value="noo_job" />
					<button type="submit" style="padding: 3px 10px;margin: 20px 45%;" class="download_xml btn btn-default" data-post-type="noo_job">
						<span class="glyphicon glyphicon-cloud-download" aria-hidden="true"></span>
						<?php _e( ' Download now', 'noo' ); ?>
					</button>
				</form>
			<!-- <div class="row"> -->
				<form method="POST" style="max-width: 650px" class="main_import form-horizontal" enctype="multipart/form-data">
				
					<div class="form-group">
					    <label for="public_id" class="col-sm-3 control-label"><?php _e( 'Publisher ID', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="public_id" name="publisher" placeholder="<?php _e( 'Enter your ID', 'noo' ); ?>" />
					      	<p class="help-block"><?php _e( 'To import jobs from Indeed you will need a publisher account. Obtain it <a href="https://ads.indeed.com/jobroll/signup" title="" target="_blank">here</a>.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
					    <label for="keyword" class="col-sm-3 control-label"><?php _e( 'Keyword', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="keyword" name="q" placeholder="<?php _e( 'Enter your keyword', 'noo' ); ?>" />
					      	<p class="help-block"><?php _e( 'The keyword to search Indeed jobs.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
					    <label for="location" class="col-sm-3 control-label"><?php _e( 'Job Location', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="location" name="l" placeholder="<?php _e( 'Enter a location', 'noo' ); ?>" />
					      	<p class="help-block"><?php _e( 'The location to filter Indeed jobs.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
					    <label for="country" class="col-sm-3 control-label"><?php _e( 'Job Country', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="country" name="co" placeholder="<?php _e( 'Enter a country', 'noo' ); ?>" />
					      	<p class="help-block"><?php _e( 'The country to filter Indeed jobs.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
						<label for="jt" class="col-sm-3 control-label"><?php _e( 'Job Type', 'noo' ); ?></label>
						<div class="col-sm-9">
							<select name="jt" id="jt" class="form-control">
								<option value="fulltime|Full time"><?php _e( 'Full time', 'noo' ); ?></option>
								<option value="parttime|Part time"><?php _e( 'Part time', 'noo' ); ?></option>
								<option value="contract|Contract"><?php _e( 'Contract', 'noo' ); ?></option>
								<option value="internship|Internship"><?php _e( 'Internship', 'noo' ); ?></option>
								<option value="temporary|Temporary"><?php _e( 'Temporary', 'noo' ); ?></option>
							</select>
							<p class="help-block"><?php _e( 'The type of jobs to import.', 'noo' ); ?></p>
						</div>
					</div>
					<hr/><br/>
					<div class="form-group">
						<label for="author" class="col-sm-3 control-label"><?php _e( 'Jobs author', 'noo' ); ?></label>
						<div class="col-sm-9">
							<select name="author" id="author" class="form-control">
								<?php
									foreach ($user as $info) {
										echo "<option value='{$info->ID}'>{$info->display_name}</option>";
									}
								?>
							</select>
							<p class="help-block"><?php _e( 'Select an user for Job Author.', 'noo' ); ?></p>
						</div>
					</div>

					<div class="form-group">
						<label for="job_category" class="col-sm-3 control-label"><?php _e( 'Job Category', 'noo' ); ?></label>
						<div class="col-sm-9">
							<select name="job_category" id="job_category" class="form-control">
								<?php 
									$terms = get_terms( 'job_category', array( 'hide_empty' => false ) );
									if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) :
									    foreach ( $terms as $term ) {
									       	echo '<option value="'. esc_attr( $term->term_id ) .'">' . esc_html( $term->name ) . '</option>';
									        
									    }
									else :
										_e('<option value="none">Please create job type</option>', 'noo');
									endif;
								?>
							</select>
							<p class="help-block"><?php _e( 'Select a category to import jobs to.', 'noo' ); ?></p>
						</div>
					</div>

					<div class="form-group">
					    <label for="start" class="col-sm-3 control-label"><?php _e( 'Start job index', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="start" name="start" placeholder="" />
					      	<p class="help-block"><?php _e( 'The index of job to start import from.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
					    <label for="limit" class="col-sm-3 control-label"><?php _e( 'Max Number of job', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="limit" name="limit" placeholder="" />
					      	<p class="help-block"><?php _e( 'The maximum number of jobs to import.', 'noo' ); ?></p>
					    </div>
					</div>
					<hr/><br/>
					<div class="form-group">
					    <label for="user_agent" class="col-sm-3 control-label"><?php _e( 'User-Agent', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="user_agent" name="useragent" value="<?php echo $_SERVER['HTTP_USER_AGENT'] ?>" />
					      	<p class="help-block"><?php _e( 'Leave this option if you don\'t know what it mean.', 'noo' ); ?></p>
					    </div>
					</div>

					<div class="form-group">
					    <label for="ip" class="col-sm-3 control-label"><?php _e( 'IP', 'noo' ); ?></label>
					    <div class="col-sm-9">
					      	<input type="text" class="form-control" id="ip" name="userip" value="<?php echo $_SERVER['REMOTE_ADDR'] ?>" />
					      	<p class="help-block"><?php _e( 'Leave this option if you don\'t know what it mean.', 'noo' ); ?></p>
					    </div>
					</div>
					<button style="margin-left: 133px;" type="submit" name="import_indeed" class="col-md-offset-3 btn btn-success"><?php _e( 'Import Job', 'noo' ); ?></button>

				</form>
			<!-- </div> -->
		</div>
		<?php

	}

	/**
	 * Function process data when submit
	 *
	 * @package 	Noo Jobs Import
	 * @author 		KENT <tuanlv@vietbrain.com>
	 * @version 	1.1
	 */
	public function processing_data() {
		$i = 0;
		if ( isset( $_POST['noo_import'] ) ) :
			$upload_dir = wp_upload_dir( );
			$file = $upload_dir['path'] . '/' . basename($_FILES['file']['name']);
			if( move_uploaded_file($_FILES['file']['tmp_name'], $file) ) :

				$xml = simplexml_load_file( $file, 'SimpleXMLElement', LIBXML_NOCDATA) or die("Error: Cannot read file");
				
				foreach($xml->channel->item as $item) :

					$post_item['post_title']   = (string)$item->title; 
					$post_item['post_type']    = esc_attr( 'noo_job' ); 
					$post_item['post_status']  = esc_attr( 'publish' ); 
					$post_item['post_content'] = (string)$item->children( 'content', true )->encoded;
					$post_item['job_category'] = array();
					$post_item['job_location'] = array();
					$post_item['job_type']     = array();

				  	// --- Process Category data
				  		if ( !empty( $item->category ) ) :
				  			$i_cat = 0;
				  			$item_category = esc_attr( $item->category );
					  		foreach ($item_category as $category) :
					  			if ( $category->attributes()->domain == 'job_listing_category' ) :

					  				$cat = (string)$item->category[$i_cat];
						  			$job_cat = get_term_by( 'name', $cat, 'job_category' );
						  			
						  			if ( $job_cat ) :

						  				$post_item['job_category'] = array_merge( $post_item['job_category'], (array)$job_cat->term_id);
						  			
						  			else :

						  				$cat_id = wp_insert_term( $cat, 'job_category', array() );
						  				$post_item['job_category'] = array_merge( $post_item['job_category'], (array)$cat_id);

						  			endif;
						  		elseif ( $category->attributes()->domain == 'job_listing_type' ) :

					  				$cat = (string)$item->category[$i_cat];
						  			$job_cat = get_term_by( 'name', $cat, 'job_type' );
						  			
						  			if ( $job_cat ) :

						  				$post_item['job_type'] = array_merge( $post_item['job_type'], (array)$job_cat->term_id);
						  			
						  			else :

						  				$cat_id = wp_insert_term( $cat, 'job_type', array() );
						  				$post_item['job_type'] = array_merge( $post_item['job_type'], (array)$cat_id);

						  			endif;
					  			endif;
					  			$i_cat++;
					  		endforeach;

					  	endif;
			  		// -- process meta data
						$xml_meta = $item->children("wp", true)->postmeta;
						foreach ($xml_meta as $key => $value) {
							
							// === Set default

							if( (string)$value->meta_key == '_job_location' ) : // -- location
								$job_location = get_term_by( 'name', (string)$value->meta_value, 'job_location' );
								
								if ( $job_location ) :

				  					$post_item['job_location'] = array_merge( $post_item['job_location'], (array)$job_location->term_id);
					  			
					  			else :

					  				$location_id = wp_insert_term( (string)$value->meta_value, 'job_location', array() );
					  				$post_item['job_location'] = array_merge( $post_item['job_location'], (array)$location_id);
					  			
					  			endif;

							endif; 


							if( (string)$value->meta_key == '_company_name' ) : // -- company

								$job_company = post_exists( (string)$value->meta_value);
								if ( $job_company != 0 ) :

				  					$post_item['job_company'] = $job_company;
					  			
					  			else :

					  				$args_company = array(
										'post_title'  => (string)$value->meta_value,
										'post_type'   => esc_attr( 'noo_company' ),
										'post_status' => esc_attr( 'publish' )
					  				);
					  				$post_item['job_company'] = wp_insert_post( $args_company );
					  			
					  			endif;

							endif; 

							if( (string)$value->meta_key == '_job_expires' ) : // -- expires

								$date = strtotime( (string)$value->meta_value );
								$post_item['_expires'] = $date;

							endif;

							if( (string)$value->meta_key == '_application' ) : // -- application

								$post_item['_application_email'] = (string)$value->meta_value;

							endif;

						}

						// -- process user data
							if ( $_POST['author'] == 'auto' ) :

					  			$xml_user = $item->children("dc", true);

					  			$user_id = get_user_by( 'slug', $xml_user->creator )->ID;

					  			if ( $user_id ) :

					  				$post_item['post_author'] = absint( $user_id );

					  			else :
					  				// -- Create user new
					  					$user_id = wp_create_user( $xml_user->creator, esc_attr( !empty($_POST['pws']) ? $_POST['pws'] : '123456' ) );
					  				
					  				// -- Update info user new
					  					wp_update_user( array( 'ID' => $user_id, 'role' => 'employer' ) );
					  					update_user_meta( $user_id, 'employer_company', $post_item['job_company'] );
					  				
					  				$post_item['post_author'] = absint( $user_id );

					  			endif;

					  		else :

					  			$post_item['post_author'] = absint( $_POST['author'] );
							
					  		endif;
					if ( $this->create_post( $post_item ) ) $i++;
			  		
	  			endforeach;
	  			$this->notice( $i );
			else :

				$this->notice( false, __( 'Not upload file, please try again!', 'noo' ), 'error' );

			endif;

		endif;

		if ( isset( $_POST['import_indeed'] ) ) :
			require 'includes/indeed.php';
			$client = new Indeed($_POST['publisher']);
			$jt = explode('|', esc_attr( $_POST['jt'] ) );
			$params = array(
				"q"         => esc_attr( $_POST['q'] ),
				"l"         => esc_attr( $_POST['l'] ),
				"co"        => esc_attr( $_POST['co'] ),
				"jt"        => esc_attr( $jt[0] ),
				"userip"    => esc_attr( $_POST['userip'] ),
				"useragent" => esc_attr( $_POST['useragent'] ),
				"start"     => esc_attr( $_POST['start'] ),
				"limit"     => esc_attr( $_POST['limit'] ),
			);
			$results = $client->search($params);
			foreach ($results['results'] as $info_job) :
				
				$post_item['post_title']   = esc_attr( $info_job['jobtitle'] ); 
				$post_item['post_type']    = esc_attr( 'noo_job' ); 
				$post_item['post_status']  = esc_attr( 'publish' ); 
				$post_item['post_content'] = $this->get_content_indeed( $info_job['url'], 'content' );
				$post_item['job_category'] = array($_POST['job_category']);
				$post_item['job_location'] = array();

			  	$job_type = get_term_by( 'name', $jt[1], 'job_type' );
				  			
	  			if ( $job_type ) :

	  				$post_item['job_type'] = (array)$job_type->term_id;
	  			
	  			else :

	  				$job_type_id = wp_insert_term( $jt[1], 'job_type', array() );
	  				$post_item['job_type'] = (array)$job_type_id;

	  			endif;

			  	$job_location = get_term_by( 'name', $info_job['formattedLocationFull'], 'job_location' );
				
				if ( $job_location ) :

  					$post_item['job_location'] = (array)$job_location->term_id;
	  			
	  			else :

					$location_id               = wp_insert_term( $info_job['formattedLocationFull'], 'job_location', array() );
					$post_item['job_location'] = (array)$location_id;
	  			
	  			endif;

	  			$name_company = $this->get_content_indeed( $info_job['url'], 'company' );
	  			$job_company = post_exists( $name_company );
				if ( $job_company != 0 ) :

  					$post_item['job_company'] = $job_company;
	  			
	  			else :

	  				$args_company = array(
						'post_title'  => esc_attr( $name_company ),
						'post_type'   => esc_attr( 'noo_company' ),
						'post_status' => esc_attr( 'publish ' )
	  				);
	  				$post_item['job_company'] = wp_insert_post( $args_company );
	  			
	  			endif;

	  			$post_item['post_author'] = absint( $_POST['author'] );

	  			if ( $this->create_post( $post_item ) ) $i++;

			endforeach;
			$this->notice( $i );

		endif;

	}

	public function notice( $i = false, $text = 'Saved updated', $status = 'success' ) {

		echo '<div id="message" class="updated notice notice-' . $status . ' is-dismissible below-h2"><p>' . $text . ' ' . $i . __( ' jobs', 'noo' ) . '. <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

	}

	public function get_content_indeed( $url, $v = null ) {

		$page = file_get_contents( $url );
		if ( $v == 'content' )
			preg_match('#<span id="job_summary" class="summary">(.*?)</span>#is', $page, $results);
		if ( $v == 'company' )
			preg_match('#<span class="company">(.*?)</span>#is', $page, $results);
		return trim($results[1]);

	}

	public function create_post( $args = array(), $update_meta = true ){
		if ( !post_exists( $args['post_title'], $args['post_content'] ) ) :
			$id_job = wp_insert_post( $args );
			if ( $id_job && $update_meta ) :
				// update_post_meta( $id_job, '_noo_resume_field_company', get_the_title( $args['job_category'] ) );
				if ( @$args['_expires'] ) update_post_meta( $id_job, '_expires', $args['_expires'] );
				// if ( @$args['_featured'] ) update_post_meta( $id_job, '_featured', ( $args['_featured'] == 1 ) ? 'yes' : 'no' );
				if ( @$args['_application_email'] ) update_post_meta( $id_job, '_application_email', $args['_application_email'] );
				wp_set_post_terms( $id_job, $args['job_category'], 'job_category' );
				wp_set_post_terms( $id_job, $args['job_location'], 'job_location' );
				wp_set_post_terms( $id_job, $args['job_type'], 'job_type' );
				// echo "<div><a target='_blank' href='" . get_post_permalink( $id_job ) . "'>{$args['post_title']}</a></div>";
				return true;
			else :
				return false;
			endif;
		else :
			return false;
		endif;
	}

	public function load_xml() {
		error_reporting(0);
		$args_list = array(

			'orderby'        => 'date',
			'order'          => 'DESC',
			'post_status'    => 'publish',
			'posts_per_page' => -1

		);

		if ( $_POST['post_type'] == 'noo_job' ) :

			$args_list['post_type'] = 'noo_job';

		elseif ( $_POST['post_type'] == 'noo_resume' ) :

			$args_list['post_type'] = 'noo_resume';

		endif;

		$info_post = get_posts( $args_list );

		$xml = new DOMDocument("1.0");
		$root = $xml->createElement("source");
		$xml->appendChild($root);

		// -- Publisher
			$publisher         = $xml->createElement("publisher");
			$publisher_content = $xml->createTextNode( get_bloginfo( 'name' ) );
			$publisher->appendChild( $publisher_content );
			$root->appendChild($publisher);

		// -- Publisherurl
			$publisherurl         = $xml->createElement("publisherurl");
			$publisherurl_content = $xml->createTextNode( get_bloginfo( 'url' ) );
			$publisherurl->appendChild( $publisherurl_content );
			$root->appendChild($publisherurl);
			

			foreach ($info_post as $item) :
				setup_postdata( $item ); 
				$job = $xml->createElement("job");
				$root->appendChild($job);

					// -- title
						$title         = $xml->createElement("title");
						$title_content = $xml->createCDATASection( get_the_title( $item->ID ) );
						$title->appendChild( $title_content );
						$job->appendChild($title);

					// -- date
						$date         = $xml->createElement("date");
						$date_content = $xml->createCDATASection( get_the_date( 'D, j M Y g:i:s', $item->ID ) .' GMT' );
						$date->appendChild( $date_content );
						$job->appendChild($date);

					// -- date
						$url         = $xml->createElement("url");
						$url_content = $xml->createCDATASection( get_the_permalink( $item->ID ) );
						$url->appendChild( $url_content );
						$job->appendChild($url);

					// -- company
						$company         = $xml->createElement("company");
						$id_company      = $this->get_info_author( $item->post_author, 'employer_company' );
						$company_content = $xml->createCDATASection( get_the_title($id_company) );
						$company->appendChild( $company_content );
						$job->appendChild($company);

					// -- city
						$city         = $xml->createElement("city");
						$city_content = $xml->createCDATASection( $_POST['city'] );
						$city->appendChild( $city_content );
						$job->appendChild($city);

					// -- state
						$state         = $xml->createElement("state");
						$state_content = $xml->createCDATASection( $_POST['state'] );
						$state->appendChild( $state_content );
						$job->appendChild($state);

					// -- country
						$country         = $xml->createElement("country");
						$country_content = $xml->createCDATASection( $_POST['country'] );
						$country->appendChild( $country_content );
						$job->appendChild($country);

					// -- postalcode
						$postalcode         = $xml->createElement("postalcode");
						$postalcode_content = $xml->createCDATASection( $_POST['postalcode'] );
						$postalcode->appendChild( $postalcode_content );
						$job->appendChild($postalcode);

					// -- description
						$description         = $xml->createElement("description");
						$description_content = $xml->createCDATASection( strip_tags(get_the_content()) );
						$description->appendChild( $description_content );
						$job->appendChild($description);

					// -- salary
						$salary         = $xml->createElement("salary");
						$salary_content = $xml->createCDATASection( $_POST['salary'] );
						$salary->appendChild( $salary_content );
						$job->appendChild($salary);

					// -- jobtype
						$jobtype         = $xml->createElement("jobtype");
						$jobtype_list    = wp_get_post_terms($item->ID, 'job_type');
						// print_r($jobtype_list);
						$jobtype_content = $xml->createCDATASection( str_replace( '-', '', $jobtype_list[0]->slug ) );
						$jobtype->appendChild( $jobtype_content );
						$job->appendChild($jobtype);

					// -- experience
						$experience         = $xml->createElement("experience");
						$experience_content = $xml->createCDATASection( $_POST['experience'] );
						$experience->appendChild( $experience_content );
						$job->appendChild($experience);

				// $content_xml .= "</job>\n";

			endforeach;
			// $root->appendChild($job);
			// print $xml->saveXML();
			$upload_dir = wp_upload_dir();
			$time = time();
			$file = $upload_dir['path'] . '/' . basename($_POST['post_type'] . "_{$time}.xml" );
			$file_redirect = $upload_dir['url'] . '/' . basename($_POST['post_type'] . "_{$time}.xml" );
			$xml->save($file) or die("Error");
			// header('Content-type: application/xml');
			// header('Content-Disposition: attachment; filename='.$file_redirect);
			echo $file_redirect;
		wp_reset_postdata();
		wp_die();

	}

	public function get_info_author( $user_id, $key ) {

		return get_user_meta( $user_id, $key, true ); 

	}

}

new Noo_Import();