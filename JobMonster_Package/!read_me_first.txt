Thank you for choosing our JobMonster theme!
Below you will find 3 simple steps that will help you setup our theme to look like our demo site.
For more detailed information, please see our online document:
http://support.nootheme.com/documentation/jobmonster/

If you have any trouble, you can post a topic in our support forum anytime.
http://support.nootheme.com/forums/forum/product-support/jobmonster-wordpress/

============== 1. Setup Auto Update ===============

This theme come with build-in auto update function, which mean that you will get notice and can install updated version whenever it's available. No need to manually download from ThemeForest or using another plugin.
You only need to input up the purchase code that come with every purchase made on ThemeForest.
Login to your Dashboard and go to Noo Settings to add your purchase code and you done.
You can see this image if you don't know how to get your purchase code.
http://support.nootheme.com/wp-content/uploads/2015/07/HowToGetPurchaseCode.png


============ 2. Import Demo content =============

You can import the dummy data if you want a quick way to setup your site. Just go to Noo Settings on your dashboard and switch to Import Demo tab. You will see your the demo data available and can import with one click.
This import function will automatically setup your site with the full menu and some basic settings so that you have a similar site like our demo.

============ 3. Add-ons =============

We created some add-ons to enhance the function of this theme. You can find them inside folder /add-ons/.

1. noo-indeed-integration.zip: help you backfill the Indeed jobs on your site. You can also generate a XML file for importing your jobs to Indeed.com
2. noo-jobs-import.zip: this plugin help you import jobs from various source, such as the popular WP Job Manager plugins or from Indeed.com site. This also include support for the WP All Import plugin so that you can import jobs from other sources.
3. [BETA] jobmonster-um-integration.zip: this plugin help integrate JobMonster theme with the plugin Ultimate Member. You can use some of the Ultimate Member's in our theme like: candidate profile or company page. This plugin is currently in Beta state so use it with caution.
