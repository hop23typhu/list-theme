function submit(url, strData, funcObj) {
    $.ajax({
        type: "POST",
        url: url,
        data: strData,
        success: funcObj
    });
    return false;
}

function appendFormDataString(dataString) {
    for(var t=0;t<myEditors.length;t++) {
        document.getElementById(myEditors[t]).value = $('#' + myEditors[t]).val().toString().replaceAll("'", "&#39;");
     }
    var name = "";
    var listInput = document.getElementsByTagName("input");
    for (var i in listInput) {
        obj = listInput[i]
        if (obj.type == "text" || obj.type == "radio" || obj.type == "checkbox") {
            name = obj.getAttribute("name");
            if (name != null && name != "" && name.indexOf("_") < 0 && obj.value != null) {
                dataString += "," + name + ":'" + encodeURIComponent(obj.value.toString().replaceAll("'", "&#39;")) + "'";
            }
        }
    }

    listInput = document.getElementsByTagName("textarea");
    for (var j in listInput) {
        obj = listInput[j]
        if (obj.name != null) {
            name = obj.name;
            if (name != null && name != "" && name.indexOf("_") < 0 && obj.value != null) {
                dataString += "," + name + ":'" + encodeURIComponent(obj.value.toString().replaceAll("'", "&#39;")) + "'";
            }
        }
        
    }

    listInput = document.getElementsByTagName("select");
    for (var k in listInput) {
        obj = listInput[k]
        if (obj.name != null) {
            name = obj.name;
            if (name != null && name != "" && name.indexOf("_") < 0 && obj.value != null) {

                dataString += "," + name + ":'" + encodeURIComponent(obj.value.toString().replaceAll("'", "&#39;")) + "'";
            }
        }

    }
    dataString += "}";
    return dataString;
}