<?php
/*
Template Name: News page
*/

get_header();
?>
<section class="main-video mg-bt-50">
	<?php the_post_thumbnail(); ?>
</section>
<section id="main-body">
	<div class="container news-home news-category">
		<div class="row">
			<div class="col-lg-12">
				<h1><?php echo __('News &amp; events', $themename); ?></h1>
			</div>
			<div class="clear"></div>
			<?php
			$args = array (
				'post_type'           => 'post',
				'post_status'         => 'publish',
				'paged'               => $paged,
				'posts_per_page'      => 6,
				'ignore_sticky_posts' => 1
			);

			$wp_query = null;
			$wp_query = new WP_Query($args);
			?>
			<?php if($wp_query->have_posts()): ?>
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post();?>
					<?php $news_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<article class="col-md-6">
					<?php if($news_img): ?>
					<figure>
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('news-thumb'); ?>
						</a>
					</figure>
					<?php endif; ?>
					<div class="row">
						<div class="col-md-2">
							<figcaption>
								<span><?php echo mysql2date('j', $post->post_date); ?></span>
								<br>
								<?php echo mysql2date('M', $post->post_date); ?>
							</figcaption>
						</div>
						<div class="col-md-10">
							<h4>
								<a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
							</h4>
							<?php the_excerpt(); ?>
						</div>
					</div>
					<a href="<?php echo the_permalink(); ?>" class="more pull-right"><?php echo __('Read More', $themename); ?> <i class="fa fa-long-arrow-right"></i></a>
				</article>
				<?php endwhile; ?>
			<?php endif; ?>
			
			<div class="clear"></div>
			<div class="col-md-12 text-right">
				<nav>
					<?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
				</nav>
			</div>
			
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>