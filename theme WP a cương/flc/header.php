<?php
global $themename;
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package deeds
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/images/picture/website/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper">
		<header id="header">
			<div class="top-bar">
				<?php
				$facebook_url = print_option('facebook-url');
				$youtube_url  = print_option('youtube-url');
				$google_url   = print_option('google-plus-url');
				$twitter_url  = print_option('twitter-url');
				$email_url    = print_option('contact-email');
				$hotline      = print_option('contact-phone');
				?>
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?php
							if($hotline) {
								echo '<span><i class="fa fa-phone"></i> <strong>HOTLINE</strong>: '.$hotline.'</span>';
							}
							if($email_url) {
								echo '<a class="fb-link" href="mailto:'.$email_url.'"><i class="fa fa-envelope"></i></a>';
							}
							if($youtube_url) {
								echo '<a class="fb-link" href="'.$youtube_url.'" target="_blank"><i class="fa fa-youtube"></i></a>';
							}if($facebook_url) {
								echo '<a class="fb-link" href="'.$facebook_url.'" target="_blank"><i class="fa fa-facebook"></i></a>';
							}
							if($google_url) {
								echo '<a class="fb-link" href="'.$google_url.'" target="_blank"><i class="fa fa-google-plus"></i></a>';
							}if($twitter_url) {
								echo '<a class="fb-link" href="'.$twitter_url.'" target="_blank"><i class="fa fa-twitter"></i></a>';
							}
							?>
			        		<?php qtranxf_generateLanguageSelectCode('image'); ?>
						</div>
					</div>
			    </div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>

						<nav id="main-nav">
							<?php
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) {
								wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'ul-top-nav', 'menu_class' => 'inline-list sf-menu', 'theme_location' => 'top-menu' ) );
							} else {
							?>
					        <ul class="inline-list sf-menu">
					        	<li class=""><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo __( 'Home', $themename ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
							</ul>
					        <?php } ?>

					        <a class="lines-button x mobile-menu nav-is-visible" href="#">
								<span class="lines"></span>
							</a>
						</nav>
					</div>
				</div>
			</div>
		</header>
		<div class="nav-mask"></div>
		<div class="nav-wrap">
		    <div class="nav-head">
		        <i class="fa fa-phone"></i> +84 (0) 3787 88888
		        <a href="#" class="nav-close"><i class="fa fa-times"></i></a>
		    </div>
		    <div class="nav-content">
		        <a href="" class="nav-logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>
		        <?php
		        if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) {
		            wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'ul-top-nav', 'menu_class' => 'Menu-mobiles', 'theme_location' => 'top-menu' ) );
		        } else {
		        ?>
		        <ul class="Menu-mobiles">
		            <?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
		        </ul>
		        <?php } ?>
		        <a class="nav-social" href="<?php print_option('facebook-url'); ?>" target="_blank"><em class="fa fa-facebook-square" title="Facebook">&nbsp;</em></a>
		        <div class="clear"></div>
		    </div>
		</div>