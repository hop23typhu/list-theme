<?php get_header(); ?>

		<?php
			$args = array (
				'post_type' => 'slider',
				'post_status' => 'publish',
				'posts_per_page' => -1,
				'caller_get_posts'=> 1
			);
			$wp_query = null;
			$wp_query = new WP_Query($args);
		?>
		<?php if ( $wp_query->have_posts() ) : ?>
		<section class="main-slider">
			<?php while ($wp_query->have_posts()) : $wp_query->the_post();?>
			<?php $slider_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
			<?php if($slider_img): ?>
			<div class="item">
				<img src="<?php echo $slider_img; ?>" alt="<?php the_title(); ?>">
			</div>
			<?php endif; ?>
			<?php endwhile; ?>
		</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>

		<section class="main-video">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video.jpg" alt="">
		</section>
		
		<section id="main-body">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="feature-box">
							<div class="row">
								<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/src/fea.jpg" alt="" class="col-sm-5">
								<article class="col-sm-7">
									<h1><span>Perfect Choices</span> Flc samson beach & golf resort</h1>
									<p>FLC Samson Beach & Golf Resort, covering 450 hectares, is the largest golf course - resort - hotel complex in the central region of Vietnam with total investment of VND5.5 trillion ($260 million)</p>
								</article>
							</div>
							
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<?php
				// Service list
				$service_arg = array(
					'hide_empty' => false,
					'orderby'    => 'id',
					'order'      => 'ASC'
				);
				$services = get_terms('service-category', $service_arg);
			?>

			<?php if(!empty($services)): ?>
			<div class="services-list">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<?php foreach ($services as $key => $value): ?>
							<figure data-scroll="#sv_<?php echo $value->term_id; ?>" <?php if($key == 4) echo 'class="big"'; ?>>
								<a href="#">
									<img src="<?php echo ($key == 4) ? z_taxonomy_image_url($value->term_id, 'service-thumb-big') : z_taxonomy_image_url($value->term_id, 'service-thumb-small') ; ?>" alt="<?php echo $value->name; ?>">
								</a>
								<figcaption>
									<h4><a href="#"><?php echo $value->name; ?></a></h4>
								</figcaption>
							</figure>
							<?php if($key == 1): ?>
								</div>
								<div class="col-sm-4 col-lg-push-4">
							<?php elseif ($key == 3): ?>
								</div>
								<div class="col-md-4 col-lg-pull-4">
							<?php endif; ?>
							<?php endforeach; ?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>
			<div class="hr"></div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>

			<?php //Show services ?>
			<?php if(!empty($services)): ?>
			<?php foreach ($services as $key => $value): ?>

			<?php
			$service_post = get_posts(array(
				'post_type' => 'service',
				'numberposts' => -1,
				'orderby'    => 'id',
				'order'      => 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => 'service-category',
						'field' => 'id',
						'terms' => $value->term_id,
						'include_children' => false
					)
				)
			));
			?>			
			<?php if(!empty($service_post)): ?>
			<div class="service-home" id="sv_<?php echo $value->term_id; ?>">
				<?php $service_logo = get_tax_meta($value->term_id, $shortname.'_service_logo_id'); ?>
				<article class="service-logo">
				<?php if(isset($service_logo['url'])): ?>
					<img src="<?php echo $service_logo['url']; ?>" alt="<?php echo $value->name; ?>">
				<?php else: ?>
					<h1><?php echo $value->name; ?></h1>
				<?php endif; ?>
				</article>
				<div class="service-tab">
					<?php foreach ($service_post as $k => $v): ?>
					<a href="#" <?php echo ($k == 0) ? 'class="active"' : ''; ?> data-tab="#tab-sv-<?php echo $v->ID; ?>"><?php echo $v->post_title; ?></a>
					<?php endforeach; ?>
				</div>				
				<div class="service-content">
				<?php foreach ($service_post as $k => $v): ?>
				<?php
					$service_type   = get_post_meta($v->ID, $shortname.'_service_type', true);
					$service_url    = get_post_meta($v->ID, $shortname.'_service_url', true);
					$service_slider = rwmb_meta($shortname.'_service_slider', 'type=image&size=service-slider', $v->ID);
				?>

				<article id="tab-sv-<?php echo $v->ID; ?>" <?php echo ($k == 0) ? 'class="active"' : ''; ?>>
				<?php if($service_type == 'info'): ?>		
					<?php $service_img = wp_get_attachment_url( get_post_thumbnail_id($v->ID) ); ?>
					<img src="<?php echo $service_img; ?>" alt="<?php echo $v->title; ?>">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-sm-8 col-sm-offset-6 service-intro">
								<div class="">
									<?php echo $v->post_content; ?>
									<a href="<?php echo $service_url; ?>" class="btn"><?php echo __('Visit Website', $themename); ?> <i class="fa fa-long-arrow-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<?php elseif($service_type == 'slider'): ?>
					<?php if(!empty($service_slider)): ?>
					<div class="main-slider">
						<?php foreach ($service_slider as $k_pic => $v_pic): ?>
						<div class="item">
							<?php echo "<img src='{$v_pic['url']}' alt='{$v_pic['alt']}' />"; ?>
						</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>				
				<?php endif; ?>
				</article>
				<?php endforeach; ?>
				</div>
				<div class="hidden">
					<div class="service-hidden">
						<?php echo $value->description; ?>
					</div>
				</div>
			</div>

			<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>

			<!-- end service home -->

			<?php
				$args = array (
					'post_status' => 'publish',
					'posts_per_page' => 3,
					'caller_get_posts'=> 1
				);
				$wp_query = null;
				$wp_query = new WP_Query($args);
			?>

			<?php if ( $wp_query->have_posts() ) : ?>
			
			<div class="news-home">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h1><?php echo __('News & events', $themename); ?></h1>
						</div>
						<div class="clear"></div>
						<?php while ($wp_query->have_posts()) : $wp_query->the_post();?>
						<?php $news_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<article class="col-md-4">
							<?php if($news_img): ?>
							<figure>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $news_img; ?>" alt="<?php the_title(); ?>"></a>
								<figcaption>
									<span><?php echo mysql2date('j', $post->post_date); ?></span>
									<br>
									<?php echo mysql2date('M', $post->post_date); ?>
								</figcaption>
							</figure>
							<?php endif; ?>
	
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="more pull-right"><?php echo __('Read More', $themename); ?> <i class="fa fa-long-arrow-right"></i></a>
						</article>
						<?php endwhile; ?>
						<div class="clear"></div>
						<div class="col-lg-12 text-center">
							<a href="<?php echo get_permalink(29); ?>" class="btn"><?php echo __('More News and Events', $themename); ?> <i class="fa fa-long-arrow-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</section>
		
<?php get_footer(); ?>
