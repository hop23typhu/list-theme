var Shuffle = (function($) {
    'use strict';

    var $grid = $('.port-shuffle'),
    $filterOptions = $('.filter-container'),
    $sizer = $grid.find('.sizer'),
    $func = $('.filter'),
    func = [],
    init = function() {

        setTimeout(function() {
            listen();
            setupFilters();
        }, 100);

        $grid.shuffle({
            itemSelector: '.portfolio-item',
            sizer: $sizer,
            speed: 250,
            easing: 'ease'
        });
    },
    setupFilters = function() {
        $('.check-filter').on('change', function() {
            var $checked = $func.find('input:checked'),
                groups = [];

            // At least one checkbox is checked, clear the array and loop through the checked checkboxes
            // to build an array of strings
            if ($checked.length !== 0) {
                $checked.each(function() {
                    groups.push(this.value);
                });
            }
            func = groups;
            if($func.find('input:checked').length) {
                $grid.shuffle('shuffle', function($el) {
                    return valueInArray($el.data('func'), func);
                });
            } else {
                $grid.shuffle('shuffle', 'all');
            }
        });

        var $btns = $filterOptions.find('.find');
        $btns.on('click', function() {
            var $this = $(this),
                isActive = $this.hasClass('active'),
                group = isActive ? 'all' : $this.data('group');

            // Hide current label, show current label in title
            if (!isActive) {
                $('.filter-container .active').removeClass('active');
            }

            $this.toggleClass('active');

            // Filter elements
            $grid.shuffle('shuffle', group);
            return false;
        });

        $btns = null;
    },
    valueInArray = function(value, arr) {
        return $.inArray(value, arr) !== -1;
    },
    listen = function() {
        var debouncedLayout = $.throttle(300, function() {
            $grid.shuffle('update');
        });

        // Get all images inside shuffle
        $grid.find('img').each(function() {
            var proxyImage;

            // Image already loaded
            if (this.complete && this.naturalWidth !== undefined) {
                return;
            }

            // If none of the checks above matched, simulate loading on detached element.
            proxyImage = new Image();
            $(proxyImage).on('load', function() {
                $(this).off('load');
                debouncedLayout();
            });

            proxyImage.src = this.src;
        });

        // Because this method doesn't seem to be perfect.
        setTimeout(function() {
            debouncedLayout();
        }, 500);
    };

    return {
        init: init
    };
}(jQuery));

(function($, window, document) {
    
	$(function() {
        
        /** Shuffle **/
        imagesLoaded('.port-shuffle', function(){
            Shuffle.init();
        });

        /** Fancybox **/
        $(".fancybox").fancybox({
            maxWidth    : 800,
            maxHeight   : 600,
            fitToView   : false,
            width       : '70%',
            height      : '70%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            helpers: {
                overlay: {
                  locked: false
                }
            }
        });

        /* Navigation menu */

        $('.mobile-menu').click(function(event) {
            $('body').addClass('menu-active');
            $(this).addClass('close');
            return false;
        });

        $('.nav-close').click(function(event) {
            $('body').removeClass('menu-active');
            $(".mobile-menu").removeClass('close');
            return false;
        });

        $('body').click(function(event) {
            //$('.search-box').hide();
            $('body').removeClass('menu-active');
            $(".mobile-menu").removeClass('close');
        });

        $('.nav-wrap').click(function(event) {
            event.stopPropagation();
        });

        $('.content-expand').click(function(event) {
            var elem = $(this);
            elem.parent().next().toggleClass('active');
            elem.children().toggleClass('fa-angle-down');
            return false;
        });
        //expand mentor menu in mobile
        $('.mentor-expand').click(function(event) {
            var elem = $(this);
            elem.children().toggleClass('fa-angle-down');
            $('.mentor-menu').toggleClass('expanded');

        });
        /* End navigation menu */

		// Language switcher
		$('.qtranxs_language_chooser').find('.active').find('a').click(function(event) {
			event.preventDefault();
			$('.qtranxs_language_chooser').toggleClass('open');

			return false;
		});

		// Main slider
		$('.main-slider').owlCarousel({
		    singleItem: true,
		    slideSpeed: 1000,
		    pagination: true,
		    responsiveRefreshRate: 200
		});

		// Tab
		$('.service-home').each(function(index, el) {
			var parent = $(this);
			$(this).find('a').click(function(event) {
				event.preventDefault();
				var elem = $(this);
				var target = elem.data('tab');

				parent.find('.service-tab').find('a').removeClass('active');
				parent.find('.service-content').find('article').removeClass('active');

				elem.addClass('active');
				$(target).addClass('active');

				return false;
			});
		});

		//scoll to
		$('[data-scroll]').click(function(event) {
			var target = $(this).data('scroll');
			if($(target).length)
				$(window).scrollTo($(target), 800, {offset:-20});
			return false;
		});

		$('.service-logo').children().click(function(event) {
			event.preventDefault();
			var elem = $(this);
			var html = elem.parent().parent().find('.hidden').html()

			$('.modal-wrapper').find('.modal-info').html(html);

			$('.modal-overlay, .modal-wrapper').css({'opacity': 1, 'visibility': 'visible'});

			return false;
		});

		$('.modal-close, .modal-overlay').click(function(event) {
			event.preventDefault();
			var elem = $(this);
			$('.modal-overlay, .modal-wrapper').css({'opacity': 0, 'visibility': 'hidden'});
			return false;
		});

        //thumbnail slider
        var sync1 = $(".post-slide");
        var sync2 = $(".post-slide-thumb");
        
        if ( sync1.length){
            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 1000,
                navigation: false,
                pagination: false,
                autoHeight : true,
                transitionStyle : "fade",
                afterAction: syncPosition,
                responsiveRefreshRate: 200
            });
        }

        if ( sync2.length){
            sync2.owlCarousel({
                items: 8,
                navigation: false,
                pagination: false,
                responsiveRefreshRate: 100,
                itemsDesktop      : [1199,8],
                itemsDesktopSmall : [1024,8],
                itemsTablet       : [768,8],
                itemsMobile       : [479,3],
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });
        }

        function syncPosition(el) {
            var current      = this.currentItem;     
            var class_string = el.find(".item").eq(current).attr('class');
            var style        = class_string.substring(5);

            el.find(".owl-buttons").attr('class', 'owl-buttons ' + style);

            $(".post-slide-thumb")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if ($(".post-slide-thumb").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $(".post-slide-thumb").on("click", ".owl-item", function(e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

        /** End slider **/

        $(window).trigger('resize');
	});

    $(window).resize(function(event) {
        
        if($(window).width() < 768 && $('figure.big').length) {
            $('figure.big').height($('[data-scroll]:first').height());
        }
    });

}(window.jQuery, window, document));

