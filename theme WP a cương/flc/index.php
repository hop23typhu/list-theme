<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package deeds
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/images/picture/website/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>
<body>
	<div id="wrapper">
		<header id="header">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>

						<nav id="main-nav">
							<?php
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) {
								wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'ul-top-nav', 'menu_class' => 'inline-list sf-menu', 'theme_location' => 'top-menu' ) );
							} else {
							?>
					        <ul class="inline-list sf-menu">
								<?php if ( is_page() ) $highlight = 'page_item'; else $highlight = 'page_item current_page_item'; ?>
								<li class="<?php echo $highlight; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e( 'Home', $themename ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
							</ul>
					        <?php } ?>
					        <a href="#" class="fb-link"><i class="fa fa-facebook"></i></a>
					        <?php qtranxf_generateLanguageSelectCode('text'); ?>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<section class="main-slider">
			<div class="item">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/slide1.jpg" alt="">
			</div>
			<div class="item">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/slide1.jpg" alt="">
			</div>
		</section>
		<section class="main-video">
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/video.jpg" alt="">
		</section>
		
		<section id="main-body">
			
		</section>

		<footer id="footer">

		</footer>
	</div>	
	<?php wp_footer(); ?>
</body>
</html>
