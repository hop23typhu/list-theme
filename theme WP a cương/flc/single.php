<?php get_header(); ?>
<?php
$page_picture = rwmb_meta($shortname.'_post_banner', 'type=image&size=post_banner', get_the_ID());

?>

<?php if(!empty($page_picture)): ?>
<section class="main-video mg-bt-50">
	<?php foreach ($page_picture as $k_pic => $v_pic): ?>
		<?php echo "<img src='{$v_pic['url']}' alt='{$v_pic['alt']}' />"; ?>
	<?php endforeach; ?>
</section>
<?php endif; ?>
<section id="main-body">
	<div class="container news-home news-category news-detail">
		<div class="row">
			<?php if (have_posts()): ?>
			<div class="col-lg-12">
				<h1><?php echo __('News & events', $themename); ?></h1>
			</div>
			<div class="clear"></div>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-md-1">
				<div class="figcaption">
					<span><?php echo mysql2date('j', $post->post_date); ?></span>
					<br>
					<?php echo mysql2date('M', $post->post_date); ?>
				</div>
			</div>
			<div class="col-md-7">
				<div class="row">
					<div class="col-md-12">
					<?php $slide_pics   = rwmb_meta($shortname.'_gallery_images', 'type=image&size=post-slider', get_the_ID()); ?>
					<?php $slide_thumbs = rwmb_meta($shortname.'_gallery_images', 'type=image&size=slider-small', get_the_ID()); ?>
					<?php if(!empty($slide_pics)): ?>
						<div class="post-slide">
							<?php foreach ($slide_pics as $k => $v): ?>
							<div class="item">
								<?php echo "<img src='{$v['url']}' alt='{$v['alt']}' />"; ?>
							</div>
							<?php endforeach; ?>
						</div>
						<div class="post-slide-thumb">
							<?php foreach ($slide_thumbs as $k => $v): ?>
							<div class="item">
								<?php echo "<img src='{$v['url']}' alt='{$v['alt']}' />"; ?>
							</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					</div>
					<div class="clear"></div>
					<div class="col-md-12">
						<h2 class="news-detail-title"><?php the_title(); ?></h2>
						<div class="news-detail-info">
							<ul id="news-detail-info">
								<li><?php the_category(', '); ?></li>
								<li><span><i class="fa fa fa-eye"></i> <?php if(function_exists('the_views')) { the_views(); } ?> <?php echo __('views', $themename); ?></span></li>
								<li>
									<i class="fa fa-share-alt"></i>
									<span class='st_facebook' displayText=''></span>
								</li>
							</ul>
						</div>
						<div class="clear"></div>
						<div class="news-detail-content">
							<?php the_content(); ?>
						</div>
						<div class="clear"></div>
						<div class="news-detail-info">
							<h6><?php echo __('Share on', $themename); ?></h6>
							<ul id="news-detail-info">
								<li><span class='st_facebook' displayText=''></span></li>
								<li><span class='st_twitter' displayText=''></li>
								<li><span class='st_googleplus' displayText=''></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; ?>			
			<?php endif; ?>

			<div class="col-md-4">
				<?php get_sidebar(); ?>				
			</div>
			<div class="clear"></div>
		</div>
	</div>
</section>
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "5ba8b240-ad18-4e79-8b1a-1ec256125c98", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<?php get_footer(); ?>