<?php
global $themename;
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package deeds
 */

?>
		<footer id="footer">
			<div class="container text-center">
				<div class="row">
					<div class="col-12">
						<?php
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'footer-menu' ) ) {
								wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'footer-menu', 'menu_class' => 'footer-menu', 'theme_location' => 'footer-menu' ) );
							} else {
							?>
					        <ul class="footer-menu">
					        	<li class=""><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo __( 'Home', $themename ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
							</ul>
					        <?php } ?>
					        <?php
					        $facebook_url = print_option('facebook-url');
							$youtube_url  = print_option('youtube-url');
							$google_url   = print_option('google-plus-url');
							$twitter_url  = print_option('twitter-url');
							$email_url    = print_option('contact-email');
					        if($email_url) {
								echo '<a class="fb-link" href="mailto:'.$email_url.'"><i class="fa fa-envelope"></i></a>';
							}
							if($youtube_url) {
								echo '<a class="fb-link" href="'.$youtube_url.'" target="_blank"><i class="fa fa-youtube"></i></a>';
							}if($facebook_url) {
								echo '<a class="fb-link" href="'.$facebook_url.'" target="_blank"><i class="fa fa-facebook"></i></a>';
							}
							if($google_url) {
								echo '<a class="fb-link" href="'.$google_url.'" target="_blank"><i class="fa fa-google-plus"></i></a>';
							}if($twitter_url) {
								echo '<a class="fb-link" href="'.$twitter_url.'" target="_blank"><i class="fa fa-twitter"></i></a>';
							}
					        ?>
					</div>
					<div class="clear"></div>
					<div class="col-12">
						<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" alt="" class="logo-footer">
						<p class="copyright">
							<?php echo __('©2015 FLC Samsonbeach&golfresort. All rights reserved', $themename); ?>.
						</p>
					</div>
				</div>
			</div>
		</footer>
	</div>
	
	<div class="modal-overlay"></div>
	<div class="modal-wrapper text-center">
		<a href="#" class="modal-close"><i class="fa fa-long-arrow-left"></i> Back</a>
		<div class="modal-info">
		</div>
	</div>
	<?php wp_footer(); ?>
</body>
</html>