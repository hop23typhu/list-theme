<?php
/*
Template Name: Contact page
*/

get_header();
?>
<section class="main-video mg-bt-50">
	<?php the_post_thumbnail(); ?>
</section>
<section id="main-body">
	<div class="container contact-form">
		<div class="row">
			<div class="col-lg-12">
				<h1><?php echo __('Get in touch with us', $themename); ?></h1>
			</div>
		</div>
		<div class="row mg-bt-50">
			<div class="col-md-6">
				<?php echo do_shortcode('[contact-form-7 id="128" title="Contact form 1"]'); ?>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12 bg-maps">
						<h4><?php echo __('Address', $themename); ?></h4>
						<p><?php echo print_option('contact-address'); ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 bg-address">
						<h4><?php echo __('Phone', $themename); ?></h4>
						<p><?php print_option('contact-phone'); ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-1"></div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="contact-route">
					<div id="contact-map"></div>					
				</div>
			</div>
			<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
			<script type="text/javascript">
			    //<!--
			function initialize(){
				var geocoder = new google.maps.Geocoder();

				geocoder.geocode( { 'address': 'Thanh Niên, Quảng Cư, tx. Sầm Sơn, Thanh Hóa, Vietnam'}, function(results, status) {
					var latlng = results[0].geometry.location;
					var opt = {
					    center:latlng,
					    zoom:12,
					    mapTypeId: google.maps.MapTypeId.ROADMAP,
					    disableAutoPan:false,
					    navigationControl:false,
					    mapTypeControl:false,
					    mapTypeControlOptions: {style:google.maps.MapTypeControlStyle.DROPDOWN_MENU},
					   	disableDefaultUI: true
					};

					var map = new google.maps.Map(document.getElementById("contact-map"),opt);

					var marker = new google.maps.Marker({
					    position: results[0].geometry.location,
						title: "Samsonbeach&golfresort.",
						clickable: true,
						map: map
					});

					var infiwindow = new google.maps.InfoWindow({
					    content: '<div class="map-infobox"><img src="../wp-content/themes/flc/img/logo.png" style="width: 200px;" alt="Samsonbeach&golfresort." /><p style="font-size: 12px;">Samsonbeach&golfresort.<br> Samsonbeach&golfresort.<br><br>T. +84 (0) 3787 88888</p></div>'
					});

					google.maps.event.addListener(marker,'mouseover',function(){
						infiwindow.open(map,marker);
					});
				});
			}
		    //-->
	    	initialize();
			</script>
		</div>
	</div>
</section>
<?php get_footer(); ?>