jQuery(function() {
	var wrap_url  = jQuery('#'+d_init_var.d_shortname+'_service_url').parent().parent();
	var wrap_slider = jQuery('#'+d_init_var.d_shortname+'_service_slider_description').parent().parent();

	//hide all post type
	wrap_url.hide();
	wrap_slider.hide();

	change_post_type(jQuery('#'+d_init_var.d_shortname+'_service_type').val());

	//post type select
	jQuery('#'+d_init_var.d_shortname+'_service_type').change(function() {
		var elem  = jQuery(this);
		var value = elem.val();
		console.log(value);

		change_post_type(value);
	});
	function change_post_type(value) {
		switch(value) {
			case 'info':
				wrap_url.show();
				wrap_slider.hide();
			break;

			case 'slider':
				wrap_url.hide();
				wrap_slider.show();
			break;
		}
	}
});