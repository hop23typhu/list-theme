<?php
/** Gallery **/
add_action( 'init', 'gallery_register' );
function gallery_register() {
    global $themename;
    $labels = array(
        'name'               => __('Gallery', $themename),
        'singular_name'      => __('Gallery', $themename),
        'add_new'            => __('Add New', $themename),
        'add_new_item'       => __('Add New Gallery', $themename),
        'edit_item'          => __('Edit Gallery', $themename),
        'new_item'           => __('New Gallery', $themename),
        'view_item'          => __('View Gallery', $themename),
        'search_items'       => __('Search Gallery', $themename),
        'not_found'          => __('No Gallery found', $themename),
        'not_found_in_trash' => __('No Gallery in the trash', $themename),
        'parent_item_colon'  => '',
    );
 
    register_post_type( 'gallery', array(
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title'),
        'menu_icon'         => 'dashicons-images-alt',
        'menu_position'     => 20,
        'rewrite'           => array('slug' => ''),
    ) );
}
?>