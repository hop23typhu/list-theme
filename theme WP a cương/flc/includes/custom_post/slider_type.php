<?php
/** Slider **/
add_action( 'init', 'slider_register' );
function slider_register() {
    global $themename;
    $labels = array(
        'name'               => __('Slider', $themename),
        'singular_name'      => __('Slider', $themename),
        'add_new'            => __('Add New', $themename),
        'add_new_item'       => __('Add New Slider', $themename),
        'edit_item'          => __('Edit Slider', $themename),
        'new_item'           => __('New Slider', $themename),
        'view_item'          => __('View Slider', $themename),
        'search_items'       => __('Search Slider', $themename),
        'not_found'          => __('No Slider found', $themename),
        'not_found_in_trash' => __('No Slider in the trash', $themename),
        'parent_item_colon'  => '',
    );
 
    register_post_type( 'slider', array(
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'thumbnail'),
        'menu_icon'         => 'dashicons-images-alt2',
        'rewrite'           => array('slug' => ''),
        'menu_position'     => 21
    ) );
}