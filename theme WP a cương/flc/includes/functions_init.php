<?php

require_once('custom_post/services_type.php');
require_once('custom_post/slider_type.php');
require_once('custom_post/gallery_type.php');

require_once('meta-box/meta-box.php');
require_once('functions_meta_box.php');

require_once('Tax-meta-class/Tax-meta-class.php');
require_once('functions_meta_tax.php');

require_once('custom_widgets/contact.php');
require_once('custom_widgets/gallery.php');

/*require_once('custom_post/testimonials_type.php');
require_once('custom_post/portfolio_type.php');
require_once('custom_post/team_type.php');
require_once('custom_post/clients_type.php');
require_once('custom_widgets/contact.php');
require_once('meta-box/meta-box.php');
require_once('functions_meta_box.php');*/

add_action( 'admin_enqueue_scripts', 'd_admin_script' );

function d_admin_script() {
	global $shortname;
	wp_enqueue_script('d-admin-scripts', get_template_directory_uri().'/d_options/js/d-admin.js', array(), '', true);
	wp_localize_script('d-admin-scripts', 'd_init_var', array(
		'd_shortname' => $shortname
	));
}