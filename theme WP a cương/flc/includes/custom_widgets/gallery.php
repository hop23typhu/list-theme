<?php
function register_d_gallery_widget() {
    register_widget( 'd_gallery' );
}
add_action( 'widgets_init', 'register_d_gallery_widget' );

class d_gallery extends WP_Widget {
	private $themename = 'FLC theme';
	private $shortname = 'deeds';

	public function __construct() {
		parent::__construct(
			'd_gallery_widget',
			__('Gallery widget', $this->themename),
			array( 'description' => __( 'Display gallery pictures', $this->themename ) )
		);
	}

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		$gallery = get_posts(array(
			'post_type' => 'gallery',
			'numberposts' => -1,
			'orderby'    => 'id',
			'order'      => 'ASC'
		));

		$gallery_pics = array();

		if ( !empty($gallery) ) {
			foreach ($gallery as $key => $value) {
				$galleries = rwmb_meta($this->shortname.'_gallery_images', 'type=image&size=gallery-thumb', $value->ID);
				if(!empty($galleries)) {
					foreach ($galleries as $k => $v) {
						$gallery_pics[] = array(
							'url'      => $v['url'],
							'full_url' => $v['full_url'],
							'alt'      => $v['alt']
						);
					}
				}
			}
		}

		if(!empty($gallery)) {
			$html  = '<ul class="gallery-list">';
			foreach ($gallery_pics as $key => $value) {
				$html .= "<li><a href='{$value['full_url']}' class='fancybox' rel='gallery-widget'><img src='{$value['url']}' alt='{$value['alt']}' /></a></li>";
			}


			$html .= '</ul><div class="clear"></div>';
		}

		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		echo $html;
		echo $args['after_widget'];
	}

 	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Gallery widget', $this->themename );
		}
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $instance;
	}
}
?>