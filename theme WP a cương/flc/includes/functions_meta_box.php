<?php
global $meta_boxes;
$meta_boxes = array();

$prefix = $shortname.'_';

/** Metabox service link **/
$meta_boxes[] = array(
	'id'      => 'service_meta_link',
	'title'   => 'Service information',
	'pages'   => array( 'service' ),
	'context' => 'normal',
	'fields'  => array(
		/*array(
			'name'             => __('Logo pictures', $themename),
			'desc'             => __('The logo will be show on top of service.', $themename),
			'id'               => "{$prefix}service_logo",
			'type'             => 'image_advanced',
			'max_file_uploads' => 1
		),*/
		array(
			'name' => __('Service type', $themename),
			'id'   => "{$prefix}service_type",
			'type' => 'select',
			'options' => array(
				'info'        => 'Information',
				'slider'       => 'Slider'
			),
			'multiple' => false,
			'std'      => 'image',
			'desc'     => __('Choose service type.', $themename),
		),
		
		array(
			'name'  => __('Visit site url', $themename),
			'id'    => $prefix . 'service_url',
			'desc'  => __('Enter the website url of this service.', $themename),
			'clone' => false,
			'type'  => 'text',
			'std'   => 'http://',
		),

		array(
			'name'             => __('Slider pictures', $themename),
			'desc'             => __('The slider will be show on service.', $themename),
			'id'               => "{$prefix}service_slider",
			'type'             => 'image_advanced',
			'max_file_uploads' => 50
		),
	)
);

// Metabox picture wordpress
$meta_boxes[] = array(
	'id'      => 'post_banner',
	'title'   => 'Page picture',
	'pages'   => array( 'post' ),
	'context' => 'normal',
	'fields'  => array(
		array(
			'name'             => __('Pictures', $themename),
			'desc'             => __('The picture will be show on top of page.', $themename),
			'id'               => "{$prefix}post_banner",
			'type'             => 'image_advanced',
			'max_file_uploads' => 1
		)
	)
);

// Metabox gallery wordpress
$meta_boxes[] = array(
	'id'      => 'gallery_images',
	'title'   => 'Images gallery',
	'pages'   => array( 'gallery', 'post', 'page' ),
	'context' => 'normal',
	'fields'  => array(
		array(
			'name'             => __('Pictures', $themename),
			'desc'             => __('The pictures will be show on gallery.', $themename),
			'id'               => "{$prefix}gallery_images",
			'type'             => 'image_advanced',
			'max_file_uploads' => 50
		)
	)
);

function deeds_register_meta_boxes() {
	global $meta_boxes;

	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
add_action( 'admin_init', 'deeds_register_meta_boxes' );
?>
