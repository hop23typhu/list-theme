<?php
/*
Template Name: About page
*/

get_header();
?>
<section class="main-video mg-bt-50">
	<?php the_post_thumbnail(); ?>
</section>
<section id="main-body">
	<div class="container">
		<div class="row">
		<?php if (have_posts()): ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-lg-12">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="clear"></div>
			<div class="col-lg-8 col-lg-offset-2 page-content">
				<?php $slide_pics   = rwmb_meta($shortname.'_gallery_images', 'type=image&size=post-slider', get_the_ID()); ?>
				<?php $slide_thumbs = rwmb_meta($shortname.'_gallery_images', 'type=image&size=slider-small', get_the_ID()); ?>
				<?php if(!empty($slide_pics)): ?>
					<div class="post-slide">
						<?php foreach ($slide_pics as $k => $v): ?>
						<div class="item">
							<?php echo "<img src='{$v['url']}' alt='{$v['alt']}' />"; ?>
						</div>
						<?php endforeach; ?>
					</div>
					<div class="post-slide-thumb">
						<?php foreach ($slide_thumbs as $k => $v): ?>
						<div class="item">
							<?php echo "<img src='{$v['url']}' alt='{$v['alt']}' />"; ?>
						</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<?php the_content(); ?>
			</div>
			<div class="clear"></div>
			<?php endwhile; // End of the loop. ?>
		<?php endif; ?>	
		</div>
	</div>
</section>
<?php get_footer(); ?>