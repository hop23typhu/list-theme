<?php
/*
Template Name: Gallery page
*/

get_header();
?>
<section class="main-video mg-bt-50">
	<?php the_post_thumbnail(); ?>
</section>

<?php
$gallery = get_posts(array(
	'post_type' => 'gallery',
	'numberposts' => -1,
	'orderby'    => 'id',
	'order'      => 'ASC'
));
?>

<section id="main-body">
	<div class="container gallery-container">
		<div class="row">
			<?php if ( !empty($gallery) ) : ?>
			<div class="col-md-12">
				<div class="gallery" id="sv_2">
					<div class="gallery-tab filter-container">
						<?php foreach ($gallery as $key => $value): ?>
						<a href="#" class="filter-btn find" data-group="gal-<?php echo $value->ID; ?>"><?php echo $value->post_title; ?></a>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="col-lg-12">
				<div class="row port-shuffle">
				<?php foreach ($gallery as $key => $value): ?>
					<?php $gallery_pics = rwmb_meta($shortname.'_gallery_images', 'type=image&size=gallery-thumb', $value->ID); ?>
					<?php if(!empty($gallery_pics)): ?>
						<?php foreach ($gallery_pics as $k => $v): ?>
					<figure class="col-md-4 portfolio-item" data-groups='["<?php echo 'gal-'.$value->ID; ?>"]'>
						<div class="wrap-img">
							<a class="fancybox" rel="gallery-<?php echo $value->ID; ?>" href="<?php echo $v['full_url']; ?>" title="<?php echo $v['alt']; ?>">
								<?php echo "<img src='{$v['url']}' alt='{$v['alt']}' />"; ?>
							</a>
						</div>
					</figure>
						<?php endforeach; ?>
					<?php endif; ?>
				<?php endforeach; ?>
					<div class="sizer col-md-4"></div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="clear"></div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>