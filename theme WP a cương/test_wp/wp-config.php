<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpt');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A^k)vN5kyoy|cHcSj!wgbzB %q9d;9[<{]%DJWxX:I3G2oS]@17V$1RrL%rkI;xP');
define('SECURE_AUTH_KEY',  'dA<}fCEag;i@bAi`<3hA2-:&P<snp*QJya*n}Ecbf1}V<P8f(LU_,1cmBBCH=`o;');
define('LOGGED_IN_KEY',    'w-^bg-e8ZHe,n][N~7|=l;u:[]$R:lv1zGQI&m`+2f$W[PL{YVmXFOn@tUSc!C2t');
define('NONCE_KEY',        'YXm7A( L{!@0X[j-=RY>PoA]r B7[yg@O]!e@*Es2#Aq7{0rOVIzS6h}/xn*[m~.');
define('AUTH_SALT',        'V]H0,xn$PPVmF7hD(ZFL-&x1E!_CUC1QYz!s/hwMd7y[Kt]4g}|*6(}V_`[r%QEK');
define('SECURE_AUTH_SALT', 'Fkv0HI9Q!NxyUf4CGt!Y5G?/{eJoa+dwcsN?HRJ~!H}UAH4t6DgU P V6fE_A53[');
define('LOGGED_IN_SALT',   '0~h`hTg5Hq%Ab6zv4Uc|19D:3P@,Z[cy&pmk5fvu8dAj6Ry-)Q!amYL=wu80zp%a');
define('NONCE_SALT',       'e4bAe^cfs.Pq4ZE/Uo3(&gWQ-MGMX+=6hf3P/:VeL:p5[x;d*w2jj:j=5=gTW7.r');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'test_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
