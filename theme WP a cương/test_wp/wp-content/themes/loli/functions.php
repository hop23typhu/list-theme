<?php
/**
 * @package WordPress
 * @subpackage Lolipopy
 * @since Lolipopy 1.0
 */

add_action( 'after_setup_theme', 'lolipopy_start_setup' );

if ( ! function_exists( 'lolipopy_start_setup' ) ) {
	function lolipopy_start_setup() {
		global $themename, $shortname, $options, $theme_path, $theme_uri;
		$themename  = 'Lolipopy';
		$shortname  = 'loli';
		
		$theme_path = get_template_directory();
		$theme_uri  = get_template_directory_uri();
		
		require_once( $theme_path . '/includes/install_plugins.php' );
		require_once( $theme_path . '/d_options/d-display.php' );
		require_once( $theme_path . '/d_options/d-custom.php' );
		require_once( $theme_path . '/d_options/inc/mailchimp/d-mailchimp.php' );
		require_once( $theme_path . '/includes/functions_frontend.php' );
		require_once( $theme_path . '/includes/functions_init.php' );
		require_once( $theme_path . '/includes/functions_styles.php' );
		require_once( $theme_path . '/includes/functions_shortcodes.php' );
	}
}

