<?php
/*
Template Name: Blog page
*/

get_header();
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
<section class="body">
<?php
	$cols        = (print_option('layout-type') == 'with-sidebar') ? 9 : 12;
	$sidebar_pos = print_option('layout-sidebar-position') ? print_option('layout-sidebar-position') : 'right-sidebar';
	$push_class = '';
	$pull_class = '';
	if($sidebar_pos == 'left-sidebar' && $cols == 9) {
		$push_class = 'push-3';
		$pull_class = 'pull-9';
	}
?>
	<div class="row">
		<?php
		$args = array (
			'post_type'           => 'post',
			'post_status'         => 'publish',
			'paged'               => $paged,
			'posts_per_page'      => get_option('posts_per_page'),
			'ignore_sticky_posts' => 1
		);
		$temp     = $wp_query;
		$wp_query = null;
		$wp_query = new WP_Query($args);
		?>
		<?php if ( $wp_query->have_posts() ) : ?>
		<section class="large-<?php echo $cols; ?> columns <?php echo $push_class; ?>">
			<?php while ($wp_query->have_posts()) : $wp_query->the_post();?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php lolipopy_pagination(); ?>
		</section>
		<?php if(print_option('layout-type') == 'with-sidebar'): ?>
		<aside class="sidebar large-3 columns <?php echo $pull_class; ?>" role="complementary">
			<?php get_sidebar(); ?>
		</aside>
		<?php endif; wp_reset_postdata(); $wp_query = null; $wp_query = $temp;?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>