<?php get_header(); ?>
<?php get_template_part('breadcrumbs', 'page'); ?>
	<section class="body">
	<?php
		$cols        = (print_option('layout-type') == 'with-sidebar') ? 9 : 12;
		$sidebar_pos = print_option('layout-sidebar-position') ? print_option('layout-sidebar-position') : 'right-sidebar';
		$push_class = '';
		$pull_class = '';
		if($sidebar_pos == 'left-sidebar' && $cols == 9) {
			$push_class = 'push-3';
			$pull_class = 'pull-9';
		}
	?>
		<div class="row">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();?>
			<section class="large-<?php echo $cols; ?> columns <?php echo $push_class; ?>">
				<?php the_content(); ?>
			</section>

			<?php if(print_option('layout-type') == 'with-sidebar'): ?>
			<aside class="sidebar large-3 columns <?php echo $pull_class; ?>" role="complementary">
				<?php get_sidebar(); ?>
			</aside>
			<?php endif; ?>
			
		<?php endwhile; ?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
		</div>
	</section>
		
<?php get_footer(); ?>