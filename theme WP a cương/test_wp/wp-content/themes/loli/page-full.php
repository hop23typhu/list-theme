<?php
/*
Template Name: Full width page
*/
get_header();
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
	<section class="body">
		<div class="row">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();?>
			<section class="large-12 columns">
				<?php the_content(); ?>
			</section>
			
		<?php endwhile; ?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
		</div>
	</section>
		
<?php get_footer(); ?>