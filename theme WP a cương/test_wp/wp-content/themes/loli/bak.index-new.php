<?php get_header(); ?>
	<section class="body">
	<?php if(print_option('home-subscribe-enabled') == 'on'): ?>
		<div class="row">
			<div class="large-12 columns">
				<div class="block block-subscribe fix">
					<div class="large-4 columns subscribe-description">
						<h2><?php echo print_option('home-subscribe-title'); ?></h2>
						<p><?php echo print_option('home-subscribe-description'); ?></p>
					</div>
					<div class="large-8 columns">
						<form action="" id="main-subscribe-form" method="post">
							<label for="mail"><?php echo esc_html('Email Address:', $themename); ?></label>

							<div class="row collapse">
				                <div class="small-9 large-10 columns">
				                  	<input type="text" placeholder="<?php echo esc_html('Your email', $themename); ?>">
				                </div>
				                <div class="small-3 large-2 columns">
				                  	<button class="button prefix"><?php echo print_option('home-subscribe-submit'); ?></button>
				                </div>
				            </div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if(print_option('home-featured-products-enabled') == 'on'): ?>
	<?php
	$query_args = array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => print_option('home-featured-products-number', 12),
		'orderby'             => 'date',
		'order'               => 'DESC',
		'tax_query'           => array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'id',
				'terms'    => print_option('home-featured-products-cat-exclude'),
				'operator' => 'NOT IN'
			)
		),
		'meta_query' => array(
			array(
				'key' => '_visibility',
				'value' => array('catalog', 'visible'),
				'compare' => 'IN'
			),
			array(
				'key' => '_featured',
				'value' => 'yes'
			)
		)
	);
	$featured_products_query = new WP_Query( $query_args );
	if ( $featured_products_query->have_posts() ) :
	?>
		<div class="row">
			<div class="block block-products featured fix">
				<h2 class="block-title"><?php echo print_option('home-featured-title', esc_html__('Featured products', $themename)); ?></h2>
				<div class="large-12 columns">
					<ul class="small-block-grid-2 large-block-grid-4">
					<?php
					while ( $featured_products_query->have_posts() ) : $featured_products_query->the_post();
					if ( function_exists( 'get_product' ) )
						$product = get_product( $featured_products_query->post->ID );
					else
						$product = new WC_Product( $featured_products_query->post->ID );
					?>
					<li class="">
						<div class="block-content">
							<a href="<?php the_permalink(); ?>" class="block-img">
							<?php
							$src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'shop_catalog');
							?>								
								<img class="lazyOwl" data-src="<?php echo $src[0]; ?>" alt="<?php the_title(); ?>">
							</a>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="product-name"><?php echo text_truncate(get_the_title(), 50); ?></a>
							<?php if(print_option('shop-hide-desc') != 'on'): ?>					
							<p class="product-des"><?php echo text_truncate(get_the_excerpt());?></p>
							<?php endif; ?>
							<?php if(print_option('shop-hide-price') != 'on'): ?>				
							<div class="product-price"><?php echo $product->get_price_html(); ?></div>
							<?php endif; ?>
							<?php woocommerce_show_product_loop_sale_flash(); ?>						
						</div>							
					</li>
					<?php endwhile; ?>
					</ul>
				</div>
				
			</div>
		</div>
	<?php endif; wp_reset_postdata();?>
	<?php endif; ?>

	<?php //Begin recent product ?>

	<?php if(print_option('home-recent-products-enabled') == 'on'): ?>
	<?php
	$query_args = array(
		'post_type'           => 'product',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => print_option('home-recent-products-number', 12),
		'orderby'             => 'date',
		'order'               => 'DESC',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'id',
				'terms'    => print_option('home-recent-products-cat-exclude'),
				'operator' => 'NOT IN'
			)
		),
		'meta_query'          => array(
			array(
				'key'     => '_visibility',
				'value'   => array('catalog', 'visible'),
				'compare' => 'IN'
			)
		)
	);
	$recent_products_query = new WP_Query( $query_args );
	if ( $recent_products_query->have_posts() ) :
	?>
		<div class="row">
			<div class="block block-products recent fix">
				<h2 class="block-title"><?php echo print_option('home-recent-products-title', esc_html__('Recent products', $themename)); ?></h2>
				<div class="carousel" data-item-num="4">
					<?php
					while ( $recent_products_query->have_posts() ) : $recent_products_query->the_post();
					if ( function_exists( 'get_product' ) )
						$product = get_product( $recent_products_query->post->ID );
					else
						$product = new WC_Product( $recent_products_query->post->ID );
					?>
					<article class="large-12 columns">
						<div class="block-content">
							<header class="block-header">
								<a href="<?php the_permalink(); ?>" class="block-img">
								<?php
								$src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'shop_catalog');
								?>								
									<img class="lazyOwl" data-src="<?php echo $src[0]; ?>" alt="<?php the_title(); ?>">
								</a>
								<?php if(print_option('shop-catalog-mode') != 'on'): ?>
								<div class="product-act">
									<?php woocommerce_template_loop_add_to_cart(); ?>
									<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
								</div>
								<?php endif; ?>
							</header>	
							
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo text_truncate(get_the_title(), 40); ?></a>

							<?php if(print_option('shop-hide-desc') != 'on'): ?>
							<p class="product-des"><?php echo text_truncate(get_the_excerpt(), 130);?></p>
							<?php endif; ?>

							<?php if(print_option('shop-hide-price') != 'on'): ?>
							<div class="product-price"><?php echo $product->get_price_html(); ?></div>
							<?php endif; ?>

							<?php woocommerce_show_product_loop_sale_flash(); ?>
						</div>							
					</article>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	<?php endif; wp_reset_postdata();?>
	<?php endif; ?>
	
	<?php //Begin recent blog posts ?>
	<?php if(print_option('home-recent-posts-enabled') == 'on'): ?>
	<?php
	$query_posts_args = array(
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'ignore_sticky_posts' => 1,
		'posts_per_page'      => 3,
		'orderby'             => 'date',
		'order'               => 'DESC',
		'category__not_in '   => print_option('home-recent-posts-categories-exclude')
	);

	$recent_posts_query = new WP_Query( $query_posts_args );
	if ( $recent_posts_query->have_posts() ) :
	?>
		<div class="row">
			<div class="block block-recent-post fix">
				<h2 class="block-title"><?php echo print_option('home-recent-posts-title', esc_html__('Recent posts', $themename)); ?></h2>
				<div class="large-12 columns">
					<div class="row">
						<?php
						while ( $recent_posts_query->have_posts() ) : $recent_posts_query->the_post();
						?>
						<article class="large-4 columns">
							<div class="block-blog-content">
								<a href="<?php the_permalink(); ?>" class="blog-img"><?php the_post_thumbnail('blog-thumb'); ?></a>
								<p class="blog-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo text_truncate(get_the_title(), 50); ?></a>
								</p>
								<span class="blog-meta">
									Posted in <?php the_category(', ', 'single'); ?> by <?php the_author_link(); ?> on <?php echo get_the_date(); ?>
								</span>
								<p class="blog-description">
									<?php echo text_truncate(get_the_excerpt(), 100); ?>
								</p>
								<div class="blog-act fix">
									<a href="<?php echo esc_url( the_permalink() . '#respond' ); ?>" title="<?php echo esc_html__('Leave a reply'); ?>" class="reply-link"><?php echo esc_html__('Leave a reply'); ?></a>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more"><?php echo esc_html__('Read more &rarr;'); ?></a>
								</div>
							</div>
						</article>
						<?php endwhile; ?>
					</div>						
				</div>
			</div>
		</div>	
	<?php endif; wp_reset_postdata();?>
	<?php endif; ?>

	<?php if(print_option('home-about-enabled') == 'on'): ?>
		<div class="row">
			<div class="block block-quote fix">
				<h2 class="block-title"><?php echo print_option('home-about-title', esc_html__('About us', $themename)); ?></h2>
				<div class="large-12 columns">
					<div class="block-quote-content">							
						<blockquote>
							<?php echo print_option('home-about-desc'); ?>
						</blockquote>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	</section>		
<?php get_footer(); ?>