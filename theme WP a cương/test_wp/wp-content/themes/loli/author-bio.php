<?php global $themename; ?>
<article class="author-info">
	<div class="row">
		<div class="author-avatar large-2 columns">
			<?php echo get_avatar(get_the_author_meta('user_email'), 90); ?>
		</div>
		<div class="author-description large-10 columns">
			<h3><?php echo esc_html__('Author: ', $themename).'<a href"'.get_the_author_meta('url').'" title="'.esc_html__('View author\'s website', $themename).'">'.get_the_author_meta('nickname'); ?></a></h3>
			<p>
			<?php echo get_the_author_meta('description'); ?>			
			</p>
			<div class="author-social">
				<?php if(!is_author()): ?>
				<a class="author-post-link" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" rel="author">
					<?php printf(__('View all posts by %s <span class="meta-nav">&rarr;</span>', $themename), get_the_author() ); ?>
				</a>
				<?php endif; ?>
				<ul class="inline-list right">
					<?php
						$facebook_profile = get_the_author_meta( 'facebook_profile' );
						if ( $facebook_profile && $facebook_profile != '' ) {
							echo '<li> <a href="' . esc_url($facebook_profile) . '"><i class="icon-facebook"></i></a></li>';
						}
						
						$twitter_profile = get_the_author_meta( 'twitter_profile' );
						if ( $twitter_profile && $twitter_profile != '' ) {
							echo '<li> <a href="' . esc_url($twitter_profile) . '"><i class="icon-twitter"></i></a></li>';
						}
						
						$google_profile = get_the_author_meta( 'google_profile' );
						if ( $google_profile && $google_profile != '' ) {
							echo '<li><a href="' . esc_url($google_profile) . '"><i class="icon-google-plus"></i></a></li>';
						}
						
						$linkedin_profile = get_the_author_meta( 'linkedin_profile' );
						if ( $linkedin_profile && $linkedin_profile != '' ) {
							echo '<li> <a href="' . esc_url($linkedin_profile) . '"><i class="icon-linkedin"></i></a></li>';
						}

						$tumblr_url = get_the_author_meta( 'tumblr_profile' );
						if ( $tumblr_url && $tumblr_url != '' ) {
							echo '<li><a href="' . esc_url($tumblr_url) . '"><i class="icon-tumblr"></i></a></li>';
						}

					?>
				</ul>
			</div>
		</div>
	</div>		
</article>