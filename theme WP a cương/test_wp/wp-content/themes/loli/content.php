<?php
//get post option

global $shortname;

$post_type   = get_post_meta($post->ID, $shortname.'_post_type', true);
$post_slider = rwmb_meta($shortname.'_post_type_slider', 'type=image&size=blog-thumb');
$post_image  = rwmb_meta($shortname.'_post_type_image', 'type=image&size=blog-thumb');
$post_video  = get_post_meta($post->ID, $shortname.'_post_type_video_url', true);
?>
<article class="row blog-post">
	<div class="large-2 columns post-meta">
		<div>
			<span><?php d_entry_meta(); ?></span>
			<span><?php comments_popup_link( '<span class="leave-reply">' . __( '<i class="icon-comment"></i> 0', 'lolipopy' ) . '</span>', __( '<i class="icon-comment"></i> 1', 'lolipopy' ), __( '<i class="icon-comment"></i> %', 'lolipopy' ) ); ?></span>
		</div>							
	</div>
	<div class="large-10 columns">		
			<?php if($post_type == 'slider'): ?>
			<div class="post-thumbnail-slider owl-carousel">
				<?php foreach ($post_slider as $key => $value): ?>
				<div class="post-thumbnail-item">
				<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
				<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
				</a>
				</div>
				<?php endforeach; ?>
			</div>
			<?php elseif($post_type == 'image'): ?>
			<div class="post-thumbnail-image">
				<?php foreach ($post_image as $key => $value): ?>
				<div class="post-thumbnail-item">
				<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
				<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
				</a>
				</div>
				<?php endforeach; ?>
			</div>
			<?php elseif ($post_type == 'video'): ?>
			<div class="post-thumbnail-video">
				<?php
				if(strpos($post_video, 'youtube'))
					echo embed_youtube($post_video);
				else
					echo embed_vimeo($post_video);
				?>
			</div>
			<?php else: ?>			
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<?php if(is_single()): ?>
					<p>
						<?php the_post_thumbnail(); ?>	
					</p>								
					<?php else: ?>
					<a href="<?php the_permalink(); ?>" class="post-img-wrap">
						<?php the_post_thumbnail('blog-thumb'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
		<div class="post-wrap">
			<h3 class="post-title">
			<?php if(is_single()): ?>
				<?php the_title(); ?>
			<?php else: ?>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<?php endif; ?>
			</h3>
			<div class="posted-in">
				<?php echo esc_html__('Posted in', $themename); ?> <?php echo get_the_category_list( __( ', ', $themename ) ); ?>
			</div>
			<div class="post-excerpt">
				<?php if(is_single()): ?>
					<?php the_content(); ?>
				<?php else: ?>
					<?php echo strip_tags(get_the_excerpt()); ?>
				<?php endif; ?>
			</div>
			<div class="link-pages">
				<?php wp_link_pages(); ?>
			</div>
			<?php echo do_shortcode('[share title="'.__('Share', $themename).'"]'); ?>
			<div class="fixy"></div>
		</div>							
	</div>
</article>