<?php
/*
Template Name: Page with dual sidebar
*/
get_header();
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
	<section class="body">
		<div class="row">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();?>
			<section class="large-6 push-3 columns">
				<?php the_content(); ?>
			</section>
			
			<aside class="sidebar large-3 pull-6 columns" role="complementary">
			<?php get_sidebar('left'); ?>
			</aside>

			<aside class="sidebar large-3 columns" role="complementary">
			<?php get_sidebar(); ?>
			</aside>
			
		<?php endwhile; ?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
		</div>
	</section>
		
<?php get_footer(); ?>