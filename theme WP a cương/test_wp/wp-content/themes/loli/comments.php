<?php
if ( post_password_required() )
	return;
?>

<section id="comments" class="comments-area">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php comments_number( __( '0 comment', 'lolipopy' ), __('1 comment', 'lolipopy'), __('% comments', 'lolipopy' )); ?>
		</h2>

		<ul class="commentlist">
			<?php wp_list_comments( 'avatar_size=50&callback=comment_list_fucntion&type=comment' ); ?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
		<nav id="comment-nav-below" class="navigation" role="navigation">
			<h1 class="assistive-text section-heading"><?php _e( 'Comment navigation', 'lolipopy' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'lolipopy' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'lolipopy' ) ); ?></div>
		</nav>
		<?php endif; ?>

		<?php
		if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="nocomments"><?php _e( 'Comments are closed.' , 'lolipopy' ); ?></p>
		<?php endif; ?>

	<?php endif; ?>

	<?php comment_form(array('comment_notes_after' => '')); ?>

</section>