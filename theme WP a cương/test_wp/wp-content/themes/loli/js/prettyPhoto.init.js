jQuery(document).ready(function($) {

	// Hide review form - it will be in a lightbox
	$('#review_form_wrapper').hide();

	// Lightbox
	if($.fn.prettyPhoto) {
		$("a.zoom").prettyPhoto({
			autoplay_slideshow: false,
	        theme: 'light_square'
		});
		$("a.show_review_form").prettyPhoto({
			autoplay_slideshow: false,
	        theme: 'light_square'
		});
		$("a[rel^='prettyPhoto']").prettyPhoto({
			autoplay_slideshow: false,
	        theme: 'light_square'
		});

		// Open review form lightbox if accessed via anchor
		if( window.location.hash == '#review_form' ) {
			$('a.show_review_form').trigger('click');
		}
	}
});