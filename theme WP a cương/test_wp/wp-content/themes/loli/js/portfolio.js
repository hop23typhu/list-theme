jQuery(function($) {
	var container = $('#portfolio-filter-container');
	
	container.isotope({
		itemSelector:'.portfolio-item'
	});

	
	$('.portfolio-filter-bar a').click(function(){
		var elem     = $(this);
		var selector = elem.attr('data-filter');

		$('.portfolio-filter-bar dd').removeClass('active');
		elem.parent().addClass('active');

		container.isotope({ filter: selector });
		return false;
	});
});