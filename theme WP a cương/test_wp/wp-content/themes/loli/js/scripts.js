jQuery(document).ready(function($) {
	//Nav for mobile
	$('nav#top-nav').meanmenu({
		meanScreenWidth:"768"
	});

	//Superfish menu
	$('.sf-menu').superfish({
		delay: 0,
		speed: 'fast',
		cssArrows:true
	});

	//show add to cart button when hover
	$('.block-products article').live('mouseenter', function(){
		var elem = $(this);		
		elem.find('.product-act').stop().animate({ 'bottom': 0 }, 200);
	});
	$('.block-products article').live('mouseleave', function(){
		var elem = $(this);
		elem.find('.product-act').stop().animate({ 'bottom': '-35px' }, 200);
	});

	//Carousel
	$('.carousel').each(function(){
		var item_num = $(this).data('item-num') ? $(this).data('item-num') : 4;
		if(item_num < 2) {
			$(this).owlCarousel({
				pagination: false,
				navigation: true,
				slideSpeed : 500,
				singleItem:true
			});
		} else {
			$(this).owlCarousel({
				items: item_num,
				lazyLoad: true,
				pagination: false,
				navigation: true,
				slideSpeed: 500
			});
		}
	});

	//post type slider
	$('.post-thumbnail-slider').owlCarousel({
		pagination: false,
		navigation: true,
		slideSpeed : 500,
		singleItem:true,
		autoHeight : true
	});

	//lightbox on portfolio
	jQuery("a[rel^='lightbox']").prettyPhoto({
		social_tools:!1,
		theme:"dark_rounded",
		horizontal_padding:20,
		opacity:.6,
		show_title:false,
		deeplinking:!1
	});


	//lightbox for gallery  
	jQuery(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.gif']").attr('rel', 'prettyPhoto[product-gallery]');

	jQuery(".gallery a[href$='.jpg'], .gallery a[href$='.png'], .gallery a[href$='.gif']").prettyPhoto({
		social_tools:!1,
		horizontal_padding:20,
		opacity:.6,
		show_title:false,
		deeplinking:!1
	});

	//wishlist action 
	var wishlist_clicked;
    $('.yith-wcwl-add-button a').on( 'click', function(){
        var elem = $(this);
        elem.addClass('loading');
        $('#add-items-ajax-loading').hide();
    });

    //wishlist tip 
    $('.yith-wcwl-add-to-wishlist a').each(function(){
    	var elem = $(this);
    	var text;

    	if(elem.parent().find('.feedback').length != 0 )
    		text = elem.parent().find('span.feedback').text();
    	else
    		text = elem.html();

    	elem.tipTip({
	        defaultPosition: "top",
	        maxWidth: 'auto',
	        edgeOffset: 25,
	        content: text
	    });	    	
    });

    //Accordion
    $('.accordion .title').click(function(){
		var elem  = $(this);
		var block = elem.parent().parent();
		var tab   = elem.parent();

    	if(!tab.hasClass('active')) {
    		block.find('section').removeClass('active');
	    	tab.toggleClass('active');
    	} else {
    		tab.toggleClass('active');
	    }

    	return false;
    });

    // Tabs
    $('.dtabs').each(function(){
		var elem          = $(this);
		var current_title = elem.find('.title.active');
    	
    	current_title.next().show();
    });

    $('.dtabs .title').click(function(){
    	var elem = $(this);
    	var target = elem.attr('href');

    	elem.parent().find('.content').hide();
    	elem.parent().find('.title').removeClass('active');
    	elem.addClass('active');
    	$(target).show();
    	return false;
    });

    $('.dtabs-vertical').each(function(){
		var elem          = $(this);
		var current_title = elem.find('.title.active');
    	var target = current_title.attr('href');

    	$(target).show();
    });
    $('.dtabs-vertical .title').click(function(){
    	var elem = $(this);
    	var target = elem.attr('href');

    	elem.parent().next().find('.content').hide();
    	elem.parent().find('.title').removeClass('active');
    	elem.addClass('active');
    	$(target).show();
    	return false;
    });

    // mini cart dropdown
   	$('.top-cart').live('mouseover', function() {  
		if (!$(this).data('init')) {  
			$(this).data('init', true);  
			$(this).hoverIntent(function(){  
				$('.sub-cart').fadeIn(200);
				$('.mini-cart-overview').addClass('active');
			}, function(){  
			  	$('.sub-cart').fadeOut(200);
			  	$('.mini-cart-overview').removeClass('active');
			});  
			$(this).trigger('mouseover');  
		}  
	});
});
//Custom event pretty photo
jQuery(document).keyup(function(e) {
    if (e.keyCode == "27") {
    	jQuery.prettyPhoto.close();
    }
    if (e.keyCode == "37") {
        jQuery.prettyPhoto.changePage('previous');
    }
    if (e.keyCode == "39") {
    	jQuery.prettyPhoto.changePage('next');
    }
});