jQuery(window).load(function() {
    jQuery('#product-carousel').flexslider({
        animation:"slide",
        directionNav:false,
        controlNav:false,
        animationLoop:false,
        slideshow:false,
        itemWidth:70,
        itemMargin: 21,
        asNavFor: '#product-slider'
    });

    jQuery('#product-slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#product-carousel"
    });
});