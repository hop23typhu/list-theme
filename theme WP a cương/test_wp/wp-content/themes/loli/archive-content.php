<?php
//get post option
global $shortname;

$post_type   = get_post_meta($post->ID, $shortname.'_post_type', true);
$post_slider = rwmb_meta($shortname.'_post_type_slider', 'type=image&size=archive-thumb');
$post_image  = rwmb_meta($shortname.'_post_type_image', 'type=image&size=archive-thumb');
$post_video  = get_post_meta($post->ID, $shortname.'_post_type_video_url', true);
?>
<article class="large-6 columns archive-post">
	<div class="block-blog-content">
		<figure>
			<?php if($post_type == 'slider'): ?>
			<div class="post-thumbnail-slider owl-carousel">
				<?php foreach ($post_slider as $key => $value): ?>
				<div class="post-thumbnail-item">
				<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
				<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
				</a>
				</div>
				<?php endforeach; ?>
			</div>
			<?php elseif($post_type == 'image'): ?>
			<div class="post-thumbnail-image">
				<?php foreach ($post_image as $key => $value): ?>
				<div class="post-thumbnail-item">
				<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
				<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
				</a>
				</div>
				<?php endforeach; ?>
			</div>
			<?php elseif ($post_type == 'video'): ?>
			<div class="post-thumbnail-video">
				<?php
				if(strpos($post_video, 'youtube'))
					echo embed_youtube($post_video);
				else
					echo embed_vimeo($post_video);
				?>
			</div>
			<?php else: ?>			
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
					<?php the_post_thumbnail('archive-thumb'); ?>
				<?php endif; ?>
			<?php endif; ?>
		</figure>
		<div class="post-detail">
			<h4 class="blog-title">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo the_title(); ?></a>
			</h4>
			<p class="blog-description">
				<?php echo strip_tags(get_the_excerpt()); ?>
			</p>
			<div class="archive-post-meta fix">
				<div class="reply-link">
					<?php d_entry_meta(); ?> - <?php comments_popup_link( '<span class="leave-reply">' . __( '<i class="icon-comment"></i> 0', 'lolipopy' ) . '</span>', __( '<i class="icon-comment"></i> 1', 'lolipopy' ), __( '<i class="icon-comment"></i> %', 'lolipopy' ) ); ?>
				</div>
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more"><?php echo esc_html__('Read more &rarr;'); ?></a>
			</div>
		</div>
	</div>
</article>