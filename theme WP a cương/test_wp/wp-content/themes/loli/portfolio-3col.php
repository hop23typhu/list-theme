<?php
/*
Template Name: Portfolio 3 Columns
*/

get_header();
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
<section class="body">
	<div class="row">
		<?php
		$args = array (
			'post_type'           => 'portfolio',
			'post_status'         => 'publish',
			'paged'               => $paged,
			'posts_per_page'      => 12,
			'ignore_sticky_posts' => 1
		);
		$temp     = $wp_query;
		$wp_query = null;
		$wp_query = new WP_Query($args);
		?>
		<?php if ( $wp_query->have_posts() ) : ?>
		<section class="large-12 columns portfolio-3col">
			<div class="row">
				<div class="large-12 columns">
					<dl class="sub-nav portfolio-filter-bar">
						<dt><?php echo __('<i class="icon-search"></i> Filter', $themename); ?>:</dt>
						<dd class="active"><a href="#" data-filter="*"><?php echo esc_html__('All', $themename); ?></a></dd>
						<?php
						$portfolio_category = get_terms('portfolio-category');
						if(!empty($portfolio_category)) {
							foreach ($portfolio_category as $key => $value) {
						?>
						<dd><a href="#" data-filter=".<?php echo $value->slug; ?>"><?php echo $value->name; ?></a></dd>
						<?php } }?>
					</dl>
				</div>
			</div>
			<div class="row" id="portfolio-filter-container">
			<?php while ($wp_query->have_posts()) : $wp_query->the_post();?>

			<?php
			$post_term  = get_the_terms($post->ID, 'portfolio-category');
			$post_class = '';
			if($post_term) {
				foreach ($post_term as $key => $value) {
					$post_class .= $value->slug.' ';
				}
			}
			
			$post_type   = get_post_meta($post->ID, $shortname.'_post_type', true);
			$post_slider = rwmb_meta($shortname.'_post_type_slider', 'type=image&size=blog-thumb');
			$post_image  = rwmb_meta($shortname.'_post_type_image', 'type=image&size=blog-thumb');
			$post_video  = get_post_meta($post->ID, $shortname.'_post_type_video_url', true);

			$post_title  = get_post_meta($post->ID, $shortname.'_portfolio_meta_title', true);
			$post_author = get_post_meta($post->ID, $shortname.'_portfolio_meta_author', true);
			$post_url    = get_post_meta($post->ID, $shortname.'_portfolio_meta_url', true);
			?>
				<div class="large-4 columns portfolio-item <?php echo $post_class; ?>">
					<?php if($post_type == 'slider'): ?>
					<div class="post-thumbnail-slider owl-carousel">
						<?php foreach ($post_slider as $key => $value): ?>
						<div class="post-thumbnail-item">
						<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
						</div>
						<?php endforeach; ?>
					</div>
					<?php elseif($post_type == 'image'): ?>
					<div class="post-thumbnail-image">
						<?php foreach ($post_image as $key => $value): ?>
						<div class="post-thumbnail-item">
							<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
							<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
							</a>
						</div>
						<div class="post-thumbnail-overlay">
							<a href="<?php echo $value['full_url']; ?>" rel="lightbox"><i class="icon-search"></i></a>
						</div>
						<?php endforeach; ?>
					</div>
					<?php elseif ($post_type == 'video'): ?>
					<div class="post-thumbnail-video">
						<?php
						if(strpos($post_video, 'youtube'))
							echo embed_youtube($post_video);
						else
							echo embed_vimeo($post_video);
						?>
					</div>
					<?php else: ?>
					<?php if(has_post_thumbnail()) { ?>
					<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url = wp_get_attachment_image_src($thumb_id, '', true);
					?>
					<div class="post-thumbnail-image">
						<a href="<?php echo $thumb_url[0]; ?>" title="<?php the_title(); ?>" rel="lightbox">
						<?php the_post_thumbnail('blog-thumb'); ?>
						</a>
						<div class="post-thumbnail-overlay">
							<a href="<?php echo $thumb_url[0]; ?>" title="<?php the_title(); ?>" rel="lightbox"><i class="icon-search"></i></a>
						</div>
					</div>
					<?php } ?>
					<?php endif; ?>
					<div class="portfo-detail">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						<p><?php echo $post_title; ?></p>
					</div>
				</div>
			<?php endwhile; ?>
			</div>

			<?php lolipopy_pagination(); ?>
		</section>
		<?php wp_reset_postdata(); $wp_query = null; $wp_query = $temp;?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>