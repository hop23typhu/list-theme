<?php $fb_api_key = print_option('facebook-api-key'); ?>		
		<footer>
		<?php if(print_option('facebook-likebox-enabled') == 'on' && is_home()): ?>
			<div class="facebook-likebox" style="background-color: <?php echo print_option('facebook-likebox-bg', '#f2f2f2'); ?>">
				<div class="row">
					<div class="large-12 columns">
					<?php if($fb_api_key != ''): ?>
						<div class="fb-like-box" data-href="<?php echo print_option('facebook-url'); ?>" data-width="<?php echo print_option('facebook-likebox-width', '1200'); ?>" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
					<?php else: ?>
					<blockquote>
						<strong><?php echo esc_html__('Please setting up the api key of you facebook application!', $themename); ?></strong>
					</blockquote>
					<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
			<section class="footer-ex-widgets">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-ex-widgets' ) ) : ?>	
						<?php dynamic_sidebar('footer-ex-widgets'); ?>
					<?php endif; ?>				
					<div class="large-3 columns footer-ex-widget">
						<h3>Contact</h3>
						<div class="widget-ex-content">
							<ul>
								<li><i class="icon-home"></i> 30 Mạc Thị Bưởi, Vĩnh Tuy, HBT, HN</li>
								<li><i class="icon-phone"></i> 0947 661 441</li>
								<li><i class="icon-print"></i> 043 625 2516</li>
								<li><i class="icon-envelope"></i> <a href="mailto:mr.deeds88@gmail.com">mr.deeds88@gmail.com</a></li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-widgets">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-widgets' ) ) : ?>	
						<?php dynamic_sidebar('footer-widgets'); ?>
					<?php endif; ?>				
					<div class="large-3 columns footer-widget">
						<h3>Contact</h3>
						<div class="widget-content">
							<ul>
								<li><i class="icon-home"></i> 30 Mạc Thị Bưởi, Vĩnh Tuy, HBT, HN</li>
								<li><i class="icon-phone"></i> 0947 661 441</li>
								<li><i class="icon-print"></i> 043 625 2516</li>
								<li><i class="icon-envelope"></i> <a href="mailto:mr.deeds88@gmail.com">mr.deeds88@gmail.com</a></li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<section class="footer-copyright">
				<div class="row">
					<div class="small-4 large-4 columns">
						<p class="copyright">
							<?php echo print_option('copyright-text', __('Copyright © 2013 Lolipopy', $themename)); ?>
						</p>
					</div>
					<div class="small-8 large-8 columns">
						<nav id="footer-nav" class="text-right">
							<?php
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'bot-menu' ) ) {
								wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'bot-nav', 'menu_class' => 'inline-list', 'theme_location' => 'bot-menu' ) );
							} else {
							?>
					        <ul class="inline-list">
								<?php if ( is_page() ) $highlight = 'page_item'; else $highlight = 'page_item current_page_item'; ?>
								<li class="<?php echo $highlight; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e( 'Home', $themename ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=1&title_li=&exclude=' ); ?>
							</ul>
					        <?php } ?>
						</nav>
					</div>
				</div>
			</section>			
		</footer>
	</div>
<?php wp_footer(); ?>
<?php if(print_option('facebook-api-key') != ''): ?>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo print_option('facebook-api-key'); ?>";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<?php endif; ?>
<?php if(print_option('google-analytics-code') != ''): ?>
<?php echo print_option('google-analytics-code'); ?>
<?php endif; ?>
</body>
</html>