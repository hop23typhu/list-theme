<?php
global $theme_path, $theme_uri, $woocommerce;
$elements_path = $theme_path . '/includes/elements/';
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <nav class="main-nav">
 *
 * @package WordPress
 * @subpackage Lolipopy
 * @since Lolipopy 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7" lang="vi-VN"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8" lang="vi-VN"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="vi-VN"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" xmlns="http://www.w3.org/1999/xhtml" lang="vi-VN"  ><!--<![endif]-->

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<!--[if lt IE 9]>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/foundation-ie8.css'; ?>">
    <![endif]-->
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php wp_enqueue_script("jquery"); ?>
	<?php
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
	?>
	<?php wp_head(); ?>
</head>
<body <?php body_class('antialiased'); ?>>
	<div id="site-wrapper">
		<div class="top-bar container center">
			<div class="row">
				<div class="large-12 columns">
					<ul class="inline-list top-social-icons left">
						<?php
							require($elements_path.'header/top_social.php');
						?>
					</ul>
					<div class="search-form right">
						<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
							<input type="text" value="" name="s" placeholder="<?php echo esc_html('Search this site...', $themename); ?>"/>
							<input type="hidden" name="post_type" value="post">
							<input type="submit" value="">
						</form>
					</div>
				</div>
			</div>
		</div>
		<header class="top-head">
			<div class="first-head">
				<div class="row">
					<div class="large-12 columns">
						<div class="left">
							<h1 id="top-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo print_option('site-logo') ? '<img src="'.print_option('site-logo').'" alt="">' : print_option('site-name', get_bloginfo('name')); ?></a></h1>
						</div>
						<nav id="top-nav" class="left">
							<?php
							if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) {
								wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'ul-top-nav', 'menu_class' => 'inline-list sf-menu', 'theme_location' => 'top-menu' ) );
							} else {
							?>
					        <ul class="inline-list sf-menu">
								<?php if ( is_page() ) $highlight = 'page_item'; else $highlight = 'page_item current_page_item'; ?>
								<li class="<?php echo $highlight; ?>"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e( 'Home', $themename ); ?></a></li>
								<?php wp_list_pages( 'sort_column=menu_order&depth=6&title_li=&exclude=' ); ?>
							</ul>
					        <?php } ?>
						</nav>
						<?php
						if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

							$cart_total = $woocommerce->cart->get_cart_total();
							$cart_count = $woocommerce->cart->cart_contents_count;

							?>
						<div class="top-cart right">
							<div class="mini-cart-overview">
								<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>">
									<span class="cart-icon"><i class="icon-shopping-bag"></i> <?php echo $cart_count; ?></span>
									<?php echo $cart_total; ?>
								</a>
							</div>
							<div class="sub-cart">
								<?php if ( sizeof($woocommerce->cart->cart_contents)>0 ) { ?>
								<ul class="cart-items">
								<?php foreach ($woocommerce->cart->cart_contents as $key => $value) { ?>
								<?php if ($value['data']->exists() && $value['quantity']>0) { ?>
									<li class="fix">
										<figure>
											<a href="<?php echo get_permalink($value['product_id']); ?>">
												<?php echo $value['data']->get_image(); ?>
											</a>
										</figure>
										<div class="item-detail">
											<p><a href="<?php echo get_permalink($value['product_id']); ?>" title="<?php echo $value['data']->get_title(); ?>"><?php echo text_truncate($value['data']->get_title(), 20); ?></a></p>
											<p><?php echo __('Quantity:', $themename) . $value['quantity']; ?> | <a href="<?php echo esc_url( $woocommerce->cart->get_remove_url( $key )); ?>" class="mini-cart-remove" title="<?php echo esc_html('Remove from cart', $themename); ?>"><i class="icon-remove"></i></a></p>
											<span class="item-price"><?php echo woocommerce_price($value['data']->get_price()); ?></span>
										</div>
									</li>
								<?php } ?>
								<?php } ?>
								</ul>
								<div class="mini-cart-total">
									<?php _e('Cart subtotal', $themename); ?> <?php echo $cart_total; ?>								</div>
								<div class="mini-cart-button">
									<?php echo '<a class="button" href="'.esc_url( $woocommerce->cart->get_cart_url() ).'">'.__('View shopping bag', $themename).'</a>'; ?>
									<?php echo '<a class="button orange" href="'.esc_url( $woocommerce->cart->get_checkout_url() ).'">'.__('Proceed to checkout', $themename).'</a>'; ?>
								</div>
								<?php } else { ?>
								<span class="mini-cart-empty"><?php echo __('Your cart is currently empty.', $themename); ?></span>
								<?php } ?>
							</div>
						</div>
						<?php  } ?>
					</div>	
				</div>
			</div>
		</header>
		<?php if(print_option('shop-cat-nav-enabled') == 'on'): ?>
		<div class="wrap-cat-nav">
			<div class="row">
				<div class="large-12 columns">
					<nav id="cat-nav">
						<ul class="inline-list sf-menu">
							<?php product_cat_nav(); ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php putRevSlider('home-slider', 'homepage'); ?>