<?php
global $theme_path, $theme_uri, $themename;
$elements_path = $theme_path . '/includes/elements/';
/*
Template Name: Contact page
*/

if(isset($_POST['contact_form_submit'])) {
	$data = $_POST['contact'];

	if( trim( $data['name'] ) == '' ) {
		$err = true;
	} else {
		$name = trim( $data['name'] );
	}

	if( trim( $data['email'] ) == '' )  {
		$err = true;
	} else if ( ! is_email(trim($data['email'] ) ) ) {
		$err = true;
	} else {
		$email = trim( $data['email'] );
	}

	if( trim( $data['message'] ) == '' ) {
		$err = true;
	} else {
		$message = stripslashes( trim( $data['message'] ) );
	}

	if( ! isset( $err ) ) {
		$email_to = print_option('contact-email');
		$subject  = __( 'Contact form message from ', $themename ).$name;
		$body     = __( "Name: $name \n\nEmail: $email \n\nComments: $message", $themename );
		$header   = __( 'From: ', $themename) . "$name <$email>" . "\r\n" . __( 'Reply-To: ', $themename ) . $email;

		wp_mail( $email_to, $subject, $body, $header );

		$sent = true;
	}
}

get_header();
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
	<section class="body no-pt">
		<?php if(print_option('enable-google-map') == 'on'): ?>
		<div class="map-container">
			<div id="map-canvas">
			</div>
		</div>
		<script>
		var map;
		var marker;
		(function($) {
		$(function() {
			var map_lat = <?php echo print_option('google-latitude'); ?>;
		    var map_lng = <?php echo print_option('google-longtitude'); ?>;
		    var map_add = $('#map_add').val();
		    
		    if(map_lat != '') {
		        var myLatlng = new google.maps.LatLng(map_lat, map_lng);
		        var mapOptions = {
		            zoom: 16,
		            center: myLatlng,
		            scrollwheel: false,
		            mapTypeId: google.maps.MapTypeId.ROADMAP,
				    mapTypeControl: true,
				    mapTypeControlOptions: {
				      	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				    },
				    zoomControl: true,
				    zoomControlOptions: {
				      	style: google.maps.ZoomControlStyle.SMALL
				    },
				    mapTypeId: google.maps.MapTypeId.ROADMAP
		            //icon: new_marker
		        };
		        map    = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		        var contentString = '<div id="content">'+
					'<div id="siteNotice">'+
					'</div>'+
					'<h1 id="firstHeading" class="firstHeading"><?php echo print_option('google-info-title'); ?></h1>'+
					'<div id="bodyContent">'+
					'<p><?php echo print_option('google-info-description'); ?></p>'+
					'</div>'+
					'</div>';

				var infowindow = new google.maps.InfoWindow({
				  content: contentString
				});
		        marker = new google.maps.Marker({
		            position: myLatlng,
		            map: map,
		            title: map_add,
		          	//icon: new_marker
		        });
		        
		        infowindow.open(map,marker);

				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map,marker);
				});
		    } else {
		        geocoder = new google.maps.Geocoder();
		        var latlng = new google.maps.LatLng(-34.397, 150.644);
		        var mapOptions = {
					zoom: 15,
					center: latlng,
					scrollwheel: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
				    mapTypeControl: true,
				    mapTypeControlOptions: {
				      	style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				    },
				    zoomControl: true,
				    zoomControlOptions: {
				      	style: google.maps.ZoomControlStyle.SMALL
				    },
				    mapTypeId: google.maps.MapTypeId.ROADMAP
		        }
		        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		        geocoder.geocode( { 'address': map_add}, function(results, status) {
		            if (status == google.maps.GeocoderStatus.OK) {
		                map.setCenter(results[0].geometry.location);
		                marker = new google.maps.Marker({
		                    map: map,
		                    position: results[0].geometry.location,
		          			//icon: new_marker
		                });
		            } else {
		                alert('Sorry, map address can not be display: ' + status);
		            }
		        });
		    }
		});

		})(jQuery);

		</script>
		<?php endif; ?>
			
		<div class="row wrap-contact-info">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();?>
			<section class="large-8 columns contact-form">
				<?php the_content(); ?>
				
				<div class="form-content">
					<form action="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" method="post" id="contact-form-big">
						<h3><?php echo esc_html__('Contact form', $themename); ?></h3>

						<?php if($err == true): ?>
						<div class="alert-box alert">
							<?php echo esc_html__('Something wrong, please check your fields again!', $themename); ?>
						</div>
						<?php endif; ?>

						<?php if($sent == true): ?>
						<div class="alert-box success">
							<?php echo esc_html__('Your message was successfully sent.', $themename); ?>
						</div>
						<?php endif; ?>

						<div class="row">
							<div class="large-12 columns">
								<label for="contact_form_name"><?php echo esc_html__('Your name', $themename); ?> <small class="validate">*</small></label>
								<input type="text" name="contact[name]" id="contact_form_name" class="required">
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns">
								<label for="contact_form_email"><?php echo esc_html__('Your email', $themename); ?> <small class="validate">*</small></label>
								<input type="text" name="contact[email]" id="contact_form_email" class="required email">
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns">
								<label for="contact_form_message"><?php echo esc_html__('Your message', $themename); ?> <small class="validate">*</small></label>
								<textarea type="text" name="contact[message]" id="contact_form_message" class="required"></textarea>
							</div>
						</div>

						<div class="row">
							<div class="large-12 columns">
								<label for="setting_account_sms_phone">&nbsp;</label>
								<input type="hidden" name="contact_form_submit" value="contact_form_submit" />
								<button class="contact-submit"><?php echo esc_html__('Send email', $themename); ?></button>
							</div>
						</div>
					</form>
				</div>
					
			</section>
			<section class="large-4 columns contact-info">
				<div class="contact-address">
					<h3><?php echo esc_html__('Contact info', $themename); ?></h3>
					<ul>
						<?php if(print_option('contact-address') != '') { ?>
						<li><i class="icon-home"></i><span> <?php echo trim(esc_html__('Address', $themename)); ?></span><?php echo print_option('contact-address'); ?></li>
						<?php } ?>

						<?php if(print_option('contact-phone') != '') { ?>
						<li><i class="icon-phone"></i><span> <?php echo trim(esc_html__('Phone', $themename)); ?></span><?php echo print_option('contact-phone'); ?></li>
						<?php } ?>

						<?php if(print_option('contact-fax') != '') { ?>
						<li><i class="icon-print"></i><span> <?php echo trim(esc_html__('Fax', $themename)); ?></span><?php echo print_option('contact-fax'); ?></li>
						<?php } ?>

						<?php if(print_option('contact-email') != '') { ?>
						<li><i class="icon-envelope"></i><span> <?php echo trim(esc_html__('Email', $themename)); ?></span><a href="mailto:<?php echo trim(print_option('contact-email')); ?>"><?php echo trim(print_option('contact-email')); ?></a></li>
						<?php } ?>
					</ul>
				</div>

				<div class="contact-social">
					<h3><?php echo esc_html__('Follow us', $themename); ?></h3>
					<ul class="inline-list top-social-icons">
						<?php require($elements_path.'header/top_social.php'); ?>
					</ul>
				</div>
			</section>
			
		<?php endwhile; ?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
		</div>
	</section>

<?php get_footer(); ?>