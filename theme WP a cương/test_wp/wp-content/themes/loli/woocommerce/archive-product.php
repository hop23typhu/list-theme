<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>

	<div class="subheader">
		<div class="row">
			<div class="shop-title large-12 columns">
				<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

					<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

				<?php endif; ?>

				<?php do_action( 'woocommerce_archive_description' ); ?>
				<?php  woocommerce_breadcrumb(); ?>
			</div>
		</div>
	</div>
	
	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_before_main_content');
	?>
	<?php
		$cols        = (print_option('shop-sidebar-enable') == 'on') ? 9 : 12;
		$sidebar_pos = print_option('layout-sidebar-position') ? print_option('layout-sidebar-position') : 'right-sidebar';
		if (isset($_GET['sidebar'])) {
			$sidebar_pos = $_GET['sidebar'];
		}
		$push_class  = '';
		$pull_class  = '';
		if($sidebar_pos == 'left-sidebar' && $cols == 9) {
			$push_class = 'push-3';
			$pull_class = 'pull-9';
		}
		if($sidebar_pos == 'no-sidebar') {
			$cols = 12;
		}
	?>
	<div class="row">
		<section class="large-<?php echo $cols; ?> columns <?php echo $push_class; ?>">
			<div class="shop-banner">
				<?php shop_banner(); ?>
			</div>
			<?php if ( have_posts() ) : ?>
				<div class="shop-action fix">
					<?php
						/**
						 * woocommerce_before_shop_loop hook
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
					?>
				</div>
				<div class="top-subcat">
					<ul class="inline-list">					
						<?php woocommerce_product_subcategories(); ?>
					</ul>					
				</div>

				<?php woocommerce_product_loop_start(); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php woocommerce_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

				<?php woocommerce_product_loop_end(); ?>

				<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				?>

			<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

			<?php endif; ?>
		</section>
		<?php if(print_option('shop-sidebar-enable') == 'on' && $sidebar_pos != 'no-sidebar' ): ?>
		<aside class="sidebar large-3 columns <?php echo $pull_class; ?>" role="complementary">
		<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action('woocommerce_sidebar');
		?>
		</aside>
		<?php endif; ?>
	</div>
	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

<?php get_footer('shop'); ?>