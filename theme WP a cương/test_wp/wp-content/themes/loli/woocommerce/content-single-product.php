<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
$cols        = (print_option('shop-single-sidebar-enable') == 'on') ? 9 : 12;
$sidebar_pos = print_option('layout-sidebar-position') ? print_option('layout-sidebar-position') : 'right-sidebar';
$push_class  = '';
$pull_class  = '';
if($sidebar_pos == 'left-sidebar' && $cols == 9) {
	$push_class = 'push-3';
	$pull_class = 'pull-9';
}
?>

<div class="row">
<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked woocommerce_show_messages - 10
	 */
	 do_action( 'woocommerce_before_single_product' );
?>
	<section class="large-<?php echo $cols; ?> columns <?php echo $push_class; ?>">
		<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="row">
				<div class="large-6 columns images-wrap">
				<?php
					/**
					 * woocommerce_show_product_images hook
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
				?>
				</div>
				<div class="summary entry-summary large-6 columns">

					<?php
						/**
						 * woocommerce_single_product_summary hook
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 */
						do_action( 'woocommerce_single_product_summary' );
					?>

				</div><!-- .summary -->
			</div>
			<?php
				/**
				 * woocommerce_after_single_product_summary hook
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
			?>

		</div><!-- #product-<?php the_ID(); ?> -->
	</section>
	<?php if(print_option('shop-single-sidebar-enable') == 'on'): ?>
		<aside class="sidebar large-3 columns <?php echo $pull_class; ?>" role="complementary">
		<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action('woocommerce_sidebar');
		?>
		</aside>
		<?php endif; ?>
	<?php do_action( 'woocommerce_after_single_product' ); ?>

</div>