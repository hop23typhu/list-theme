<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Ensure visibility
if ( ! $product->is_visible() )
	return;

// Increase loop count
$woocommerce_loop['loop']++;

// Extra post classes
$shop_columns = print_option('shop-columns', 4);

if(isset($_GET['sidebar']) && $_GET['sidebar'] == 'no-sidebar')
	$shop_columns = 4;
$classes = array();
//setting class for shop columns
$classes[] = 'large-'. (12/$shop_columns);
$classes[] = 'columns';
if ( 0 == ( $woocommerce_loop['loop'] - 1 ) % $woocommerce_loop['columns'] || 1 == $woocommerce_loop['columns'] )
	$classes[] = 'first';
if ( 0 == $woocommerce_loop['loop'] % $woocommerce_loop['columns'] )
	$classes[] = 'last';
?>
<article <?php post_class( $classes ); ?>>
	<div class="block-content">
		<header class="block-header">
			<a href="<?php the_permalink(); ?>" alt="" class="block-img"><?php echo woocommerce_get_product_thumbnail('shop_catalog'); ?></a>
			<?php if(print_option('shop-catalog-mode') != 'on'): ?>
			<div class="product-act">
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
				<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
			</div>
			<?php endif; ?>
		</header>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo text_truncate(get_the_title(), 40); ?></a>
		<?php if(print_option('shop-hide-price') != 'on'): ?>
		<div class="product-price"><?php echo $product->get_price_html(); ?></div>
		<?php endif; ?>
		<?php if(print_option('shop-hide-desc') != 'on'): ?>
		<p class="product-des"><?php echo text_truncate(get_the_excerpt(), 130);?></p>
		<?php endif; ?>
		<?php woocommerce_show_product_loop_sale_flash(); ?>
	</div>
</article>
