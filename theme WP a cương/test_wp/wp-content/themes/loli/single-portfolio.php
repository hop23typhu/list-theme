<?php get_header(); ?>
<?php
//get post option
$post_type   = get_post_meta($post->ID, $shortname.'_post_type', true);
$post_slider = rwmb_meta($shortname.'_post_type_slider', 'type=image&size=blog-thumb');
$post_image  = rwmb_meta($shortname.'_post_type_image', 'type=image&size=blog-thumb');
$post_video  = get_post_meta($post->ID, $shortname.'_post_type_video_url', true);

$post_title  = get_post_meta($post->ID, $shortname.'_portfolio_meta_title', true);
$post_author = get_post_meta($post->ID, $shortname.'_portfolio_meta_author', true);
$post_url    = get_post_meta($post->ID, $shortname.'_portfolio_meta_url', true);

$post_layout = get_post_meta($post->ID, $shortname.'_post_layout', true);

$sidebar_pos = $post_layout ? $post_layout : 'right-sidebar';
$cols = 8;
$cols_side = 4;
$push_class  = '';
$pull_class  = '';

if($sidebar_pos == 'left-sidebar') {
	$push_class = 'push-4';
	$pull_class = 'pull-8';
} elseif ($sidebar_pos == 'no-sidebar') {
	$cols = 12;
	$cols_side = 12;
}
?>
<?php get_template_part('breadcrumbs', 'page'); ?>
	<section class="body">
		<div class="row">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post();?>
			<section class="large-12 columns">
				<div class="row">
					<div class="large-<?php echo $cols; ?> columns <?php echo $push_class; ?>">
						<?php if($post_type == 'slider'): ?>
						<div class="post-thumbnail-slider owl-carousel">
							<?php foreach ($post_slider as $key => $value): ?>
							<div class="post-thumbnail-item">
							<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
							<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
							</a>
							</div>
							<?php endforeach; ?>
						</div>
						<?php elseif($post_type == 'image'): ?>
						<div class="post-thumbnail-image">
							<?php foreach ($post_image as $key => $value): ?>
							<div class="post-thumbnail-item">
							<a href="<?php echo $value['full_url']; ?>" rel="lightbox">	
							<?php echo "<img src='{$value['url']}' width='{$value['width']}' height='{$value['height']}' alt='{$value['alt']}' />"; ?>
							</a>
							</div>
							<?php endforeach; ?>
						</div>
						<?php elseif ($post_type == 'video'): ?>
						<div class="post-thumbnail-video">
							<?php
							if(strpos($post_video, 'youtube'))
								echo embed_youtube($post_video);
							else
								echo embed_vimeo($post_video);
							?>
						</div>
						<?php else: ?>
						<?php if(has_post_thumbnail()) { ?>
						<?php
						$thumb_id = get_post_thumbnail_id();
						$thumb_url = wp_get_attachment_image_src($thumb_id, '', true);
						?>
						<div class="post-thumbnail-image">
							<a href="<?php echo $thumb_url[0]; ?>" title="<?php the_title(); ?>" rel="lightbox">
							<?php the_post_thumbnail('blog-thumb'); ?>
							</a>
						</div>
						<?php } ?>
						<?php endif; ?>
						
					</div>
					<div class="large-<?php echo $cols_side; ?> columns <?php echo $pull_class; ?>">
						<div class="portfolio-description">
							<?php if($post_title != ''): ?>
								<h5><?php echo $post_title; ?></h5>
							<?php endif; ?>
							<?php the_content(); ?>
						</div>					
						<div class="portfolio-meta">
							<h5><?php echo esc_html__('Project detail', $themename); ?></h5>
							<table class="table-project-detail">
								<tbody>
								<?php if($post_author != ''): ?>
									<tr>
										<td>
											<strong><?php echo esc_html__('Author:', $themename); ?></strong>
										</td>
										<td><?php echo $post_author; ?></td>
									</tr>
								<?php endif; ?>
									<tr>
										<td>
											<strong><?php echo esc_html__('Posted:', $themename); ?></strong>
										</td>
										<td><?php d_entry_meta(); ?></td>
									</tr>
								<?php if(get_the_term_list($post->ID, 'portfolio-category', '', ', ') != ''): ?>
									<tr>
										<td>
											<strong><?php echo esc_html__('Category:', $themename); ?></strong>
										</td>
										<td><?php echo get_the_term_list($post->ID, 'portfolio-category', '', ', '); ?></td>
									</tr>
								<?php endif; ?>
								</tbody>
							</table>
							<?php if($post_url != ''): ?>
								<a class="button" href="<?php echo $post_url; ?>"><?php echo esc_html__('View project', $themename); ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
					
			</section>
			
		<?php endwhile; ?>
		<?php else: ?>
			<?php get_template_part( 'page-empty', get_post_format() ); ?>
		<?php endif; ?>
		</div>
	</section>
<?php get_footer(); ?>