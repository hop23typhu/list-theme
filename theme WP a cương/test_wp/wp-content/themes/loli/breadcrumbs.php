<?php
global $themename;
if(is_home() || is_front_page()) {
	return;
}
?>
<div class="subheader">
	<div class="row">
		<div class="shop-title large-12 columns">
<h1 class="page-title">
<?php if(is_search()): ?>
<?php esc_html_e('Seach results for: ')?>"<?php the_search_query(); ?>"

<?php elseif(is_category()): ?>
<?php esc_html_e('Archive by Category: ').single_cat_title(); ?>

<?php elseif (is_day()): ?>
<?php esc_html_e('Posts made in ',$themename) . the_time('F jS, Y'); ?>

<?php elseif (is_month()): ?>
<?php esc_html_e('Posts made in ',$themename) . the_time('F, Y'); ?>

<?php elseif (is_year()): ?>
<?php esc_html_e('Posts made in ',$themename) . the_time('Y'); ?>

<?php elseif(is_author()): ?>
<?php
global $wp_query;
$curauth = $wp_query->get_queried_object();
esc_html_e('Posts by ',$themename); echo ' ',$curauth->nickname;
?>

<?php else: ?>
<?php echo get_the_title(); ?>
<?php endif; ?>
</h1>
<?php
if ( class_exists( 'woocommerce' ) && (is_woocommerce() ) ) {
	woocommerce_breadcrumb();
	return;
}
?>
<?php if(function_exists('bcn_display')) { bcn_display(); }
else { ?>
<ul class="breadcrumbs">
	<li><a href="<?php echo esc_url( home_url() ); ?>" class="breadcrumbs_home"><?php esc_html_e('Home',$themename) ?></a></li>
	<?php if( is_tag() ) { ?>
		<li><?php esc_html_e('Posts Tagged ',$themename) ?></li><li><?php single_tag_title(); echo('&quot;'); ?></li>
	<?php } elseif (is_day()) { ?>
		<li><?php esc_html_e('Posts made in ',$themename) . the_time('F jS, Y'); ?></li>
	<?php } elseif (is_month()) { ?>
		<li><?php esc_html_e('Posts made in ',$themename) . the_time('F, Y'); ?></li>
	<?php } elseif (is_year()) { ?>
		<li><?php esc_html_e('Posts made in ',$themename) . the_time('Y'); ?></li>
	<?php } elseif (is_search()) { ?>
		<li><?php esc_html_e('Search results for',$themename) ?>: <?php the_search_query() ?></li>
	<?php } elseif (is_single()) { ?>
	<?php
		if ( 'listing' == get_post_type() ) {
			$categories = get_the_terms( get_the_ID(), 'listing_type' );
			if ( $categories ) {
				foreach( $categories as $category ) {
					$catlink = get_term_link( $category );
					echo ('<li><a href="'.esc_url( $catlink ).'">'.esc_html($category->name).'</a></li>');
					break;
				}
			}
		} else {
			$category = get_the_category();
			if ( $category ) {
				$catlink = get_category_link( $category[0]->cat_ID );
				echo ('<li><a href="'.esc_url($catlink).'">'.esc_html($category[0]->cat_name).'</a></li>');
			}
		}

		echo '<li>'.get_the_title().'</li>';
	?>
	<?php } elseif (is_category()) { ?>
		<li><?php single_cat_title(); ?></li>
	<?php } elseif (is_tax()) { ?>
		<?php
			$et_taxonomy_links = array();
			$et_term = get_queried_object();
			$et_term_parent_id = $et_term->parent;
			$et_term_taxonomy = $et_term->taxonomy;

			while ( $et_term_parent_id ) {
				$et_current_term = get_term( $et_term_parent_id, $et_term_taxonomy );
				$et_taxonomy_links[] = '<li><a href="' . esc_url( get_term_link( $et_current_term, $et_term_taxonomy ) ) . '" title="' . esc_attr( $et_current_term->name ) . '">' . esc_html( $et_current_term->name ) . '</a></li>';
				$et_term_parent_id = $et_current_term->parent;
			}

			if ( !empty( $et_taxonomy_links ) ) echo implode( ' <i class="bread-arrow"></i> ', array_reverse( $et_taxonomy_links ) ) . ' <i class="bread-arrow"></i> ';

			echo '<li>'.esc_html( $et_term->name ).'</li>';
		?>
	<?php } elseif (is_author()) { ?>
		<li><?php esc_html_e('Posts by ',$themename); echo ' ',$curauth->nickname; ?></li>
	<?php } elseif (is_page()) { ?>
		<li><?php echo get_the_title(); ?></li>
	<?php }; ?>
<?php } ?>
</ul>

		</div>
	</div>
</div>