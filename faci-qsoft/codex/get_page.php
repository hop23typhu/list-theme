<!-----------------------------------Slider img link ------------------------------------>
cai dat adf-repeater trong adf

<ul class="slides">
	<?php $slider_footer = get_field("slider_footer"); ?>
	<?php 
	foreach ($slider_footer as $key => $value) :
		?>
	<li>
		<a href="<?php echo $value['link_footer'] ?>">
			<img src="<?php echo $value['img_footer']['url'] ?>" alt="" />
		</a>
	</li>
	<?php
	endforeach;
	?>
</ul>
<!-----------------------------------Accordion ------------------------------------>
<?php
get_header();
$home_repeater = get_field( 'repeater_why' );
?>

<div class="about-home col-md-7 col-sm-7 col-xs-12">
	<?php 
	if ( $home_repeater ) {
		?>		
		<div class="about-info">
			<p><?php echo get_theme_mod('dichvu8'); ?></p>
			<h2><?php echo get_theme_mod('dichvu10'); ?></h2>
		</div>
		<div id="accordionhome">
			<?php 
			foreach ( $home_repeater as $value ) { 
				?>
				<h3>
					<i class="fa fa-lightbulb-o"></i>
					<p><?php echo $value['ten_tieu_de'] ?></p>
				</h3>
				<div class="accordion">
					<p><?php echo $value['noi_dung'] ?></p>
				</div>
				<div class="clear-fix"></div>	
				<?php 
			}
		}
		?>
	</div>
</div>

<!---------------------------- get page id ---------------------------->
https://codex.wordpress.org/Function_Reference/get_page
<?php 
$page_id = 5;
$page_data = get_page( $page_id );
echo '<h3>'. $page_data->post_title .'</h3>';

echo '<h3>'. $page_data->post_content .'</h3>';

echo apply_filters('the_content', $page_data->post_content);
?>

<!---------------------------------- footer ------------------------------------>

<p>© <?php echo date('Y'); ?> <?php bloginfo( 'sitename' ); ?>. <?php _e('All rights reserved', 'DungHoang'); ?>. <?php _e('This website is proundly to use WordPress', 'DungHoang'); ?></p>

