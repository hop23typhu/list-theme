<?php
//Custom Contact Info
class zotheme_contact_widget extends WP_Widget {
	
	function zotheme_contact_widget() {
		
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'zotheme_contact_widget', 'description' => 'Contact Info' );

		/* Create the widget. */
		$this->WP_Widget( 'zotheme-contact-widget', 'Contact Info', $widget_ops);
	
	}
	 
	function widget($args, $instance) {
	?>
	<div class="box-footer">
		<div class="header-box-footer header-box-contact-info">
			<h4><?php echo $instance['title_contact_info'] ?></h4>
			<p><?php echo $instance['des_contact_info'] ?></p>
		</div>		
		<div class="content-box-contact-info">
			<?php echo $instance['content_contact_info'] ?>
		</div>	
	</div>	
	<?php
	}
 
	function update($new_instance, $old_instance) {
		
		return $new_instance;
		
	}
 
	function form($instance) {
		// tạo form options bên trang quản trị
		?>
		<label>Title:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'title_contact_info' ); ?>" name="<?php echo $this->get_field_name( 'title_contact_info' ); ?>" value="<?php echo $instance['title_contact_info'] ?>" style="width:99%"/>
		<br/>
		<label>Description:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'des_contact_info' ); ?>" name="<?php echo $this->get_field_name( 'des_contact_info' ); ?>" value="<?php echo $instance['des_contact_info'] ?>" style="width:99%"/>
		<br/>
		<label>Content:</label>
		<textarea id="<?php echo $this->get_field_id( 'content_contact_info' ); ?>" name="<?php echo $this->get_field_name('content_contact_info' ); ?>" style="width:99%"/><?php echo $instance['content_contact_info'] ?></textarea>
		<?php
	}
}

add_action( 'widgets_init', create_function('', 'return register_widget("zotheme_contact_widget");') );
//Custom Latest News
class zotheme_latest_widget extends WP_Widget {
	
	function zotheme_latest_widget() {
		
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'zotheme_latest_widget', 'description' => 'Custom Latest News' );

		/* Create the widget. */
		$this->WP_Widget( 'zotheme-latest-widget', 'Custom Latest News', $widget_ops);
	
	}
	 
	function widget($args, $instance) {
	?>
	<div class="box-footer">
		<div class="header-box-footer header-box-latest-news">
			<h4><?php echo $instance['title_latest_news'] ?></h4>
			<p><?php echo $instance['des_latest_news'] ?></p>
		</div>		
		<div class="content-box-latest-news">
			<ul>
				<?php
					global $post;
					$args=array('post_type'=>'post','numberposts'=>$instance['number_latest_news']);
					$posts=get_posts($args);
					foreach($posts as $post) : setup_postdata($post);
				?>
				<li><a href="<?php the_permalink()?>"><?php the_title();?></a></li>
				<?php endforeach;wp_reset_postdata();?>
			</ul>
		</div>	
	</div>	
	<?php
	}
 
	function update($new_instance, $old_instance) {
		
		return $new_instance;
		
	}
 
	function form($instance) {
		// tạo form options bên trang quản trị
		?>
		<label>Title:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'title_latest_news' ); ?>" name="<?php echo $this->get_field_name( 'title_latest_news' ); ?>" value="<?php echo $instance['title_latest_news'] ?>" style="width:99%"/>
		<br/>
		<label>Description:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'des_latest_news' ); ?>" name="<?php echo $this->get_field_name( 'des_latest_news' ); ?>" value="<?php echo $instance['des_latest_news'] ?>" style="width:99%"/>
		<br/>
		<label>Number Latest News:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'number_latest_news' ); ?>" name="<?php echo $this->get_field_name( 'number_latest_news' ); ?>" value="<?php echo $instance['number_latest_news'] ?>" style="width:99%"/>
		<?php
	}
}

add_action( 'widgets_init', create_function('', 'return register_widget("zotheme_latest_widget");') );
//Photo Gallery
class zotheme_gallery_widget extends WP_Widget {
	
	function zotheme_gallery_widget() {
		
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'zotheme_gallery_widget', 'description' => 'Photo Gallery' );

		/* Create the widget. */
		$this->WP_Widget( 'zotheme-gallery-widget', 'Photo Gallery', $widget_ops);
	
	}
	 
	function widget($args, $instance) {
	?>
	<div class="box-footer">
		<div class="header-box-footer header-box-photo-gallery">
			<h4><?php echo $instance['title_photo_gallery'] ?></h4>
			<p><?php echo $instance['des_photo_gallery'] ?></p>
		</div>		
		<div class="content-box-photo-gallery">
			<ul>
				<?php
					global $post;
					$args=array('post_type'=>'gallery','numberposts'=>$instance['number_photo_gallery']);
					$posts=get_posts($args);
					foreach($posts as $post) : setup_postdata($post);
					$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );  
					$params = array('width' => 80,'height' => 60 );
					$img = bfi_thumb($image_attributes[0], $params);
				?>
				<li>
					<a class="fancybox" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID))?>"><img src="<?php echo $img;?>" alt=""/></a>
				</li>
				<?php endforeach;wp_reset_postdata();?>
			</ul>
		</div>	
	</div>	
	<?php
	}
 
	function update($new_instance, $old_instance) {
		
		return $new_instance;
		
	}
 
	function form($instance) {
		// tạo form options bên trang quản trị
		?>
		<label>Title:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'title_photo_gallery' ); ?>" name="<?php echo $this->get_field_name( 'title_photo_gallery' ); ?>" value="<?php echo $instance['title_photo_gallery'] ?>" style="width:99%"/>
		<br/>
		<label>Description:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'des_photo_gallery' ); ?>" name="<?php echo $this->get_field_name( 'des_photo_gallery' ); ?>" value="<?php echo $instance['des_photo_gallery'] ?>" style="width:99%"/>
		<br/>
		<label>Number Photo:</label>
		<input type="text" id="<?php echo $this->get_field_id( 'number_photo_gallery' ); ?>" name="<?php echo $this->get_field_name( 'number_photo_gallery' ); ?>" value="<?php echo $instance['number_photo_gallery'] ?>" style="width:99%"/>
		<?php
	}
}

add_action( 'widgets_init', create_function('', 'return register_widget("zotheme_gallery_widget");') );
//Custom Subscribe Newsletter
class zotheme_newsletter_widget extends WP_Widget {
	
	function zotheme_newsletter_widget() {
		
		/* Widget settings. */
		$widget_ops = array( 'classname' => 'zotheme_newsletter_widget', 'description' => 'Custom Subscribe Newsletter' );

		/* Create the widget. */
		$this->WP_Widget( 'zotheme-newsletter-widget', 'Custom Subscribe Newsletter', $widget_ops);
	
	}
	 
	function widget($args, $instance) {
	?>
	<div class="box-footer">
		<div class="header-box-footer header-box-newsletter">
			<h4><?php echo $instance['title_newsletter'] ?></h4>
			<p><?php echo $instance['des_newsletter'] ?></p>
		</div>		
		<div class="content-box-newsletter ">
			<form method="post" action="" class="subscribe">
				<input name="subscrible_email" type="email" placeholder="What’s your e-mail?" required/>
				<input name="subscrible_submit" type="submit" value="send!" />
			</form>
			<?php
				if (isset($_POST['subscrible_submit']))
				{
				 $mail = get_option('admin_email');
				 $from = $_POST['subscrible_email'];
				 
				 $subject='Mail from: '.$from;  
				 $message .= "Subscribe To Newsletter: ".$from."\r\n";
				 
				 $options="Content-type:text/plain;charset=utf-8\r\nFrom:$from\r\nReply-to:$from";
				 mail($mail,$subject,$message,$options);
				 /* if(mail($mail,$subject,$message,$options)){
					 echo 'Sended Successfull!';        
				 } */
				 }
			?>
		</div>	
	</div>	
	<?php
	}
 
	function update($new_instance, $old_instance) {
		
		return $new_instance;
		
	}
 
	function form($instance) {
		// tạo form options bên trang quản trị
		?>
			<label>Title:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title_newsletter' ); ?>" name="<?php echo $this->get_field_name( 'title_newsletter' ); ?>" value="<?php echo $instance['title_newsletter'] ?>" style="width:99%"/>
			<br/>
			<label>Description:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'des_newsletter' ); ?>" name="<?php echo $this->get_field_name( 'des_newsletter' ); ?>" value="<?php echo $instance['des_newsletter'] ?>" style="width:99%"/>
		<?php
	}
}

add_action( 'widgets_init', create_function('', 'return register_widget("zotheme_newsletter_widget");') );
?>