<?php 
	/* add New Fields For taxonomy */
$fields = array('products');
				
foreach($fields as $meta_name){
	add_action("{$meta_name}_edit_form_fields",'menu_category_edit_form_fields');
	add_action("{$meta_name}_add_form_fields",'menu_category_edit_form_fields');
	add_action("edited_{$meta_name}", 'menu_category_save_form_fields', 10, 2);
	add_action("created_{$meta_name}", 'menu_category_save_form_fields', 10, 2);
}

function menu_category_save_form_fields($term_id) {
	$fields = array('category_feature_image');
	foreach($fields as $meta_name){
		if ( isset( $_POST[$meta_name] ) ) {
			$meta_value = $_POST[$meta_name];
			// This is an associative array with keys and values:
			// $term_metas = Array($meta_name => $meta_value, ...)
			$term_metas = get_option("taxonomy_{$term_id}_metas");
			if (!is_array($term_metas)) {
				$term_metas = Array();
			}
			// Save the meta value
			$val = str_replace("\'", "'",$_POST[$meta_name]);
			$val = str_replace('\"', '"', $val);
			$term_metas[$meta_name] = $val;
			update_option( "taxonomy_{$term_id}_metas", $term_metas );
		}
	}
}

function menu_category_edit_form_fields ($term_obj) {
    // Read in the order from the options db
    $term_id = $term_obj->term_id;
    $term_metas = get_option("taxonomy_{$term_id}_metas");
	
	if ( isset($term_metas['category_feature_image']) ) {
        $category_feature_image = $term_metas['category_feature_image'];
    } else {
        $category_feature_image = '';
    }
?>
	<tr class="form-field">
		<th valign="top" scope="row">
			<label for="category_feature_image"><?php _e('Feature image', ''); ?></label>
		</th>
		<td>
			<?php 
				global $RS;
				$RS->upload(array(
					'name' => 'category_feature_image',
					'value' => $category_feature_image
				));
			?>
		</td>
    </tr>
<?php 
}
?>