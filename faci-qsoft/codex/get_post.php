<?php
<!---------------------------------- get posts ------------------------------------>
<?php
$args = array( 'post_type' => array( 'educations', 'travels', 'hospitals' ), 'posts_per_page' => 6 );
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
<?php the_post_thumbnail(array(300,300)); ?>
<h1>
	<?php echo the_title(); ?>
</h1>
<?php echo the_content(); ?>
<?php endwhile;?>

<!---------------------------------- get post ------------------------------------>
<?php
$args = array( 'post_type' => 'educations', 'posts_per_page' => 4, 'orderby'=>'rand' );
$wp_query = new WP_Query( $args );
while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
<?php the_post_thumbnail(array(300,300)); ?>
<h1>
	<?php echo the_title(); ?>
</h1>
<?php echo the_content(); ?>
<?php endwhile;?>
<!---------------------------------- permalink ------------------------------------>
<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
	<?php the_title(); ?>
</a>
<!---------------------------------- get search form ------------------------------------>

<?php include (TEMPLATEPATH.'/woocommerce/searchform.php'); ?>


<div class="box">
	<form class="container-1">
		<span class="icon"><i class="fa fa-search"></i></span>
		<input type="text" name="s" id="search" placeholder="Search...">
	</form>
</div>
<!-- style cho box -->
.container-1{
width: 293px;
vertical-align: middle;
white-space: nowrap;
position: relative;
}
.container-1 input#search{
width: 293px;
height: 30px;
background: #e6e6e6;
border: none;
font-size: 10pt;
float: left;
color: #63717f;
padding-left: 45px;
-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
padding-top: 5px;
}
.container-1 .icon{
position: absolute;
top: 50%;
padding-left: 17px;
padding-top: 7px;
z-index: 1;
color: #4f5b66;
}
.container-1 input#search:hover, .container-1 input#search:focus, .container-1 input#search:active{
outline:none;
background: #3e433e;
color: #fff;
}
<!-- close style cho box -->
<ul>
	<?php
	$args = array( 'post_type' => 'educations', 'posts_per_page' => 4, 'offset' => 1 );
	$loop = new WP_Query( $args );
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<li>
		<?php the_post_thumbnail(); ?>
		<h3>
			<a href="<?php the_permalink(); ?>"><?php echo ShortenText(get_the_title()); ?></a>
		</h3>
		<?php echo the_excerpt(); ?>
	</li>
<?php endwhile;?>
</ul>
<!-----------------------------------Excerpt ------------------------------------>
function the_excerpt_max_charlength($charlength) {
$excerpt = get_the_excerpt();
$charlength++;

if ( mb_strlen( $excerpt ) > $charlength ) {
$subex = mb_substr( $excerpt, 0, $charlength - 5 );
$exwords = explode( ' ', $subex );
$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
if ( $excut < 0 ) {
echo mb_substr( $subex, 0, $excut );
} else {
echo $subex;
}
echo '...';
} else {
echo $excerpt;
}
}
<!-- Function call -->
<?php the_excerpt_max_charlength(60); ?>

<!-----------------------------------Slider img link ------------------------------------>
cai dat adf-repeater trong adf

<ul class="slides">
	<?php $slider_footer = get_field("slider_footer"); ?>
	<?php 
	foreach ($slider_footer as $key => $value) :
		?>
	<li>
		<a href="<?php echo $value['link_footer'] ?>">
			<img src="<?php echo $value['img_footer']['url'] ?>" alt="" />
		</a>
	</li>
	<?php
	endforeach;
	?>
</ul>

<!---------------------------------- get post by id category ------------------------------------>

<h3>Thong tin cong ty</h3>
<ul>
	<?php
	$args = array(
		'post_type' => 'post',
		'orderby'  =>'id',
		'order'    =>'asc',

		'posts_per_page' => 4,
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'terms' => 16,
				'field' => 'term_id',
				)
			)
		);
	$category_posts = new WP_Query($args);
	if($category_posts->have_posts()) : 
		while($category_posts->have_posts()) : 
			$category_posts->the_post();
		?>

		<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

		<?php
		endwhile;
		else: 
			endif;
		?>
	</ul>

<!---------------------------------- get 1 post by id category ------------------------------------>
	<div class="col-md-9">
		<?php $args = array( 'numberposts' => 1, 'category' => 70 ); 
		$lastposts = get_posts( $args ); 
		foreach($lastposts as $post) : 
			setup_postdata($post); ?> 
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3> 
		<p>
			<?php 
			echo substr(strip_tags($post->post_content), 0, 600); 

			echo "...";?> 
			<em><a href="<?php the_permalink(); ?>">đọc tiếp</a></em>
		</p>
	<?php endforeach; ?>
</div>

<!---------------------------------- get post by id category ------------------------------------>

<div class="older-blog col-md-6">
	<ul>
		<?php
		global $post;
		$args = array( 'posts_per_page' => 6, 'offset'=> 1, 'category' => 70 hoac 'category_name' => 'slug' );
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : 
			setup_postdata( $post ); ?>
		<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endforeach;
	wp_reset_postdata(); ?>
</ul>
</div>

<!---------------------------------- get page by id ------------------------------------>
https://codex.wordpress.org/Function_Reference/get_page
<?php 
$page_id = 5;
$page_data = get_page( $page_id );
echo '<h3>'. $page_data->post_title .'</h3>';

echo '<h3>'. $page_data->post_content .'</h3>';

echo apply_filters('the_content', $page_data->post_content);
?>
<!---------------------------------- get number post field --------------------------->
<?php $so_dich_vu = get_field( 'so_luong_dich_vu' )?get_field( 'so_luong_dich_vu' ):8; ?>
	<div class="service">
		<div class="container">
			<?php $args = array( 'post_type' => 'service', 'posts_per_page' => $so_dich_vu);
			$wp_query = new WP_Query( $args );
			while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?> 
			<div class="service-info col-md-3 col-sm-6 col-xs-12">
				<div class="service-thumb">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('index-thumb'); ?></a>
				</div>
				<h4><a href="<?php the_permalink(); ?>"><?php echo short_title('', 12); ?></a></h4> 
				<p><a href="<?php the_permalink(); ?>">xem thêm</a></p>
			</div>
		<?php endwhile;?>
	</div>
</div>
<!---------------------------------- footer ------------------------------------>


<p>© <?php echo date('Y'); ?> <?php bloginfo( 'sitename' ); ?>. <?php _e('All rights reserved', 'DungHoang'); ?>. <?php _e('This website is proundly to use WordPress', 'DungHoang'); ?></p>