<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
  <div class="container">
    <div class="row">
      <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
              <div class="post-entry">
                  <?php if(have_posts()):while(have_posts()):the_post(); ?>
                  <h1 class="title-page"><?php the_title(); ?></h1>
                   <div class="post-content">
                      <?php the_content(); ?>
                   </div>
                  <?php endwhile; else : get_template_part('template-parts/content','none'); endif; ?> 
                  
              </div> <!-- end .post-entry -->
          </div>
      <!-- end #content -->
    <?php get_sidebar(); ?>     
     
    </div>
  </div>
</div> <!-- end .content-wrapper -->
<?php get_footer(); ?>