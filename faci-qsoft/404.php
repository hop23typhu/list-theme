<?php get_header(); ?>
<br/>
<div class="container">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="text-center">
          <small style="color:#fff">Chúng tôi không tìm thấy trang bạn cần - <b> lỗi không tìm thấy trang  </b></small>
          </h3>
        </div>
        <div class="panel-body">
          <p>Trang bạn đang tìm có thể đã được loại bỏ, hoặc tên của nó đã thay đổi, hoặc tạm thời không có. Hãy thử như sau:</p>
            <ul class="list-group">
              <li class="list-group-item">Hãy chắc chắn rằng địa chỉ trang web hiển thị trong thanh địa chỉ của trình duyệt của bạn được viết và định dạng đúng.</li>

                <li class="list-group-item">Quay lại   <a class="" href="<?php bloginfo('wpurl'); ?>">trang chủ</a> </li>
              </ul>
          </div>

        </div>
      </div>
      <div class="col-md-2">
      </div>
    </div>
</div>
<?php get_footer(); ?>