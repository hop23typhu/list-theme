<?php
function custom_stylesheet() {
	$color_general= get_theme_mod('color_general');
	$color_menu= get_theme_mod('color_menu');
	$color_menu_sub= get_theme_mod('color_menu_sub');
	//$color_link_tag= get_theme_mod('color_link_tag');
?>
<style type="text/css">
	#header,#footer,.tu-van .title-khoi,#sidebar .title-widget,.mo-ban .post-thumb .title-thumb ,.quy-trinh .nav-pills > li.active > a:hover,.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus,.tu-van form input[type="submit"],[class*="button-"] {
		background: <?php echo $color_general; ?>;
	}
	#menu-main{
		background: <?php echo $color_menu; ?>;
	}
	#menu-main ul li ul.sub-menu,#menu-main ul li ul.sub-menu li{
		background: <?php echo $color_menu_sub; ?>;
	}
	
	
	.alignnone {
		margin: 5px 20px 20px 0;
	}
	.aligncenter, div.aligncenter {
		display:block;
		margin: 5px auto 5px auto;
	}
	.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	.aligncenter {
		display: block;
		margin: 5px auto 5px auto;
	}
	a img.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	a img.alignnone {
		margin: 5px 20px 20px 0;
	}
	a img.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	a img.aligncenter {
		display: block;
		margin-left: auto;
		margin-right: auto
	}
	.wp-caption {
		background: #fff;
		border: 1px solid #f0f0f0;
		max-width: 96%; 
		padding: 5px 3px 10px;
		text-align: center;
	}
	.wp-caption.alignnone {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignleft {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignright {
		margin: 5px 0 20px 20px;
	}
	.wp-caption img {
		border: 0 none;
		height: auto;
		margin:0;
		max-width: 98.5%;
		padding:0;
		width: auto;
	}
	.wp-caption p.wp-caption-text {
		font-size:11px;
		line-height:17px;
		margin:0;
		padding:0 4px 5px;
	}

</style>
<?php
}
add_action('wp_head', 'custom_stylesheet', 50);

?>

