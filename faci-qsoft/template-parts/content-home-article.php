<div class="post-thumb col-xs-12 col-sm-4 col-md-4 col-lg-4">
    <a href="<?php the_permalink() ?>" >
        <?php 
            if(has_post_thumbnail( ))
                the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>'img-thumb moban img-responsive'  ));
            else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'"  class="img-thumb moban img-responsive" width="370px" height="auto" />';
        ?>
    </a>
    <h3 class="title-thumb"><a href="<?php the_permalink() ?>" ><?php the_title(); ?></a></h3>
</div>