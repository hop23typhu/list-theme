<div class="item">
    <div class="col-left img-person">
    	<?php 
            if(has_post_thumbnail( ))
                the_post_thumbnail('large',array('alt'=>get_the_title()));
            else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'" />';
        ?>
    </div>
    <div class="col-right">
        <h4 class="title"><?php the_title(); ?>:</h4>
        <div class="text">
        “ <?php echo strip_tags(  get_the_content() ); ?> ”
        </div>
    </div>
</div>