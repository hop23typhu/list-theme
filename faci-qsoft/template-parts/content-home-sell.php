 <div class="post-thumb col-xs-12 col-sm-6 col-md-3 col-lg-3">
    <a target="_blank" href="<?php the_permalink() ?>" >
    	<?php 
            if(has_post_thumbnail( ))
                the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>'img-thumb moban img-responsive'  ));
            else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'"  class="img-thumb moban img-responsive" width="370px" height="auto" />';
        ?>
        <h3 class="title-thumb"><?php the_title(); ?></h3>
    </a>
</div>
