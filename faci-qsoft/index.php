<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
	<div class="container">
		<div class="row">
			<div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
				<h1 class="title-page"><?php single_cat_title(); ?></h1>
				<h3 class="des-cat"></h3>
				<div class="list-post-thumb">
					<?php if(have_posts()):while(have_posts()):the_post(); ?>
						<div class="post-thumb">
							<div class="row">
								<a href="<?php the_permalink(); ?>"  class="col-left col-xs-12 -col-sm-6 col-md-6 col-lg-6">
									<?php 
							            if(has_post_thumbnail( ))
							                the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>'img-thumb img-responsive','width'=>'370px','height'=>'auto'  ));
							            else echo ' <img src="'.get_theme_mod("img_error").'" alt="'.get_the_title().'"  class="img-thumb img-responsive" width="370px" height="auto" />';
							        ?>
								</a>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<h2 class="title-thumb"><a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a></h2>
									<div class="the-excerpt"><p><?php the_excerpt(); ?></p></div>
								</div>
							</div>
						</div>
					<?php endwhile; else : get_template_part('template-parts/content','none'); endif; ?>			
					<div class='wp-pagenavi'><?php wp_pagenavi(); ?></div>
				</div>
			</div> <!-- end #content -->
		<?php get_sidebar(); ?>			
			<script type="text/javascript">
				$(document).ready(function(){
					$(".menu-sidebar .dropdown").append("<span class='icon-add dropdown-toggle' data-toggle='dropdown'>+</span>"); //add text sau nội dung trong thẻ html
						$(".menu-sidebar .icon-add").click(function(){
						$(this).parent().find(".sub-menu").slideToggle(500);
					});
				});
			</script>
		</div>
	</div>
</div> <!-- end .content-wrapper -->
<?php get_footer(); ?>