<?php get_header(); ?>
<div class="content-wrapper nd-khoi">
  <div class="container">
    <div class="row">
      <div id="content" class="col-xs-12 col-sm-12 col-md-9 col-lg-9 pull-right">
              <div class="post-entry">
                 
                  <h1 class="title-page"><?php single_cat_title( ); ?></h1>
                   <div class="post-content">
                      <?php echo tag_description( ); ?>
                   </div>   
                     <div class="container">   
                        <div class="row">
                            <div class="col-md-9 ">
                                <!-- Nav tabs category -->
                                 <?php 
                                    $obj_cat=get_the_terms( get_the_ID(), 'faq-category' );
                                   
                                    $taxonomy = 'faq-category';
                                    $args = array(
                                        'orderby'           => 'menu_order', 
                                        'order'             => 'asc',
                                        'hide_empty'        => false,
                                        'fields'            => 'all', 
                                    ); 
                                    $tax_terms = get_terms($taxonomy,$args);
                                ?>
                                <ul class="nav nav-tabs faq-cat-tabs hidden">
                                  <?php $stt=1;  ?>
                                    <li class="<?php if($stt==1) echo 'active'; ?>"><a href="#faq-cat-<?php echo $stt; ?>" data-toggle="tab"></a></li>
                                  
                                </ul>
                        
                                <!-- Tab panes -->
                                <div class="tab-content faq-cat-content">

                                    <?php $stt=1;  ?>
                                    <div class="tab-pane <?php if($stt==1) echo 'active'; ?> in fade" id="faq-cat-<?php echo $stt; ?>">
                                        <div class="panel-group" id="accordion-cat-1">

                                          <?php  
                          
                                            while(have_posts()):the_post();$i++;
                                          ?>
                                            <div class="panel panel-default panel-faq">
                                                <div class="panel-heading">
                                                    <a data-toggle="collapse" data-parent="#accordion-cat-<?php echo $stt; ?>" href="#faq-cat-<?php echo $stt; ?>-sub-<?php echo $i; ?>">
                                                        <h4 class="panel-title">
                                                            <?php the_title(); ?>
                                                            <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                                        </h4>
                                                    </a>
                                                </div>
                                                <div id="faq-cat-<?php echo $stt; ?>-sub-<?php echo $i; ?>" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <?php the_content(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endwhile; wp_reset_postdata(); ?>
                                        </div>
                                    </div>
                                    



                                </div>
                              </div>
                            </div>
                        </div>
                  
                  
              </div> <!-- end .post-entry -->
          </div>
      <!-- end #content -->
    <?php get_sidebar(); ?>     
    </div>
  </div>
</div> <!-- end .content-wrapper -->

<?php get_footer(); ?>