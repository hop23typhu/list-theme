
            <div class="khoi-end nd-khoi">
                <div class="container">
                    <?php if(is_active_sidebar( 'footer-2box' )) dynamic_sidebar( 'footer-2box' ); ?>
                </div>
            </div> <!-- end .khoi-end -->
            <?php if(is_active_sidebar( 'footer-partner' )) dynamic_sidebar( 'footer-partner' ); ?>
    </main>
    <footer id="footer">
        <div class="container">
            <?php if(is_active_sidebar( 'footer-widgets' )) dynamic_sidebar( 'footer-widgets' ); ?>
        </div>
    </footer>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"  type="text/javascript" charset="utf-8" async defer></script>
    <!-- Back to top -->
    <a class="scrollToTop">
      <?php 
        if( get_theme_mod( 'img_backtop' ) ) echo '<img src="'.get_theme_mod( "img_backtop" ).'" alt="back to top"/>'; else echo "Back top";
      ?>
    </a>
    <script>
        $(document).ready(function(){
            //Check to see if the window is top if not then display button
            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });
            //Click event to scroll to top
            $('.scrollToTop').click(function(){
                $('html, body').animate({scrollTop : 0},800);
                return false;
            });
        });
    </script>
    <!-- end back to top -->

    <style type="text/css">

            * html div#slidehelper {z-index:9998;position: absolute; overflow:hidden;
            top:expression(eval(document.compatMode &&
            document.compatMode=='CSS1Compat') ?
            documentElement.scrollTop
            +(documentElement.clientHeight-this.clientHeight)
            : document.body.scrollTop
            +(document.body.clientHeight-this.clientHeight)); z-index:1000000}
            #slidehelper{z-index:9998;font: 12px Arial, Helvetica, sans-serif; color:#666; position:fixed; _position: absolute; right:0; bottom:0;}

    </style>
    <?php if( get_theme_mod( 'img_helper' ) ) { ?>
    <div id="slidehelper" style="height: 156px;">
      <div>
        <div onclick="slide_clickhide();" align="right" style="cursor: pointer;font-weight: bold;color: red"><?php echo get_theme_mod( 'txt_close' ); ?></div>
          <div><a href="tel:<?php echo get_theme_mod( 'txt_phone' ); ?>"><img border="0" src="<?php echo get_theme_mod( 'img_helper' ); ?>"  width="225px"></a>
          <div class="spacer"></div></div>
      </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
          $(".menu-sidebar .dropdown").append("<span class='icon-add dropdown-toggle' data-toggle='dropdown'>+</span>"); //add text sau nội dung trong thẻ html
            $(".menu-sidebar .icon-add").click(function(){
            $(this).parent().find(".sub-menu").slideToggle(500);
          });
        });
    </script>
    <script type="text/javascript">
        slide_bottomLayer = document.getElementById('slidehelper');
        var slide_IntervalId = 0;
        var slide_maxHeight = 154;//Chieu cao khung quang cao
        var slide_minHeight = 0;
        var slide_curHeight = 0;
        function slide_show(){
            slide_curHeight += 2;
            if(slide_curHeight > slide_maxHeight){
                clearInterval(slide_IntervalId);
            }
            slide_bottomLayer.style.height = slide_curHeight+'px';
        }
        function slide_hide(){
            slide_curHeight -= 3;
            if(slide_curHeight < slide_minHeight){
                clearInterval(slide_IntervalId);
            }
            slide_bottomLayer.style.height=slide_curHeight+'px';
        }
        slide_IntervalId=setInterval('slide_show()',5);
        function slide_clickhide(){
            slide_IntervalId=setInterval('slide_hide()',5);
        }
        function slide_clickshow(){
            slide_IntervalId=setInterval('slide_show()',5);
        }
        function slide_clickclose(){
            document.body.style.marginBottom = '0px';
            slide_bottomLayer.style.display = 'none';
        }
    </script>
    <?php } ?>
</div> <!-- end #wrapper -->
<!-- jquery smooth scroll to id's -->   
<script>
$(function() {
  $('a[href*=dang-ky]:not([href=nd-scroll-cart])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 50
        }, 500);
        return false;
      }
    }
  });
});
</script>
<script type="text/javascript">
window.___gcfg = {lang: 'vi'};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
po.src = 'https://apis.google.com/js/platform.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1171840959527199";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php wp_footer(); ?>
</body>
</html>
