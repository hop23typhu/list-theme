<?php

$lib_dir = trailingslashit( str_replace( '\\', '/', dirname(__FILE__) ) );
$lib_abs = trailingslashit( str_replace( '\\', '/', ABSPATH ) );

if( !defined('YA_DIR') ){
	define( 'YA_DIR', $lib_dir );
}

if( !defined('YA_URL') ){
	define( 'YA_URL', site_url( str_replace( $lib_abs, '', $lib_dir ) ) );
}

defined('__YATHEME__') or die;

if (!isset($content_width)) { $content_width = 940; }

define("PRODUCT_TYPE","product");
define("PRODUCT_DETAIL_TYPE","product_detail");

require_once( get_template_directory().'/lib/options.php' );
function Ya_Options_Setup(){
	global $ya_options, $options, $options_args;
	$options = array();

	$options[] = array(
			'title' => __('General', 'sw_supershop'),
			'desc' => __('<p class="description">The theme allows to build your own styles right out of the backend without any coding knowledge. Start your own color scheme by selecting one of 3 predefined schemes. Upload new logo and favicon or get their URL.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_019_cogwheel.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
					array(
							'id' => 'scheme',
							'type' => 'radio_img',
							'title' => __('Color Scheme', 'sw_supershop'),
							'sub_desc' => 'Select one of 3 predefined schemes',
							'desc' => '',
							'options' => array(
											'default' => array('title' => 'Default', 'img' => get_template_directory_uri().'/assets/img/default.png'),
											'green' => array('title' => 'Green', 'img' => get_template_directory_uri().'/assets/img/green.png'),
											'blue' => array('title' => 'Blue', 'img' => get_template_directory_uri().'/assets/img/blue.png'),
											'orange' => array('title' => 'Orange', 'img' => get_template_directory_uri().'/assets/img/orange.png')
												),//Must provide key => value(array:title|img) pairs for radio options
							'std' => 'default'
						),

					array(
							'id' => 'bg_img',
							'type' => 'upload',
							'title' => __('Background Image', 'sw_supershop'),
							'sub_desc' => '',
							'std' => ''
							),

					array(
							'id' => 'bg_color',
							'type' => 'color',
							'title' => __('Color Option', 'sw_supershop'),
							'sub_desc' => __('Only color validation can be done on this field type', 'sw_supershop'),
							'desc' => __('This is the description field, again good for additional info.', 'sw_supershop'),
							'std' => '#FFFFFF'
							),
					array(
							'id' => 'bg_repeat',
							'type' => 'checkbox',
							'title' => __('Background Repeat', 'sw_supershop'),
							'sub_desc' => '',
							'desc' => '',
							'std' => '1'// 1 = on | 0 = off
							),

					array(
							'id' => 'favicon',
							'type' => 'upload',
							'title' => __('Favicon Icon', 'sw_supershop'),
							'sub_desc' => 'Use the Upload button to upload the new favicon and get URL of the favicon. To config Favicon in WordPress 4.3 or higher, please go to Appearance -> Customize',
							'std' => get_template_directory_uri().'/assets/img/favicon.ico'
						),

					array(
							'id' => 'responsive_support',
							'type' => 'checkbox',
							'title' => __('Responsive Support', 'sw_supershop'),
							'sub_desc' => 'Support reponsive layout, if you do not want to use this function, please uncheck.',
							'desc' => '',
							'std' => '1'// 1 = on | 0 = off
						),
					array(
							'id' => 'sitelogo',
							'type' => 'upload',
							'title' => __('Logo Image', 'sw_supershop'),
							'sub_desc' => 'Use the Upload button to upload the new logo and get URL of the logo',
							'std' => get_template_directory_uri().'/assets/img/logo-default.png'
						),
				)
		);

	$options[] = array(
			'title' => __('Layout', 'sw_supershop'),
			'desc' => __('<p class="description">Ya Framework comes with a layout setting that allows you to build any number of stunning layouts and apply theme to your entries.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_319_sort.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
					
					array(
						'id' => 'layout',
						'type' => 'select',
						'title' => __('Box Layout', 'sw_supershop'),
						'sub_desc' => 'Select Layout Box or Wide',
						'options' => array(
								'wide' => 'Wide',								
								'boxed' => 'Box'
								),
						'std' => 'wide'
					),
					array(
							'id' => 'bg_box_img',
							'type' => 'upload',
							'title' => __('Background Box Image', 'sw_supershop'),
							'sub_desc' => '',
							'std' => ''
						),
					array(
							'id' => 'sidebar_left_expand',
							'type' => 'select',
							'title' => __('Left Sidebar Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12', 
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '3',
							'sub_desc' => 'Select width of left sidebar.',
						),
					
					array(
							'id' => 'sidebar_right_expand',
							'type' => 'select',
							'title' => __('Right Sidebar Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '3',
							'sub_desc' => 'Select width of right sidebar medium desktop.',
						),
						array(
							'id' => 'sidebar_left_expand_md',
							'type' => 'select',
							'title' => __('Left Sidebar Medium Desktop Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => 'Select width of left sidebar medium desktop.',
						),
					array(
							'id' => 'sidebar_right_expand_md',
							'type' => 'select',
							'title' => __('Right Sidebar Medium Desktop Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => 'Select width of right sidebar.',
						),
						array(
							'id' => 'sidebar_left_expand_sm',
							'type' => 'select',
							'title' => __('Left Sidebar Tablet Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => 'Select width of left sidebar tablet.',
						),
					array(
							'id' => 'sidebar_right_expand_sm',
							'type' => 'select',
							'title' => __('Right Sidebar Tablet Expand', 'sw_supershop'),
							'options' => array(
									'2' => '2/12',
									'3' => '3/12',
									'4' => '4/12',
									'5' => '5/12',
									'6' => '6/12',
									'7' => '7/12',
									'8' => '8/12',
									'9' => '9/12',
									'10' => '10/12',
									'11' => '11/12',
									'12' => '12/12'
								),
							'std' => '4',
							'sub_desc' => 'Select width of right sidebar tablet.',
						),				
				)
		);
	$options[] = array(
		'title' => __('Header', 'sw_supershop'),
			'desc' => __('<p class="description">Ya Framework comes with a header setting that allows you to build style header.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_336_read_it_later.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
				 array(
							'id' => 'header_style',
							'type' => 'select',
							'title' => __('Header Style', 'sw_supershop'),
							'sub_desc' => 'Select Header style',
							'options' => array(
									'style1'  => 'Style 1',
									'style2'  => 'Style 2',
									'style3'  => 'Style 3'
									),
							'std' => 'style1'
						),
				 array(
						'id' => 'header_shortcode',
						'type' => 'text',
						'sub_desc' => __( 'Insert the shortcode to the header.', 'sw_supershop' ),
						'title' => __( 'Header Shortcode', 'sw_supershop' )
					),
				array(
							'id' => 'footer_style',
							'type' => 'select',
							'title' => esc_html__('Footer Style', 'sw_toppy'),
							'sub_desc' => 'Select Footer style',
							'options' => array(
							'default' => 'Default',
							'style1'  => 'Index 1'        
							),
							'std' => 'default'
				),
				
			)



	);
	$options[] = array(
			'title' => __('Navbar Options', 'sw_supershop'),
			'desc' => __('<p class="description">If you got a big site with a lot of sub menus we recommend using a mega menu. Just select the dropbox to display a menu as mega menu or dropdown menu.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_157_show_lines.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
				array(
						'id' => 'menu_type',
						'type' => 'select',
						'title' => __('Menu Type', 'sw_supershop'),
						'options' => array( 'dropdown' => 'Dropdown Menu', 'mega' => 'Mega Menu' ),
						'std' => 'mega'
					),
				array(
						'id' => 'menu_location',
						'type' => 'select',
						'title' => __('Theme Location', 'sw_supershop'),
						'sub_desc' => 'Select theme location to active mega menu.',
						'options' => array( '' => 'Non Location', 'primary_menu' => 'Primary Menu' ),
						'std' => 'primary_menu'
					),
				array(
						'id' => 'menu_visible',
						'type' => 'select',
						'title' => __('Responsive Menu Visible', 'sw_supershop'),
						'sub_desc' => 'Select option to show responsive menu visible',
						'options' => array( 'visible-tablet' => 'Visible Tablet', 'visible-phone' => 'Visible Phone' ),
						'std' => 'primary_menu'
					),
				array(
						'id' => 'menu_style',
						'type' => 'select',
						'title' => __('Select Menu Style', 'sw_supershop'),
						'sub_desc' => 'Select fixed menu or not',
						'options' => array( '' => 'Default', 'fixed_menu' => 'Fixed Menu' ),
						'std' => ''
					),
			)
		);
	$options[] = array(
		'title' => __('Blog Options', 'sw_supershop'),
		'desc' => __('<p class="description">Select layout in blog listing page.</p>', 'sw_supershop'),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => YA_URL.'/options/img/glyphicons/glyphicons_319_sort.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array(
				array(
						'id' => 'sidebar_blog',
						'type' => 'select',
						'title' => __('Sidebar Blog Layout', 'sw_supershop'),
						'options' => array(
								'full' => 'Full Layout',		
								'left_sidebar'	=> 'Left Sidebar',
								'right_sidebar' =>'Right Sidebar',
								'lr_sidebar'   =>'Left Right Sidebar'	
						),
						'std' => 'left_sidebar',
						'sub_desc' => 'Select style sidebar blog',
					),
					array(
						'id' => 'blog_layout',
						'type' => 'select',
						'title' => __('Layout blog', 'sw_supershop'),
						'options' => array(
								'list'	=> 'List Layout',
								'grid' => 'Grid Layout'								
						),
						'std' => 'list',
						'sub_desc' => 'Select style layout blog',
					),
					array(
						'id' => 'blog_column',
						'type' => 'select',
						'title' => __('Blog column', 'sw_supershop'),
						'options' => array(								
								'2' => '2 columns',
								'3' => '3 columns',
								'4' => '4 columns'								
							),
						'std' => '2',
						'sub_desc' => 'Select style number column blog',
					),
			)
	);	
	$options[] = array(
		'title' => __('Product Options', 'sw_supershop'),
		'desc' => __('<p class="description">Select layout in product listing page.</p>', 'sw_supershop'),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => YA_URL.'/options/img/glyphicons/glyphicons_319_sort.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array(
				array(
					'id' => 'product_col_large',
					'type' => 'select',
					'title' => __('Product Listing column Desktop', 'sw_supershop'),
					'options' => array(
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '3',
					'sub_desc' => 'Select number of column on Desktop Screen',
				),
				array(
					'id' => 'product_col_medium',
					'type' => 'select',
					'title' => __('Product Listing column Medium Desktop', 'sw_supershop'),
					'options' => array(
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '3',
					'sub_desc' => 'Select number of column on Medium Desktop Screen',
				),
				array(
					'id' => 'product_col_sm',
					'type' => 'select',
					'title' => __('Product Listing column Tablet', 'sw_supershop'),
					'options' => array(
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '2',
					'sub_desc' => 'Select number of column on Tablet Screen',
				),
				array(
					'id' => 'sidebar_product',
					'type' => 'select',
					'title' => __('Sidebar Product Layout', 'sw_supershop'),
					'options' => array(
							'left'	=> 'Left Sidebar',
							'full' => 'Full Layout',		
							'right' =>'Right Sidebar'					
					),
					'std' => 'left',
					'sub_desc' => 'Select style sidebar product',
				),
			)
	);	
	$options[] = array(
		'title' => __('Portfolio Options', 'sw_supershop'),
		'desc' => __('<p class="description">Select layout in Portfolio listing page.</p>', 'sw_supershop'),
		//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
		//You dont have to though, leave it blank for default.
		'icon' => YA_URL.'/options/img/glyphicons/glyphicons_319_sort.png',
		//Lets leave this as a blank section, no options just some intro text set above.
		'fields' => array(
				array(
					'id' => 'portfolio_imgintro',
					'type' => 'upload',
					'title' => __('Portfolio image intro', 'sw_supershop'),				
					'std' => '',
					'sub_desc' => 'Select portfolio image intro',
				),
				array(
					'id' => 'portfolio_id',
					'type' => 'multi_select_terms',
					'title' => __('Portfolio Categories', 'sw_supershop'),				
					'std' => 'portfolio_cat',
					'sub_desc' => 'Select Portfolio Category',
				),
				array(
					'id' => 'p_style',
					'type' => 'select',
					'title' => __('Portfolio column Desktop', 'sw_supershop'),
					'options' => array(
						'default'	=> 'Default Column(s)',
						'fitrows'	=> 'FitRows',
						'masonry'	=> 'Masonry'
					),
					'std' => 'default',
					'sub_desc' => 'Select number of column on Desktop Screen',
				),
				array(
					'id' => 'p_col_large',
					'type' => 'select',
					'title' => __('Portfolio column Desktop', 'sw_supershop'),
					'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '4',
					'sub_desc' => 'Select number of column on Desktop Screen',
				),
				array(
					'id' => 'p_col_medium',
					'type' => 'select',
					'title' => __('Portfolio column Medium Desktop', 'sw_supershop'),
					'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '3',
					'sub_desc' => 'Select number of column on Medium Desktop Screen',
				),
				array(
					'id' => 'p_col_sm',
					'type' => 'select',
					'title' => __('Portfolio column Tablet', 'sw_supershop'),
					'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'							
						),
					'std' => '3',
					'sub_desc' => 'Select number of column on Tablet Screen',
				),
				array(
					'id' => 'p_col_xs',
					'type' => 'select',
					'title' => __('Portfolio column Smartphone', 'sw_supershop'),
					'options' => array(
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',							
							'6' => '6'								
						),
					'std' => '2',
					'sub_desc' => 'Select number of column on Smartphone Screen',
				)
			)
	);	
	$options[] = array(
			'title' => __('Typography', 'sw_supershop'),
			'desc' => __('<p class="description">Change the font style of your blog, custom with Google Font.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_151_edit.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
					array(
							'id' => 'google_webfonts',
							'type' => 'text',
							'title' => __('Use Google Webfont', 'sw_supershop'),
							'sub_desc' => 'Insert font style that you actually need on your webpage.',
							'std' => ''
						),
					array(
							'id' => 'webfonts_weight',
							'type' => 'multi_select',
							'sub_desc' => 'For weight, see Google Fonts to custom for each font style.',
							'title' => __('Webfont Weight', 'sw_supershop'),
							'options' => array(
									'100' => '100',
									'200' => '200',
									'300' => '300',
									'400' => '400',
									'600' => '600',
									'700' => '700',
									'800' => '800',
									'900' => '900'
								),
							'std' => ''
						),
					array(
							'id' => 'webfonts_assign',
							'type' => 'select',
							'title' => __( 'Webfont Assign to', 'sw_supershop' ),
							'sub_desc' => 'Select the place will apply the font style headers, every where or custom.',
							'options' => array(
									'headers' => __( 'Headers',    'sw_supershop' ),
									'all'     => __( 'Everywhere', 'sw_supershop' ),
									'custom'  => __( 'Custom',     'sw_supershop' )
								)
						),
					 array(
							'id' => 'webfonts_custom',
							'type' => 'text',
							'sub_desc' => 'Insert the places will be custom here, after selected custom Webfont assign.',
							'title' => __( 'Webfont Custom Selector', 'sw_supershop' )
						),
				)
		);

	$options[] = array(
			'title' => __('Social share', 'sw_supershop'),
			'desc' => __('<p class="description">Social sharing is ready to use and built in. You can share your pages with just a click and your post can go to their wall and you can gain vistitors from Social Networks. Check Social Networks that you want to use.</p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_222_share.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
					array(
							'id' => 'social-share',
							'title' => __( 'Social share', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => '',
							'desc' => '',
							'std' => '0'
						),
					array(
							'id' => 'social-share-fb',
							'title' => __( 'Facebook', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => '',
							'desc' => '',
							'std' => '1',
						),
					array(
							'id' => 'social-share-tw',
							'title' => __( 'Twitter', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => '',
							'desc' => '',
							'std' => '1',
						),
					array(
							'id' => 'social-share-in',
							'title' => __( 'Linked_in', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => '',
							'desc' => '',
							'std' => '1',
						),
					array(
							'id' => 'social-share-go',
							'title' => __( 'Google+', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => '',
							'desc' => '',
							'std' => '1',
						),

				)
		);

	$options[] = array(
			'title' => __('Advanced', 'sw_supershop'),
			'desc' => __('<p class="description">Custom advanced with Cpanel, Widget advanced, Developer mode </p>', 'sw_supershop'),
			//all the glyphicons are included in the options folder, so you can hook into them, or link to your own custom ones.
			//You dont have to though, leave it blank for default.
			'icon' => YA_URL.'/options/img/glyphicons/glyphicons_083_random.png',
			//Lets leave this as a blank section, no options just some intro text set above.
			'fields' => array(
					array(
							'id' => 'show_cpanel',
							'title' => __( 'Show cPanel', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => 'Turn on/off Cpanel',
							'desc' => '',
							'std' => ''
						),
					array(
							'id' => 'widget-advanced',
							'title' => __('Widget Advanced', 'sw_supershop'),
							'type' => 'checkbox',
							'sub_desc' => 'Turn on/off Widget Advanced',
							'desc' => '',
							'std' => '1'
						),
					array(
							'id' => 'developer_mode',
							'title' => __( 'Developer Mode', 'sw_supershop' ),
							'type' => 'checkbox',
							'sub_desc' => 'Turn on/off preset',
							'desc' => '',
							'std' => '0'
						),
					array(
							'id' => 'back_active',
							'type' => 'checkbox',
							'title' => __('Back to top', 'sw_supershop'),
							'sub_desc' => '',
							'desc' => '',
							'std' => '1'// 1 = on | 0 = off
							),
					array(
							'id' => 'sticky_menu',
							'type' => 'checkbox',
							'title' => __('Active sticky menu', 'sw_supershop'),
							'sub_desc' => '',
							'desc' => '',
							'std' => '0'// 1 = on | 0 = off
							),
					array(
							'id' => 'direction',
							'type' => 'select',
							'title' => __('Direction', 'sw_supershop'),
							'options' => array( 'ltr' => 'Left to Right', 'rtl' => 'Right to Left' ),
							'std' => 'ltr'
						),		
					array(
							'id' => 'effect_active',
							'type' => 'checkbox',
							'title' => __('Active Effect Scroll', 'sw_supershop'),
							'sub_desc' => 'Check to active effect scroll in homepage',
							'desc' => '',
							'std' => '1'// 1 = on | 0 = off
							),					
					array(
							'id' => 'advanced_head',
							'type' => 'textarea',
							'sub_desc' => 'Insert your own CSS into this block. This overrides all default styles located throughout the theme',
							'title' => __( 'Custom CSS/JS', 'sw_supershop' )
						)
				)
		);

	$options_args = array();

	//Setup custom links in the footer for share icons
	$options_args['share_icons']['facebook'] = array(
			'link' => 'https://www.facebook.com/flytheme',
			'title' => 'Facebook',
			'img' => YA_URL.'/options/img/glyphicons/glyphicons_320_facebook.png'
	);
	$options_args['share_icons']['twitter'] = array(
			'link' => 'https://twitter.com/Flytheme',
			'title' => 'Folow me on Twitter',
			'img' => YA_URL.'/options/img/glyphicons/glyphicons_322_twitter.png'
	);
	$options_args['share_icons']['linked_in'] = array(
			'link' => 'https://www.linkedin.com/in/flytheme',
			'title' => 'Find me on LinkedIn',
			'img' => YA_URL.'/options/img/glyphicons/glyphicons_337_linked_in.png'
	);

	//Choose to disable the import/export feature
	// $options_args['show_import_export'] = true;

	//Choose a custom option name for your theme options, the default is the theme name in lowercase with spaces replaced by underscores
	$options_args['opt_name'] = __YATHEME__;

	$options_args['google_api_key'] = '';//must be defined for use with google webfonts field type

	//Custom menu icon
	//$options_args['menu_icon'] = '';

	//Custom menu title for options page - default is "Options"
	$options_args['menu_title'] = __('Theme Options', 'sw_supershop');

	//Custom Page Title for options page - default is "Options"
	$options_args['page_title'] = __('YA Options :: ', 'sw_supershop') . wp_get_theme()->get('Name');

	//Custom page slug for options page (wp-admin/themes.php?page=***) - default is "ya_theme_options"
	$options_args['page_slug'] = 'ya_theme_options';

	//Custom page capability - default is set to "manage_options"
	//$options_args['page_cap'] = 'manage_options';

	//page type - "menu" (adds a top menu section) or "submenu" (adds a submenu) - default is set to "menu"
	$options_args['page_type'] = 'submenu';

	//parent menu - default is set to "themes.php" (Appearance)
	//the list of available parent menus is available here: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
	//$options_args['page_parent'] = 'themes.php';

	//custom page location - default 100 - must be unique or will override other items
	$options_args['page_position'] = 27;
	$ya_options = new YA_Options($options, $options_args);
}
add_action( 'admin_init', 'Ya_Options_Setup', 0 );
Ya_Options_Setup();
$widget_areas = array(
	
	array(
			'name' => __('Sidebar Left Blog', 'sw_supershop'),
			'id'   => 'left-blog',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Sidebar Right Blog', 'sw_supershop'),
			'id'   => 'right-blog',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Sidebar Blog Cat Full', 'sw_supershop'),
			'id'   => 'categories-blog-full',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	array(
			'name' => __('Sidebar Blog Cat Left', 'sw_supershop'),
			'id'   => 'categories-blog-left',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Top', 'sw_supershop'),
			'id'   => 'top',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Top Home2', 'sw_supershop'),
			'id'   => 'top-home2',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Top Language', 'sw_supershop'),
			'id'   => 'top-language',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Top Home3', 'sw_supershop'),
			'id'   => 'top-home3',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Sidebar Left Detail Product', 'sw_supershop'),
			'id'   => 'left-detail-product',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	array(
			'name' => __('Sidebar Bottom Detail Product', 'sw_supershop'),
			'id'   => 'bottom-detail-product',
			'before_widget' => '<div class="widget %1$s %2$s" data-scroll-reveal="enter bottom move 20px wait 0.2s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	
	array(
			'name' => __('Sidebar Top Full Product', 'sw_supershop'),
			'id'   => 'sidebar-top-cat',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Sidebar Top Left Product', 'sw_supershop'),
			'id'   => 'sidebar-top-left-cat',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Sidebar Left Product', 'sw_supershop'),
			'id'   => 'left-product',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Sidebar Right Product', 'sw_supershop'),
			'id'   => 'right-product',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget' => '</div></div>',
			'before_title' => '<div class="block-title-widget"><h2><span>',
			'after_title' => '</span></h2></div>'
	),
	
	array(
			'name' => __('Above Footer', 'sw_supershop'),
			'id'   => 'above-footer',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer', 'sw_supershop'),
			'id'   => 'footer',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Menu', 'sw_supershop'),
			'id'   => 'footer-menu',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Copyright', 'sw_supershop'),
			'id'   => 'footer-copyright',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Block', 'sw_supershop'),
			'id'   => 'footer-block',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Style1', 'sw_supershop'),
			'id'   => 'footer1',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Menu Style1', 'sw_supershop'),
			'id'   => 'footer-menu1',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
	array(
			'name' => __('Footer Copyright Style1', 'sw_supershop'),
			'id'   => 'footer-copyright1',
			'before_widget' => '<div class="widget %1$s %2$s"><div class="widget-inner">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
	),
);
