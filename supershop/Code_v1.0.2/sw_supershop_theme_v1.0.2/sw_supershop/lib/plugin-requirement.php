<?php
/***** Active Plugin ********/
require_once( get_template_directory().'/lib/class-tgm-plugin-activation.php' );

add_action( 'tgmpa_register', 'ya_register_required_plugins' );
function ya_register_required_plugins() {
    $plugins = array(
		array(
            'name'               => 'Woocommerce', 
            'slug'               => 'woocommerce', 
            'required'           => true, 
			'version'			 => '2.4.13'
        ),
        array(
            'name'               => 'SW Woocommerce Slider', 
            'slug'               => 'sw-woo-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-woo-slider.zip', 
            'required'           => true, 
        ),
		 array(
            'name'               => 'SW Woocommerce Tab Slider', 
            'slug'               => 'sw-woo-tab-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-woo-tab-slider.zip', 
            'required'           => true, 
        ),
		  array(
            'name'               => 'SW Woo Countdown Slider', 
            'slug'               => 'sw-woo-countdown-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-woo-countdown-slider.zip', 
            'required'           => true, 
        ),
		  array(
            'name'               => 'SW Twitter Slider', 
            'slug'               => 'sw-twitter-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-twitter-slider.zip', 
            'required'           => true, 
        ),
		  array(
            'name'               => 'SW Responsive Post Slider', 
            'slug'               => 'sw-responsive-post-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-responsive-post-slider.zip', 
            'required'           => true, 
        ),
		
		array(
            'name'               => 'SW Testimonial Slider', 
            'slug'               => 'sw-testimonial-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-testimonial-slider.zip', 
            'required'           => true, 
        ),
		array(
            'name'               => 'SW Partner Slider', 
            'slug'               => 'sw-partner-slider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw-partner-slider.zip', 
            'required'           => true, 
        ),
		array(
            'name'               => 'SW Our Team', 
            'slug'               => 'sw_ourteam', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw_ourteam.zip', 
            'required'           => true, 
        ),
		array(
            'name'               => 'SW Portfolio', 
            'slug'               => 'sw_portfolio', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw_portfolio.zip', 
            'required'           => true, 
        ),
		array(
            'name'               => 'SW Core', 
            'slug'               => 'sw_core', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/sw_core.zip', 
            'required'           => true, 
        ),
		array(
            'name'               => 'Visual Composer', 
            'slug'               => 'js_composer', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/js_composer.zip', 
            'required'           => true,
			'version'			 => '4.9.2'
        ),
		array(
            'name'               => 'Revolution Slider', 
            'slug'               => 'revslider', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/revslider.zip', 
            'required'           => true,
			'version'			 => '5.1.5'
        ),
		array(
            'name'               => 'One Click Install', 
            'slug'               => 'one-click-install', 
            'source'             => get_stylesheet_directory() . '/lib/plugins/one-click-install.zip', 
            'required'           => true, 
        ),
		array(
            'name'      		 => 'Contact Form 7',
            'slug'     			 => 'contact-form-7',
            'required' 			 => false,
        ),
		array(
            'name'      		 => 'MailChimp for WordPress Lite',
            'slug'     			 => 'mailchimp-for-wp',
            'required' 			 => true,
        ),
		array(
            'name'     			 => 'Widget Importer Exporter',
            'slug'      		 => 'widget-importer-exporter',
            'required' 			 => true,
        ), 
		array(
            'name'     			 => 'WordPress Importer',
            'slug'      		 => 'wordpress-importer',
            'required' 			 => true,
        ), 
		 array(
            'name'      		 => 'YITH Woocommerce Compare',
            'slug'      		 => 'yith-woocommerce-compare',
            'required'			 => false,
			'version'			 => '2.0.6'
        ),
		 array(
            'name'     			 => 'YITH Woocommerce Wishlist',
            'slug'      		 => 'yith-woocommerce-wishlist',
            'required' 			 => false,
			'version'			 => '2.0.13'
        ), 
		array(
            'name'     			 => 'Wordpress Seo',
            'slug'      		 => 'wordpress-seo',
            'required'  		 => true,
        ),

    );
    $config = array();

    tgmpa( $plugins, $config );

}	