<?php
$number    		= isset( $instance['numberposts'] ) ? intval($instance['numberposts']) : 5;
$el_class  = isset( $instance['el_class'] )    ? 	strip_tags($instance['el_class']) : '';
?>

<p>
	<label for="<?php echo $this->get_field_id('numberposts'); ?>"><?php esc_html_e('Number of Posts', 'sw_supershop')?></label>
	<br />
	<input class="widefat"
		id="<?php echo $this->get_field_id('numberposts'); ?>"name="<?php echo $this->get_field_name('numberposts'); ?>" type="text"
		value="<?php echo esc_attr($number); ?>" />
</p>
<p>
			<label for="<?php echo $this->get_field_id('el_class'); ?>"><?php esc_html_e('El_class', 'sw_supershop')?></label>
			<br />
			<input class="widefat" id="<?php echo $this->get_field_id('el_class'); ?>" name="<?php echo $this->get_field_name('el_class'); ?>"
				type="text"	value="<?php echo esc_attr($el_class); ?>" />
</p>