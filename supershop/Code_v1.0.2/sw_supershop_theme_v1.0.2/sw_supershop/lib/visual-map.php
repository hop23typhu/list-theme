<?php 
add_action( 'vc_before_init', 'my_shortcodeVC' );
function my_shortcodeVC(){
$target_arr = array(
	__( 'Same window', 'sw_supershop' ) => '_self',
	__( 'New window', 'sw_supershop' ) => "_blank"
);
$link_category = array( __( 'All Links', 'sw_supershop' ) => '' );
$link_cats     = get_categories();
if ( is_array( $link_cats ) ) {
	foreach ( $link_cats as $link_cat ) {
		$link_category[ $link_cat->name ] = $link_cat->term_id;
	}
}
//category product
$terms = get_terms( 'product_cat', array( 'parent' => 0, 'hide_emty' => false ) );
	if( count( $terms ) == 0 ){
		return ;
	}
	$term = array( __( 'All Category Product', 'sw_supershop' ) => '' );
	foreach( $terms as $cat ){
		$term[$cat->name] = $cat -> term_id;
	}
	
$args = array(
			'type' => 'post',
			'child_of' => 0,
			'parent' => 0,
			'orderby' => 'name',
			'order' => 'ASC',
			'hide_empty' => false,
			'hierarchical' => 1,
			'exclude' => '',
			'include' => '',
			'number' => '',
			'taxonomy' => 'product_cat',
			'pad_counts' => false,

		);
		$product_categories_dropdown = array( __( 'All Category Product', 'sw_supershop' ) => '' );;
		$categories = get_categories( $args );
		foreach($categories as $category){
			$product_categories_dropdown[$category->name] = $category -> term_id;
		}
$menu_locations_array = array( __( 'All Links', 'sw_supershop' ) => '' );
$menu_locations = wp_get_nav_menus();	
foreach ($menu_locations as $menu_location){
	$menu_locations_array[$menu_location->name] = $menu_location -> term_id;
}

/* YTC VC */
//YTC post
vc_map( array(
	'name' => 'YTC_' . __( 'POSTS', 'sw_supershop' ),
	'base' => 'ya_post',
	'icon' => 'icon-wpb-ytc',
	'category' => __( 'My shortcodes', 'sw_supershop' ),
	'class' => 'wpb_vc_wp_widget',
	'weight' => - 50,
	'description' => __( 'Display posts-seclect category', 'sw_supershop' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'sw_supershop' ),
			'param_name' => 'title',
			'description' => __( 'What text use as a widget title. Leave blank to use default widget title.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Style title', 'sw_supershop' ),
			'param_name' => 'style_title',
			'description' =>__( 'What text use as a style title. Leave blank to use default style title.', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Type post', 'sw_supershop' ),
			'param_name' => 'type',
			'value' => array(
				'Select type',
				__( 'The_blog', 'sw_supershop' ) => 'the_blog',
				__( 'Style 2', 'sw_supershop' ) => 'style2'
			),
			'description' => sprintf( __( 'Select different style posts.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
		array(
			'param_name'    => 'category_id',
			'type'          => 'dropdown',
			'value'         => $link_category, // here I'm stuck
			'heading'       => __('Category filter:', 'sw_supershop'),
			'description'   => '',
			'holder'        => 'div',
			'class'         => ''
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of posts to show', 'sw_supershop' ),
			'param_name' => 'number',
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Excerpt length (in words)', 'sw_supershop' ),
			'param_name' => 'length',
			'description' => __( 'Excerpt length (in words).', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'sw_supershop' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sw_supershop' )
		),
			

		array(
			'type' => 'dropdown',
			'heading' => __( 'Order way', 'sw_supershop' ),
			'param_name' => 'order',
			'value' => array(
				__( 'Descending', 'sw_supershop' ) => 'DESC',
				__( 'Ascending', 'sw_supershop' ) => 'ASC'
			),
			'description' => sprintf( __( 'Designates the ascending or descending order. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
				
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order by', 'sw_supershop' ),
			'param_name' => 'orderby',
			'value' => array(
				'Select orderby',
				__( 'Date', 'sw_supershop' ) => 'date',
				__( 'ID', 'sw_supershop' ) => 'ID',
				__( 'Author', 'sw_supershop' ) => 'author',
				__( 'Title', 'sw_supershop' ) => 'title',
				__( 'Modified', 'sw_supershop' ) => 'modified',
				__( 'Random', 'sw_supershop' ) => 'rand',
				__( 'Comment count', 'sw_supershop' ) => 'comment_count',
				__( 'Menu order', 'sw_supershop' ) => 'menu_order'
			),
			'description' => sprintf( __( 'Select how to sort retrieved posts. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
			
	)
) );

// ytc tesminial

vc_map( array(
	'name' => 'YTC_ ' . __( 'Testimonial Slide', 'sw_supershop' ),
	'base' => 'testimonial_slide',
	'icon' => 'icon-wpb-ytc',
	'category' => __( 'My shortcodes', 'sw_supershop' ),
	'class' => 'wpb_vc_wp_widget',
	'weight' => - 50,
	'description' => __( 'The tesminial on your site', 'sw_supershop' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'sw_supershop' ),
			'param_name' => 'title',
			'description' => __( 'What text use as a widget title. Leave blank to use default widget title.', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Style title', 'sw_supershop' ),
			'param_name' => 'style_title',
			'value' => array(
				'Select type',
			),
			'description' =>__( 'What text use as a style title. Leave blank to use default style title.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of posts to show', 'sw_supershop' ),
			'param_name' => 'numberposts',
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Excerpt length (in words)', 'sw_supershop' ),
			'param_name' => 'length',
			'description' => __( 'Excerpt length (in words).', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Template', 'sw_supershop' ),
			'param_name' => 'type',
			'value' => array(
				__('Indicators Style 2','sw_supershop') => 'indicators_up3',
				__('Indicators Style 3','sw_supershop') => 'indicators_up4'
				
			),
			'description' => sprintf( __( 'Chose template for testimonial', 'sw_supershop' ) )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order way', 'sw_supershop' ),
			'param_name' => 'order',
			'value' => array(
				__( 'Descending', 'sw_supershop' ) => 'DESC',
				__( 'Ascending', 'sw_supershop' ) => 'ASC'
			),
			'description' => sprintf( __( 'Designates the ascending or descending order. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
				
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order by', 'sw_supershop' ),
			'param_name' => 'orderby',
			'value' => array(
				'Select orderby',
				__( 'Date', 'sw_supershop' ) => 'date',
				__( 'ID', 'sw_supershop' ) => 'ID',
				__( 'Author', 'sw_supershop' ) => 'author',
				__( 'Title', 'sw_supershop' ) => 'title',
				__( 'Modified', 'sw_supershop' ) => 'modified',
				__( 'Random', 'sw_supershop' ) => 'rand',
				__( 'Comment count', 'sw_supershop' ) => 'comment_count',
				__( 'Menu order', 'sw_supershop' ) => 'menu_order'
			),
			'description' => sprintf( __( 'Select how to sort retrieved posts. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
			
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'sw_supershop' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sw_supershop' )
		)
	)
) );

///// Gallery 
vc_map( array(
	'name' => __( 'YTC_Gallery', 'sw_supershop' ),
	'base' => 'gallerys',
	'icon' => 'icon-wpb-images-carousel',
	'category' => __( 'My shortcodes', 'sw_supershop' ),
	'description' => __( 'Animated carousel with images', 'sw_supershop' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'sw_supershop' ),
			'param_name' => 'title',
			'description' => __( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'sw_supershop' )
		),
		array(
			'type' => 'attach_images',
			'heading' => __( 'Images', 'sw_supershop' ),
			'param_name' => 'ids',
			'value' => '',
			'description' => __( 'Select images from media library.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'gallery size', 'sw_supershop' ),
			'param_name' => 'size',
			'description' => __( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size. If used slides per view, this will be used to define carousel wrapper size.', 'sw_supershop' )
		),
		
		array(
			'type' => 'dropdown',
			'heading' => __( 'Gallery caption', 'sw_supershop' ),
			'param_name' => 'caption',
			'value' => array(
				__( 'true', 'sw_supershop' ) => 'true',
				__( 'false', 'sw_supershop' ) => 'false'
			),
			'description' => __( 'Images display caption true or false', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Gallery type', 'sw_supershop' ),
			'param_name' => 'type',
			'value' => array(
				__( 'Column', 'sw_supershop' ) => 'column',
				__( 'Slide1', 'sw_supershop' ) => 'slide',
				__( 'Flex', 'sw_supershop' ) => 'flex',
				__( 'Slide2', 'sw_supershop' ) => 'slider_gallery'
			),
			'description' => __( 'Images display type', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'gallery columns', 'sw_supershop' ),
			'param_name' => 'columns',
			'description' => __( 'Enter gallery columns. Example: 1,2,3,4 ... Only use gallery type="column".', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Slider speed', 'sw_supershop' ),
			'param_name' => 'interval',
			'value' => '5000',
			'description' => __( 'Duration of animation between slides (in ms)', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Gallery event', 'sw_supershop' ),
			'param_name' => 'event',
			'value' => array(
				__( 'slide', 'sw_supershop' ) => 'slide',
				__( 'fade', 'sw_supershop' ) => 'fade'
			),
			'description' => __( 'event slide images', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'sw_supershop' ),
			'param_name' => 'class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sw_supershop' )
		)
		)
) );

/////////////////// ya post slider/////////////////////
vc_map( array(
	'name' => 'Ya_' . __( 'Post Slider', 'sw_supershop' ),
	'base' => 'ya_post_slider',
	'icon' => 'icon-wpb-ytc',
	'category' => __( 'YA Slider', 'sw_supershop' ),
	'class' => 'wpb_vc_wp_widget',
	'weight' => - 50,
	'description' => __( 'Ya Post Slider', 'sw_supershop' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => __( 'Title', 'sw_supershop' ),
			'param_name' => 'title',
			'description' => __( 'What text use as a widget title. Leave blank to use default widget title.', 'sw_supershop' )
		),
		array(
			'param_name'    => 'category_id',
			'type'          => 'dropdown',
			'value'         => $product_categories_dropdown, // here I'm stuck
			'heading'       => __('Category filter:', 'sw_supershop'),
			'description'   => '',
			'holder'        => 'div',
			'class'         => ''
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of post to show', 'sw_supershop' ),
			'param_name' => 'numberposts',
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Excerpt length (in words)', 'sw_supershop' ),
			'param_name' => 'length',
			'description' => __( 'Excerpt length (in words).', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of Columns >1200px:', 'sw_supershop' ),
			'param_name' => 'col_lg',
			'description' => __( 'Number colums you want display  > 1200px.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of Columns on 768px to 1199px:', 'sw_supershop' ),
			'param_name' => 'col_md',
			'description' => __( 'Number colums you want display  on 768px to 1199px.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of Columns on 480px to 767px:', 'sw_supershop' ),
			'param_name' => 'col_sm',
			'description' => __( 'Number colums you want display  on 480px to 767px.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of Columns on 321px to 479px:', 'sw_supershop' ),
			'param_name' => 'col_xs',
			'description' => __( 'Number colums you want display  on 321px to 479px.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number of Columns in 320px or less than:', 'sw_supershop' ),
			'param_name' => 'col_moble',
			'description' => __( 'Number colums you want display  in 320px or less than.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Speed slide', 'sw_supershop' ),
			'param_name' => 'speed',
			'description' => __( 'Speed for slide', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Interval slide', 'sw_supershop' ),
			'param_name' => 'interval',
			'description' => __( 'Interval for slide', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Autoplay for slider', 'sw_supershop' ),
			'param_name' => 'autoplay',
			'value' => array(
				'Select type',
				__( 'True', 'sw_supershop' ) => 'true',
				__( 'False', 'sw_supershop' ) => 'false',
			),
			'description' => sprintf( __( 'Select autoplay slider or not autoplay slider.', 'sw_supershop' ) )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number Slided', 'sw_supershop' ),
			'param_name' => 'number_slided',
			'description' => __( 'Number Slided for slide', 'sw_supershop' )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Layout shortcode want display', 'sw_supershop' ),
			'param_name' => 'layout',
			'value' => array(
				'Select layout',
				__( 'Default', 'sw_supershop' ) => 'default',
			),
			'description' => sprintf( __( 'Select different style posts.', 'sw_supershop' ) )
		),
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order way', 'sw_supershop' ),
			'param_name' => 'order',
			'value' => array(
				__( 'Descending', 'sw_supershop' ) => 'DESC',
				__( 'Ascending', 'sw_supershop' ) => 'ASC'
			),
			'description' => sprintf( __( 'Designates the ascending or descending order. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
				
		array(
			'type' => 'dropdown',
			'heading' => __( 'Order by', 'sw_supershop' ),
			'param_name' => 'orderby',
			'value' => array(
				'Select orderby',
				__( 'Date', 'sw_supershop' ) => 'date',
				__( 'ID', 'sw_supershop' ) => 'ID',
				__( 'Author', 'sw_supershop' ) => 'author',
				__( 'Title', 'sw_supershop' ) => 'title',
				__( 'Modified', 'sw_supershop' ) => 'modified',
				__( 'Random', 'sw_supershop' ) => 'rand',
				__( 'Comment count', 'sw_supershop' ) => 'comment_count',
				__( 'Menu order', 'sw_supershop' ) => 'menu_order'
			),
			'description' => sprintf( __( 'Select how to sort retrieved posts. More at %s.', 'sw_supershop' ), '<a href="http://codex.wordpress.org/Class_Reference/WP_Query#Order_.26_Orderby_Parameters" target="_blank">WordPress codex page</a>' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'sw_supershop' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sw_supershop' )
		),	
	)
) );
/*===================================================Counter Box===========================================*/
vc_map( array(
	'name' => 'YTC_' . __( 'Counter', 'sw_supershop' ),
	'base' => 'counters',
	'icon' => 'icon-wpb-ytc',
	'category' => __( 'My shortcodes', 'sw_supershop' ),
	'class' => 'wpb_vc_wp_widget',
	'weight' => - 50,
	'description' => __( 'Display counter box', 'sw_supershop' ),
	'params' => array(
	    array(
			'type' => 'dropdown',
			'heading' => __( 'Style', 'sw_supershop' ),
			'param_name' => 'style',
			'value' => array(
				'Select type',
				__( 'Border', 'sw_supershop' ) => 'border',
				__( 'Background', 'sw_supershop' ) => 'bg',
			),
			'description' => sprintf( __( 'Select different style counter.', 'sw_supershop' ) )
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Widget title', 'sw_supershop' ),
			'param_name' => 'title',
			'description' => __( 'What text use as a widget title. Leave blank to use default widget title.', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Type', 'sw_supershop' ),
			'param_name' => 'type',
			'description' =>__( 'show type', 'sw_supershop' )
		),
		array(
			'type' => 'textfield',
			'heading' => __( 'Number to show', 'sw_supershop' ),
			'param_name' => 'number',
			'admin_label' => true
		),
		
		array(
			'type' => 'textfield',
			'heading' => __( 'Extra class name', 'sw_supershop' ),
			'param_name' => 'el_class',
			'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'sw_supershop' )
		),	
	)
) );

}
?>