<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce; ?>

<?php do_action('woocommerce_before_customer_login_form'); ?>
<form action="<?php echo get_permalink( woocommerce_get_page_id( 'myaccount' ) ); ?>" method="post" class="login">
			<input name="form_key" type="hidden" value="lDLFLGU1hYlZ9gVL">
			<div class="block-content">
				<div class="col-reg registered-account">
					<div class="email-input">
						<input type="text" class="form-control input-text username" name="username" id="username" placeholder="Username or Email" />
					</div>
					<div class="pass-input">
						<input class="form-control input-text password" type="password" placeholder="Password" name="password" id="password" />
					</div>
					<div class="actions">
						<div class="submit-login">
							<?php wp_nonce_field( 'woocommerce-login' ); ?>
			                <input type="submit" class="button btn-submit-login" name="login" value="<?php esc_html_e( 'Login', 'sw_supershop' ); ?>" />
						</div>	
					</div>
					<div class="ft-link-p">
						<a href="<?php echo esc_url( wc_lostpassword_url() ); ?>" title="Forgot your password"><?php esc_html_e( 'Forgot password?', 'sw_supershop' ); ?></a>
					</div>
				</div>
				<div class="col-reg login-customer">
					<p class="note-reg"><?php esc_html_e( 'Not registered yet? ', 'sw_supershop' ); ?><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="Register" class="btn-reg-popup"><?php _e( 'Create an account', 'sw_supershop' ); ?></a></p>
					<ul class="list-log">
						<li><a class="facebook-connect" href="https://www.facebook.com/flytheme" title="Facebook Connect"></a></li>
						<li><a class="twitter-connect" href="https://twitter.com/Flytheme" title="Twitter Connect"></a></li>
						<li><a class="google-plus-connect" href="https://plus.google.com/u/0/b/102399087761949580069/102399087761949580069/posts" title="Google Plus Connect"></a></li>
					</ul>
				</div>
				<div style="clear:both;"></div>
			</div>
		</form>
<div class="clear"></div>
	
<?php do_action('woocommerce_after_cphone-icon-login ustomer_login_form'); ?>