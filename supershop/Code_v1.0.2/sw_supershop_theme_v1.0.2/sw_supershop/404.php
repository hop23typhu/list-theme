<?php get_template_part('header'); ?>
<div class="container">
		<div class="row">
            <div class="col-lg-12 col-md-12 row">
				<div class="col-1-wrapper">
					<div class="std"><div class="wrapper_404page">
						<div class="col-lg-12 col-md-12">
							<div class="content-404page">
								<p class="top-text"><?php esc_html_e( "Don't worry you will be back on track in no time!", 'sw_supershop' )?></p>
								<p class="img-404"><span></span></p>
								<p class="bottom-text"><?php esc_html_e( "Page doesn't exist or some other error occured. Go to", 'sw_supershop' )?> <a href="<?php echo esc_url( home_url('/') ) ; ?>" ><?php esc_html_e( " home page ", 'sw_supershop' ); ?></a><?php esc_html_e( "or go back to previous page", 'sw_supershop' ) ;?> </p>
								<div class="button-404">
									
								</div>
							</div>
						</div>
				<div style="clear:both; height:0px">&nbsp;</div>
				<script>
				function goBack() {
					window.history.back()
				}
				</script>
					</div>
					</div> 
				<form role="search" method="get" class="form-search searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<label class="hide"><?php esc_html_e('Search for:', 'sw_supershop'); ?></label>
					<input type="text" value="<?php if (is_search()) { echo get_search_query(); } ?>" name="s" class="search-query" placeholder="Search...">
					<button type="submit" class="icon-search button-search-pro form-button"></button>
				</form>
				</div>
			</div>
        </div>
</div>
<?php get_template_part('footer'); ?>