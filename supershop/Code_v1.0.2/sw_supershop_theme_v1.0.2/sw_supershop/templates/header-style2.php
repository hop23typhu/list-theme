<header id="header"  class="header">
    <div class="header-msg">
		<div class="menu-closes"></div>
        <?php if (is_active_sidebar_YA('top-home2')) {?>
            <div id="sidebar-top" class="sidebar-top">
                <?php dynamic_sidebar('top-home2'); ?>
            </div>
        <?php }?>
    </div>
	<div class="container top">
		<div class="header-fix">
			<div class="row">
				<div class="top-header col-lg-4 col-md-3 col-xs-12 pull-left">
					<div class="ya-logo">
						<a  class="logo-big" href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php $logo = get_template_directory_uri().'/assets/img/logo-home2.png'; ?>
							<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php bloginfo('name'); ?>"/>
						</a>
					</div>
				</div>
				<div id="sidebar-top-header" class="sidebar-top-header col-lg-8 col-md-9 col-xs-12 pull-right">
					<a class="home-menu-verticle " href="javascript:void(0)" title="Search"></a>
					<div id="sidebar-top-menu" class="sidebar-top-menu">
						<?php get_template_part( 'woocommerce/minicart-ajax-style1' ); ?>
					</div>
					<a class="home-search" href="javascript:void(0)" title="Search"></a>
					<div class="widget ya_top-3 ya_top non-margin pull-right">
						<div class="widget-inner">
							<?php get_template_part( 'widgets/ya_top/searchcate' ); ?>
						</div>
					</div>
					<?php if ( has_nav_menu('primary_menu') ) {?>
						<!-- Primary navbar -->
					<div id="main-menu" class="main-menu pull-right">
						<nav id="primary-menu" class="primary-menu">
							<div class="container">
								<div class="mid-header clearfix">
									<a href="javascript:void(0)" class="phone-icon-menu"></a>
									<div class="navbar-inner navbar-inverse">
											<?php
												$ya_menu_class = 'nav nav-pills';
												if ( 'mega' == ya_options()->getCpanelValue('menu_type') ){
													$ya_menu_class .= ' nav-mega';
												} else $ya_menu_class .= ' nav-css';
											?>
											<?php wp_nav_menu(array('theme_location' => 'primary_menu', 'menu_class' => $ya_menu_class)); ?>
									</div>
								</div>
							</div>
						</nav>
					</div>
						<!-- /Primary navbar -->
					<?php 
						} 
					?>
				</div>
			</div>
		</div>
	</div>
</header>

