<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage tfbasedetails
 * @since tfbasedetails 1.0
 */
?>
</div>
<footer id="site-footer" role="contentinfo">
    <div class="container">
        <?php
        get_sidebar('footer');
        ?>
        <div class="clearboth"></div> 
    </div>
</footer>
<div class="clearboth"></div> 
<div class="bottomnav">
    <div class="container">
        <section class="botwrap">
            <div class="two_third first">
                <?php wp_nav_menu(array('theme_location' => 'Footer Navigation', 'container' => 'ul', 'menu_id' => 'footer-nav', 'depth' => '1', 'menu_class' => 'footer_nav fl')); ?>
            </div>
            <div class="bottextright one_third">
                <a href="<?php echo home_url('/') ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php _e('Copyright', 'tfbasedetails'); ?> <?php bloginfo('name'); ?></a>
            </div>
        </section>  
    </div>
</div>
<div class="clearboth"></div>
<?php
/* Always have wp_footer() just before the closing </body>
 * tag of your theme, or you will break many plugins, which
 * generally use this hook to reference JavaScript files.
 */
wp_footer();
?>
</body>
</html>