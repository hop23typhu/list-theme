<?php
/**
 * 
 * @package WordPress
 * @subpackage tfbasedetails
 * @since tfbasedetails 1.0
 */
global $woo_options;
global $woocommerce;
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<html <?php language_attributes(); ?> class="no-js">
    <!--<![endif]-->

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php tf_wp_title(); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:700' rel='stylesheet' type='text/css'>
        <?php if (get_option('tf_optstylesheet') != null) : ?>
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styleoptions/<?php echo get_option('tf_optstylesheet'); ?>" type="text/css" media="screen" />
            <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styleoptions/switcher.php?default=default.css" type="text/css" media="screen" />  
        <?php endif; ?> 
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php
        if (is_singular() && get_option('thread_comments'))
            wp_enqueue_script('comment-reply');
        ?>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class('boxed'); ?>>
        <header id="top" role="banner">
            <div id="toplinks">
                <div class="container">
                    <div id="vtoplinks" class="clearfix">
                        <?php if (get_option('tf_enable_wooheadercart') == 'true') : ?>
                            <?php echo tf_woocommerce_cart_dropdown(); ?>
                        <?php endif; ?>
                        <div class="sub_menu">
                            <?php
                            echo tf_shop_nav();
                            ?>
                        </div>
                        <?php if (get_option('tf_enable_smicons') == 'true') : ?>
                            <div class="social-icons" id="slogan"> 
                                <?php if (get_option('tf_fb_link') != null) : ?>
                                    <a class="facebook" href="<?php echo get_option('tf_fb_link'); ?>">f</a>
                                <?php endif; ?>            
                                <?php if ((get_option('tf_fb_link') != null) && ((get_option('tf_tw_link') != null))) : ?>
                                <?php endif; ?>
                                <?php if (get_option('tf_tw_link') != null) : ?>
                                    <a class="twitter" href="<?php echo get_option('tf_tw_link'); ?>">l</a> 
                                <?php endif; ?>
                                <?php if (get_option('tf_gplus_link') != null) : ?>
                                    <?php if ((get_option('tf_fb_link') != null) || ((get_option('tf_tw_link') != null))) : ?>
                                    <?php endif; ?>     
                                    <a class="google" href="<?php echo get_option('tf_gplus_link'); ?>">g</a>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div id="topbar">
                <div class="container">
                    <div id="toptop" class="clearfix">
                    <div id="logosearch" class="clearfix">
                        <?php /*
                          If "plain text logo" is set in theme options then use text
                          if a logo url has been set in theme options then use that
                          if none of the above then use the default logo.png */
                        if (get_option('tf_logotype') == 'text') { ?>

                            <div class="headerwrap one_third first">

                                <h1><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php echo get_option('tf_textlogo'); ?></a></h1>
                                <p><?php echo get_option('tf_logotag') ?></p>
                            </div>        

                        <?php } elseif ((get_option('tf_logotype') == 'image')) { ?>
                            <div class="headerwrap-img one_third first">
                                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_option('tf_sitelogo'); ?>" alt="<?php bloginfo('name'); ?>"></a> </div>
                        <?php } else { ?> 

                            <div class="headerwrap-img one_third first">
                                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>"></a> </div> <?php } ?>
                        <div class="header-widget two_third">
                            <?php dynamic_sidebar('header-widget-area'); ?>
                        </div>
                    </div>
                    </div>
                                        <nav id="access" class="navbg" role="navigation">
                        <div id="navigation" class="clearfix">
                            <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?><a id="skip" href="#content" class="hidden" title="<?php esc_attr_e('Skip to content', 'tfbasedetails'); ?>"><?php _e('Skip to content', 'tfbasedetails'); ?></a> <?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?> <?php
                            if (function_exists('has_nav_menu') && has_nav_menu('Main Navigation')) {
                                wp_nav_menu(array('theme_location' => 'Main Navigation', 'container' => 'ul', 'menu_id' => 'main-nav', 'menu_class' => 'nav fl', 'walker' => new menu_walker()));
                            } else {
                                ?>

                                <ul id="nav" class="nav fl">
                                    <?php
                                    if (is_page())
                                        $highlight = "page_item"; else
                                        $highlight = "page_item current_page_item";
                                    ?>
                                    <li class="<?php echo $highlight; ?>"><a href="<?php echo home_url(); ?>"><?php _e('Home', 'tfbasedetails') ?></a></li><?php wp_list_pages('sort_column=menu_order&depth=6&title_li=&exclude='); ?>
                                </ul><!-- /#nav -->
                            <?php } ?>
                        </div>
                        <?php
                        dropdown_menu(array(
                            'theme_location' => 'Main Navigation',
                            'menu_id' => 'select-main-nav',
                            // You can alter the blanking text eg. "- Menu Name -" using the following
                            'dropdown_title' => '-- Select a page --',
                            // indent_string is a string that gets output before the title of a
                            // sub-menu item. It is repeated twice for sub-sub-menu items and so on
                            'indent_string' => '- ',
                            // indent_after is an optional string to output after the indent_string
                            // if the item is a sub-menu item
                            'indent_after' => ''
                        ));
                        ?>
                    </nav>
                </div>
            </div>
        </header>