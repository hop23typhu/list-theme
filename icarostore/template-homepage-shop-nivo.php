<?php
/**
 * Template Name: Shop Homepage :: Nivo Slider
 *
 * @package WordPress
 * @subpackage tfbasedetails
 * @since tfbasedetails 1.0
 */
get_header();
?>
<div id="wrap_all">
    <aside id="hpslider" class="clearfix">
        <div class="container">
            <div class="hpslider">
                <div class="home-banner-wrap">
                    <div id="slider" class="nivoSlider">
                        <?php
                        remove_filter('pre_get_posts', 'wploop_exclude');
                        $query_string = "post_type=tf_hpslide&posts_per_page=10";
                        query_posts($query_string);
                        if (have_posts()) : while (have_posts()) : the_post();
                                $jcycle_sliderid = '#slider-caption-' . get_the_ID();
                                $jcycle_url = get_post_meta($post->ID, '_jcycle_url_value', true);
                                ?>
                                <?php //get_the_image(array('size' => 'slide-hp', 'title' => '$jcycle_sliderid',  'echo' => true, 'link_to_post' => false, 'width' => 960, 'height' => 300)); ?>
                                <?php the_post_thumbnail('slide-hp', array('title' => '' . $jcycle_sliderid . '')); ?>

                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>

                    <?php $pj_slider_caption = new WP_Query('post_type=tf_hpslide&showposts=10');
                    while ($pj_slider_caption->have_posts()) : $pj_slider_caption->the_post(); ?>

                        <div id="slider-caption-<?php the_ID(); ?>" class="nivo-html-caption">                  
                            <div class="nivo-slidewrap">
                                <h2><a href="<?php echo get_post_meta($post->ID, 'hpslide_url', true); ?>"><?php the_title(); ?></a></h2>
    <?php $pj_nivo_caption_content = get_the_excerpt(); ?>
                                <p><?php echo $pj_nivo_caption_content; ?></p>
                            </div>
                            <?php if (get_post_meta($post->ID, 'hpslide_pricemsg', true)) { ?>
                                <div class="hpslide_pricewrap">
                                    <a href="<?php echo get_post_meta($post->ID, 'hpslide_url', true); ?>">
                                        <span class="hpslide_pricemsg"><?php echo get_post_meta($post->ID, 'hpslide_pricemsg', true); ?></span>
                                        <span class="hpslide_price"><?php echo get_post_meta($post->ID, 'hpslide_price', true); ?></span>
                                    </a>
                                </div> 
                            <?php }
                            ?>                       
                        </div><!-- // nivo-html-caption -->

                    <?php endwhile; ?>

                </div>
            </div> 
        </div>
    </aside>
        <section id="hpmiddle" role="contentinfo" class="container">
        <div class="content twelve alpha units">
            <?php get_sidebar('center-middle'); ?>
        </div>
    </section>  
    <section id="hpshop" role="contentinfo" class="container clearfix">
        <section class="woohpwidgets-main">
                <div class="hpleft clearfix">
                    <?php get_sidebar('center-bottom'); ?>
                </div>
        </section>

        <section class="woohpwidgets-side hpdark">
                <div class="hpright">
                    <div class="hpnanoscroll">
                        <?php dynamic_sidebar('homepage-sidebar-widget-area'); ?>                        
                    </div>
                </div>
        </section>

    </section>  
    <div class="clearboth"></div>
    <?php get_footer(); ?>