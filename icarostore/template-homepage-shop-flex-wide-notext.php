<?php
/**
 * Template Name: Shop Homepage :: Flex Slider : No Text - Wide 
 *
 * @package WordPress
 * @subpackage tfbasedetails
 * @since tfbasedetails 1.0
 */
get_header();
?>
<div id="wrap_all">
    <aside id="hpslider" class="clearfix">
        <div class="container">
            <div class="hpslider">
                <div class="home-banner-wrap preloading flexslider">
                    <ul class="slides">
                        <?php
                        remove_filter('pre_get_posts', 'wploop_exclude');
                        $query_string = "post_type=tf_hpslide&posts_per_page=10";
                        query_posts($query_string);
                        if (have_posts()) : while (have_posts()) : the_post();
                                $jcycle_url = get_post_meta($post->ID, '_jcycle_url_value', true);
                                ?>
                                <li class="slide">
                                            <a href="<?php echo get_post_meta($post->ID, 'hpslide_url', true); ?>">
                                                <?php get_the_image(array('size' => 'slide-hp', 'echo' => true, 'link_to_post' => false, 'width' => 960, 'height' => 300)); ?>
                                            </a>                       
                                </li>   
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </ul>
                    <!-- <div class="hpslider-pager">&nbsp;</div> -->
                </div>
            </div> 
        </div>
    </aside>    
        <section id="hpmiddle" role="contentinfo" class="container">
        <div class="content twelve alpha units">
            <?php get_sidebar('center-middle'); ?>
        </div>
    </section>  
    <section id="hpshop" role="contentinfo" class="container clearfix">
        <section class="woohpwidgets-main-wide">
                <div class="hpleft clearfix">
                    <?php get_sidebar('center-bottom'); ?>
                </div>
        </section>
    </section>  
    <div class="clearboth"></div>
    <?php get_footer(); ?>