<?php
/**
 * @package WordPress default theme
 * @subpackage Deeds theme of qsoft
 * @since deeds 1.0.1
 */

add_action( 'after_setup_theme', 'deeds_start_setup' );

if ( ! function_exists( 'deeds_start_setup' ) ) {
	function deeds_start_setup() {
		global $themename, $shortname, $options, $theme_path, $theme_uri;
		$themename  = 'deeds';
		$shortname  = 'deeds';
		
		$theme_path = get_template_directory();
		$theme_uri  = get_template_directory_uri();
		
		require_once( $theme_path . '/includes/install_plugins.php' );
		require_once( $theme_path . '/d_options/d-display.php' );
		require_once( $theme_path . '/d_options/d-custom.php' );
		require_once( $theme_path . '/d_options/inc/mailchimp/d-mailchimp.php' );
		require_once( $theme_path . '/includes/functions_frontend.php' );
		require_once( $theme_path . '/includes/functions_init.php' );
		require_once( $theme_path . '/includes/functions_styles.php' );//print option  Typography on head tag
		require_once( $theme_path . '/includes/functions_shortcodes.php' );// make shortcode for website . All shortcode here
		require_once( $theme_path . '/includes/function_ajax.php' );//process ajax 
		require_once( $theme_path . '/includes/custom-header.php' );
		require_once( $theme_path . '/includes/template-tags.php' );
		require_once( $theme_path . '/includes/extras.php' );
		require_once( $theme_path . '/includes/customizer.php' );
		require_once( $theme_path . '/includes/jetpack.php' );

		load_theme_textdomain( 'deeds', get_template_directory() . '/languages' );
		
		add_theme_support( 'automatic-feed-links' );//make feed link in head
		add_theme_support( 'title-tag' );//make auto title tag
		add_theme_support( 'html5', array(//support html5 tags 
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'post-formats', array(//support some post format in post
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );
		//custom your template
		add_theme_support( 'custom-background', apply_filters( 'deeds_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
}
//unregister some widgets
 function remove_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Links');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Search');
     unregister_widget('WP_Widget_Text');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
 }
 add_action('widgets_init', 'remove_default_widgets', 11);
