<?php get_header(); ?>

<!-- ==================start content body section=============== -->
<section id="contentbody">
  <div class="container">
    <div class="row">
    <!-- start left bar content -->
      <div class=" col-sm-12 col-md-6 col-lg-6">
      <?php 
          $args=array(
             'taxonomy'           => 'category',
             'hide_empty'         => 0, 
             'show_option_none'   => '---chon danh muc---',
             'option_none_value'  => 'none',
             'selected'           => 22,
          );
          wp_dropdown_categories( $args );
        ?>
        <div class="row">
          <div class="leftbar_content">
            <h2>Bài viêt gần đây</h2>


            <?php 
            	if(have_posts()):while(have_posts()):the_post();
            ?>
            <!-- start single stuff post -->
            <div class="single_stuff wow fadeInDown">
              <div class="single_stuff_img">
                <a href="<?php the_permalink(); ?>">
                <?php 
                	if(has_post_thumbnail()) the_post_thumbnail('blog-thumb',array('class'=>'img-responsive','alt'=>get_the_title()));
                	else echo '<img src=""/>';
                ?>
                </a>
              </div>
              <div class="single_stuff_article">
                  <div class="single_sarticle_inner">
                  		<?php the_category(' '); ?>
                    <div class="stuff_article_inner">
                      <span class="stuff_date"><?php the_date('d-m'); ?> <strong></strong></span>
                      <h2>
                      	<a href="<?php the_permalink(); ?>">
                      		<?php the_title(); ?>
                      	</a>
                      </h2>
                      <p><?php the_excerpt(); ?></p>
                    </div>
                  </div>
              </div>
            </div>
            <!-- End single stuff post -->
			<?php 
				endwhile;
				else :get_template_part('template-parts','none'); 
				endif;
			?>	
          

            <div class="stuffpost_paginatinonarea wow slideInLeft">
              <ul class="newstuff_pagnav">
                <li><a class="active_page" href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
              </ul>
            </div>
          </div>
        </div>  
      </div>
      <!-- End left bar content -->
	  <?php get_sidebar(); ?>
      
    </div>
  </div>
</section>
<?php get_footer(); ?>