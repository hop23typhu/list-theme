<!-- start middle bar content -->
<div class="col-sm-6 col-md-2 col-lg-2">
<div class="row">
  <div class="middlebar_content">
  <h2 class="yellow_bg">What's Hot</h2>
  <div class="middlebar_content_inner wow fadeInUp">
    <ul class="middlebar_nav">
      <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img2.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
        <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
        <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
        <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
        <li>
        <a class="mbar_thubnail" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg" alt="img"></a>
        <a class="mbar_title" href="#">Sed luctus semper odio aliquam rhoncus</a>
      </li>
    </ul>
  </div>
  <div class="popular_categori  wow fadeInUp">
    <h2 class="limeblue_bg">Most Popular Categories</h2>
    <ul class="poplr_catgnva">
        <li><a href="#">Business</a></li>
        <li><a href="#">Gallery</a></li>
        <li><a href="#">Life & Style</a></li>
        <li><a href="#">Games</a></li>
        <li><a href="#">Slider</a></li>
        <li><a href="#">Sports</a></li>
      </ul>  
  </div>        
</div>
</div>
</div>
<!-- End middle bar content -->

<div class="col-sm-6 col-md-4 col-lg-4">
<div class="row">
  <div class="rightbar_content">
  <!-- start featured post -->
    <div class="single_blog_sidebar wow fadeInUp">
    <h2>The Featured Stuff</h2>  
    <ul class="featured_nav">
      <li>
        <a class="featured_img" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/featured_img1.jpg" alt="img"></a>
        <div class="featured_title">
          <a class="" href="#">Sed luctus semper odio aliquam rhoncus</a>
        </div>
      </li>
      <li>
        <a class="featured_img" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/featured_img1.jpg" alt="img"></a>
        <div class="featured_title">
          <a class="" href="#">Sed luctus semper odio aliquam rhoncus</a>
        </div>
      </li>
      <li>
        <a class="featured_img" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/featured_img1.jpg" alt="img"></a>
        <div class="featured_title">
          <a class="" href="#">Sed luctus semper odio aliquam rhoncus</a>
        </div>
      </li>
    </ul>
    </div>
    <!-- End featured post -->

    <!-- start Popular Posts -->
    <div class="single_blog_sidebar wow fadeInUp">
    <h2>Popular Posts</h2>  
    <ul class="middlebar_nav wow">
      <li>
        <a href="#" class="mbar_thubnail"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg"></a>
        <a href="#" class="mbar_title">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a href="#" class="mbar_thubnail"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/hot_img2.jpg"></a>
        <a href="#" class="mbar_title">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a href="#" class="mbar_thubnail"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg"></a>
        <a href="#" class="mbar_title">Sed luctus semper odio aliquam rhoncus</a>
      </li>
       <li>
        <a href="#" class="mbar_thubnail"><img alt="img" src="<?php echo get_template_directory_uri(); ?>/img/hot_img1.jpg"></a>
        <a href="#" class="mbar_title">Sed luctus semper odio aliquam rhoncus</a>
      </li>
    </ul>
    </div>
    <!-- End Popular Posts -->  

    <!-- start Popular Posts -->
    <div class="single_blog_sidebar wow fadeInUp">
    <h2>Popular Tags</h2>  
    <ul class="poplr_tagnav">
      <li><a href="#">Arts</a></li>
      <li><a href="#">Games</a></li>
      <li><a href="#">Nature</a></li>
      <li><a href="#">Comedy</a></li>
      <li><a href="#">Sports</a></li>
      <li><a href="#">Tourism</a></li>
      <li><a href="#">Videos</a></li>
    </ul>
    </div>
    <!-- End Popular Posts -->              
  </div>
</div>
</div>