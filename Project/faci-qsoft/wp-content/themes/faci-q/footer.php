    <footer id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="footer_inner">
              <p class="pull-left">All Rights Reserved <a href="#">ColorMag</a></p>
              <p class="pull-right">Developed By <a href="http://www.wpfreeware.com" rel="nofollow">WpFreeware</a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- ==================End content body section=============== -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
    <!-- For content animatin  -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script> 
    <!-- custom js file include -->
     <script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>     
    <?php wp_footer(); ?>
  </body>
</html>