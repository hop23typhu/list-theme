<?php
if ( ! function_exists( 'jm_render_field' ) ) :
	function jm_render_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		switch ( $field['type'] ) {
			case "textarea":
				jm_render_textarea_field( $field, $field_id, $value, $form_type );
				break;
			case "select":
			case "multiple_select":
				jm_render_select_field( $field, $field_id, $value, $form_type );
				break;
			case "radio" :
				jm_render_radio_field( $field, $field_id, $value, $form_type );
				break;
			case "checkbox" :
				jm_render_checkbox_field( $field, $field_id, $value, $form_type );
				break;
			case "text" :
				jm_render_text_field( $field, $field_id, $value, $form_type );
				break;
			default :
				do_action( 'jm_render_field_' . $field['type'], $field, $field_id, $value, $form_type );
				break;
		}
	}
endif;
if ( ! function_exists( 'jm_render_text_field' ) ) :
	function jm_render_text_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		$field['value'] = isset( $field['value'] ) ? $field['value'] : '';
		$input_id = $form_type == 'search' ? 'search-' . $field_id : $field_id;
		$class = isset($field['required']) && $field['required'] ? ' class="form-control jform-validate" required aria-required="true"' : ' class="form-control"';
		?>
		<input id="<?php echo esc_attr($input_id)?>" <?php echo $class; ?> type="text" name="<?php echo esc_attr($field_id)?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo $field['value']; ?>"/>
		<?php
	}
endif;
if ( ! function_exists( 'jm_render_textarea_field' ) ) :
	function jm_render_textarea_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		$field['value'] = isset( $field['value'] ) ? $field['value'] : '';
		$input_id = $form_type == 'search' ? 'search-' . $field_id : $field_id;
		$class = isset($field['required']) && $field['required'] ? ' class="form-control jform-validate" required aria-required="true"' : ' class="form-control"';
		?>
		<textarea <?php echo $class; ?> id="<?php echo esc_attr($input_id)?>"  name="<?php echo esc_attr($field_id)?>" placeholder="<?php echo $field['value']; ?>" rows="8"><?php echo esc_html($value); ?></textarea>
		<?php
	}

endif;
if ( ! function_exists( 'jm_render_radio_field' ) ) :
	function jm_render_radio_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		$field['value'] = isset( $field['value'] ) ? $field['value'] : array();
		$input_id = $form_type == 'search' ? 'search-' . $field_id : $field_id;
		$class = isset($field['required']) && $field['required'] ? ' class="form-control jform-validate" required aria-required="true"' : ' class="form-control"';

		$list_option = !is_array( $field['value'] ) ? explode( "\n", $field['value'] ) : $field['value'];
		if( $form_type == 'search' ) {
			array_unshift( $list_option, '|' . __('All', 'noo') );
		}
		foreach ($list_option as $index => $option) :
			$option_key = explode( '|', $option );
			$option_key[0] = trim( $option_key[0] );
			if( $index > 0 && empty( $option_key[0] ) ) continue;
			$option_key[1] = isset( $option_key[1] ) ? $option_key[1] : $option_key[0];
			$option_key[0] = sanitize_title( $option_key[0] );
			$checked = ( $option_key[0] == $value || ( empty( $value ) && $index == 0 ) ) ? 'checked="checked"' : '';
		?>
			<div id="<?php echo esc_attr($input_id); ?>" class="form-control-flat">
				<label class="radio">
					<input type="radio" name="<?php echo esc_attr($field_id); ?>" value="<?php echo esc_attr($option_key[0]); ?>" <?php echo $class; ?> <?php echo esc_attr($checked); ?>><i></i><?php echo esc_attr($option_key[1]); ?>
				</label>
			</div>
		<?php endforeach;
	}

endif;
if ( ! function_exists( 'jm_render_checkbox_field' ) ) :
	function jm_render_checkbox_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		$field['value'] = isset( $field['value'] ) ? $field['value'] : array();
		$input_id = $form_type == 'search' ? 'search-' . $field_id : $field_id;
		$class = isset($field['required']) && $field['required'] ? ' class="form-control jform-validate" required aria-required="true"' : ' class="form-control"';

		$list_option = !is_array( $field['value'] ) ? explode( "\n", $field['value'] ) : $field['value'];
		if( !is_array( $value ) ) $value = noo_json_decode( $value ); ?>
		<?php foreach ($list_option as $option) :
			$option_key = explode( '|', $option );
			$option_key[0] = trim( $option_key[0] );
			if( empty( $option_key[0] ) ) continue;
			$option_key[1] = isset( $option_key[1] ) ? $option_key[1] : $option_key[0];
			$option_key[0] = sanitize_title( $option_key[0] );
			$checked = in_array($option_key[0], $value) ? 'checked="checked"' : '';
			?>
			<div id="<?php echo esc_attr($input_id)?>" class="checkbox">
				<div class="form-control-flat">
					<label class="checkbox">
						<input name="<?php echo $field_id; ?>[]" type="checkbox" <?php echo $class; ?> <?php echo $checked; ?> value="<?php echo esc_attr( $option_key[0] ); ?>" /><i></i> 
						<?php echo $option_key[1]; ?>
					</label>
				</div>
			</div>
		<?php endforeach;
	}

endif;
if ( ! function_exists( 'jm_render_select_field' ) ) :
	function jm_render_select_field( $field = array(), $field_id = '', $value = '', $form_type = '' ) {
		$field['value'] = isset( $field['value'] ) ? $field['value'] : array();
		$input_id = $form_type == 'search' ? 'search-' . $field_id : $field_id;
		$rtl_class = is_rtl() ? ' chosen-rtl' : '';
		$class = isset($field['required']) && $field['required'] ? ' class="form-control form-control-chosen ignore-valid jform-chosen-validate' . $rtl_class . '" required aria-required="true"' : ' class="form-control form-control-chosen ignore-valid' . $rtl_class . '"';

		$is_multiple_select = isset( $field['type'] ) && $field['type'] === 'multiple_select';
		$value = ( $is_multiple_select && !is_array( $value ) ) ? noo_json_decode( $value ) : $value;

		$list_option = !is_array( $field['value'] ) ? explode( "\n", $field['value'] ) : $field['value'];
		if( !$is_multiple_select ) {
			array_unshift( $list_option, '' );
		}
		$placeholder = $form_type != 'search' ? sprintf( __("Select %s",'noo'), $field['label'] ) : sprintf( __("All %s",'noo'), $field['label'] );
		?>
		<?php if( $is_multiple_select ) : ?>
			<select id="<?php echo esc_attr($input_id)?>" <?php echo $class; ?> name="<?php echo esc_attr($field_id); ?>[]" multiple="multiple" data-placeholder="<?php echo $placeholder; ?>">
		<?php else : ?>
			<select id="<?php echo esc_attr($input_id)?>" <?php echo $class; ?> name="<?php echo esc_attr($field_id); ?>" data-placeholder="<?php echo $placeholder; ?>">
		<?php endif; ?>
			<?php
				foreach ($list_option as $index => $option) {
					$option_key = explode( '|', $option );
					$option_key[0] = trim( $option_key[0] );
					if( $index > 0 && empty( $option_key[0] ) ) continue;
					$option_key[1] = isset( $option_key[1] ) ? $option_key[1] : $option_key[0];
					$option_key[0] = sanitize_title( $option_key[0] );
					if( is_array( $value ) ) {
						$selected = in_array($option_key[0], $value) ? 'selected="selected"' : '';
					} else {
						$selected = ( $option_key[0] == $value ) ? 'selected="selected"' : '';
					}
					?>
					<option value="<?php echo $option_key[0]; ?>" <?php echo $selected; ?> class="<?php echo esc_attr( $option_key[0] ); ?>" ><?php echo $option_key[1]; ?></option>
					<?php
				}
			?>
		</select>
		<?php
	}
endif;
