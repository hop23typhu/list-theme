<?php

if ( ! function_exists( 'jm_custom_fields_type' ) ) :
	function jm_custom_fields_type() {
		
		$types = array(
			'text'				=> __( 'Text', 'noo' ),
			'number'			=> __( 'Number', 'noo' ),
			'textarea'			=> __( 'Textarea', 'noo' ),
			'select'			=> __( 'Select', 'noo' ),
			'multiple_select'	=> __( 'Multiple Select', 'noo' ),
			'radio'				=> __( 'Radio', 'noo' ),
			'checkbox'			=> __( 'Checkbox', 'noo' ),
		);

		return apply_filters( 'jm_custom_fields_type', $types );
	}
endif;

if ( ! function_exists( 'jm_get_custom_fields' ) ) :
	function jm_get_custom_fields( $setting_name = '', $wpml_prefix = '' ) {
		
		$custom_fields = array();
		if( is_string( $setting_name ) ) {
			$custom_fields = jm_get_setting($setting_name, array());
			$custom_fields = isset( $custom_fields['custom_field'] ) ? $custom_fields['custom_field'] : $custom_fields;
		} elseif ( is_array( $setting_name ) ) {
			$custom_fields = isset( $setting_name['custom_field'] ) ? $setting_name['custom_field'] : $setting_name;
		}
		
		if( !$custom_fields || !is_array($custom_fields) ) {
			$custom_fields = array();
		}

		// __option__ is reserved for other setting
		if( isset( $custom_fields['__options__'] ) ) {
			unset( $custom_fields['__options__'] );
		}

		if( defined( 'ICL_SITEPRESS_VERSION' ) ) {
			$wpml_prefix = empty( $wpml_prefix ) ? $setting_name . '_' : $wpml_prefix;
			foreach ($custom_fields as $index => $custom_field) {
				if( !is_array($custom_field) ) continue;
				
				$custom_fields[$index]['label_translated'] = apply_filters('wpml_translate_single_string', @$custom_field['label'], 'NOO Custom Fields', $wpml_prefix. sanitize_title(@$custom_field['name']), apply_filters( 'wpml_current_language', null ) );
			}
		}

		return $custom_fields;
	}
endif;

if ( ! function_exists( 'jm_merge_custom_fields' ) ) :
	function jm_merge_custom_fields( $default_fields = array(), $custom_fields = array() ) {
		
		// $custom_fields = array_merge( array_diff_key($default_fields, $custom_fields), $custom_fields );
		foreach ( array_reverse( $default_fields ) as $key => $field ) {
			if( array_key_exists( $key, $custom_fields ) ) {
				$custom_fields[$key]['is_default'] = true;
				$custom_fields[$key]['is_tax'] = isset( $field['is_tax'] );
				$custom_fields[$key]['allowed_type'] = isset( $field['allowed_type'] ) ? $field['allowed_type'] : null;
				// $custom_fields[$key]['label'] = isset( $field['label'] ) ? $field['label'] : $custom_fields[$key]['label'];
				// unset( $custom_fields[$key]['label_translated']  );
			} else {
				$custom_fields = array( $key => $field ) + $custom_fields;
			}
		}

		return $custom_fields;
	}
endif;

if( !function_exists( 'jm_custom_fields_admin_script' ) ) :
	function jm_custom_fields_admin_script(){
		if( get_post_type() === 'noo_job' || get_post_type() === 'noo_resume' || ( isset( $_GET['page'] ) && ( $_GET['page']=='job_custom_field' || $_GET['page']=='resume_custom_field' ) ) ) {
			wp_enqueue_style( 'noo-custom-fields', NOO_FRAMEWORK_ADMIN_URI . '/assets/css/noo-custom-fields.css');

			$custom_field_tmpl = '';
			$custom_field_tmpl.= '<tr>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<input type="text" value="" placeholder="'.esc_attr__('Field Name','noo').'" name="__name__[__i__][name]">';
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<input type="text" value="" placeholder="'.esc_attr__('Field Label','noo').'" name="__name__[__i__][label]">';
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<select name="__name__[__i__][type]">';
			$custom_field_tmpl.= '<option value="text">'.esc_attr__('Text','noo').'</option>';
			$custom_field_tmpl.= '<option value="number">'.esc_attr__('Number','noo').'</option>';
			$custom_field_tmpl.= '<option value="textarea">'.esc_attr__('Textarea','noo').'</option>';
			$custom_field_tmpl.= '<option value="select">'.esc_attr__('Select','noo').'</option>';
			$custom_field_tmpl.= '<option value="multiple_select">'.esc_attr__('Multiple Select','noo').'</option>';
			$custom_field_tmpl.= '<option value="radio">'.esc_attr__('Radio','noo').'</option>';
			$custom_field_tmpl.= '<option value="checkbox">'.esc_attr__('Checkbox','noo').'</option>';
			$custom_field_tmpl.= '</select>';
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<textarea placeholder="'.esc_attr__('Field Value','noo').'" name="__name__[__i__][value]"></textarea>';
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<input type="checkbox" name="__name__[__i__][required]" /> '.esc_attr__('Yes','noo');
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '<td>';
			$custom_field_tmpl.= '<input class="button button-primary" onclick="return delete_custom_field(this);" type="button" value="'.esc_attr__('Delete','noo').'">';
			$custom_field_tmpl.= '</td>';
			$custom_field_tmpl.= '</tr>';

			$nooCustomFieldL10n = array(
				'custom_field_tmpl'=>$custom_field_tmpl,
				'disable_text'=>__('Disable', 'noo'),
				'enable_text'=>__('Enable', 'noo'),
			);
			
			wp_register_script( 'noo-custom-fields', NOO_FRAMEWORK_ADMIN_URI . '/assets/js/noo-custom-fields.js', array( 'jquery', 'jquery-ui-sortable' ), null, true );
			wp_localize_script('noo-custom-fields', 'nooCustomFieldL10n', $nooCustomFieldL10n);
			wp_enqueue_script('noo-custom-fields');
		}
	}
	add_filter( 'admin_enqueue_scripts', 'jm_custom_fields_admin_script', 10, 2 );
endif;

if ( ! function_exists( 'jm_custom_fields_setting' ) ) :
	
	function jm_custom_fields_setting( $setting_name, $wpml_prefix = '', $custom_fields = array(), $default_fields = array() ) {
		if(isset($_GET['settings-updated']) && $_GET['settings-updated']) {
			if( defined( 'ICL_SITEPRESS_VERSION' ) ) {
				$wpml_prefix = empty( $wpml_prefix ) ? $setting_name . '_' : $wpml_prefix;
				foreach ($custom_fields as $custom_field) {
					do_action( 'wpml_register_single_string', 'NOO Custom Fields', $wpml_prefix.sanitize_title(@$custom_field['name']), @$custom_field['label'] );
				}
			}
		}
		
		settings_fields( $setting_name ); // @TODO: remove this line

		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '', 'is_disabled' => '' );
		
		// -- Check value
			$custom_fields = $custom_fields ? $custom_fields : array();
			$default_fields = $default_fields ? $default_fields : array();

		$fields = !empty( $default_fields ) ? jm_merge_custom_fields( $default_fields, $custom_fields ) : $custom_fields;
		$cf_types = jm_custom_fields_type();
		$key_types = array_keys( $cf_types );

		?>
		<h3><?php echo __('Custom Fields','noo')?></h3>
		<table class="form-table" cellspacing="0">
			<tbody>
				<tr>
					<td>
						<?php 
							$num_arr = count($fields) ? array_map( 'absint', array_keys($fields) ) : array();
							$num = !empty($num_arr) ? end($num_arr) : 1;
						?>
						<table class="widefat noo_custom_field_table" data-num="<?php echo esc_attr( $num ); ?>" data-field_name="<?php echo $setting_name; ?>" cellspacing="0" >
							<thead>
								<tr>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Field Key','noo')?>
									</th>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Field Label','noo')?>
									</th>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Field Type','noo')?>
									</th>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Field Value','noo')?>
									</th>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Is Mandatory?','noo')?>
									</th>
									<th style="padding: 9px 7px">
										<?php esc_html_e('Action','noo')?>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php  if(!empty($fields)): ?>
									<?php foreach ($fields as $field):
										$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field; 
										if( !isset($field['name']) || empty($field['name'])) continue;
										$field['name'] = sanitize_title($field['name']);

										$key = $field['name'];
										$is_default = isset( $field['is_default'] );
										$is_disabled = $is_default && isset( $field['is_disabled'] ) && ($field['is_disabled'] == 'yes');
										$is_tax = $is_default && isset( $field['is_tax'] );
										$field['type'] = isset($field['type']) && !empty( $field['type'] ) ? $field['type'] : 'text';
										$allowed_types = $is_default && isset($field['allowed_type']) ? $field['allowed_type'] : false;
										$required = !empty($field['required']) ? 'checked' : '';
									?>
									<tr data-stt="<?php echo esc_attr($key)?>" <?php echo ($is_disabled ? 'class="noo-disable-field"' : ''); ?>>
										<td>
											<input type="text" value="<?php echo esc_attr($field['name'])?>" <?php echo ( $is_default ? 'readonly="readonly"' : '' ); ?> placeholder="<?php _e('Field Key','noo')?>" name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][name]">
										</td>
										<td>
											<input type="text" value="<?php echo esc_attr($field['label'])?>" <?php echo ( $is_tax ? 'readonly="readonly"' : '' ); ?> placeholder="<?php _e('Field Label','noo')?>" name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][label]">
										</td>
										<td>
											<?php if( !empty( $allowed_types ) && is_array( $allowed_types ) ) : ?>
												<?php if( count( $allowed_types ) > 1 ) : ?>
													<select name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][type]">
														<?php foreach ($allowed_types as $value => $label) : ?>
															<option value="<?php echo $value; ?>" <?php selected( $field['type'], $value ); ?>><?php echo $label; ?></option>
														<?php endforeach; ?>
													</select>
												<?php else : ?>
													<?php echo current( $allowed_types ); ?>
												<?php endif; ?>
											<?php elseif( in_array( $field['type'], $key_types ) ): ?>
												<select name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][type]">
													<?php foreach ($cf_types as $value => $label) : ?>
														<option value="<?php echo $value; ?>" <?php selected( $field['type'], $value ); ?>><?php echo $label; ?></option>
													<?php endforeach; ?>
												</select>
											<?php else : ?>
												<?php echo $field_['type']; ?>
											<?php endif; ?>
										</td>
										<td>
											<textarea <?php echo ( $is_tax ? ' disabled' : '' ); ?> placeholder="<?php _e('Field Value','noo')?>" name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][value]"><?php echo $field['value'];?></textarea>
										</td>
										<td>
											<input type="checkbox" value="true" name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][required]" <?php echo $required ?>/>
											<?php _e('Yes','noo')?>
										</td>
										<td>
											<?php if( $is_default ) : ?>
												<input type="hidden" value="<?php echo ($is_disabled ? 'yes' : 'no'); ?>" name="<?php echo $setting_name; ?>[<?php echo esc_attr($key)?>][is_disabled]">
												<input class="button button-primary" onclick="return toggle_disable_custom_field(this);" type="button" value="<?php echo ( $is_disabled ? __('Enable','noo') : __('Disable','noo') );?>">
											<?php else : ?>
												<input class="button button-primary" onclick="return delete_custom_field(this);" type="button" value="<?php _e('Delete','noo')?>">
											<?php endif; ?>
										</td>
									</tr>
									<?php endforeach; ?>
								<?php endif;?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
										<input class="button button-primary" id="add_custom_field" type="button" value="<?php esc_attr_e('Add','noo')?>">
									</td>
								</tr>
							</tfoot>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<?php
	}
endif;

if ( ! function_exists( 'jm_render_custom_field' ) ) :
	function jm_render_custom_field( $field = array(), $job_id = 0 ) {
		if( empty( $field_id ) ) {
			return;
		}

		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '', 'required' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;

		$value = !empty( $post_id ) ? noo_get_post_meta( $post_id, $field_id, '' ) : '';
		$value = !is_array($value) ? trim($value) : $value;

		$params = apply_filters( 'jm_render_custom_field_params', compact( 'field', 'field_id', 'value' ), $post_id );
		extract($params);

		$html = apply_filters( 'jm_render_custom_field', '', $field_id, $value, $field, $post_id );
		if( !empty( $html ) ) {
			echo $html;

			return;
		}
		
		jm_render_field( $field, $field_id, $value );
	}
endif;

if ( ! function_exists( 'jm_render_search_custom_field' ) ) :
	function jm_render_search_custom_field( $field = array(), $field_id = '' ) {
		$blank_field = array( 'name' => '', 'label' => '', 'type' => 'text', 'value' => '' );
		$field = is_array( $field ) ? array_merge( $blank_field, $field ) : $blank_field;

		if( empty( $field_id ) ) {
			return;
		}

		$params = apply_filters( 'jm_render_search_custom_field_params', compact( 'field', 'field_id', 'value' ), $post_id );
		extract($params);

		$field['required'] = ''; // no need for required fields in search form

		$value = isset($_GET[$field_id]) ? $_GET[$field_id] : '';
		$value = !is_array($value) ? trim($value) : $value;

		$html = apply_filters( 'jm_render_search_custom_field', '', $field_id, $value, $field );
		if( !empty( $html ) ) {
			echo $html;

			return;
		}
		
		jm_render_field( $field, $field_id, $value, 'search' );
	}
endif;

// Not used
if ( ! function_exists( 'jm_display_custom_fields' ) ) :
	
	function jm_display_custom_fields( $field = array(), $value = '', $id = '', $type = '', $label = '' ) {

		if( empty($type) && isset($field['type'] ) ) $type = $field['type']; 
		if( empty($id) && isset($field['id'] ) ) $id = $field['id']; 
		if( empty($label) && isset($field['label'] ) ) $label = $field['label']; 
		// if( empty($value) && isset($field['value'] ) ) $value = $field['value'];

		$value = jm_convert_custom_field_value( $field, $value );

		if ( $field['type'] === 'multiple_select' ) : 
			$value = implode(', ', $value);
		?>
			<h3 class="label-<?php echo $id; ?>"><?php esc_html_e($label)?></h3>
			<p class="value-<?php echo $id; ?>"><?php echo esc_html( $value ); ?></p>
		<?php elseif ( $field['type'] === 'checkbox' || $field['type'] === 'radio' ) : ?>
			<h3 class="label-<?php echo $id; ?>"><?php esc_html_e($label); ?></h3>
			<?php foreach ($value as $v) : ?>
				<div class="value-<?php echo $id ?> <?php echo $v ?>">
					<i class="fa fa-check-circle"></i>
					<?php echo esc_html_e($v); ?>
				</div>
			<?php endforeach; ?>
		<?php elseif( $field['type'] === "textarea" ) : ?> 
			<h3 class="label-<?php echo $id; ?>"><?php esc_html_e($label); ?></h3>
			<p class="value-<?php echo $id; ?>"><?php echo do_shortcode( $value ); ?></p>
		<?php else : ?> 
			<h3 class="label-<?php echo $id; ?>"><?php esc_html_e($label)?></h3>
			<p class="value-<?php echo $id; ?>"><?php echo esc_html( $value ); ?></p>
		<?php endif;
	}

endif;


if ( ! function_exists( 'jm_convert_custom_field_value' ) ) :
	
	function jm_convert_custom_field_value( $field = array(), $value = '' ) {

		if( empty($type) && isset($field['type'] ) ) $type = $field['type']; 

		$new_value = !is_array($value) ? trim($value) : $value;
		if( empty( $type ) || empty( $new_value ) ) return '';

		if( in_array( $field['type'], array( 'select', 'multiple_select', 'checkbox', 'radio' ) ) ) {
			$list_option = explode( "\n", $field['value'] );
			$field_value = array();
			foreach ($list_option as $index => $option) {
				$option_key = explode( '|', $option );
				$option_key[0] = trim( $option_key[0] );
				if( empty( $option_key[0] ) ) continue;
				$option_key[1] = isset( $option_key[1] ) ? $option_key[1] : $option_key[0];
				$option_key[0] = sanitize_title( $option_key[0] );

				$field_value[$option_key[0]] = $option_key[1];
			}

			if( in_array( $field['type'], array( 'multiple_select', 'checkbox', 'radio' ) ) ) {
				$new_value = !is_array( $new_value ) ? noo_json_decode( $new_value ) : $new_value;

				foreach ($new_value as $index => $v) {
					if( empty( $v ) ) {
						unset( $new_value[$index] );
					} elseif( isset( $field_value[$v] ) ) {
						$new_value[$index] = $field_value[$v];
					}
				}
			} else { // select
				if( isset( $field_value[$new_value] ) ) {
					$new_value = $field_value[$new_value];
				}
			}
		}

		return apply_filters( 'jm_convert_custom_field_value', $new_value, $field, $value );
	}

endif;

/** ====== END noo_get_default_fields ====== **/

/* -------------------------------------------------------
 * Create functions noo_get_custom_fields
 * ------------------------------------------------------- */

if ( ! function_exists( 'noo_get_custom_fields' ) ) :
	
	function noo_get_custom_fields( $custom_fields, $name ) {
		return jm_get_custom_fields( $custom_fields, $name );
	}

endif;

/** ====== END noo_get_custom_fields ====== **/

/* -------------------------------------------------------
 * Create functions setting_custom_field
 * ------------------------------------------------------- */

if ( ! function_exists( 'setting_custom_field' ) ) :
	
	function setting_custom_field( $name_settings, $default_fields, $custom_fields, $field_display, $class ) {
		$wpml_prefix = ( $name_settings == 'noo_resume[custom_field]' ? 'noo_resume_field_' : 'noo_job_field_' );
		jm_custom_fields_setting( $name_settings, $wpml_prefix, $custom_fields, $default_fields );
	}

endif;

/** ====== END setting_custom_field ====== **/

/* -------------------------------------------------------
 * Create functions noo_show_custom_fields
 * ------------------------------------------------------- */

if ( ! function_exists( 'noo_show_custom_fields' ) ) :
	
	function noo_show_custom_fields( $field, $id_type, $id_fields ) {

		return jm_render_custom_field( $field, $id_fields, $id_type );
	}

endif;

/** ====== END noo_show_custom_fields ====== **/

/* -------------------------------------------------------
 * Create functions noo_display_custom_fields: display the fields on frontend.
 * ------------------------------------------------------- */

if ( ! function_exists( 'noo_display_custom_fields' ) ) :
	
	function noo_display_custom_fields( $field = array(), $value = '', $id = '', $type = '', $label = '' ) {

		if( empty($type) && isset($field['type'] ) ) $type = $field['type']; 
		if( empty($id) && isset($field['id'] ) ) $id = $field['id']; 
		if( empty($label) && isset($field['label'] ) ) $label = $field['label']; 
		// if( empty($value) && isset($field['value'] ) ) $value = $field['value'];

		jm_display_field( $field, $id, $value );
	}

endif;

