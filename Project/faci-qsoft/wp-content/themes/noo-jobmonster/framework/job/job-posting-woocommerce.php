<?php

if( !function_exists('jm_is_woo_job_posting') ) :
	function jm_is_woo_job_posting(){
		return 'woo' == jm_get_job_posting_mode();
	}
endif;
