<?php

if( !function_exists('jm_can_view_candidate_contact') ) :
	function jm_can_view_candidate_contact( $resume_id = null ) {
		if( jm_is_resume_posting_page() ) {
			return true;
		}
		if( empty( $resume_id ) ) {
			return false;
		}
		
		$can_view_candidate_contact_setting = jm_get_resume_setting('can_view_candidate_contact','');
		if( empty( $can_view_candidate_contact_setting ) ) return true;

		// Resume's author can view his/her contact
		$candidate_id = get_post_field( 'post_author', $resume_id );
		if( $candidate_id == get_current_user_id() ) {
			return true;
		}

		if( isset($_GET['application_id'] ) && !empty($_GET['application_id']) ) {
			// Employers can view candidate contact from their applications

			$job_id = get_post_field( 'post_parent', $_GET['application_id'] );
			
			$employer_id = get_post_field( 'post_author', $job_id );
			if( $employer_id == get_current_user_id() ) {
				if( $resume_id == noo_get_post_meta( $_GET['application_id'], '_attachment', '' ) ) {
					return true;
				}
			}
		}

		$can_view_candidate_contact = true;
		switch( $can_view_candidate_contact_setting ) {
			case 'noone':
				$can_view_candidate_contact = false;
				break;
			case 'employer':
				$can_view_candidate_contact = Noo_Member::is_employer();
				break;
			case 'premium_package':
				if( Noo_Member::is_employer() ) {
					$package = jm_get_job_posting_info();
					if(is_array( $package ) && isset( $package['product_id'] ) && !empty( $package['product_id'] ) ) {
						$product_package = get_product( absint( $package['product_id'] ) );
						
						if( $product_package && $product_package->product_type === 'job_package' ) {
							if( $can_view_resume_setting === 'premium_package'){
								$can_view_candidate_contact = $product_package->get_price() > 0;
							}
						}
					}
				}
				break;
			case 'package':
				$package = jm_get_job_posting_info();
				if( Noo_Member::is_employer() ) {
					$can_view_candidate_contact = isset( $package['can_view_candidate_contact'] ) && $package['can_view_candidate_contact'] == '1';
				}
				break;
			default:
				$can_view_candidate_contact = true;
				break;
		}

		return apply_filters( 'jm_can_view_candidate_contact', $can_view_candidate_contact, $resume_id );
	};
endif;

if( !function_exists('jm_resume_is_show_candidate_contact') ) :
	function jm_resume_is_show_candidate_contact( $show_contact = true, $resume_id = '' ) {
		return jm_can_view_candidate_contact( $resume_id );
	};

	add_filter( 'jm_resume_show_candidate_contact', 'jm_resume_is_show_candidate_contact', 10, 2 );
endif;

if( !function_exists('jm_resume_contact_settings') ) :
	function jm_resume_contact_settings( $fields = array() ) {
		$fields[] = array(
			'id' => 'can_view_candidate_contact',
			'label' => __( 'Show Candidate Contact on Resumes for', 'noo' ),
			'label_desc' => __( 'Select how you want to show the candidate contact on the Resumes.', 'noo' ),
			'type' => 'radio',
			'default' => '',
			'options'=>array(
				array('label'=>__('All who can see resumes','noo'),'value'=>''),
				array('label'=>__('Logged in Employer','noo'),'value'=>'employer'),
				array('label'=>__('Employers bought a Premium ( not free ) Job Package','noo'),'value'=>'premium_package'),
				array('label'=>__('Employers bought a certain Job Package ( You will have to setup the right Job Package )','noo'),'value'=>'package'),
				array('label'=>__('No-one - always hide candidate contact ( Employers who received resumes from applications can still see the contact ).','noo'),'value'=>'noone'),
			),
		);
		foreach ($fields as $index => $value) {
			if( $value['id'] == 'enable_resume' ) {
				$value['child_fields']['on'] = $value['child_fields']['on'] . ',can_view_candidate_contact';
				$fields[$index]= $value;

				break;
			}
		}

		return $fields;
	};

	add_filter( 'jm_resume_setting_display_fields', 'jm_resume_contact_settings' );
endif;

if( !function_exists('jm_job_package_view_candidate_contact_data') ) :
	function jm_job_package_view_candidate_contact_data() {
		global $post;
		$can_view_candidate_contact_setting = jm_get_resume_setting('can_view_candidate_contact','');
		if( $can_view_candidate_contact_setting == 'package' ) {
			woocommerce_wp_checkbox(
				array(
					'id' => '_can_view_candidate_contact',
					'label' => __( 'Can view Candidate Contact', 'noo' ),
					'description' => __( 'Allowing buyers to see Candidate Contact.', 'noo' ),
					'cbvalue' => 1,
					'desc_tip' => false,) );
		}
	}

	add_action( 'noo_job_package_data', 'jm_job_package_view_candidate_contact_data' );
endif;

if( !function_exists('jm_job_package_save_view_candidate_contact_data') ) :
	function jm_job_package_save_view_candidate_contact_data($post_id) {
		$can_view_candidate_contact_setting = jm_get_resume_setting('can_view_candidate_contact','');
		if( $can_view_candidate_contact_setting == 'package' ) {
			// Save meta
			$fields = array(
				'_can_view_candidate_contact'  => '',
			);
			foreach ( $fields as $key => $value ) {
				$value = ! empty( $_POST[ $key ] ) ? $_POST[ $key ] : '';
				switch ( $value ) {
					case 'int' :
						$value = intval( $value );
						break;
					case 'float' :
						$value = floatval( $value );
						break;
					default :
						$value = sanitize_text_field( $value );
				}
				update_post_meta( $post_id, $key, $value );
			}
		}
	}

	add_action( 'noo_job_package_save_data', 'jm_job_package_save_view_candidate_contact_data' );
endif;

if( !function_exists('jm_job_package_view_candidate_contact_user_data') ) :
	function jm_job_package_view_candidate_contact_user_data( $data, $product ) {
		$can_view_candidate_contact_setting = jm_get_resume_setting('can_view_candidate_contact','employer');
		if( $can_view_candidate_contact_setting == 'package' && is_object( $product ) ) {
			$data['can_view_candidate_contact'] = $product->can_view_candidate_contact;
		}

		return $data;
	}

	add_filter( 'jm_job_package_user_data', 'jm_job_package_view_candidate_contact_user_data', 10, 2 );
endif;

if( !function_exists('jm_job_package_view_candidate_contact_features') ) :
	function jm_job_package_view_candidate_contact_features( $product ) {
		$can_view_candidate_contact_setting = jm_get_resume_setting('can_view_candidate_contact','');
    	$resume_view_limit = $product->resume_view_limit;
    	$resume_view_duration = $product->resume_view_duration;
		if( ( $can_view_candidate_contact_setting == 'premium_package' && $product->get_price() > 0 )
			|| ( $can_view_candidate_contact_setting == 'package' && $product->can_view_candidate_contact == '1' ) ) : ?>
			<li class="noo-li-icon"><i class="fa fa-check-circle"></i> <?php _e('Allow viewing Candidate Contact','noo');?></li>
    	<?php endif;
	}

	add_action( 'jm_job_package_features_list', 'jm_job_package_view_candidate_contact_features' );
endif;