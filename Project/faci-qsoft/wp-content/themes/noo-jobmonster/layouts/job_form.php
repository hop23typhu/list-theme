<?php 
$job_id = isset($_GET['job_id']) ?absint($_GET['job_id']) : 0;
$employer_id = get_current_user_id();
$job = get_post($job_id);
$default_job_content = jm_get_job_setting( 'default_job_content', '' );
?>
<div class="job-form">
	<div class="job-form-detail">
		<div class="form-group row">
			<label for="position" class="col-sm-3 control-label"><?php _e('Job Title','noo')?></label>
			<div class="col-sm-9">
		    	<input type="text" value="<?php echo ($job_id ? $job->post_title : '')?>" class="form-control jform-validate" id="position"  name="position" autofocus required placeholder="<?php echo esc_attr__('Enter a short title for your job','noo')?>">
		    </div>
		</div>
		<div class="form-group row">
		    <label for="desc" class="col-sm-3 control-label"><?php _e('Job Description','noo')?></label>
		    <div class="col-sm-9">
		    	<textarea class="form-control form-control-editor ignore-valid jform-validate" id="desc"  name="desc" rows="8" placeholder="<?php echo esc_attr__('Describe your job in a few paragraphs','noo')?>"><?php echo ($job_id ? $job->post_content : $default_job_content)?></textarea>
		    </div>
		</div>
		<?php
			if ( jm_get_job_setting( 'cover_image','yes') == 'yes' ) :
		?>
			<div class="form-group  row">
				<label class="col-sm-3 control-label"><?php _e('Cover Image','noo')?></label>
				<div class="col-sm-9">
					<div class="row">
						<div id="noo_upload-thumb-cover" class="col-md-2 noo_upload">
							<?php
								$job_cover_image = ($job_id ? noo_get_post_meta($job_id,'_cover_image') : '');
								show_list_image_upload($job_cover_image, 'cover_image');
							?>
						</div>
						<div id="noo_upload-wrap" class="col-md-10">
							
							<div id="noo_upload-cover" class="btn btn-default">
								<i class="fa fa-folder-open-o"></i> <?php _e('Browse','noo');?>
							</div>
							
						</div>
						<script>
						jQuery(document).ready(function($) {
							//$('#noo_upload-cover').click(function(event) {
								$('#noo_upload-cover').noo_upload({
									input_name : 'cover_image',
									container : 'noo_upload-wrap',
									browse_button : 'noo_upload-cover',
									tag_thumb : 'noo_upload-thumb-cover'
								});
							//});
						});
						</script>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php 
			$fields = jm_get_job_custom_fields( true );
			if( !empty( $fields ) ) {
				foreach ($fields as $field) {
					jm_job_render_form_field( $field, $job_id );
				}
			}
		?>
		<div class="form-group row">
			<label for="closing" class="col-sm-3 control-label"><?php _e('Closing Date','noo')?></label>
			<div class="col-sm-9">
				<?php $closing = $job ? noo_get_post_meta($job_id,'_closing') : ''; ?>
				<?php $closing = is_numeric( $closing ) ? date_i18n('Y-m-d', $closing) : $closing; 
					$employer_package = jm_get_job_posting_info( $employer_id );
				?>
		    	<input type="text" value="<?php echo $closing; ?>" class="form-control jform-datepicker" id="closing"  name="closing">
		    	<em><?php echo __('Set a date or leave blank to automatically use the Expired date', 'noo') . ( isset( $employer_package['job_duration'] ) ? sprintf( __(' - %s day(s) after published.', 'noo'), $employer_package['job_duration'] ) : '' ); ?></em>
		    </div>
		</div>
		<div class="form-group row">
			<label for="application_email" class="col-sm-3 control-label"><?php _e('Application Notify Email','noo')?></label>
			<div class="col-sm-9">
		    	<input type="text" value="<?php echo ($job ? noo_get_post_meta($job_id,'_application_email') : '')?>" class="form-control" id="application_email"  name="application_email" >
		    	<em><?php _e('Email to receive application notification. Leave it blank to use your account email.','noo'); ?></em>
		    </div>
		</div>
		<?php $custom_apply_link = jm_get_setting('noo_job_linkedin', 'custom_apply_link' );
			if( $custom_apply_link == 'employer' ) :
		?>
			<div class="form-group row">
				<label for="custom_application_url" class="col-sm-3 control-label"><?php _e('Custom Application URL','noo')?></label>
				<div class="col-sm-9">
			    	<input type="text" value="<?php echo ($job ? noo_get_post_meta($job_id,'_custom_application_url') : '')?>" class="form-control" id="custom_application_url"  name="custom_application_url" >
			    	<em><?php _e('Custom link to redirect job seekers to when applying for this job.','noo'); ?></em>
			    </div>
			</div>
		<?php endif; ?>
	</div>
	<?php if(!jm_get_job_company($job_id)):?>
	<div class="job-form-company">
	<h4><?php _e('Company Profile', 'noo')?></h4>
	<?php echo Noo_Member::get_company_profile_form(false)?>
	</div>
	<?php 
	endif;
	?>
</div>
