<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'faci-qsoft');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2+s:w/U}xU<Ez^]$<-z[?NW.ni~H9LK7J;8~dT8;Ve?q:k50j-9$_,+MzX|Xsp@+');
define('SECURE_AUTH_KEY',  ')Oe5||tg~]VjYOq1:ZKU80DRlnwZ3!0=eA5syR!cM6GPe4Z[:&08 pvJ<B.1PSY}');
define('LOGGED_IN_KEY',    'r2FE5;JQEOTW.h79|%$9ZB4^FCllgO|YY<kHpqj;:K+=tB*=LdW9qB(fdYJ?gZNx');
define('NONCE_KEY',        '+cAY_>jt:i?-p;XSsQe3x.Lu{&RtU(v<oXX|Q#;+dDv6(4+j,TzB&Erv.yalK|uB');
define('AUTH_SALT',        ')<:iuj?]v:Y**=RaisAn`fyC.G8)t|xMghK_LDw4EiEjrZ-WZ+:_|zG2A)f>;/K.');
define('SECURE_AUTH_SALT', '7?!PI+&(8pMz>>+o)R>IXS-L)]ho2e](Y4MS<i3^GDr1Tm&^{/2e;?mWLv^K+ w(');
define('LOGGED_IN_SALT',   '%beX2UEKHENzan/q&,`EZ9_a,1/Nod,v,,e}-oc]WV5Gj.D1Qs`7DQ(VuL+t-B{%');
define('NONCE_SALT',       'd!VKg~=KU4]bYae {;@uF[WWzx.k[C.LNlNDE%%*TZ:+1URW($NglUUJfi!#;2BI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'test_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
