<?php
add_action('widgets_init', create_function('', "register_widget( 'RT_Recent_Post' );"));
class RT_Recent_Post extends WP_Widget {

	function RT_Recent_Post() {
		$widget_ops = array(
            'classname'   => 'featured-content featuredpost',
            'description' => __( 'Hiển thị các bài viết tin tức.', 'genesis' ),
        );
        $control_ops = array(
            'id_base' => 'rt-featured-post',
            'width'   => 505,
            'height'  => 350,
        );
		$this->WP_Widget( 'rt-featured-post', __('Bee - Danh sách bài viết', 'genesis'), $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		echo $before_widget;

        //* Set up the author bio
        if ( ! empty( $instance['title'] ) )
            echo $before_title . apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) . $after_title;

        $query_args = array(
            'post_type' => 'tin-tuc',
            'tax_query' => array(
                array(
                    'taxonomy' => 'danhmuc',
                    'field' => 'id',
                    'terms' => $instance['posts_cat']
                )
            ),
            'showposts' => $instance['posts_num']
        );
        $wp_query = new WP_Query( $query_args );
        if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();

        genesis_markup( array(
            'html5'   => '<article %s>',
            'xhtml'   => sprintf( '<div class="%s">', implode( ' ', get_post_class() ) ),
            'context' => 'entry',
        ) );

        $image = genesis_get_image( array(
            'format'  => 'html',
            'size'    => $instance['image_size'],
            'context' => 'featured-post-widget',
            'attr'    => genesis_parse_attr( 'entry-image-widget' ),
        ) );
        if ( $instance['show_image'] && $image )
        printf( '<a href="%s" title="%s" class="%s">%s</a>', get_permalink(), the_title_attribute( 'echo=0' ), esc_attr( $instance['image_alignment'] ), $image );

        echo genesis_html5() ? '<header class="entry-header">' : '';
        if ( genesis_html5() )
            printf( '<h2 class="entry-title"><a href="%s" title="%s">%s</a></h2>', get_permalink(), the_title_attribute( 'echo=0' ), get_the_title() );
        else
            printf( '<h2><a href="%s" title="%s">%s</a></h2>', get_permalink(), the_title_attribute( 'echo=0' ), get_the_title() );
        echo genesis_html5() ? '</header>' : '';

        if ( ! empty( $instance['show_content'] ) ) {
            echo genesis_html5() ? '<div class="entry-content">' : '';
            if ( 'excerpt' == $instance['show_content'] ) {
                    the_excerpt();
                }
            elseif ( 'content-limit' == $instance['show_content'] ) {
                    the_content_limit( (int) $instance['content_limit'], esc_html( $instance['more_text'] ) );
                }
            else {
                global $more;
                $orig_more = $more;
                $more = 0;
                the_content( esc_html( $instance['more_text'] ) );
                $more = $orig_more;
            }
                echo genesis_html5() ? '</div>' : '';
        }
        genesis_markup( array(
                'html5' => '</article>',
                'xhtml' => '</div>',
            ) );
        endwhile; endif;

        wp_reset_query();

        if ( ! empty( $instance['more_from_category'] ) && ! empty( $instance['posts_cat'] ) )
            printf(
                '<p class="more-from-category"><a href="%1$s" title="%2$s">%3$s</a></p>',
                esc_url( get_category_link( $instance['posts_cat'] ) ),
                esc_attr( get_cat_name( $instance['posts_cat'] ) ),
                esc_html( $instance['more_from_category_text'] )
            );

        echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
        return $new_instance;
    }

	function form( $instance ) {

        //* Merge with defaults
        $instance = wp_parse_args( (array)$instance, array (
            'title'                   => '',
            'posts_cat'               => '',
            'posts_num'               => 1,
            'show_image'              => 0,
            'image_alignment'         => '',
            'image_size'              => 'thumbnail',
            'show_content'            => '',
            'content_limit'           => '',
            'more_text'               => 'Xem tiếp »',
            'more_from_category'      => '',
            'more_from_category_text' => 'Xem thêm',
        ) );

    ?>
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tiêu đề', 'genesis' ); ?>:</label>
        <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" class="widefat" />
        </p>

        <div class="genesis-widget-column">

            <div class="genesis-widget-column-box genesis-widget-column-box-top">

                <p>
                    <label for="<?php echo $this->get_field_id( 'posts_cat' ); ?>"><?php _e( 'Chuyên mục', 'genesis' ); ?>:</label>
                    <?php
                    $categories_args = array(
                        'name'            => $this->get_field_name( 'posts_cat' ),
                        'selected'        => $instance['posts_cat'],
                        'orderby'         => 'Name',
                        'hierarchical'    => 1,
                        'show_option_all' => __( 'Tất cả', 'genesis' ),
                        'hide_empty'      => '0',
                        'taxonomy'        => 'danhmuc'
                    );
                    wp_dropdown_categories( $categories_args ); ?>
                </p>

                <p>
                    <label for="<?php echo $this->get_field_id( 'posts_num' ); ?>"><?php _e( 'Số bài hiển thị', 'genesis' ); ?>:</label>
                    <input type="text" id="<?php echo $this->get_field_id( 'posts_num' ); ?>" name="<?php echo $this->get_field_name( 'posts_num' ); ?>" value="<?php echo esc_attr( $instance['posts_num'] ); ?>" size="2" />
                </p>

            </div>

            <div class="genesis-widget-column-box">

                <p>
                    <input id="<?php echo $this->get_field_id( 'show_image' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'show_image' ); ?>" value="1" <?php checked( $instance['show_image'] ); ?>/>
                    <label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php _e( 'Hiển thị ảnh đại diện', 'genesis' ); ?></label>
                </p>

                <p>
                    <label for="<?php echo $this->get_field_id( 'image_size' ); ?>"><?php _e( 'Kích thước ảnh', 'genesis' ); ?>:</label>
                    <select id="<?php echo $this->get_field_id( 'image_size' ); ?>" class="genesis-image-size-selector" name="<?php echo $this->get_field_name( 'image_size' ); ?>">
                        <?php
                        $sizes = genesis_get_image_sizes();
                        foreach( (array) $sizes as $name => $size )
                            echo '<option value="'.esc_attr( $name ).'" '.selected( $name, $instance['image_size'], FALSE ).'>'.esc_html( $name ).' ( '.$size['width'].'x'.$size['height'].' )</option>';
                        ?>
                    </select>
                </p>

                <p>
                    <label for="<?php echo $this->get_field_id( 'image_alignment' ); ?>"><?php _e( 'Căn lề ảnh', 'genesis' ); ?>:</label>
                    <select id="<?php echo $this->get_field_id( 'image_alignment' ); ?>" name="<?php echo $this->get_field_name( 'image_alignment' ); ?>">
                        <option value="alignnone">- <?php _e( 'None', 'genesis' ); ?> -</option>
                        <option value="alignleft" <?php selected( 'alignleft', $instance['image_alignment'] ); ?>><?php _e( 'Trái', 'genesis' ); ?></option>
                        <option value="alignright" <?php selected( 'alignright', $instance['image_alignment'] ); ?>><?php _e( 'Phải', 'genesis' ); ?></option>
                        <option value="aligncenter" <?php selected( 'aligncenter', $instance['image_alignment'] ); ?>><?php _e( 'Giữa', 'genesis' ); ?></option>
                    </select>
                </p>

            </div>

        </div>

        <div class="genesis-widget-column genesis-widget-column-right">

            <div class="genesis-widget-column-box genesis-widget-column-box-top">

                <p>
                    <label for="<?php echo $this->get_field_id( 'show_content' ); ?>"><?php _e( 'Tùy chỉnh nội dung', 'genesis' ); ?>:</label>
                    <select id="<?php echo $this->get_field_id( 'show_content' ); ?>" name="<?php echo $this->get_field_name( 'show_content' ); ?>">
                        <option value="content-limit" <?php selected( 'content-limit', $instance['show_content'] ); ?>><?php _e( 'Nội dung giới hạn kí tự', 'genesis' ); ?></option>
                        <option value="" <?php selected( '', $instance['show_content'] ); ?>><?php _e( 'Không hiển thị', 'genesis' ); ?></option>
                    </select>
                    <br />
                    <label for="<?php echo $this->get_field_id( 'content_limit' ); ?>"><?php _e( 'Giới hạn kí tự', 'genesis' ); ?>
                        <input type="text" id="<?php echo $this->get_field_id( 'image_alignment' ); ?>" name="<?php echo $this->get_field_name( 'content_limit' ); ?>" value="<?php echo esc_attr( intval( $instance['content_limit'] ) ); ?>" size="3" />
                        <?php _e( 'characters', 'genesis' ); ?>
                    </label>
                </p>

                <p>
                    <label for="<?php echo $this->get_field_id( 'more_text' ); ?>"><?php _e( 'Tùy chỉnh hiển thị thêm', 'genesis' ); ?>:</label>
                    <input type="text" id="<?php echo $this->get_field_id( 'more_text' ); ?>" name="<?php echo $this->get_field_name( 'more_text' ); ?>" value="<?php echo esc_attr( $instance['more_text'] ); ?>" />
                </p>

            </div>

            <div class="genesis-widget-column-box">

                <p>
                    <input id="<?php echo $this->get_field_id( 'more_from_category' ); ?>" type="checkbox" name="<?php echo $this->get_field_name( 'more_from_category' ); ?>" value="1" <?php checked( $instance['more_from_category'] ); ?>/>
                    <label for="<?php echo $this->get_field_id( 'more_from_category' ); ?>"><?php _e( 'Hiển thị liên kết chuyên mục', 'genesis' ); ?></label>
                </p>

                <p>
                    <label for="<?php echo $this->get_field_id( 'more_from_category_text' ); ?>"><?php _e( 'Liên kết', 'genesis' ); ?>:</label>
                    <input type="text" id="<?php echo $this->get_field_id( 'more_from_category_text' ); ?>" name="<?php echo $this->get_field_name( 'more_from_category_text' ); ?>" value="<?php echo esc_attr( $instance['more_from_category_text'] ); ?>" class="widefat" />
                </p>

            </div>

        </div>
        <?php

    }

}