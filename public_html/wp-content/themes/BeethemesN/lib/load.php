<?php

// add css
function bt_add_css() {
	wp_enqueue_style( "font-opensans", "//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,700,300,600,400" );
	wp_enqueue_style( "default-css", THEME_URI."/asset/css/bt-default.css" );
	wp_enqueue_style( "font-awesome", THEME_URI."/asset/css/font-awesome.min.css" );
    wp_enqueue_style( "main-css", THEME_URI."/asset/css/main.css" );
    // wp_enqueue_style( "responsive-css", THEME_URI."/asset/css/responsive.css" );
}
add_action( "wp_enqueue_scripts", "bt_add_css" );

// add js
function bt_add_js() {
	wp_enqueue_script( "owl-carousel", THEME_URI."/asset/js/owl.carousel.min.js", array( "jquery" ), "1.0", true );
	wp_enqueue_script( "isotope", THEME_URI."/asset/js/isotope.pkgd.min.js", array( "jquery" ), "1.0", true );
	wp_enqueue_script( "main-js", THEME_URI."/asset/js/main.js", array( "jquery" ), "1.0", true );
	// wp_enqueue_script( "responsive-js", THEME_URI."/asset/js/responsive.js", array( "jquery" ), "1.0", true );
}
add_action( "wp_enqueue_scripts", "bt_add_js" );