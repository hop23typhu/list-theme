<?php
    get_header();
    do_action( 'genesis_before_content_sidebar_wrap' );
?>
    <div class="content-sidebar-wrap">
        <?php do_action( 'genesis_before_content' ); ?>
        <main class="content" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
            <?php
                do_action( 'genesis_before_loop' );
            ?>
        <div id="home-content" class="clearfix">
                <?php

                    $boxes = gtid_get_option( 'rt_num_home_boxes' ); // num cat
                    $num = gtid_get_option( 'number_home_product' ); // num pro
                    $news_box = gtid_get_option( 'rt_num_home_news' ); // num cat news
                    $num_news =gtid_get_option( 'number_home_product2' ); // num news
                ?>

            <div class="product-home">
                <?php
                    for( $i=1; $i<=$boxes; $i++ ) {
                        $products = gtid_get_option( 'home_cat_'.$i );
                        if ( $products != 0 ) {
                            $main_post = new WP_Query( 'cat='.$products.'&showposts='.$num );
              ?>
                <div class="product-wrap">
                   <h2 class="heading">
                        <a href="<?php echo get_category_link( $products ) ?>" title="<?php echo get_cat_name( $products ); ?>">
                            <?php echo get_cat_name($products); ?>
                        </a>

                    </h2>
                    <div class="clearfix"></div>
                    <div class="product-list clearfix">
                    <?php
                        while ( $main_post -> have_posts() ) :
                            $main_post -> the_post();

                        get_template_part( 'loop' );

                        endwhile; wp_reset_postdata();
                    ?>
                    </div><!--.product-list-->
                </div>
                <div class="viewall"><a href="<?php echo get_category_link( $products ) ?>">>> Xem Tất Cả << </a></div>
                <?php  } }// Kết thúc vòng lặp For số box sản phẩm ?>
                </div><!-- Product Wrap -->

                <div class="news-wrap">
                    <?php
                        for( $i2=1;$i2<=$news_box;$i2++ ) {
                             $news = gtid_get_option( 'news_cat_'.$i2 );
                             if( $news!=0 ) {
                     ?>
                        <div class="viewall"><a href="<?php the_permalink() ?>"> >> Xem Tất Cả << </a></div>
                        <h2 class="heading">
                            <span>
                              <?php $array = get_term_by( 'id', $news, 'danhmuc' );
                                    echo $array->name;
                               ?>
                            </span>
                        </h2>
                    <div class="news-list clearfix">
                    <?php
                        $args = array(
                        'post_type' => 'tin-tuc',
                        'posts_per_page' => $num_news,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'danhmuc',
                                'field'    => 'id',
                                'terms'    => $news,
                            ),
                        ),
                        );
                         $news_list = new WP_Query( $args ); ?>
                    <?php
                        $d = 0;
                            while( $news_list->have_posts() ) :
                            $news_list->the_post();
                        $d++;
                        if ( $d == 1 ) {
                    ?>
                        <div class="news-first-item clearfix">
                            <a href="<?php the_permalink() ?>" class="img" title="<?php the_title() ?>">
                                <?php bt_thumbnail(); ?>
                            </a>
                            <a href="<?php the_permalink() ?>" class="news-title" title="<?php the_title() ?>"><?php the_title(); ?></a>
                            <?php the_content_limit( "100", "" ); ?>
                        </div>
                    <?php } else { ?>
                        <div class="news-item">
                            <a href="<?php the_permalink() ?>" class="news-title" title="<?php the_title() ?>"><i class="fa fa-caret-right"></i> <?php the_title(); ?></a>
                        </div>
                    <?php
                        }
                        endwhile; wp_reset_postdata();
                    ?>
                    </div><!--News-List-->
                <?php   } } // enn for news ?>
            </div><!--End #news-wrap-->


            <div class="wrapcontent home-page clearfix">
                
                    
                <div class="content-home-page">
                <?php
                    $page = gtid_get_option( 'rt_home_page_1' ); // page home
                    if ( $page != '' ) {
                        $p = new WP_Query('page_id='.$page);
                        while ( $p->have_posts() ) : $p->the_post();
                ?>
                <div class="home-page-des">
                    <div class="content-left-home-page">
                        <?php do_action('genesis_after_entry') ?>
                    </div>
                    <div class="content-right-home-page">
                        <?php the_content() ?>
                    </div>
                </div>

                <?php
                    endwhile; wp_reset_postdata();
                    }
                ?>
                </div>
            </div>

        </div> <!--#End #home-content-->
     </main><!-- end #content -->

    <?php do_action( 'genesis_after_content' ); ?>
</div> <!-- end content-sidebar-wrap -->
<?php
    do_action( 'genesis_after_content_sidebar_wrap' );
    get_footer();

?>