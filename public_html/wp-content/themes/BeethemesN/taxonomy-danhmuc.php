<?php
	get_header();
    do_action( 'genesis_before_content_sidebar_wrap' );

    $term = $wp_query->get_queried_object();
    $catid = $term->term_id;
?>
	<div class="content-sidebar-wrap">
		<?php do_action( 'genesis_before_content' );  ?>
		<main class="content" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">
			<?php
				do_action( 'genesis_before_loop' );
			?>
            <div id="home-content" class="clearfix">
                    <h1 class="heading"><?php single_term_title(); ?></h1>
					<?php
                        $arg = array(
                            'post_type' => 'tin-tuc',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'danhmuc',
                                    'field' => 'id',
                                    'terms' => $catid
                                )
                            ),
                            'paged'=> get_query_var( 'paged' )
                        );
                        $news_post = new WP_Query($arg);
                        while( $news_post->have_posts() ) :
                            $news_post->the_post();
                    ?>
                        <div class="news-post">
                            <h2>
                                <a href="<?php the_permalink();?>" class="news-title" title="<?php the_title();?>"><?php echo the_title(); ?></a>
                            </h2>

                            <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                <?php bt_thumbnail(); ?>
                            </a>

                            <?php the_content_limit( 300,'Đọc Thêm' ); ?>

                        </div><!--End .news-post-->
                            
                            <a href="">Xem thêm thiết kế khác</a>
                        <?php
                            endwhile;
							if(function_exists('wp_pagenavi')) { wp_pagenavi(); }
                            wp_reset_postdata();
        			     ?>
                        
                </div><!--End #news-wrap-->

                

            <?php do_action( 'genesis_after_loop' ); ?>
		</main><!-- end #content -->
		<?php do_action( 'genesis_after_content' ); ?>
	</div><!-- end #content-sidebar-wrap -->
<?php
	do_action( 'genesis_after_content_sidebar_wrap' );
	get_footer();
?>