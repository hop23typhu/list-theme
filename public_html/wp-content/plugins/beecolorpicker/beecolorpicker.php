<?php
/*
Plugin Name: Bee Color Picker
Plugin URI: http://beeweb.com.vn
Description: Tools color picker for BeeThemes
Version: 1.0
Author: Sang Minh <sang.nguyen1691@gmail.com>
Author URI: http://beeweb.com.vn
License: GPLv2 or later
Text Domain: beecp
*/

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

register_activation_hook( __FILE__, 'beecp_plugin_install' );
register_deactivation_hook( __FILE__, 'beecp_plugin_remove' );

function beecp_plugin_install(){
	add_option( 'beecp_color_main_one', 'f55d01' );
	add_option( 'beecp_color_main_two', 'f55d01' );

	add_option( 'beecp_active_color_primary', '0' );
	add_option( 'beecp_color_primary', 'f55d01' );

	add_option( 'beecp_active_color_text', '1' );
	add_option( 'beecp_color_text', '333333' );
	add_option( 'beecp_link', '//beeweb.com.vn/dang-ki-website' );
}

function beecp_plugin_remove() {
	delete_option('beecp_title_theme');
	delete_option('beecp_active_color_text');
	delete_option('beecp_color_main_one');
	delete_option('beecp_color_main_two');
	delete_option('beecp_color_text');

	delete_option( 'beecp_active_color_primary' );
	delete_option( 'beecp_color_primary' );

	delete_option('beecp_link');

	delete_option('beecp_div_bg1');
    delete_option('beecp_div_bd1');
    delete_option('beecp_div_cl1');

    delete_option('beecp_div_bg2');
    delete_option('beecp_div_bd2');
    delete_option('beecp_div_cl2');

    delete_option('beecp_div_bg3');
    delete_option('beecp_div_bd3');
    delete_option('beecp_div_cl3');
}

// add menu admin
add_action('admin_menu', 'beecp_menu');
function beecp_menu() {
  add_menu_page( __( 'Bee Color Picker', 'beecp' ), __( 'Bee Color Picker', 'beecp' ), 'edit_theme_options', 'beecp_setting', 'create_beecp_setting', 'dashicons-admin-generic', '80.1' );
}

// admin setting
function create_beecp_setting() {
  if ( isset( $_POST['submit'] ) ) {
    update_option('beecp_title_theme', stripslashes( $_POST['beecp_title_theme'] ) );
    update_option('beecp_color_main_one', stripslashes( $_POST['beecp_color_main_one'] ) );
    update_option('beecp_color_main_two', stripslashes( $_POST['beecp_color_main_two'] ) );
    update_option('beecp_active_color_text', intval( $_POST['beecp_active_color_text'] ) );
    update_option('beecp_color_text', stripslashes( $_POST['beecp_color_text'] ) );

    update_option('beecp_active_color_primary', intval( $_POST['beecp_active_color_primary'] ) );
    update_option('beecp_color_primary', stripslashes( $_POST['beecp_color_primary'] ) );

    update_option('beecp_div_bg1', stripslashes( $_POST['beecp_div_bg1'] ) );
    update_option('beecp_div_bd1', stripslashes( $_POST['beecp_div_bd1'] ) );
    update_option('beecp_div_cl1', stripslashes( $_POST['beecp_div_cl1'] ) );

    update_option('beecp_div_bg2', stripslashes( $_POST['beecp_div_bg2'] ) );
    update_option('beecp_div_bd2', stripslashes( $_POST['beecp_div_bd2'] ) );
    update_option('beecp_div_cl2', stripslashes( $_POST['beecp_div_cl2'] ) );

    update_option('beecp_div_bg3', stripslashes( $_POST['beecp_div_bg3'] ) );
    update_option('beecp_div_bd3', stripslashes( $_POST['beecp_div_bd3'] ) );
    update_option('beecp_div_cl3', stripslashes( $_POST['beecp_div_cl3'] ) );

    update_option('beecp_link', stripslashes( $_POST['beecp_link'] ) );
  }
?>
  <div class="wrap">
    <h2>Bee Color Picker</h2>
    <style type="text/css">
		fieldset.beecl {
			float: left;
			width: 45%;
			margin-right: 10px;
			border: 1px solid #dcdcdc;
			padding: 10px;
			margin-bottom: 15px;
		}
		p.submit {
			clear: both;
		}
    </style>
    <form method="post" action="">
      <table class="form-table">
        <tbody>

			<tr>
				<th scope="row"><label for="blogname">Tên Theme</label></th>
				<td><input name="beecp_title_theme" type="text" value="<?php echo get_option( 'beecp_title_theme' ) ?>" class="regular-text"></td>
			</tr>

			<tr>
	            <th scope="row"><label for="blogname">Màu chủ đạo 1 mặc định</label></th>
	            <td><input name="beecp_color_main_one" type="text" value="<?php echo get_option( 'beecp_color_main_one' ) ?>" class="regular-text"></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Màu chủ đạo 2 mặc định</label></th>
	            <td><input name="beecp_color_main_two" type="text" value="<?php echo get_option( 'beecp_color_main_two' ) ?>" class="regular-text"></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Bật chỉnh màu phụ 1</label></th>
	            <td><input name="beecp_active_color_primary" type="checkbox" value="1" <?php if( get_option( 'beecp_active_color_primary' ) == 1 ) echo 'checked="checked"' ?>></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Màu phụ mặc định</label></th>
	            <td><input name="beecp_color_primary" type="text" value="<?php echo get_option( 'beecp_color_primary' ) ?>" class="regular-text"></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Bật chỉnh màu phụ 2</label></th>
	            <td><input name="beecp_active_color_text" type="checkbox" value="1" <?php if( get_option( 'beecp_active_color_text' ) == 1 ) echo 'checked="checked"' ?>></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Màu chữ mặc định</label></th>
	            <td><input name="beecp_color_text" type="text" value="<?php echo get_option( 'beecp_color_text' ) ?>" class="regular-text"></td>
      		</tr>

      		<tr>
	            <th scope="row"><label for="blogname">Link nhận thông tin</label></th>
	            <td><input name="beecp_link" type="text" value="<?php echo get_option( 'beecp_link' ) ?>" class="regular-text"></td>
      		</tr>

      </tbody>
    </table>

    <fieldset class="beecl">
    	<legend>Màu chủ đạo</legend>

	    	<p>
	    		<label>Vùng thay đổi nền</label>
	    		<input name="beecp_div_bg1" type="text" value="<?php echo get_option( 'beecp_div_bg1' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi viền</label>
	    		<input name="beecp_div_bd1" type="text" value="<?php echo get_option( 'beecp_div_bd1' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi chữ</label>
	    		<input name="beecp_div_cl1" type="text" value="<?php echo get_option( 'beecp_div_cl1' ) ?>" class="regular-text">
	    	</p>

    </fieldset>

    <fieldset class="beecl">
    	<legend>Màu phụ 1</legend>

	    	<p>
	    		<label>Vùng thay đổi nền</label>
	    		<input name="beecp_div_bg2" type="text" value="<?php echo get_option( 'beecp_div_bg2' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi viền</label>
	    		<input name="beecp_div_bd2" type="text" value="<?php echo get_option( 'beecp_div_bd2' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi chữ</label>
	    		<input name="beecp_div_cl2" type="text" value="<?php echo get_option( 'beecp_div_cl2' ) ?>" class="regular-text">
	    	</p>

    </fieldset>

    <fieldset class="beecl">
    	<legend>Màu phụ 2</legend>

	    	<p>
	    		<label>Vùng thay đổi nền</label>
	    		<input name="beecp_div_bg3" type="text" value="<?php echo get_option( 'beecp_div_bg3' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi viền</label>
	    		<input name="beecp_div_bd3" type="text" value="<?php echo get_option( 'beecp_div_bd3' ) ?>" class="regular-text">
	    	</p>

	    	<p>
	    		<label>Vùng thay đổi chữ</label>
	    		<input name="beecp_div_cl3" type="text" value="<?php echo get_option( 'beecp_div_cl3' ) ?>" class="regular-text">
	    	</p>

    </fieldset>

    <?php
      submit_button();
    ?>
    </form>
  </div>
<?php
}

add_action( "wp_enqueue_scripts", "beecp_add_script" );
function beecp_add_script() {
	wp_enqueue_style( "beecp-css", plugins_url('/css/beecp_colorpicker.css', __FILE__) );
	wp_enqueue_script( "beecp-js", plugins_url('/js/beecp_colorpicker.js', __FILE__), array("jquery"), "1.0", false );
}

add_action( 'genesis_before', function() {
	$beecp_title_theme = get_option( 'beecp_title_theme' );
	$beecp_active_color_text = get_option( 'beecp_active_color_text' );

	$beecp_active_color_primary = get_option( 'beecp_active_color_primary' );
	$beecp_color_primary = get_option( 'beecp_color_primary' );

	$beecp_color_main_one = get_option( 'beecp_color_main_one' );
	$beecp_color_main_two = get_option( 'beecp_color_main_two' );
	$beecp_color_text = get_option( 'beecp_color_text' );
?>
    <div id="btaddon">
      <div class="container">
          <h3 class="demo-title">Mã Web: <?php echo $beecp_title_theme ?></h3>

          <div id="pickcolor-main">
            <div class="label">
              <span>Màu chủ đạo</span>
              <select>
                <option value="one">Đơn màu</option>
                <option value="two">Đổ bóng</option>
              </select>
            </div>

            <div id="pickbg-one" class="pickcolor">
            	<div class="wrap" style="background-color: #<?php echo $beecp_color_main_one ?>"></div>
            </div>

            <div id="pickbg-two" class="pickcolor">
              <div class="wrap" style="background-color: #<?php echo $beecp_color_main_two ?>"></div>
            </div>

            <button id="apply">Áp dụng</button>

          </div>

        <?php if ( $beecp_active_color_primary == 1 ) : ?>
          <div id="pickcolor-label">
            <div class="label">Màu phụ 1</div>
            <div id="pickbg-primary" class="pickcolor"><div class="wrap" style="background-color: #<?php echo $beecp_color_primary ?>"></div></div>
          </div>
        <?php endif; ?>

		<?php if ( $beecp_active_color_text == 1 ) : ?>
          <div id="pickcolor-label">
            <div class="label">Màu phụ 2</div>
            <div id="pickcl" class="pickcolor"><div class="wrap" style="background-color: #<?php echo $beecp_color_text ?>"></div></div>
          </div>
        <?php endif; ?>

          <div class="pick-theme">
            <span databg-one="<?php echo $beecp_color_main_one ?>" datacl-text="<?php echo $beecp_color_text ?>" data-title="<?php echo $beecp_title_theme ?>"><i class="fa fa-shopping-cart"></i> Đăng kí mua</span>
          </div>
        </div>
    </div>
<?php
}, 1 );

add_action( 'wp_footer', function() {

    $beecp_div_bg1 = get_option( 'beecp_div_bg1' );
    $beecp_div_bd1 = get_option( 'beecp_div_bd1' );
    $beecp_div_cl1 = get_option( 'beecp_div_cl1' );

    $beecp_div_bg2 = get_option( 'beecp_div_bg2' );
    $beecp_div_bd2 = get_option( 'beecp_div_bd2' );
    $beecp_div_cl2 = get_option( 'beecp_div_cl2' );

    $beecp_div_bg3 = get_option( 'beecp_div_bg3' );
    $beecp_div_bd3 = get_option( 'beecp_div_bd3' );
    $beecp_div_cl3 = get_option( 'beecp_div_cl3' );

	$beecp_color_main_one = get_option( 'beecp_color_main_one' );
	$beecp_color_main_two = get_option( 'beecp_color_main_two' );
	$beecp_color_primary = get_option( 'beecp_color_primary' );
	$beecp_color_text = get_option( 'beecp_color_text' );
	$beecp_link = get_option( 'beecp_link' );
?>
	<script type="text/javascript">
		(function($) {
			"use strict";
		$( document ).ready( function() {

			var beecp_div_bg1 = $('<?php echo $beecp_div_bg1 ?>');
			var beecp_div_bd1 = $('<?php echo $beecp_div_bd1 ?>');
			var beecp_div_cl1 = $('<?php echo $beecp_div_cl1 ?>');

			var beecp_div_bg2 = $('<?php echo $beecp_div_bg2 ?>');
			var beecp_div_bd2 = $('<?php echo $beecp_div_bd2 ?>');
			var beecp_div_cl2 = $('<?php echo $beecp_div_cl2 ?>');

			var beecp_div_bg3 = $('<?php echo $beecp_div_bg3 ?>');
			var beecp_div_bd3 = $('<?php echo $beecp_div_bd3 ?>');
			var beecp_div_cl3 = $('<?php echo $beecp_div_cl3 ?>');

			function checkcolor() {
		        return $('#pickcolor-main select').val();
		    }

		    // pick color
		    $('#pickbg-one').ColorPicker({
		        color: '#<?php echo $beecp_color_main_one ?>',
		        onShow: function (colpkr) {
		            $(colpkr).fadeIn(500);
		            return false;
		        },
		        onHide: function (colpkr) {
		            $(colpkr).fadeOut(500);
		            return false;
		        },
		        onChange: function (hsb, hex, rgb) {
		            $('#pickbg-one .wrap').css('backgroundColor', '#' + hex);
		            if ( checkcolor() == 'one' ) {
		                $(beecp_div_bg1).css('background', '#' + hex);
		            }
		            $(beecp_div_bd1).css('border-color', '#' + hex);
		            $(beecp_div_cl1).css('color', '#' + hex);
	                $('.pick-theme span').attr('databg-one', hex);
		        }
		    });
		    // end pick one

		    // pick two
		    $('#pickcolor-main select').change( function() {
		        var val = checkcolor();
		        if ( val == 'two' ) {
		            $('#pickbg-two, #apply').fadeIn();
		        } else {
		            $('#pickbg-two, #apply').fadeOut();
		        }
		    } );
		    $('#pickbg-two').ColorPicker({
		        color: '#<?php echo $beecp_color_main_two ?>',
		        onShow: function (colpkr) {
		            $(colpkr).fadeIn(500);
		            return false;
		        },
		        onHide: function (colpkr) {
		            $(colpkr).fadeOut(500);
		            return false;
		        },
		        onChange: function (hsb, hex, rgb) {
		            $('#pickbg-two .wrap').css('backgroundColor', '#' + hex);
		            $('.pick-theme span').attr('databg-two', hex);
		        }
		    });

		    $('#apply').on('click', function() {
		        var bg1 = $('.pick-theme span').attr('databg-one');
		        var bg2 = $('.pick-theme span').attr('databg-two');
		        if ( bg2 ) {
		            $(beecp_div_bg1).css( 'background', '-webkit-linear-gradient(top, #' + bg1 + ' , #' + bg2 + ')');
		            $(beecp_div_bg1).css( 'background', '-moz-linear-gradient(top, #' + bg1 + ' , #' + bg2 + ')');
		            $(beecp_div_bg1).css( 'background', '-o-linear-gradient(top, #' + bg1 + ' , #' + bg2 + ')');
		            $(beecp_div_bg1).css( 'background', 'linear-gradient(to bottom, #' + bg1 + ' , #' + bg2 + ')');
		        }
		    });
		    // end pick two

		    // pick color primary
		    $('#pickbg-primary').ColorPicker({
		        color: '#<?php echo $beecp_color_primary ?>',
		        onShow: function (colpkr) {
		            $(colpkr).fadeIn(500);
		            return false;
		        },
		        onHide: function (colpkr) {
		            $(colpkr).fadeOut(500);
		            return false;
		        },
		        onChange: function (hsb, hex, rgb) {
		            $('#pickbg-primary .wrap').css('backgroundColor', '#' + hex);
		            $(beecp_div_bg2).css('background', '#' + hex);
		            $(beecp_div_bd2).css('border-color', '#' + hex);
		            $(beecp_div_cl2).css('color', '#' + hex);
	                $('.pick-theme span').attr('databg-primary', hex);
		        }
		    });
		    // end pick one

		    // pick color text
		    $('#pickcl').ColorPicker({
		        color: '<?php echo $beecp_color_text ?>',
		        onShow: function (colpkr) {
		            $(colpkr).fadeIn(500);
		            return false;
		        },
		        onHide: function (colpkr) {
		            $(colpkr).fadeOut(500);
		            return false;
		        },
		        onChange: function (hsb, hex, rgb) {
		            $('#pickcl .wrap').css('backgroundColor', '#' + hex);
		            $(beecp_div_bg3).css('background', '#' + hex);
		            $(beecp_div_bd3).css('border-color', '#' + hex);
		            $(beecp_div_cl3).css('color', '#' + hex);
		            $('.pick-theme span').attr('datacl-text', hex);
		        }
		    });
		    // end pick color text

		    // click pick theme
		    $('.pick-theme span').click( function() {
		        var bg1 = $(this).attr('databg-one');
		        var cl = $(this).attr('datacl-text');
		        var bg2 = $(this).attr('databg-two');
		        var bgprimary = $(this).attr('databg-primary');
		        var title = $(this).attr('data-title');
		        if ( bg2 && !bgprimary ) {
		        	window.open('<?php echo $beecp_link ?>?data-title=' + title + '&databg-one=' + bg1 + '&databg-two=' + bg2 + '&datacl-text=' + cl, '_blank');
		        } else if ( bgprimary && !bg2 ) {
		        	window.open('<?php echo $beecp_link ?>?data-title=' + title + '&databg-one=' + bg1 + '&databg-primary=' + bgprimary + '&datacl-text=' + cl, '_blank');
		        } else if( bg2 && bgprimary ) {
		        	window.open('<?php echo $beecp_link ?>?data-title=' + title + '&databg-one=' + bg1 + '&databg-two=' + bg2 + '&databg-primary=' + bgprimary + '&datacl-text=' + cl, '_blank');
		        }else {
		        	window.open('<?php echo $beecp_link ?>?data-title=' + title + '&databg-one=' + bg1 + '&datacl-text=' + cl, '_blank');
		        }
		    });

		});
		})(jQuery);
	</script>
<?php
} );