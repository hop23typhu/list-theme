<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kientrucsh_db');

/** MySQL database username */
define('DB_USER', 'kientrucsh_db');

/** MySQL database password */
define('DB_PASSWORD', 'syBkleK0L');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_SITEURL', 'http://kientrucshome.com');
define('WP_HOME', 'http://kientrucshome.com');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eZ[ 6%;jzF3GXZNdA1|&rQ2s24)hn?J;V>y7qDT>~GhsMjeL?GD`.(>LD1KuZAVI');
define('SECURE_AUTH_KEY',  '<S^1Z)5v/yQQyO;m{[b[16z?jYF~9(aY2Jon{)vpFzwr2=wvP9?1[+of&_*1~j47');
define('LOGGED_IN_KEY',    ';K`vJOsFqY;ExuGQ%~<t&V}Sy`lgvicuq8o;H=^5-JW~i;g x%+,6olTC<V6/+9p');
define('NONCE_KEY',        'ISQ.X6W]GG[<rxh9MM>r=1UaCSycJJg4McfW2&E`qaO.JNMVelH9#1jChng7K5tj');
define('AUTH_SALT',        'HEfoc1[^HL{PR^7eMG`Y=JP>vZ?a5zYlp#0tR7]1gJl@ah6OC8TzCEf8hr&C*c?=');
define('SECURE_AUTH_SALT', 'y]cg)dwx+jacp*#(&J#@M5|WYeOFZ/RN1|dl QV~.jz>T_rFuoTGzDr}};DY;c_?');
define('LOGGED_IN_SALT',   'xd=s1kb[Oz`g;dHML:evwOP&+&9XtZdqFkHVcyf)/eAJ2REYw6>LM)LDvQ$5P-iD');
define('NONCE_SALT',       'A&tT&!m -85WH+oG@7S7ZAYMsX:EGa:?6><3,&y>..]c}b1?P:__5C);y1U[&/n9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bt_';

define( 'WP_POST_REVISIONS', 1 );

define('DISALLOW_FILE_EDIT', true);

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
