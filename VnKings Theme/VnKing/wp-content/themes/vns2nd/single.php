<?php get_header();?>
		<div class="content_vn clearfix">
			<div class="col-1200">
				<div class="right-ct">
				<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				<div class="top1-tienluc">
					<h1><a href="<?php the_permalink() ;?>" title="<?php the_title() ;?>"><?php the_title() ;?></a></h1>
				</div>
					<div class="content-single">
						<div class="post_bottom_bg">					
							<span itemtype="http://schema.org/Person" itemscope="itemscope" itemprop="author">
							<i class="fa fa-user"></i> <a class="entry-author-link" rel="author" itemprop="url" href="<?php the_author_url(); ?>"><span class="entry-author-name" itemprop="name"><?php the_author(); ?></span></a>
							</span>
							<span itemprop="datePublished"><i class="fa fa-calendar-o"></i> <?php the_time('d/m/y') ;?></span>
							<span itemprop="dateModified"> <i class="fa fa-refresh"></i> <?php the_modified_date(); ?></p></span>
							<span><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Lượt xem</span>	
							<span><a href="<?php comments_link(); ?>"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '%'); ?> Bình luận </a></span>
							<span><i class="fa fa-folder-open"></i> <?php the_category();?></span>
							<span><i class="fa fa-pencil-square-o"></i> <?php edit_post_link( __( 'Sửa', 'vns2' )); ?></span>
							<?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings($pid); endif; ?>
						</div>
						<?php  while (have_posts()):the_post();?>
							<?php the_content() ;?>
							<?php setPostViews(get_the_ID()); ?>
						<?php $i++; endwhile ; wp_pagenavi(); wp_reset_query()  ;?>
					</div>
					<div class="comment-single">
						<?php
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
		
		<div class="bottom_vn clearfix">
			<div class="col-1200">
				<?php
					if(is_active_sidebar('love-bottom-1')){
					dynamic_sidebar('love-bottom-1');
					}
				?>
			</div>
		</div>
	<?php get_footer();?>