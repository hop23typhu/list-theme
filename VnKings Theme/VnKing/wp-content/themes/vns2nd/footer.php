	<div class="footer_vn">
		<div class="col-1200">
			<div class="large-footer">			
				<?php
					if(is_active_sidebar('love-footer-1')){
					dynamic_sidebar('love-footer-1');
					}
				?>
				<div class="clear"></div>
				</div>
		</div>
	</div>
	<div class="small-footer">
		<div class="col-1200">
			<div class="sixteen powerby clearfix">
		<div class="copyright">
			<p>Powered By <a href="http://vnkings.com/">VnKings.Com</a></p>
			<p>Thiết Kế bởi <a target="_blank" href="https://www.facebook.com/viruslove.nd">Tiến Lực</a></p>
		</div>
		<ul class="socials">
			<li><a href="<?php global $shortname; echo get_option($shortname.'_twitter');?>" class="twitter">twitter</a></li>
			<li><a href="<?php global $shortname; echo get_option($shortname.'_facebook');?>" class="facebook">facebook</a></li>
			<li><a href="<?php global $shortname; echo get_option($shortname.'_google');?>" class="googlep">google+</a></li>
			<li><a href="<?php global $shortname; echo get_option($shortname.'_vimeo');?>" class="vimeo">vimeo</a></li>
			<li><a href="<?php global $shortname; echo get_option($shortname.'_skype');?>" class="skype">skype</a></li>
			<li><a href="<?php global $shortname; echo get_option($shortname.'_in');?>" class="linked">linked</a></li>
		</ul>
	</div>
		</div>
	</div>
		<?php wp_footer();?>
	</div>
	<nav id="menu">
		<ul>
			<?php $defaults = array(
			'theme_location'  => 'mobile-menu',
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
			); ?>
			<?php wp_nav_menu( $defaults ); wp_reset_query();?>
		</ul>
	</nav>
</body>
</html>