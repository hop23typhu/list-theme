<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="icon" href="<?php global $shortname; echo get_option($shortname.'_favico');?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?php bloginfo('stylesheet_directory');?>/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory');?>/css/font-awesome.css">
	
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('stylesheet_directory');?>/js/menulovend.js"></script>
	<?php wp_head();?>
	<script type="text/javascript">
			$(function() {
				$('nav#menu').mmenu();
			});
	</script>
</head>
<body <?php body_class( $class ); ?>>
	<div id="container_vn">
		<div class="header_vn">
			<div class="top-menu">
				<div class="col-1200">
					<div class="menu-left">
						<ul class="menu-nav">
						<?php $defaults = array(
							'theme_location'  => 'top-menu',
							'menu'            => '',
							'container'       => '',
							'container_class' => '',
							'container_id'    => '',
							'menu_class'      => '',
							'menu_id'         => '',
							'echo'            => true,
							'fallback_cb'     => 'wp_page_menu',
							'before'          => '',
							'after'           => '',
							'link_before'     => '',
							'link_after'      => '',
							'items_wrap'      => '%3$s',
							'depth'           => 0,
							'walker'          => ''
						); ?>
						<?php wp_nav_menu( $defaults ); wp_reset_query();?>  	
						</ul>		
					</div>
					<div class="info-right">
					</div>
				</div>
			</div>
			<div class="logo">
			<div class="header"><a href="#menu"></a></div>
			<a href="<?php bloginfo('url');?>"><img src="<?php global $shortname; echo get_option($shortname.'_logo');?>"></a></div>
			<div class="mainNav">
				<div class="col-1200">
					<ul class="menu">
					 <?php $defaults = array(
						'theme_location'  => 'header-menu',
						'menu'            => '',
						'container'       => '',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '%3$s',
						'depth'           => 0,
						'walker'          => ''
					); ?>
					<?php wp_nav_menu( $defaults ); wp_reset_query();?>
					</ul>
					<div class="search-nd">
						<form method="get" action="<?php bloginfo('url');?>">
							<input type="text" placeholder="Tìm kiếm..." value="" class="query" name="s">
							<button type="submit" class="search-button"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
			</div>
		</div>