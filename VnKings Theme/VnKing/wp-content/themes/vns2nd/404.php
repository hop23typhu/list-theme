<?php
/*
 Template Name: 404 Not Found
 */
 ?>
<?php get_header();?>
<link rel='stylesheet' id='wpuf-css'  href='<?php bloginfo('url');?>/wp-content/plugins/wp-user-frontend/css/wpuf.css?ver=4.1.2' type='text/css' media='all' />
		<div class="content_vn clearfix">
			<div class="col-1200">
				<div class="right-ct">
				<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				<div class="top1-tienluc">
					<h1><a href="<?php the_permalink() ;?>" title="<?php the_title() ;?>"><?php the_title() ;?></a></h1>
				</div>
					<div class="content-single">
						<?php  while (have_posts()):the_post();?>
							<?php the_content() ;?>
							<?php setPostViews(get_the_ID()); ?>
						<?php $i++; endwhile ; wp_pagenavi(); wp_reset_query()  ;?>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
		
		<div class="bottom_vn clearfix">
			<div class="col-1200">
				<?php
					if(is_active_sidebar('love-bottom-1')){
					dynamic_sidebar('love-bottom-1');
					}
				?>
			</div>
		</div>
	<?php get_footer();?>