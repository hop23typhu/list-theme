<?php get_header();?>
		<div class="content_vn clearfix">
			<div class="col-1200">
				<div class="right-ct">
				<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				<div class="top1-tienluc">
					<h1><a href="<?php echo $link; ?>" title="<?php the_permalink() ;?>">Kết quả tìm kiếm cho: <?php echo get_search_query();?></a></h1>
				</div>
					<div class="content-right2">
						<?php  while (have_posts()):the_post();?>
						<div class="post-top-news full">
							<a class="thumb-post" href="<?php the_permalink() ;?>"><?php the_post_thumbnail('love_topic',array( 'title' => get_the_title(),'alt' => get_the_title() ));?></a>
							<div class="info-content"><h2 class="head-hd"><a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"><?php the_title() ;?></a></h2>
								<p><?php echo excerpt(30) ;?></p>
								<div class="post_bottom_bg">					
									<span><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
									<span><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Lượt xem</span>	
									<span><a href="<?php comments_link(); ?>"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '%'); ?> Bình luận </a></span>
									<span><i class="fa fa-folder-open"></i> <?php the_category();?></span>										
								</div>
							</div>
						</div>
						
						<?php $i++; endwhile ; wp_pagenavi(); wp_reset_query()  ;?>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
		
		<div class="bottom_vn clearfix">
			<div class="col-1200">
				<?php
					if(is_active_sidebar('love-bottom-1')){
					dynamic_sidebar('love-bottom-1');
					}
				?>
			</div>
		</div>
	<?php get_footer();?>