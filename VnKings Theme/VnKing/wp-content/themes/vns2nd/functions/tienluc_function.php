<?php
global $product;
// WIDGET  PIC POSTS LOVEND
add_action( 'widgets_init', 'create_posts_pic_lovend_widget' );
function create_posts_pic_lovend_widget() {
	register_widget( 'Posts_Picture' );
}
class Posts_Picture extends WP_Widget {
	// Process widget
	function Posts_Picture() {
		$widget_ops = array(
			'classname'   => 'lovepostnd',
			'description' => 'Bài viết theo danh mục'
		);
		$this->WP_Widget( 'lovepostnd', __( 'Lovend Category' ), $widget_ops );
	}
	// Build the widget settings form
	function form( $instance ) {
		$defaults  = array( 'title' => '', 'category' => '', 'number' => 5,);
		$instance  = wp_parse_args( ( array ) $instance, $defaults );
		$categorynd  = $instance['category'];
		$number    = $instance['number'];
		?>
		<p>
			<label for="Posts_Picture_username"><?php _e( 'Danh mục' ); ?>:</label>				
			<?php
			wp_dropdown_categories( array(
				'orderby'    => 'title',

				'hide_empty' => false,

				'name'       => $this->get_field_name( 'category' ),

				'id'         => 'Posts_Picture_category',

				'class'      => 'widefat',

				'selected'   => $categorynd

			) );

			?>

		</p>

		<p>

			<label for="Posts_Picture_number"><?php _e( 'Số bài' ); ?>: </label>

			<input type="text" id="Posts_Picture_number" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo esc_attr( $number ); ?>" size="3" />

		</p>			

		<?php

	}

	// Save widget settings

	function update( $new_instance, $old_instance ) {

		$instance              = $old_instance;

		$instance['category']  = wp_strip_all_tags( $new_instance['category'] );

		$instance['number']    = is_numeric( $new_instance['number'] ) ? intval( $new_instance['number'] ) : 5;

		return $instance;

	}

	// Display widget

	function widget( $args, $instance ) {

		extract( $args );

		echo $before_widget;

		$categorynd  = $instance['category'];

		$number    = $instance['number'];

		$cat_recent_posts = new WP_Query( array( 

			'post_type'      => 'post',

			'posts_per_page' => $number,

			'cat'            => $categorynd

		) ); ?>

<div class="box-heading2"> <h3> <a title="<?php echo get_cat_name( $categorynd);?>" href="<?php echo get_category_link( $categorynd ); ?>"><?php echo get_cat_name( $categorynd);?></a></h3></div>

<div class="post-summary-list">
							<ul>
<?php $vonglap9 = new wp_query('showposts='.$number.'&cat='.$categorynd.''); $i=1 ;  while ($vonglap9->have_posts()): $vonglap9->the_post();?>
<li><i class="fa fa-caret-right"></i> <a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"><?php the_title_limit(30);?></a></li>
<?php $i++; endwhile ; wp_reset_query() ;?>
</ul>
</div>			
				<?php echo $after_widget;
	}
}

/** Welcome **/
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');
	function my_custom_dashboard_widgets() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'Giới thiệu', 'lovend_system');
	}
	function lovend_system() { ?>
    <p>Chào mừng Quý khách đến với hệ thống Quản Trị Website.<p>
    <p><strong>THÔNG TIN WEBSITE</strong></p>
    <P><?php echo bloginfo( 'name' ); ?> | <?php echo bloginfo( 'description' ); ?></p>
    <p>Hệ thống được phát triển bởi <strong><a href="http://facebook.com/viruslove.nd">Tiến Lực</a></strong> trên nền tảng <strong> WordPress <?php echo bloginfo("version") ; ?> </strong>.</p>
    <p><strong>Đoàn Tiến Lực </strong> 
    <p> Web Developer</p> 
    <p><strong>Email</strong>: doantienluc@gmail.com <strong>Website</strong>: <a href="http://vnkings.com/">VnKings.Com</a></p> 
    <p>Cảm ơn bạn đã sử dụng và ủng hộ giao diện của Mình!</p>
	<?php }

// WIDGET  PIC POSTS LOVEND
add_action( 'widgets_init', 'create_posts_image_widget' );
function create_posts_image_widget() {
	register_widget( 'Posts_Image' );
}
class Posts_Image extends WP_Widget {
	// Process widget
	function Posts_Image() {
		$widget_ops = array(
			'classname'   => 'lovepostndpic',
			'description' => 'Bài viết kèm hình ảnh'
		);
		$this->WP_Widget( 'lovepostndpic', __( 'Lovend Posts' ), $widget_ops );
	}
	// Build the widget settings form
	function form( $instance ) {
		$defaults  = array( 'title' => '', 'number' => 5);
		$instance  = wp_parse_args( ( array ) $instance, $defaults );
		$title     = $instance['title'];
		$number    = $instance['number'];
		?>
		<p>
			<label for="Posts_Image_title"><?php _e( 'Title' ); ?>:</label>
			<input type="text" class="widefat" id="Posts_Image_title" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="Posts_Image_number"><?php _e( 'Số bài' ); ?>: </label>
			<input type="text" id="Posts_Image_number" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo esc_attr( $number ); ?>" size="3" />
		</p>			
		<?php
	}
	// Save widget settings
	function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = wp_strip_all_tags( $new_instance['title'] );
		$instance['number']    = is_numeric( $new_instance['number'] ) ? intval( $new_instance['number'] ) : 5;
		return $instance;
	}
	// Display widget
	function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
		$number    = $instance['number'];
		if ( !empty( $title ) ) echo $before_title . $title . $after_title;
		$cat_recent_posts = new WP_Query( array( 
			'post_type'      => 'post',
			'posts_per_page' => $number,
		) );
		if ( $cat_recent_posts->have_posts() ) {
			echo '<div class="sidebar-latest-news"><ul>';
			while ( $cat_recent_posts->have_posts() ) {
				$cat_recent_posts->the_post();
				echo '<li>';?>
				<a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"> 
					<?php the_post_thumbnail('footer_topic',array( 'title' => get_the_title(),'alt' => get_the_title() ));?>
				</a>
				
				<p><a class="blog_title" href="<?php the_permalink() ;?>"><?php the_title() ;?></a></p>
				<p class="blog_sum"><?php echo excerpt(6) ;?></p>
				
								
					<p><a class="latest-news-read-more" href="<?php the_permalink() ;?>">Đọc tiếp »</a></p>			
				<div class="clear"></div>
				<?php echo '</li>';
			}
			echo '</ul></div>';
		} else {
			echo 'No posts yet...';
		}
		wp_reset_postdata();
		echo $after_widget;
	}
}
?>