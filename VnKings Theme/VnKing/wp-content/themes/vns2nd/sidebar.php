<div class="sidebar-nd">
	<div class="box-category">
		<div class="box-heading2"><h3>Danh mục</h3></div>
		<ul class="menu-child-ul">
		<?php $defaults = array(
			'theme_location'  => 'sidebar-menu',
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '<i class="fa fa-heart-o"></i>',
			'link_after'      => '',
			'items_wrap'      => '%3$s',
			'depth'           => 0,
			'walker'          => ''
		); ?>
		<?php wp_nav_menu( $defaults ); wp_reset_query();?>
		</ul>
	</div>
	<div class="box-category">
		<div class="box-heading2"><h3>Thành viên</h3></div>
		<div class="login-or-infouser">
		<?php if(is_user_logged_in()) {  $user_id = get_current_user_id();$current_user = wp_get_current_user();$profile_url = get_author_posts_url($user_id);$edit_profile_url  = get_edit_profile_url($user_id);?>
		<div class="info-user">
			<a href="<?php echo $profile_url ?>" class="name-user"><?php echo $current_user->display_name; ?></a>
			<a class="user-logout" href="<?php echo esc_url(wp_logout_url($current_url)); ?>">Thoát</a>
		</div>
		
		<?php } else { ?>
		 <div class="loginBox">
		<?php wp_login_form(); ?></div>
		<?php } ?>
		</div>
	</div>
	<div class="box-category truyenbaihot">
		<div class="box-heading2"><h3>Top bài Hot</h3></div>
		<div class="post-summary-list">
			<ul>
			<?php $loop2 = new wp_query('showposts=10'); while ($loop2->have_posts()): $loop2->the_post();?>
				<li><i class="fa fa-caret-right"></i> <a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"><?php the_title() ;?></a></li>
			<?php endwhile ; wp_reset_query() ;?>
			</ul>
		</div>
	</div>
	<?php
		if(is_active_sidebar('love-left-1')){
		dynamic_sidebar('love-left-1');
		}
	?>

</div>