<?php
/*
 Template Name: Đăng Ký
 */
 ?>
<?php get_header();?>
<link rel='stylesheet' id='wpuf-css'  href='<?php bloginfo('url');?>/wp-content/plugins/wp-user-frontend/css/wpuf.css?ver=4.1.2' type='text/css' media='all' />
		<div class="content_vn clearfix">
			<div class="col-1200">
				<div class="right-ct">
				<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				<div class="top1-tienluc">
					<h1><a href="<?php the_permalink() ;?>" title="<?php the_title() ;?>"><?php the_title() ;?></a></h1>
				</div>
					<div class="content-single">
						
						<?php if(is_user_logged_in()) { $user_id = get_current_user_id();$current_user = wp_get_current_user();$profile_url = get_author_posts_url($user_id);$edit_profile_url  = get_edit_profile_url($user_id); ?>
							<div class="regted">
							Bạn đã đăng nhập với tên nick <a href="<?php echo $profile_url ?>"><?php echo $current_user->display_name; ?></a> Bạn có muốn <a href="<?php echo esc_url(wp_logout_url($current_url)); ?>">Thoát</a> không ?
							</div>
						<?php } else { ?>
						<div class="dangkytaikhoan">	
							<?php
							$err = '';
							$success = '';
							global $wpdb, $PasswordHash, $current_user, $user_ID;
							if(isset($_POST['task']) && $_POST['task'] == 'register' ) {	
								$pwd1 = $wpdb->escape(trim($_POST['pwd1']));
								$pwd2 = $wpdb->escape(trim($_POST['pwd2']));
								$email = $wpdb->escape(trim($_POST['email']));
								$username = $wpdb->escape(trim($_POST['username']));
								
								if( $email == "" || $pwd1 == "" || $pwd2 == "" || $username == "") {
									$err = 'Vui lòng không bỏ trống những thông tin bắt buộc!';
								} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
									$err = 'Địa chỉ Email không hợp lệ!.';
								} else if(email_exists($email) ) {
									$err = 'Địa chỉ Email đã tồn tại!.';
								} else if($pwd1 <> $pwd2 ){
									$err = '2 Password không giống nhau!.';        
								} else {
						 
									$user_id = wp_insert_user( array ('user_pass' => apply_filters('pre_user_user_pass', $pwd1), 'user_login' => apply_filters('pre_user_user_login', $username), 'user_email' => apply_filters('pre_user_user_email', $email), 'role' => 'subscriber' ) );
									if( is_wp_error($user_id) ) {
										$err = 'Error on user creation.';
									} else {
										do_action('user_register', $user_id);
										
										$success = 'Bạn đã đăng ký thành công!';
									}
								}
							}
							?>
								<!--display error/success message-->
							<div id="message">
								<?php
									if(! empty($err) ) :
										echo '<p class="thongbaoregloi">'.$err.'</p>';
									endif;
								?>
								
								<?php
									if(! empty($success) ) :
										$login_page  = home_url( '/dang-nhap.html' );
										echo '<p class="regsuccess">'.$success. '<a href='.$login_page.'> Đăng nhập</a>'.'</p>';
									endif;
								?>
							</div>
							<form method="post">
								<div class="row"><label>Tên đăng nhập</label><input class="input" type="text" value="" name="username" id="username" /></div>
								<div class="row"><label>Email</label><input id="email" class="input" type="text" value="" name="email" id="email" /></div>
								<div class="row"><label>Password</label><input class="input" type="password" value="" name="pwd1" id="pwd1" /></div>
								<div class="row"><label>Nhập lại Password</label><input class="input" type="password" value="" name="pwd2" id="pwd2" /></div>
								<button type="submit" name="btnregister" class="submit-reg button button-primary" >Đăng ký</button>
								<input type="hidden" name="task" value="register" />
							</form>
						</div>
						<div class="thongbaologin">
							<?php
								$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
								if ( $login === "failed" ) {
										echo '<p><strong>ERROR:</strong> Sai username hoặc mật khẩu.!</p>';
								} elseif ( $login === "empty" ) {
										echo '<p><strong>ERROR:</strong> Username và mật khẩu không thể bỏ trống.</p>';
								} elseif ( $login === "false" ) {
										echo '<p><strong>ERROR:</strong> Bạn đã thoát ra.</p>';
								}
							?>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
		
		<div class="bottom_vn clearfix">
			<div class="col-1200">
				<?php
					if(is_active_sidebar('love-bottom-1')){
					dynamic_sidebar('love-bottom-1');
					}
				?>
			</div>
		</div>
	<?php get_footer();?>