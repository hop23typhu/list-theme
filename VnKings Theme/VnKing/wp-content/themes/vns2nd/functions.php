<?php 

add_theme_support( 'post-thumbnails' );
add_image_size( 'love_topic', 250, 160, true );
add_image_size( 'footer_topic', 50, 50, true );
add_filter('show_admin_bar', '__return_false');
add_action( 'after_setup_theme', 'et_setup_theme' );
if ( ! function_exists( 'et_setup_theme' ) ){
	function et_setup_theme(){
		global $themename, $shortname, $default_colorscheme;
		$themename = "vns2nd";
		$shortname = "vns2nd";
		$default_colorscheme = "Blue";
		$template_dir = get_template_directory();
		require_once($template_dir . '/epanel/custom_functions.php');
		require_once($template_dir . '/epanel/core_functions.php');
		require_once($template_dir . '/epanel/post_thumbnails_vns2nd.php');

	}
}
// Tạo Sidebar 
register_sidebar( array(
'name' => 'LoveNd - WG ',
'id' => 'love-left-1',
'description' => 'WG - Left',
'before_widget' => '<div class="box-category">',
'after_widget' => '</div>',
'before_title' => '<div class="box-heading2"><h3>',
'after_title' => '</h3></div>',
) );
register_sidebar( array(
'name' => 'LoveNd - Bottom ',
'id' => 'love-bottom-1',
'description' => 'Bottom - LoveNd',
'before_widget' => '<div class="box-category">',
'after_widget' => '</div>',
'before_title' => '<div class="box-heading2"><h3>',
'after_title' => '</h3></div>',
) );
register_sidebar( array(
'name' => 'LoveNd - Footer ',
'id' => 'love-footer-1',
'description' => 'Footer - LoveNd',
'before_widget' => '<div class="one_third"><div class="widget">',
'after_widget' => '</div></div>',
'before_title' => '<h3 class="title_line"><span>',
'after_title' => '</span></h3>',
) );
require_once (TEMPLATEPATH . '/functions/tienluc_function.php');
function register_main_menus() {
	register_nav_menus(
		array(
		'top-menu' => __( 'Top Menu' ),
		'mobile-menu' => __( 'Mobile-Menu' ),
		'header-menu' => __( 'header-menu' ),
		'sidebar-menu' => __( 'sidebar-menu' ),
		'foot-menu' => __( 'foot-menu' ),
		)
	);
};
if (function_exists('register_nav_menus')) add_action( 'init', 'register_main_menus' );

/*** Tính View ***/
function getPostViews($postID){ // hàm này dùng để lấy số người đã xem qua bài viết
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){ // Nếu như lượt xem không có
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0"; // giá trị trả về bằng 0
    }
    return $count; // Trả về giá trị lượt xem
}
function setPostViews($postID) {// hàm này dùng để set và update số lượt người xem bài viết.
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++; // cộng đồn view
        update_post_meta($postID, $count_key, $count); // update count
    }
}
/*** End Tính View ***/

 /***** Title Limit *****/
function the_title_limit($max_char, $more_link_text = '') {
$title = get_the_title();
$title = strip_tags($title);

if (strlen($_GET['p']) > 0) {
echo $title;
echo " …";
}
else if ((strlen($title)>$max_char) && ($espacio = strpos($title, " ", $max_char ))) {
$title = substr($title, 0, $espacio);
$title = $title;
echo $title;
echo " …";
echo "$more_link_text";
}
else {
echo $title;
}
}
/***** End Title Limit *****/
// Tại giới hạn Excerpt
function excerpt($limit) {
$excerpt = explode(' ', get_the_excerpt(), $limit);
if (count($excerpt)>=$limit) {
array_pop($excerpt);
$excerpt = implode(" ",$excerpt).' ...';
} else {
$excerpt = implode(" ",$excerpt);
}
$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
return $excerpt;
}
// Kết thúc Tạo giới hạn Excerpt
function ilc_mce_buttons($buttons){
  array_push($buttons,
     "backcolor",
     "anchor",
     "hr",
     "fontselect",
     "sub",
     "sup",
     "fontselect",
     "fontsizeselect",
     "styleselect",
     "cleanup"
);
  return $buttons;
}
add_filter("mce_buttons", "ilc_mce_buttons");
// Redirect khi đăng nhập
function my_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    global $user;
    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        //check for admins
        if ( in_array( 'administrator', $user->roles ) ) {
        // redirect them to the default place
            return home_url();
        } else {
            return home_url();
        }
    } else {
		return $redirect_to;
    }
}
add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
function redirect_login_page() {
    $login_page  = home_url( '/dang-nhap/' );
    $page_viewed = basename($_SERVER['REQUEST_URI']); 
    if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }
}
add_action('init','redirect_login_page');
// Kết thúc Redirect khi đăng nhập

// Kiểm tra lỗi đăng nhập
function login_failed() {
    $login_page  = home_url( '/dang-nhap/' );
    wp_redirect( $login_page . '?login=failed' );
    exit;
}
add_action( 'wp_login_failed', 'login_failed' ); 
function verify_username_password( $user, $username, $password ) {
    $login_page  = home_url( '/dang-nhap/' );
    if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);
add_filter('login_errors',create_function('$a', "return null;"));
add_action('template_include','wpse57122_change_on_p2');
function wpse57122_change_on_p2( $template ){
    if( is_front_page() && is_paged() ){
        $template = locate_template(array('archive.php','index.php'));
    }
    return $template;
}
// Bỏ cột trong post
function xko_seo_columns_filter( $columns ) {
    unset($columns['wpseo-title']);
    unset($columns['wpseo-metadesc']);
    unset($columns['wpseo-focuskw']);
    return $columns;
}
add_filter( 'manage_edit-post_columns', 'xko_seo_columns_filter', 10, 1 );
add_filter( 'manage_edit-page_columns', 'xko_seo_columns_filter', 10, 1 );
// Kết thúc bỏ cột trong post
// nofollow link trong bài viết

function lovend_remove_follow($text) {
$return = str_replace('<a', '<a rel="nofollow"', $text);return $return;
}
add_filter('the_content', 'lovend_remove_follow');
// Kết thúc nofollow link trong bài viết

?>