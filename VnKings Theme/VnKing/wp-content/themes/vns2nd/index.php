<?php get_header();?>
		<div class="content_vn clearfix">
			<div class="col-1200">
				<div class="right-ct">
					<div class="content-right1">
						<?php  $loop1 = new WP_Query('showposts=8');  $i = 1;  while ($loop1->have_posts()): $loop1->the_post();?>
							<div class="post-top-news  haft">
								<a class="thumb-post" href="<?php the_permalink() ;?>"><?php the_post_thumbnail('thumbnail',array( 'title' => get_the_title(),'alt' => get_the_title() ));?></a>
								<div class="info-content"><h4 class="head-hd"><a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"><?php the_title() ;?></a></h4>
									<p><?php echo excerpt(6) ;?></p>
									<a class="read-more" href="#">Đọc tiếp <i class="fa fa-angle-double-right"></i></a>
								</div>
							</div>
						<?php $i++; endwhile ; wp_reset_query()  ;?>
						
					</div>
					<div class="content-right2">
						<?php  while (have_posts()):the_post();?>
						<div class="post-top-news full">
							<a class="thumb-post" href="<?php the_permalink() ;?>"><?php the_post_thumbnail('love_topic',array( 'title' => get_the_title(),'alt' => get_the_title() ));?></a>
							<div class="info-content"><h4 class="head-hd"><a title="<?php the_title() ;?>" href="<?php the_permalink() ;?>"><?php the_title() ;?></a></h4>
								<p><?php echo excerpt(30) ;?></p>
								<div class="post_bottom_bg">					
									<span><i class="fa fa-user"></i> <?php the_author_posts_link(); ?></span>
									<span><i class="fa fa-eye"></i> <?php echo getPostViews(get_the_ID()); ?> Lượt xem</span>									
									<span><a href="<?php comments_link(); ?>"><i class="fa fa-comment"></i> <?php comments_number('0', '1', '%'); ?> Bình luận </a></span>
									<span><i class="fa fa-folder-open"></i> <?php the_category();?></span>								
								</div>
							</div>
						</div>
						
						<?php $i++; endwhile ; wp_pagenavi(); wp_reset_query()  ;?>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>
		
		<div class="bottom_vn clearfix">
			<div class="col-1200">
				<?php
					if(is_active_sidebar('love-bottom-1')){
					dynamic_sidebar('love-bottom-1');
					}
				?>
			</div>
		</div>
	<?php get_footer();?>