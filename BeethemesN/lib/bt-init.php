<?php

//Add Footer widget
add_theme_support('genesis-footer-widgets',1); // add number widget in footer
remove_action('genesis_after_header','genesis_do_subnav');
remove_action('genesis_footer','genesis_do_footer');

// theme support
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
add_theme_support( 'custom-background' );
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Menu top', 'genesis' ) ) );
// add_theme_support( 'genesis-responsive-viewport' );

// Add Image Header
function add_image_header() {
  $url = gtid_get_option( 'rt_image_banner' );
  $linkurl = changestring( $url );
  $webname = get_bloginfo('name');
  if ( !empty( $linkurl ) )
  echo "<a class='imgbanner' href='" .WP_HOME. "' title='" .$webname. "'><img src='{$linkurl}' width='980' height='260' alt='" .$webname. "' /></a>";
}
add_action( 'genesis_header','add_image_header' );

// Add slide
function add_slide() {
  $idsl = gtid_get_option( 'rt_slider_home' );
  echo do_shortcode( "[metaslider id={$idsl}]" );
  echo '<div id="text-slogan">';
    dynamic_sidebar('chuchay');
  echo '</div>';
}
add_action( 'genesis_before_content_sidebar_wrap', 'add_slide', 2 );

// add form search
function add_form_search() {
//   echo get_search_form();
}
add_action( 'genesis_before_loop', 'add_form_search', 1 );

// Rename siderbar
genesis_register_sidebar(
    array(
        'id'               => 'header-right',
        'name'             => __( 'Đầu trang', 'genesis' ),
        'description'      => __( 'Chứa các chức năng cần sử dụng. Sẽ hiển thị trên đầu trang.', 'genesis' ),
        '_genesis_builtin' => true,
                'before_title'  => '<h4 class="widget-title widgettitle">', // replaced H4 to H2
        'after_title'   => "</h4>\n", // replaced H4 to H2
    )
);
genesis_register_sidebar(
    array(
        'id'               => 'sidebar',
        'name'             => __( 'Menu Dọc Trái', 'genesis' ),
        'description'      => __( 'Chứa các chức năng cần sử dụng nằm trong cột bên trái.', 'genesis' ),
        '_genesis_builtin' => true,
                'before_title'  => '<h4 class="widget-title widgettitle">', // replaced H4 to H2
        'after_title'   => "</h4>\n", // replaced H4 to H2
    )
);
genesis_register_sidebar(
    array(
        'id'               => 'sidebar-alt',
        'name'             => __( 'Menu dọc phải', 'genesis' ),
        'description'      => __( 'Chứa các chức năng cần sử dụng nằm trong cột bên phải.', 'genesis' ),
        '_genesis_builtin' => true,
                'before_title'  => '<h4 class="widget-title widgettitle">', // replaced H4 to H2
        'after_title'   => "</h4>\n", // replaced H4 to H2
    )
);


add_filter( 'genesis_pre_get_option_site_layout', 'child_do_layout' );
function child_do_layout( $opt ) {
 if ( is_home() ) { // Modify the conditions to apply the layout to here
  $opt = 'full-width-content'; // You can change this to any Genesis layout
  return $opt;
 }
}

/*Them tin tuc sidebar* */
if ( function_exists('register_sidebar') ){
    register_sidebar(array(
        'name' => 'Home - Chữ chạy',
        'id'   => 'chuchay',
        'before_widget' => '<div id="%s" class="widget"><div class="wrap">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
));
}

if ( function_exists('register_sidebar') ){
    register_sidebar(array(
        'name' => 'Home - Tin tức',
        'id'   => 'tt_fb',
        'before_widget' => '<div id="%s" class="widget"><div class="wrap">',
        'after_widget' => '</div></div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>',
));
}
add_action( 'genesis_after_content_sidebar_wrap', function() {
  echo '<div id="footer-bl1">';
    dynamic_sidebar('tt_fb');
  echo '</div>';
} );

$args = array(
  'name'          => __( 'Logo Slider', 'beethemes' ),
  'id'            => 'logo-slider',
  'before_widget' => '<div id="%s" class="logoslider">',
  'after_widget'  => '</div>',
  'before_title'  => '<h4 class="widget-title">',
  'after_title'   => '</h4>'
);
register_sidebar( $args );
add_action( 'genesis_before_footer', function() {
  echo '<div id="logo-slide">';
    dynamic_sidebar('logo-slider');
  echo '</div>';
}, 1 );

// add code funtions
add_action('login_enqueue_scripts', 'my_customlogin_css');
function my_customlogin_css() {
  echo '<link rel="stylesheet" href="' .THEME_URI. '/lib/css/loginadmin.css" type="text/css" media="all" />';
}

remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav' );


//them 1 widget gioi thieu
if ( function_exists('register_sidebar') ){
    register_sidebar(array(
        'name' => 'Nội dung giới thiệu ',
        'id'   => 'gioithieu',
        'before_widget' => '<div id="%s" class="widget"><div class="wrap">',
        'after_widget' => '</div></div>',
        'before_title' => '',
        'after_title' => '',
));
}
add_action( 'genesis_after_entry', function() {
  echo '<div id="gioithieu-bl1"><div class="wrapcontent">';
    dynamic_sidebar('gioithieu');
  echo '</div></div>';
});