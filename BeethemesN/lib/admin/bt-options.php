<?php
/**
 * Bee Option Theme setting
 *
 * @package      Bee Option
 * @author       Sang Minh
 * @copyright    Copyright (c) 2015
 */


/* Define our constants
------------------------------------------------------------ */

define( 'RT_SETTINGS_FIELD', 'rt-options' );

function add_my_js(){
	wp_enqueue_script( "my-upload", THEME_URI. "/lib/js/upload.js", array( "jquery" ) );
	wp_enqueue_media();
}
add_action('admin_enqueue_scripts','add_my_js');


/* Setup default options
------------------------------------------------------------ */
function rt_default_theme_options() {
	$options = array(
		'rt_num_home_boxes' => 1,
        'rt_image_banner' => '',
        'rt_adv'		=> 0,
	);
	return apply_filters( 'rt_default_theme_options', $options );
}

/* Sanitize any inputs
------------------------------------------------------------ */

add_action( 'genesis_settings_sanitizer_init', 'rt_sanitize_inputs' );
function rt_sanitize_inputs() {
    genesis_add_option_filter( 'no_html', RT_SETTINGS_FIELD, array( 'rt_num_home_boxes', 'rt_image_banner' ) );
}


/* Register our settings and add the options to the database
------------------------------------------------------------ */

add_action( 'admin_init', 'rt_register_settings' );
function rt_register_settings() {
	register_setting( RT_SETTINGS_FIELD, RT_SETTINGS_FIELD );
	add_option( RT_SETTINGS_FIELD, rt_default_theme_options() );

	if ( genesis_get_option( 'reset', RT_SETTINGS_FIELD ) ) {
		update_option( RT_SETTINGS_FIELD, rt_default_theme_options() );
		genesis_admin_redirect( RT_SETTINGS_FIELD, array( 'reset' => 'true' ) );
		exit;
	}
}


/* Admin notices for when options are saved/reset
------------------------------------------------------------ */
add_action( 'admin_notices', 'rt_theme_settings_notice' );
function rt_theme_settings_notice() {
	if ( ! isset( $_REQUEST['page'] ) || $_REQUEST['page'] != RT_SETTINGS_FIELD )
		return;

	if ( isset( $_REQUEST['reset'] ) && 'true' == $_REQUEST['reset'] )
		echo '<div id="message" class="updated"><p><strong>' . __( 'Reset thành công', 'beethemes' ) . '</strong></p></div>';
	elseif ( isset( $_REQUEST['settings-updated'] ) && 'true' == $_REQUEST['settings-updated'] )
		echo '<div id="message" class="updated"><p><strong>' . __( 'Lưu thành công', 'beethemes' ) . '</strong></p></div>';
}


/* Register our theme options page
------------------------------------------------------------ */

add_action( 'admin_menu', 'rt_theme_options' );
function rt_theme_options() {
	global $_rt_settings_pagehook;
	$_rt_settings_pagehook = add_menu_page( 'Bee Options', 'Bee Options', 'manage_options', RT_SETTINGS_FIELD, 'rt_theme_options_page', THEME_URI.'/images/logobee.png',59 );
	//add_action( 'load-'.$_ctsettings_settings_pagehook, 'ctsettings_settings_styles' );
	add_action( 'load-'.$_rt_settings_pagehook, 'rt_settings_scripts' );
	add_action( 'load-'.$_rt_settings_pagehook, 'rt_settings_boxes' );
}


/* Setup our scripts
------------------------------------------------------------ */
function rt_settings_scripts() {
	global $_rt_settings_pagehook;
	wp_enqueue_script( 'common' );
	wp_enqueue_script( 'wp-lists' );
	wp_enqueue_script( 'postbox' );
}


/* Setup our metaboxes
------------------------------------------------------------ */
function rt_settings_boxes() {
	global $_rt_settings_pagehook;
	add_meta_box( 'rt-settings-home', __( 'Thiết lập trang chủ', 'beethemes' ), 'rt_settings_home', $_rt_settings_pagehook, 'main' );
	add_meta_box( 'rt-image-banner', __( 'Thiết lập Banner/Logo - Favicon', 'beethemes' ), 'rt_image_banner', $_rt_settings_pagehook, 'main' );
	add_meta_box( 'rt-slider-home', __( 'Thiết lập Slider trang chủ', 'beethemes' ), 'rt_slider_home', $_rt_settings_pagehook, 'main' );
}

/* Add our custom post metabox for social sharing
------------------------------------------------------------ */
function rt_settings_home() { ?>

	<fieldset>
	<legend><?php _e('Trang', 'beethemes'); ?></legend>
	<?php
		$home_setting_i = 'rt_home_page_1';
	?>
	<p><?php _e("Chọn trang hiển thị ", 'beethemes'); ?>
	<?php wp_dropdown_pages(array('selected' => gtid_get_option($home_setting_i), 'name' => RT_SETTINGS_FIELD.'['.$home_setting_i.']', 'show_option_none' => __("Tất cả", 'beethemes') )); ?></p>
	</fieldset>
	<!--End_1-->

	<fieldset>
	<legend><?php _e('Danh mục', 'beethemes'); ?></legend>
	<p><strong><?php _e('Số lượng danh mục: ', 'beethemes'); ?></strong>
	<input type="number" min="0" max="30" name="<?php echo RT_SETTINGS_FIELD; ?>[rt_num_home_boxes]" value="<?php echo esc_attr( gtid_get_option('rt_num_home_boxes') ); ?>" style="width:50px" /><input type="submit" class="button-secondary" value="<?php _e('Lưu lại', 'beethemes') ?>" /><br />
	</p>
	<hr />
	<?php
		$total_home_boxes = gtid_get_option('rt_num_home_boxes');
		$total_home_boxes = !isset($total_home_boxes) ? 0 : $total_home_boxes;
		for( $i = 1; $i <= $total_home_boxes; $i++ ) {
			$home_setting_i = 'home_cat_'.$i;
		?>
				<p><?php _e("Chọn danh mục hiển thị ", 'beethemes'); echo $i.' '; ?>
	<?php wp_dropdown_categories(array('selected' => gtid_get_option($home_setting_i), 'name' => RT_SETTINGS_FIELD.'['.$home_setting_i.']', 'orderby' => 'Name' , 'hierarchical' => 1, 'show_option_all' => __("Tất cả", 'beethemes'), 'hide_empty' => '0' )); ?></p>
			<?php
		}
	?>
	<hr />
	<p><?php _e('Số sản phẩm: ', 'beethemes'); ?>

	<input type="number" min="0" max="100" name="<?php echo RT_SETTINGS_FIELD; ?>[number_home_product]" value="<?php echo esc_attr( gtid_get_option('number_home_product') ); ?>" style="width:10%" /><br />

	</p>
	</fieldset>
	<!--End_2-->

<!-- Box News-->
	<fieldset>
	<legend><?php _e('Tin tức', 'beethemes'); ?></legend>
	<p><strong><?php _e('Số lượng danh mục tin: ', 'beethemes'); ?></strong>
	<input type="number" min="0" max="30" name="<?php echo RT_SETTINGS_FIELD; ?>[rt_num_home_news]" value="<?php echo esc_attr( gtid_get_option('rt_num_home_news') ); ?>" style="width:50px" /><input type="submit" class="button-secondary" value="<?php _e('Lưu lại', 'beethemes') ?>" /><br />
	</p>
	<hr />
	<?php
		$total_home_boxes2 = gtid_get_option('rt_num_home_news');
		$total_home_boxes2 = !isset($total_home_boxes2) ? 0 : $total_home_boxes2;
		for( $i2 = 1; $i2 <= $total_home_boxes2; $i2++ ) {
			$home_setting_i2 = 'news_cat_'.$i2;
			?>
				<p><?php _e("Chọn danh mục hiển thị", 'beethemes'); echo $i2; ?>
	<?php wp_dropdown_categories(array('selected' => gtid_get_option($home_setting_i2), 'name' => RT_SETTINGS_FIELD.'['.$home_setting_i2.']', 'orderby' => 'Name' , 'hierarchical' => 1, 'show_option_all' => __("Tất cả", 'beethemes'), 'hide_empty' => '0','taxonomy'=> 'danhmuc' )); ?></p>
			<?php
		}
	?>
	<hr />
	<p><?php _e('Số tin: ', 'beethemes'); ?>

	<input type="number" min="0" max="100" name="<?php echo RT_SETTINGS_FIELD; ?>[number_home_product2]" value="<?php echo esc_attr( gtid_get_option('number_home_product2') ); ?>" style="width:10%" /><br />

	</p>
	</fieldset>
	<!--Box News -->

<?php }

function rt_image_banner(){
// embed upload
?>
<p>
	<label id="banner-logo" for="upload_image1"><b>Banner / Logo : </b>
    <input class="rt-value-upload" type="text" size="36" name="<?php echo RT_SETTINGS_FIELD; ?>[rt_image_banner]" value="<?php echo esc_attr( gtid_get_option('rt_image_banner') ); ?>" />
    <input id="upload_image_button1" class="button rt-upload" type="button" value="Upload Image" />
    </label>
</p>
<p>
	<label id="rt-favicon" ><b>Favicon ( 32px x 32px ) : </b>
    <input class="rt-value-upload" type="text" size="30" name="<?php echo RT_SETTINGS_FIELD; ?>[rt_image_favicon]" value="<?php echo esc_attr( gtid_get_option('rt_image_favicon') ); ?>" />
    <input id="upload_favicon" class="button rt-upload" type="button" value="Upload" />
    </label> ( .png, .jpg, .ico, .jpeg, .bmp )
</p>
<?php
}

// function select slider home
function rt_slider_home(){
?>
<p>
	<label><b><?php _e('Chọn slider hiển thị : ', 'beethemes'); ?></b>
		<select id="select-slider" name="<?php echo RT_SETTINGS_FIELD ?>[rt_slider_home]" class='postform'>
			<option value="0"><?php _e('Chọn tên slider', 'beethemes'); ?></option>
			<?php
				$sl = new WP_Query('post_type=ml-slider');
				while ( $sl->have_posts() ) : $sl->the_post();
				$id = get_the_ID();
			?>
			<option value="<?php echo $id ?>" <?php if ( $id == gtid_get_option('rt_slider_home') ) echo 'selected="selected"'; ?>><?php the_title() ?></option>
			<?php endwhile; wp_reset_postdata(); ?>
		</select>
		<span> <?php _e('Chọn tên slider đã tạo ở mục ', 'beethemes'); ?><b>Meta Slider</b> </span>
    </label>
</p>
<?php
}

/* Set the screen layout to one column
------------------------------------------------------------ */
add_filter( 'screen_layout_columns', 'rt_settings_layout_columns', 10, 2 );
function rt_settings_layout_columns( $columns, $screen ) {
	global $_rt_settings_pagehook;
	if ( $screen == $_rt_settings_pagehook ) {
		$columns[$_rt_settings_pagehook] = 1;
	}
	return $columns;
}


/* Build our theme options page
------------------------------------------------------------ */
function rt_theme_options_page() {
	global $_rt_settings_pagehook, $screen_layout_columns;
	$width = "width: 99%;";
	$hide2 = $hide3 = " display: none;";
	?>
	<div id="rtsettings" class="wrap genesis-metaboxes">
		<form method="post" action="options.php">

			<?php wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false ); ?>
			<?php wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false ); ?>
			<?php settings_fields( RT_SETTINGS_FIELD ); ?>

			<?php screen_icon( 'options-general' ); ?>

			<h2 style="line-height:66px;">
				<img style="float:left;width:60px;height:auto;margin-right:16px;" src="<?php echo get_stylesheet_directory_uri(); ?>/images/logobee1.png" />
				<?php _e( 'Bee Options', 'beethemes' ); ?>
			</h2>
			<div style="float:right;margin:40px 8px 0 0">
			<input type="submit" class="button-primary genesis-h2-button" value="<?php _e( 'Lưu lại', 'beethemes' ) ?>" />
			<input type="submit" class="button-highlighted genesis-h2-button" name="<?php echo RT_SETTINGS_FIELD; ?>[reset]" value="<?php _e( 'Reset', 'beethemes' ); ?>" onclick="return genesis_confirm('<?php echo esc_js( __( 'Bạn có muốn cài đặt lại từ đầu ?', 'beethemes' ) ); ?>');" />
			</div>
			<div style="clear:both"></div>
			<div class="metabox-holder">
				<div class="postbox-container" style="<?php echo $width; ?>">
					<?php do_meta_boxes( $_rt_settings_pagehook, 'main', null ); ?>
				</div>
			</div>

			<div class="bottom-buttons">
				<input type="submit" class="button-primary" value="<?php _e('Lưu lại', 'beethemes') ?>" />
				<input type="submit" class="button-highlighted" name="<?php echo RT_SETTINGS_FIELD; ?>[reset]" value="<?php _e('Reset', 'beethemes'); ?>" />
			</div>

		</form>
	</div>
	<script type="text/javascript">
		//<![CDATA[
		jQuery(document).ready( function($) {
			// close postboxes that should be closed
			$('.if-js-closed').removeClass('if-js-closed').addClass('closed');
			// postboxes setup
			postboxes.add_postbox_toggles('<?php echo $_rt_settings_pagehook; ?>');
		});
		//]]>
	</script>

<?php }