<?php
//Widget contracter
add_action('widgets_init', 'register_gtid_promotion_products_slider_widget');

function register_gtid_promotion_products_slider_widget() {
    //unregister_widget('WP_Widget_Categories');
    register_widget('Gtid_Promotion_Products_Widget');
}


class Gtid_Promotion_Products_Widget extends WP_Widget {

    function Gtid_Promotion_Products_Widget() {
        $widget_ops = array('classname' => 'products-slider-widget', 'description' => __('Hiển thị một slide', 'genesis') );
        $this->WP_Widget( 'products-slider', __('Bee - Slide Product', 'genesis'), $widget_ops );
    }

    function widget($args, $instance) {
        global $post;
        extract($args);
        $instance = wp_parse_args( (array)$instance, array( 'title' => '', 'numpro' => '',  'cat' => '' ) );
        $carousel = $instance['carousel'] ? '1' : '0';

        echo $before_widget;

        if ($instance['title']) echo $before_title . apply_filters('widget_title', $instance['title']) . $after_title;
    ?>
        <div class="promoteslider">
		<?php
            $hotPosts = new WP_Query( 'showposts=' .$instance['numpro']. '&cat='.$instance['cat'] );
            if( $carousel == 0) :
                while( $hotPosts->have_posts() ):
                $hotPosts->the_post();
                $gia = get_field('gia');
                $khuyenmai = get_field('giakhuyenmai');
            ?>
			<div class="bt-slide-item">
				<a href="<?php  the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php bt_thumbnail(); ?>
				</a>
				<h3 class="title-pro"><a href="<?php the_permalink() ?>" class="title"><?php the_title() ?></a></h3>
                
			</div>

        <?php
            endwhile;
            wp_reset_postdata();

            else :
        ?>
        <div class="bt-enable-slide">
        <?php
            $count = 0;
            while( $hotPosts->have_posts() ):
            $hotPosts->the_post();
            $gia = get_field('gia');
            $khuyenmai = get_field('giakhuyenmai');
        ?>
        <?php if ( $count % $instance['limit'] == 0 ) echo '<div class="bt-wrap-item">' ?>
            <div class="bt-slide-item">
                <a href="<?php  the_permalink(); ?>" title="<?php  the_title(); ?>">
                    <?php the_post_thumbnail( "medium",array( "title" => get_the_title() ) ) ?>
                </a>
                <h3 class="title-pro"><a href="<?php the_permalink() ?>" class="title"><?php the_title() ?></a></h3>
                <?php
                    if(!empty($khuyenmai)) {
                        echo "<p class='price'><span>".number_format($khuyenmai,0,'','.')." VNĐ</span></p>";
                    }
                ?>
                <p class="price<?php if(!empty($khuyenmai)) echo " textline"?>">
                    <?php
                        if(!empty($gia)) echo number_format($gia,0,'','.')." VNĐ"; else echo " Liên Hệ"; ?>
                </p>
            </div>

        <?php
            $count++;
            if( $count % $instance['limit'] == 0 ) echo '</div>';

            endwhile;
            wp_reset_postdata();
        ?>

        <script type="text/javascript">
            (function($) {
                "use strict";
            $( document ).ready( function() {
                $(".bt-enable-slide").owlCarousel({
                singleItem: true,
                slideSpeed: 500,
                pagination: false,
                navigation: true,
                navigationText: [
                                "<i class='fa fa-angle-left'></i>",
                                "<i class='fa fa-angle-right'></i>"
                                ]
                });
            });
            })(jQuery);
        </script>
        </div> <!-- end enable slide -->
        <?php
            endif; // end if carousel
        ?>
        

        </div> <!-- end promoteslider -->

	<?php
		echo $after_widget;
    }

    function update($new_instance, $old_instance) {

        ! empty( $new_instance['carousel'] ) ? 1 : 0;
        ! empty( $new_instance['limit'] ) ? $new_instance['limit'] : 1;

        return $new_instance;
    }

    function form($instance) {
        $instance = wp_parse_args( (array)$instance, array( 'title' => '', 'numpro' => '', 'cat' => '' ) );
        $limit    = ( int ) $instance['limit'];
        $carousel = isset( $instance['carousel'] ) ? ( bool ) $instance['carousel'] : false;
    ?>
		<p>
			<label for="<?php  echo $this->get_field_id('title'); ?>">
			<?php  _e('Tiêu đề', 'genesis'); ?>:
			</label>
			<input type="text" id="<?php  echo $this->get_field_id('title'); ?>" name="<?php  echo $this->get_field_name('title'); ?>" value="<?php  echo esc_attr( $instance['title'] ); ?>" style="width:95%;" />
		</p>
        <p>
			<label for="<?php  echo $this-> get_field_id('cat'); ?>"><?php  _e('Chuyên mục','genesis'); ?>:</label>
			<?php
wp_dropdown_categories(array('name'=> $this->get_field_name('cat'),'selected'=>$instance['cat'],'orderby'=>'Name','hierarchical'=>1,'show_option_all'=>__('Tất cả','genesis'),'hide_empty'=>'0')); ?>
        </p>

        <p>
            <label for="<?php  echo $this->get_field_id('numpro'); ?>">
            <?php  _e('Số sản phẩm', 'genesis'); ?>:
            </label>
            <input type="number" min="0" id="<?php  echo $this->get_field_id('numpro'); ?>" name="<?php echo $this->get_field_name('numpro'); ?>" value="<?php  echo esc_attr( $instance['numpro'] ); ?>" style="width:20%;" />
        </p>

        <p>
            <input class="checkbox bee-check-carousel" type="checkbox" id="<?php echo $this->get_field_id( 'carousel' ); ?>" name="<?php echo $this->get_field_name( 'carousel' ); ?>"<?php checked( $carousel ); ?> />
            <label for="<?php echo $this->get_field_id( 'carousel' ); ?>"><?php _e( 'Bật/Tắt Slide', 'beethemes' ); ?></label>
        </p>

        <p>
            <label for="<?php echo $this-> get_field_id('limit'); ?>"><?php  _e('Số sản phẩm hiển thị slide', 'beethemes'); ?>:</label>
            <input class="textbox" type="number" min="1" name="<?php echo $this->get_field_name( 'limit' ); ?>" value="<?php  echo esc_attr( $instance['limit'] ); ?>" style="width:20%" />
        </p>
	<?php
    }

}

?>